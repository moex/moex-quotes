//
//  MEAppDelegate.m
//  MExchangeInfo-Universal
//
//  Created by denn on 10/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEAppDelegate.h"
#import "MEDispatcher.h"
#import "MESettings.h"
#import <ObjectiveRecord/ObjectiveRecord.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TSMessage.h>
#import <TSMessages/TSMessageView.h>

@implementation MEAppDelegate
{
    MEDispatcher *dispatcher;
    MESettings   *settings;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Setup Core Data Stack
    
    [Fabric with:@[[Crashlytics class]]];
    // TODO: Move this to where you establish a user session
    
    [UIView setAnimationsEnabled:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    dispatcher = [MEDispatcher sharedInstance];
    settings = [MESettings sharedInstance];

    [TSMessage addCustomDesignFromFileWithName:@"TSMessageDesign.json"];

    return YES;
}

- (NSString *)dbStore
{
    return [NSString stringWithFormat:@"%@/%@",[[NSLocale preferredLanguages] objectAtIndex:0],@"MExchangeInfo_Universal.sqlite"];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if ([settings.closeInActiveConnection boolValue]) {
        [dispatcher disconnect];
    }
    
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self->dispatcher connect];
    });
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [dispatcher disconnect];
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (BOOL) application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder{
    return YES;
}

- (BOOL) application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder{
    return YES;
}

- (void)saveContext
{
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
