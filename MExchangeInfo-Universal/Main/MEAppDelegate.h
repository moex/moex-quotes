//
//  MEAppDelegate.h
//  MExchangeInfo-Universal
//
//  Created by denn on 10/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
- (NSURL *)applicationDocumentsDirectory;
@end
