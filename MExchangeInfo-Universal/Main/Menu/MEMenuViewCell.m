//
//  MEMenuViewCell.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 15/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEMenuViewCell.h"
#import "MEUIPreferences.h"


@implementation MEMenuViewCell
- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.font = [UIFont fontWithName:ME_UI_FONT size:17];
        self.textLabel.textColor = ME_UI_MENU_FONT_COLOR;
        self.devider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu_divider"]];
        self.backgroundColor = self.contentView.backgroundColor = [UIColor clearColor];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.selectionBackgroundImageView = [[UIImageView alloc] init];
        self.selectionBackgroundImageView.backgroundColor = [UIColor clearColor];
        [self addDevider];
    }
    return self;
}
@end


@implementation MEMenuLoginCell
- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"top_panel.png"]];
        self.textLabel.font = [UIFont fontWithName:ME_UI_FONT size:13];
        self.textLabel.textColor = ME_UI_MENU_FONT_COLOR;
        self.selectionBackgroundImageView = [[UIImageView alloc] init];
        self.selectionBackgroundImageView.backgroundColor = [UIColor clearColor];
        self.backgroundColor=self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}
@end
