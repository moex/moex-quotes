//
//  MEMenuDefaults.h
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

static CGFloat const MEUI_ANIMATION_DELAY=0.5;
static CGFloat const MEUI_MENU_ANIMATION_DELAY=0.3;

#define MEUI_MENU_GESTURE_VELOCITY_THRESHOLD 600.0f
#define MEUI_MENU_GESTURE_EDGES 50.0f
#define MEUI_MENU_GESTURE_MINSLIDE_VELOCITY 300.0f
#define MEUI_MENU_WIDTH 240.0f

#define MEUI_MENU_SHADOW_RADIUS 8.0
#define MEUI_MENU_SHADOW_OFFSET (CGSizeMake(3, 3))
#define MEUI_MENU_SHADOW_OPACITY 0.2

#define MEUI_MENU_ITEMS_SEGUE @"menuRootSegueID"
#define MEUI_MENU_SPLIT_ITEMS_SEGUE @"menuSplitRootSegueID"