//
//  MEMainMenuStructure.m
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuRootAppStructure.h"
#import "MEDispatcher.h"

static MEMenuRootAppStructure *__shared_instance = nil;

@implementation MEMenuRootAppStructure
{
    //MEMenuItem *login;
}

- (void) resetUsername{
    MEDispatcher *dispatcher =  [MEDispatcher sharedInstance];
    MEConfig *config = dispatcher.config;
    
    NSString *username = config.login.user;
    
    //if (!config.isAuthorized) {
    //    username =  NSLocalizedString(@"UnAuthorized", @"UnAuthorized user name in login menu");
    //}
    //else
    
    if ([config.login.domain isEqualToString:GIC_GUEST_DOMAIN]){
        username =  NSLocalizedString(@"Guest", @"Guest user name in login menu");
    }
    else{
        username = NSLocalizedString(config.login.user, @"Guest user name in login menu");
    }
    
    _loginMenuItem = [MEMenuItem
             menuItemWithID:@"login"
             withCaption: username
             withIcon:[UIImage imageNamed:@"menu_icon_change_user.png"]
             withSelectedIcon: nil
             forViewController:@"loginNСID"];
    
    _loginMenuItem.hasDisclosureIndicator = NO;
}

- (void) configure{
    //
    // Markets menu
    //
    
    _menu = [[MEMenuModel alloc] init];
    
    //
    // Login section
    //
    MEMenuSection *_section = [_menu addSectionWithCaption:@""];
    
    [self resetUsername];
    
    [_section.items addObject:_loginMenuItem];
    
    
    //
    // Markets
    //
    _section = [_menu addSectionWithCaption:NSLocalizedString(@"Markets", @"Menu Markets Section")];
    
    [_section.items addObject: _watchListMenuItem?_watchListMenuItem:(_watchListMenuItem=[MEMenuItem
                               menuItemWithID:@"watchList"
                               withCaption:NSLocalizedString(@"Watch List", @"Menu item Watchlist")
                               withIcon:[UIImage imageNamed:@"menu_icon_watchlist.png"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_watchlist_selected.png"]
                               forViewController:@"watchListNСID"])];

    [_section.items addObject: _turnoversMenuItem?_turnoversMenuItem:(_turnoversMenuItem=[MEMenuItem
                               menuItemWithID:@"turnovers"
                               withCaption:NSLocalizedString(@"Turnovers", @"Menu item Turnovers")
                               withIcon:[UIImage imageNamed:@"menu_icon_turnovers.png"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_turnovers_selected.png"]
                               forViewController: (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?@"turnoversNCID":@"sectorsTurnoversTCID"])];

    
    [_section.items addObject: _newsMenuItem?_newsMenuItem:(_newsMenuItem=[MEMenuItem
                                                                           menuItemWithID:@"newsList"
                                                                           withCaption:NSLocalizedString(@"News", @"Menu item News")
                                                                           withIcon:[UIImage imageNamed:@"menu_icon_news.png"]
                                                                           withSelectedIcon: [UIImage imageNamed:@"menu_icon_news_selected.png"]
                                                                           forViewController:@"newsListNCID"])];
    
    //
    // Extentions
    //
    _section = [_menu addSectionWithCaption:NSLocalizedString(@"About", @"Menu extentions Moscow Exchange section")];
    
    
    [_section.items addObject: _settingsMenuItem?_settingsMenuItem:(_settingsMenuItem=[MEMenuItem
                               menuItemWithID:@"settings"
                               withCaption:NSLocalizedString(@"Settings", @"Menu item Settings")
                               withIcon:[UIImage imageNamed:@"menu_icon_settings.png"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_settings_selected.png"]
                               forViewController:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)?@"settingsNCID":@"settingsiPADCID"])];    
    
    _settingsMenuItem.isPopover = YES;
    

    [_section.items addObject:[MEMenuItem
                               menuItemWithID:@"about"
                               withCaption:NSLocalizedString(@"Tech support", @"Menu item tech support")
                               withIcon:[UIImage imageNamed:@"menu_icon_about.png"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_about_selected.png"]
                               forViewController:@"aboutVCID"]];
    
}


+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEMenuRootAppStructure alloc] init];
    });
    
    return __shared_instance;
}

- (id) init{
    
    if (__shared_instance) {
        return __shared_instance;
    }
    
    self = [super init];
    if (self) {
        [self configure];            
    }
    
    __shared_instance = self;
    
    return self;
}

@end
