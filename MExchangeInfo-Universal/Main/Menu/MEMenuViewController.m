    //
    //  MEMenuViewController.m
    //  MExchange-MosQuotes
    //
    //  Created by denn on 06.12.14.
    //  Copyright (c) 2014 Moscow Exchange. All rights reserved.
    //

#import "MEMenuViewController.h"
#import "MEMenuViewCell.h"

#define ME_MENU_LAST_INDEX_PATH @"ME_MENU_LAST_INDEX_PATH"

@interface __MEMenuControllersCacheItem : NSObject
@property (strong,nonatomic) NSArray *controllers;
@property (weak,nonatomic)   UIViewController *topViewController;
@end

@implementation __MEMenuControllersCacheItem
@end

@interface MEMenuViewController () <UITableViewDataSource, UIGestureRecognizerDelegate>
@property (strong, nonatomic)	UINavigationController*	contentController;
@property (strong, nonatomic)	UITableView             *tableView;
@property (assign, nonatomic)   CGRect                  tableViewFrame;

@property (strong, nonatomic)	UIView					*overlayView;
@property (strong, nonatomic)	UIBarButtonItem         *barButton;
@property (strong, nonatomic)	UITapGestureRecognizer	*tapGesture;
@property (strong, nonatomic)	UIPanGestureRecognizer	*panGesture;

@property (assign, nonatomic)   BOOL                    menuVisible;
@property (assign, nonatomic)   BOOL                    isPaning;
@property (assign, nonatomic)   BOOL                    isTapped;

@property (weak, nonatomic)		UIViewController*		currentViewController;
@property (strong,nonatomic)    NSIndexPath *lastSelectedIndexPath;

@property (strong,nonatomic)    NSMutableDictionary    *controllersCache;
@property (strong,nonatomic)    UIImage                *sectionHeaderBgImage;

@end

@implementation MEMenuViewController

@synthesize lastSelectedIndexPath=_lastSelectedIndexPath;

- (MEMenuModel*) menuSource{
    return nil;
}

- (MEMenuModel*) menu{
    return [self menuSource];
}

- (void) reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void) configure{
    _menuWidth = MEUI_MENU_WIDTH;
    _menuMotionPercentage = 0.2f;
    _gestureEdges = MEUI_MENU_GESTURE_EDGES;
}

- (id) init{
    self = [super init];
    if (self) {
        [self configure];
    }
    return self;
}


- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self configure];
    }
    
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self configure];
    }
    return self;
}

- (UIImage*)    sectionHeaderBgImage{
    if (!_sectionHeaderBgImage) {
        _sectionHeaderBgImage = [UIImage imageNamed:@"grey_panel.png"];
    }
    
    return _sectionHeaderBgImage;
}

- (NSMutableDictionary *) controllersCache{
    if (!_controllersCache) {
        _controllersCache = [[NSMutableDictionary alloc] init];
    }
    
    return _controllersCache;
}

- (void)viewDidLayoutSubviews
{
    if (self.tableView.isDecelerating || self.tableView.isDragging)
        return;
    
    [super viewDidLayoutSubviews];
    if (!self.isPaning && !self.isTapped) {
        self.tableView.frame = self.tableViewFrame;
    }
}

-(void) setLastSelectedIndexPath:(NSIndexPath *)lastSelectedIndexPath{
    _lastSelectedIndexPath = lastSelectedIndexPath;
    
    [[NSUserDefaults standardUserDefaults]
     setObject:@[[NSNumber numberWithInteger:
                  _lastSelectedIndexPath.row],
                 [NSNumber numberWithInteger:
                  _lastSelectedIndexPath.section]]
     forKey:ME_MENU_LAST_INDEX_PATH];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSIndexPath*) lastSelectedIndexPath{
    
    if (_lastSelectedIndexPath) {
        return _lastSelectedIndexPath;
    }
    
    NSArray *_lastSelected = [[NSUserDefaults standardUserDefaults] objectForKey:ME_MENU_LAST_INDEX_PATH];
    if (_lastSelected)
        _lastSelectedIndexPath = [NSIndexPath indexPathForRow:[_lastSelected[0] integerValue] inSection:[_lastSelected[1] integerValue]];
    
    if (!_lastSelectedIndexPath)
        _lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    return _lastSelectedIndexPath;
}


- (void)loadView
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    view.backgroundColor = [UIColor clearColor];
    
        // Table View setup
    self.tableView = [[UITableView alloc] initWithFrame:self.tableViewFrame];
    
    self.tableView.autoresizingMask = ~UIViewAutoresizingFlexibleBottomMargin;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollsToTop = NO;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:ME_UI_BACKGROUND_IMAGE];
    
        // The content is displayed in a UINavigationController
    
    self.contentController = [[UINavigationController alloc] initWithNavigationBarClass:[UINavigationBar class] toolbarClass:[UIToolbar class]];
    [self.contentController.view setFrame:view.frame];
    self.contentController.navigationBar.translucent = NO;
    
    self.contentController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.contentController.view.bounds].CGPath;
    self.contentController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.contentController.view.layer.shadowOffset = CGSizeMake(-2.0f, 0.0f);
    self.contentController.view.layer.shadowOpacity = 0.2f;
    self.contentController.view.clipsToBounds = NO;
    
    /* The transparent overlay view will catch all the user touches in the content area
     when the slide menu is visible */
    self.overlayView = [[UIView alloc] initWithFrame:self.contentController.view.frame];
    self.overlayView.userInteractionEnabled = YES;
    self.overlayView.backgroundColor = [UIColor clearColor];
    
    [view addSubview:self.tableView];
    [view addSubview:self.contentController.view];
    
    [self addChildViewController:self.contentController];
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentViewController = nil;
    
    [self.tableView registerClass:[MEMenuViewCell class] forCellReuseIdentifier:CellIdentifierMenu];
    [self.tableView registerClass:[MEMenuLoginCell class] forCellReuseIdentifier:CellIdentifierLogin];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    self.barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"left_menu_button"]
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                                                     action:@selector(toggleMenu)];
    
        // Detect when the content recieves a single tap
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    self.tapGesture.numberOfTapsRequired = 1;
    [self.overlayView addGestureRecognizer:self.tapGesture];
    
        // Detect when the content is touched and dragged
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    self.panGesture.maximumNumberOfTouches=2;
    self.panGesture.delegate=self;
    
    [self.view addGestureRecognizer:self.panGesture];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.menu) {
        MEMenuItem    *mitem   = [self menuItemWithIndexPath:self.lastSelectedIndexPath];
            //[self setContentViewController:mitem.controller withNewIndex:self.lastSelectedIndexPath];
        if (mitem==[[MEMenuRootAppStructure sharedInstance] settingsMenuItem]) {
            self.lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        [self tableView: self.tableView didSelectRowAtIndexPath:self.lastSelectedIndexPath];
    }
}

#pragma mark - Handlers

- (void) toggleMenu{
    self.isTapped = YES;
    if (self.menuVisible) {
        [self hideSideMenu];
    } else {
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.x = -self.menuWidth;
        self.tableView.frame = tableFrame;
        [self reloadData];
        [self showSideMenu];
    }
}

- (void)showSideMenu{
    
    [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
        
            // Slide the table
        CGRect tableFrame = self.tableViewFrame;
        tableFrame.origin.x = 0;
        self.tableView.frame = tableFrame;
        
            // Move the whole NavigationController view aside
        self.contentController.view.frame = [self contentViewFrame:YES];
        
        [self.tableView layoutIfNeeded];
        [self.contentController.view layoutIfNeeded];
    }
                     completion:^(BOOL finished) {
            // Add the overlay that will receive the gestures
        [self.tableView setScrollsToTop:YES];
        [self.contentController.view addSubview:self.overlayView];
        self.menuVisible = YES;
        self.isTapped = NO;
    }];
    
}

- (void)hideSideMenu
{
        // this animates the view back to the left before telling the app delegate to swap out the MenuViewController
        // it tells the app delegate using the completion block of the animation
    
    [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
        
            // Slide the table
        CGRect tableFrame = self.tableViewFrame;
        tableFrame.origin.x = -self.menuWidth;
        tableFrame.origin.x = tableFrame.origin.x * self.menuMotionPercentage;
        self.tableView.frame=tableFrame;
        
            // Move back the NavigationController
        
        [self.tableView layoutIfNeeded];
        self.contentController.view.frame = [self contentViewFrame:NO];
    }
                     completion:^(BOOL finished) {
        [self.contentController.view layoutIfNeeded];
        [self.overlayView removeFromSuperview];
        self.menuVisible = NO;
        [self.tableView setScrollsToTop:NO];
        self.isTapped = NO;
    }];
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{    
    if (self.isPaning)
        return;
    
    self.isTapped = YES;
    
        // A single tap hides the slide menu
    [self hideSideMenu];
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint touchPosition = [gestureRecognizer locationInView:self.view];
    
    BOOL touch_inside = YES;
    
    if (!self.menuVisible)
            //
            // Menu is hidden && touch is inside edges
            //
        touch_inside = (touchPosition.x < self.gestureEdges);
    
    return touch_inside;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) return NO;
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

/* The following is from
 http://blog.shoguniphicus.com/2011/06/15/working-with-uigesturerecognizers-uipangesturerecognizer-uipinchgesturerecognizer/
 as mentioned by Nick Harris, in his approach to slide-out navigation:
 http://nickharris.wordpress.com/2012/02/05/ios-slide-out-navigation-code/
 */
- (void)handlePan:(UIPanGestureRecognizer *)gesture;
{
    
    if (self.tableView.isDecelerating || self.tableView.isDragging)
        return;
    
        // The pan gesture moves horizontally the view
    UIView *piece = self.contentController.view;
    [self adjustAnchorPointForGestureRecognizer:gesture];
    
    CGFloat velocity = [gesture velocityInView:gesture.view].x;
    
    if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {
        
        self.isPaning  = YES;
        
        CGPoint translation = [gesture translationInView:[piece superview]];
        
        float x = piece.frame.origin.x + translation.x;
        float maxWidth = self.menuWidth + 40.0;
        
        if (x < 0)
            x = 0;
        
        if (x>maxWidth)
            x = maxWidth;
        
        float frameWidth = (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad
                            && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))?self.view.bounds.size.width-x:piece.frame.size.width;
        
        piece.frame = CGRectMake(x, piece.frame.origin.y, frameWidth, piece.frame.size.height);
        
        [gesture setTranslation:CGPointZero inView:[piece superview]];
        
            // Move the table if needed
        
        CGFloat maxValue = piece.frame.origin.x;
        
        if (maxValue > maxWidth)
            maxValue = maxWidth;
        
        CGRect frame = self.tableView.frame;
        frame.origin.x = maxValue - self.menuWidth;
        
        if (frame.origin.x<=0)
            frame.origin.x = frame.origin.x * self.menuMotionPercentage;
        else
            frame.origin.x = frame.origin.x * 0.5;
        
        [self.tableView setFrame:frame];
        
        [self.tableView layoutIfNeeded];
        [self.contentController.view layoutIfNeeded];
    }
    else if ([gesture state] == UIGestureRecognizerStateEnded) {
        
        self.isTapped = YES;
        
        if (ABS(velocity) > MEUI_MENU_GESTURE_VELOCITY_THRESHOLD) {
            if (velocity>0) {
                [self reloadData];
                [self showSideMenu];
            }
            else
                [self hideSideMenu];
        }
        else{
                // Hide the slide menu only if the view is released under a certain threshold, the threshold is lower when the menu is hidden
            float threshold;
            if (self.menuVisible) {
                threshold = self.menuWidth;
            } else {
                threshold = self.menuWidth / 2;
            }
            
            if (self.contentController.view.frame.origin.x < threshold)
                [self hideSideMenu];
            else {
                [self reloadData];
                [self showSideMenu];
            }
        }
        self.isPaning = NO;
    }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = self.contentController.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}


#pragma mark - UI Utilities

- (void)setLeftBarButton:(UIBarButtonItem*)barButton
{
    self.barButton = barButton;
    if (self.barButton.target == nil || self.barButton.action == nil) {
        self.barButton.target = self;
        self.barButton.action = @selector(toggleMenu);
    }
    [self.currentViewController.navigationItem setLeftBarButtonItem:self.barButton];
}

- (void)setContentViewController:(UIViewController *)controller withNewIndex:(NSIndexPath*) newIndexPath
{
    MEMenuItem *mitem = [self menuItemWithIndexPath:self.lastSelectedIndexPath];
    MEMenuItem *newitem = [self menuItemWithIndexPath:newIndexPath];
    
    __MEMenuControllersCacheItem *citem = [self.controllersCache objectForKey:mitem.menuID];
    
    if (self.contentController.viewControllers.count>0 && (citem==nil || citem.controllers.count!=self.contentController.viewControllers.count)) {
        citem = [__MEMenuControllersCacheItem new];
        citem.controllers = self.contentController.viewControllers;
        citem.topViewController = self.contentController.topViewController;
        [self.controllersCache setObject:citem forKey:mitem.menuID];
    }
    
    __MEMenuControllersCacheItem *newcitem = [self.controllersCache objectForKey:newitem.menuID];
    
    NSArray *controllers = @[controller];
    UIView  *toView = controller.view;
    if (newcitem.controllers!=nil){
        controllers = newcitem.controllers;
        if (newcitem.topViewController.view)
            toView = newcitem.topViewController.view;
    }
    
    [controller.navigationItem setLeftBarButtonItem:self.barButton];
    [self.contentController setViewControllers:controllers animated:NO];
    
    [UIView transitionFromView:self.currentViewController.view
                        toView:toView
                      duration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
        
        self.currentViewController = controller;
        self.lastSelectedIndexPath = newIndexPath;
        
        if ([self.currentViewController respondsToSelector:@selector(edgesForExtendedLayout)]) {
            self.currentViewController.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
    }];
    
        //    [self.contentController setViewControllers:controllers animated:NO];
        //    [UIView transitionWithView:toView
        //                      duration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
        //                       options:UIViewAnimationOptionTransitionCrossDissolve
        //                    animations:nil
        //                    completion:nil];
        //
}

- (CGRect) tableViewFrame{
    
    float h = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")?
    [UIApplication sharedApplication].statusBarFrame.size.height:
    (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)?
     [UIApplication sharedApplication].statusBarFrame.size.width:
     [UIApplication sharedApplication].statusBarFrame.size.height);
    
    CGFloat offsetY = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? h : 0.0f;
    
    return (CGRect){
        (CGPoint){
            0,
            offsetY
        }, (CGSize) {
            self.menuWidth,
            SCREEN_HEIGHT - offsetY}
    };
}

- (CGRect) contentViewFrame:(BOOL)menuVisible{
    CGRect frame = self.view.bounds;
    if (menuVisible) {
        frame.origin.x = self.menuWidth;
        if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM() && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            frame.size.width -= frame.origin.x;
    }
    return frame;
}

#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    self.contentController.view.frame = [self contentViewFrame:self.menuVisible];
    [self.currentViewController.navigationController.navigationBar sizeToFit];
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)setMenuScrollingEnabled:(BOOL)enabled
{
    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
        [self.tableView setScrollEnabled:enabled];
        [self.tableView setAlwaysBounceVertical:enabled];
    }
    else{
        [self.tableView setScrollEnabled:NO];
        [self.tableView setAlwaysBounceVertical:NO];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.tableView.frame = self.tableViewFrame;
}


#pragma mark - UITableView Data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return self.menu.sections.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    MEMenuSection *msection = [self.menu.sections objectAtIndex:section];
    return msection.items.count;
}

- (MEMenuItem*) menuItemWithIndexPath:(NSIndexPath*)indexPath{
    MEMenuSection *msection = [self.menu.sections objectAtIndex:indexPath.section];
    MEMenuItem    *mitem    = [msection.items objectAtIndex:indexPath.row];
    return mitem;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MEMenuItem    *mitem    = [self menuItemWithIndexPath:indexPath];
    BOOL is_login_item = indexPath.row==0&&indexPath.section==0;
    
    MEBaseViewCell *cell = is_login_item?[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierLogin forIndexPath:indexPath]:[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierMenu forIndexPath:indexPath];
    
    UILabel *textLabel= cell.textLabel;
    
    textLabel.backgroundColor = [UIColor clearColor];
    
    textLabel.text = [self menuItemWithIndexPath:indexPath].caption;
    
    UIImageView *icon =  cell.imageView;
    
    if ([indexPath compare:self.lastSelectedIndexPath]==NSOrderedSame){
        if (!is_login_item) {
            textLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:textLabel.font.pointSize];
            textLabel.textColor = ME_UI_SELECTED_MENU_FONT_COLOR;
        }
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        icon.image = mitem.selectedIcon;
    }
    else{
        if (!is_login_item) {
            textLabel.font = [UIFont fontWithName:ME_UI_FONT size:textLabel.font.pointSize];
            textLabel.textColor = ME_UI_MENU_FONT_COLOR;
        }
        icon.image = mitem.icon;
    }
    
    if (!is_login_item && indexPath.row < [[self.menu.sections[indexPath.section] items] count]-1)
        cell.devider.alpha = 1.0;
    else
        cell.devider.alpha = 0.0;
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *caption = [self.menu.sections[section] caption];
    
    if (!caption || caption.length==0) {
        return 0.0;
    }
    
    return self.sectionHeaderBgImage.size.height;
};

-(UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSString *caption = [self.menu.sections[section] caption];
    
    if (!caption || caption.length==0) {
        return nil;
    }
    
    UIImageView *view   = [[UIImageView alloc] initWithImage:self.sectionHeaderBgImage];
    
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = ME_UI_MENU_FONT_COLOR;
    label.font = [UIFont fontWithName:ME_UI_FONT size:14.];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = caption;
    
    [view addSubview:label];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:0 views:@{@"label": label}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[label]-2-|" options:0 metrics:0 views:@{@"label": label}]];
    
    return view;
}

#pragma mark - UITableView Delegate

- (void) popOverViewController:(UIViewController*)viewController withIndex:(NSIndexPath*) indexPath{
    
    viewController.preferredContentSize = CGSizeMake(320.0, 800.0);
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:viewController];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect cellFrame = [self.view convertRect:cell.frame toView:nil];
    
    popover.popoverContentSize = viewController.preferredContentSize;
    
    cellFrame.origin.y += cell.bounds.size.height/2.0;
    [popover presentPopoverFromRect: cellFrame
                             inView:self.view
           permittedArrowDirections:UIPopoverArrowDirectionAny
                           animated:YES];
};

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    UILabel *text =  cell.textLabel;
    BOOL is_login_item = indexPath.row==0&&indexPath.section==0;
    
    if (!is_login_item) {
        text.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:text.font.pointSize];
        text.textColor = ME_UI_SELECTED_MENU_FONT_COLOR;
    }
    
    MEMenuItem    *mitem    = [self menuItemWithIndexPath:indexPath];
    
    UIViewController* newController = mitem.controller;
    
    if (self.currentViewController != newController) {
        newController.title = mitem.caption;
        if (mitem.isPopover) {
            if ([newController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *nvc = (UINavigationController*)newController;
                nvc.topViewController.navigationItem.title = mitem.caption;
            }
            [self popOverViewController:newController withIndex:indexPath];
        }
        else{
            [self setContentViewController:newController withNewIndex:indexPath];
        }
    }
    
    self.isTapped = YES;
    
    if (!mitem.isPopover) {
        [self hideSideMenu];
    }
    
    if (mitem!=[[MEMenuRootAppStructure sharedInstance] settingsMenuItem]) {
        self.lastSelectedIndexPath = indexPath;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    UILabel *text = cell.textLabel;
    BOOL is_login_item = indexPath.row==0&&indexPath.section==0;
    if (!is_login_item) {
        text.font = [UIFont fontWithName:ME_UI_FONT size:text.font.pointSize];
        text.textColor = ME_UI_MENU_FONT_COLOR;
    }
}

- (void) tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    MEMenuItem    *mitem    = [self menuItemWithIndexPath:indexPath];
    
    cell.imageView.image = mitem.selectedIcon;
    UILabel *text = cell.textLabel;
    BOOL is_login_item = indexPath.row==0&&indexPath.section==0;
    
    if (!is_login_item) {
        text.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:text.font.pointSize];
        text.textColor = ME_UI_SELECTED_MENU_FONT_COLOR;
    }
}


@end
