//
//  MEMenuViewCell.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 15/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEBaseViewCell.h"

static NSString *CellIdentifierMenu  = @"CellMenu";
static NSString *CellIdentifierLogin = @"CellLogin";

@interface MEMenuViewCell : MEBaseViewCell
@property (strong,nonatomic) UIImage *icon;
@end

@interface MEMenuLoginCell : MEBaseViewCell
@property (strong,nonatomic) UIImage *icon;
@end
