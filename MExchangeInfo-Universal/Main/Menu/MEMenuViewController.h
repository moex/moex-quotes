//
//  MEMenuViewController.h
//  MExchange-MosQuotes
//
//  Created by denn on 06.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEConfig.h"
#import "MEUIPreferences.h"
#import "MEMenuRootAppStructure.h"

@interface MEMenuViewController : UIViewController <UITableViewDelegate>

@property (readonly, nonatomic   ) UITableView *tableView;
@property (assign,   nonatomic   ) float       menuWidth;
@property (assign,   nonatomic   ) float       menuMotionPercentage;
@property (readonly, nonatomic   ) NSIndexPath *lastSelectedIndexPath;
@property (readonly, nonatomic   ) MEMenuModel *menu;
@property (assign,   nonatomic   ) float       gestureEdges;

- (void) reloadData;
- (MEMenuModel *) menuSource;
- (void) setMenuScrollingEnabled:(BOOL)enabled;
@end
