//
//  MELeftMenyViewController.m
//  MExchange-MosQuotes
//
//  Created by denn on 06.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELeftMenuViewController.h"
#import "MEUIPreferences.h"
#import "MEUIPreferences.h"
#import "MEDispatcher.h"
#import "MESettings.h"

@interface MELeftMenuViewController ()
@property (strong,nonatomic) MEDispatcher *dispatcher;
@property (strong,nonatomic) UIImageView *logoImageView;
@end

@implementation MELeftMenuViewController

- (MEDispatcher *) dispatcher{
    if (!_dispatcher) {
        _dispatcher = [MEDispatcher sharedInstance];
    }
    return _dispatcher;
}

- (MEMenuModel*) menuSource{
    return [[MEMenuRootAppStructure sharedInstance] menu];
}

- (void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    
    //self.menu = [[MEMenuRootAppStructure sharedInstance] menu];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    // Customize navigation bar
    //
    UINavigationBar *_naviBar = [UINavigationBar appearance];
    
    [_naviBar setTitleTextAttributes: @{
                                        NSForegroundColorAttributeName: [UIColor whiteColor],
                                        NSFontAttributeName: [UIFont fontWithName:ME_UI_NAVIBAR_FONT size:ME_UI_NAVIBAR_FONT_SIZE]
                                        }];
    
    [_naviBar setTintColor:[UIColor whiteColor]];
    
    [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationController class],nil]
     setTitleTextAttributes: @{
                               NSForegroundColorAttributeName: [UIColor whiteColor],
                               NSFontAttributeName: [UIFont fontWithName:ME_UI_NAVIBAR_BACK_BUTTON_FONT size:ME_UI_NAVIBAR_FONT_BACK_BUTTON_SIZE]
                               }
     forState:UIControlStateNormal];
    
    UIImage *image = [UIImage imageNamed:@"top_panel.png"];
    [_naviBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    [[UIToolbar appearance] setBackgroundImage: image forToolbarPosition: UIToolbarPositionAny barMetrics: UIBarMetricsDefault];
    [[UIToolbar appearance] setTintColor:ME_UI_TOOLBAR_TINT_COLOR];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginError:) name:ME_LOGIN_ERROR_NOTIFICATION object:nil];
    
    
    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    self.logoImageView.translatesAutoresizingMaskIntoConstraints  = NO;
    [self.view insertSubview:self.logoImageView aboveSubview:self.tableView];
    
    [self.logoImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-15-|"
                                                                                    options:0
                                                                                    metrics:0
                                                                                      views:@{@"logo": self.logoImageView}]];
    
    [self.logoImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[logo]"
                                                                                    options:0
                                                                                    metrics:0
                                                                                      views:@{@"logo": self.logoImageView}]];

}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setMenuScrollingEnabled:UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)];
}

- (void) show_watchlist_controller{
    double delayInSeconds = .5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self tableView: self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    });
}

- (void) show_login_controller{
    double delayInSeconds = .5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self tableView: self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    });
}


- (void) loginSuccessed:(NSNotification*)event{  
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEMenuRootAppStructure sharedInstance] configure];
        [self reloadData];
        
        NSIndexPath *index = self.lastSelectedIndexPath;
        
        if ((index.section==0 && index.row == 0) || [self.dispatcher.config.login.user isEqualToString:GIC_GUEST_USER] ) {
            [self show_watchlist_controller];
        }
    });        
}

- (void) loginError:(NSNotification*)event{    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[MEMenuRootAppStructure sharedInstance] configure];
        [self reloadData];
        
        if ((self.lastSelectedIndexPath.section!=0 && self.lastSelectedIndexPath.row != 0)) {
            [self show_login_controller];
        }
        
    });
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        [UIView animateWithDuration:duration animations:^{
            if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
                self.logoImageView.alpha = 0.0;
            }
            else{
                self.logoImageView.alpha = 1.0;
            }
        }];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self setMenuScrollingEnabled:UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)==YES];
}

@end
