//
//  MEMainMenuStructure.h
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMenuModel.h"

@interface MEMenuRootAppStructure : NSObject
@property (readonly,nonatomic) MEMenuModel *menu;
@property (readonly,nonatomic) MEMenuItem  *loginMenuItem;
@property (readonly,nonatomic) MEMenuItem  *watchListMenuItem;
@property (readonly,nonatomic) MEMenuItem  *newsMenuItem;
@property (readonly,nonatomic) MEMenuItem  *turnoversMenuItem;
@property (readonly,nonatomic) MEMenuItem  *settingsMenuItem;
+ (id) sharedInstance;
- (void) configure;
@end
