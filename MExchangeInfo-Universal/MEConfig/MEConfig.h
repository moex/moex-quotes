//
//  MEConfig.h
//  MExchange
//
//  Created by denn on 4/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIConfig.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define UISCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define UISCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define IS_PORTRAIT_ORIENTATION ((UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)))
#define SCREEN_WIDTH (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")? UISCREEN_WIDTH : (IS_PORTRAIT_ORIENTATION ? UISCREEN_WIDTH : UISCREEN_HEIGHT))
#define SCREEN_HEIGHT (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")? UISCREEN_HEIGHT : (IS_PORTRAIT_ORIENTATION ? UISCREEN_HEIGHT : UISCREEN_WIDTH))
#define STATUSBAR_HEIGHT (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")? [UIApplication sharedApplication].statusBarFrame.size.height : (IS_PORTRAIT_ORIENTATION ? [UIApplication sharedApplication].statusBarFrame.size.height : [UIApplication sharedApplication].statusBarFrame.size.width))

#define ME_APP_TECH_SUPPORT_EMAIL @"help@moex.com"
#if DEBUG == 1
#define ME_APP_AUTHOR_SUPPORT_EMAIL @"Denis.Svinarchuk@moex.com"
#else
#define ME_APP_AUTHOR_SUPPORT_EMAIL @""
#endif
#define ME_APP_NAME @"MOEX (iOS)"
#define ME_APP_VERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]
#define ME_APP_RELEASE_DATE @"2019-12-31 01:35:00"

#if DEBUG == 1
#define ME_APP_BUILD @"b" // == (b-beta|rc-release-candidate|r-release))
#else
#define ME_APP_BUILD @"rc" // == (b-beta|rc-release-candidate|r-release))
#endif

#if DEBUG == 1
#define ME_APP_VESRION_INFO [NSString stringWithFormat:@"%@ (%@%@)", [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"], ME_APP_BUILD, [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]]
#else
#define ME_APP_VESRION_INFO [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]]
#endif

#define ME_NEWS_BASEURL @"http://moex.com"

#define ME_VALIDATE_CERT                YES
#define ME_SERVER                       @"wss://iss.moex.com/infocx/v2/websocket"
#define ME_PASSPORT_REGISTRATION_URL    @"https://passport.moex.com/%@registration"
#define ME_PASSPORT_RECOVERY_URL        @"https://passport.moex.com/%@password/recovery"
#define ME_PASSPORT_CHANGE_PASSWORD_URL @"https://passport.moex.com/%@password"

@interface MEConfig : GIConfig

@property(nonatomic) NSURL *registrationUrl;
@property(nonatomic) NSURL *passwordRecoveryUrl;
@property(nonatomic) NSURL *passwordChangeUrl;

@property(nonatomic,assign) BOOL isAuthorized;
@property(nonatomic,readonly) BOOL hasUser;

+ (MEConfig*) sharedConfig;

@end
