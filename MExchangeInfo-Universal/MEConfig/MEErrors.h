//
//  MEErrors.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/11/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ME_ERROR_STOMP_DOMAIN @"com.moex.mexchange.stomp"
#define ME_ERROR_NETWORK_DOMAIN @"com.moex.mexchange.network"
#define ME_ERROR_ACTION_DOMAIN @"com.moex.mexchange.action"


typedef enum {
    ME_ERROR_STOMP_CODE = 4000,
    ME_ERROR_NETWORK_CODE,
    ME_ERROR_ACTION_CODE
} MEErrorCodes;