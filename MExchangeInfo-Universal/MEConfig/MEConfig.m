//
//  MEConfig.m
//  MExchange
//
//  Created by denn on 4/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEConfig.h"


MEConfig *__shared_config = nil;

#define __ME_CONFIG_AUTHORIZED__ @"__ME_CONFIG_AUTHORIZED__"

@implementation MEConfig

+ (MEConfig*) sharedConfig{
    if (__shared_config) {
        return __shared_config;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_config = [[MEConfig alloc] init];  
        __shared_config.isAuthorized = [[NSUserDefaults standardUserDefaults] boolForKey:__ME_CONFIG_AUTHORIZED__];
        //if (!__shared_config.isAuthorized) {
        //    __shared_config.login.domain = GIC_GUEST_DOMAIN;
        //}
    });
    
    return __shared_config;
}

- (void) setIsAuthorized:(BOOL)isAuthorized{
    [[NSUserDefaults standardUserDefaults] setBool:isAuthorized forKey:__ME_CONFIG_AUTHORIZED__];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) isAuthorized{
    return [[NSUserDefaults standardUserDefaults] boolForKey:__ME_CONFIG_AUTHORIZED__];
}

- (BOOL) hasUser{
    if (self.login.user && ![self.login.user isEqualToString:@""]) {
        return YES;
    }
    
    return NO;
}

- (id) init{
    self = [super initWithUrl:ME_SERVER];
    
    if (self) {
        self.validatesSecureCertificate = ME_VALIDATE_CERT;

        NSString *lang = @"en/";
        NSString *languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([languageCode hasPrefix:@"ru"])
            lang = @"";
        
        self.registrationUrl = [NSURL URLWithString: [NSString stringWithFormat: ME_PASSPORT_REGISTRATION_URL, lang]];
        self.passwordChangeUrl = [NSURL URLWithString: [NSString stringWithFormat: ME_PASSPORT_CHANGE_PASSWORD_URL, lang]];
        self.passwordRecoveryUrl = [NSURL URLWithString: [NSString stringWithFormat: ME_PASSPORT_RECOVERY_URL, lang]];
    }
    return self;
}

@end
