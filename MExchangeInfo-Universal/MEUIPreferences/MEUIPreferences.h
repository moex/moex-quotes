//
//  MEUIPreferences.h
//  MExchangeInfo-Universal
//
//  Created by denn on 28.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMenuDefaults.h"

static CGFloat const ME_UI_ANIMATION_DURATION=MEUI_ANIMATION_DELAY;
static CGFloat const  ME_UI_HIDESHOW_DURATION=0.3;

#define IS_PHONEPOD5 ([UIScreen mainScreen].bounds.size.height == 568.0f && [UIScreen mainScreen].scale == 2.f && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define ME_UI_UPDATE_RATE (0.3f) // 1/50 = 50Hz 1/25 = 20Hz 1/10 = 10 Hz

#define ME_UI_BACKGROUND_IMAGE  ((IS_PHONEPOD5)?([UIImage imageNamed:@"background-586h.png"]):([UIImage imageNamed:@"background.png"]))

#define ME_UI_FONT  @"HelveticaNeue-Light"
#define ME_UI_BOLD_FONT @"HelveticaNeue"
#define ME_UI_BUTTON_FONT_SIZE 14

#define ME_UI_NAVIBAR_FONT @"HelveticaNeue"
#define ME_UI_NAVIBAR_BACK_BUTTON_FONT @"HelveticaNeue-Light"
#define ME_UI_NAVIBAR_FONT_SIZE 18
#define ME_UI_NAVIBAR_FONT_BACK_BUTTON_SIZE 14
#define ME_UI_TOOLBAR_COLOR [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1]
#define ME_UI_TOOLBAR_TINT_COLOR [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1]

#define ME_UI_MENU_FONT_COLOR ([UIColor colorWithRed:(204./255.) green:(204./255.) blue:(204./255.) alpha:1.])
#define ME_UI_SELECTED_MENU_FONT_COLOR   ([UIColor colorWithRed:1. green:1. blue:1. alpha:1.])

#define ME_UI_TREND_NEGATIVE_COLOR  [UIColor colorWithRed:207.0/255.0 green:34.0/255.0 blue:0 alpha:1 ]
#define ME_UI_TREND_POSITIVE_COLOR  [UIColor colorWithRed:100.0/255.0 green:158.0/255.0 blue:0 alpha:1 ]
#define ME_UI_TREND_NEUTRAL_COLOR   [UIColor colorWithRed:0.01 green:0.1 blue:0.8 alpha:0.7];

#define ME_UI_TREND_NEGATIVE_COLOR_ON_CHART  [UIColor colorWithRed:207.0/255.0 green:34.0/255.0 blue:0 alpha:0.7 ]
#define ME_UI_TREND_POSITIVE_COLOR_ON_CHART  [UIColor colorWithRed:100.0/255.0 green:158.0/255.0 blue:0 alpha:0.7 ]

#define ME_UI_TREND_NEGATIVE_COLOR_ON_GREY  [UIColor colorWithRed:255.0/255.0 green:54.0/255.0 blue:0 alpha:1.0 ]
#define ME_UI_TREND_POSITIVE_COLOR_ON_GREY  [UIColor colorWithRed:130.0/255.0 green:220.0/255.0 blue:0 alpha:1.0 ]

#define ME_UI_DARK_GREY_FONT_COLOR [UIColor colorWithRed:60./256. green:60./256. blue:60./256. alpha:1.]
#define ME_UI_GREY_FONT_COLOR [UIColor colorWithRed:145./256. green:145./256. blue:145./256. alpha:1.]
#define ME_UI_LIGHT_GREY_FONT_COLOR [UIColor colorWithRed:204./256. green:204./256. blue:204./256. alpha:1.]
#define ME_UI_LIGHT_GREY_ARROW_COLOR [UIColor colorWithRed:204./256. green:204./256. blue:204./256. alpha:.6]

#define ME_UI_DIMMER_COLOR [UIColor colorWithRed:.3 green:.3 blue:.3 alpha:0.2]
#define ME_MESSAGE_VIEW_DEF_TIMEOUT 10.0f
#define ME_MESSAGE_VIEW_NUMBERS_OF_LINES 10
#define ME_MESSAGE_MAX_FONT_SIZE  17.0f
#define ME_MESSAGE_MIN_FONT_SIZE  14.0f

#define ME_MESSAGE_ALPHA .7f
#define ME_MESSAGE_BACKGROUND_COLOR [UIColor colorWithRed:104.f/255.f green:104.f/255.f blue:104.f/255.f alpha:1.f]
#define ME_MESSAGE_VIEW_EDGE_INSETS  UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)
#define ME_MESSAGE_VIEW_CONER_RADIUS 2.0f

#define ME_DEFERRED_ACTION_DELAY 0.5
#define ME_DEFERRED_SCROLL_DELAY 5

#define ME_TABLE_EVEN_COLOR     [UIColor colorWithWhite:0.98 alpha:1.]
