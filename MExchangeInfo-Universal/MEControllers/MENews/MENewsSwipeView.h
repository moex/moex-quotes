//
//  MENewsScrollView.h
//  MExchange
//
//  Created by denn on 8/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//  Some ideas has been taken from http://idevrecipes.com/
//

#import <UIKit/UIKit.h>
#import "MENewsContentView.h"

#define ME_NEWS_SWIPE_MAX_DESTINATION    100

#define ME_NEWS_SWIPE_HEIGHT             78
#define ME_NEWS_SWIPE_VERTICAL_PADDING   12
#define ME_NEWS_SWIPE_HORIZONTAL_PADDING 12
#define ME_NEWS_CAPTION_LINES   7
#define ME_NEWS_TOOLBAR_HEIGHT  44

typedef enum {
    ME_NEWS_SWIPE_PULLDOWN,
    ME_NEWS_SWIPE_PULLUP,
    ME_NEWS_SWIPE_CHANGE
} MENewsSwipeDirection;


@class MENewsSwipeView;

@protocol MENewsSwipeDelegate <NSObject>
- (void) newsSwipeView:(MENewsSwipeView*)view didSwiping:(MENewsSwipeDirection)direction;
@optional
- (void) newsSwipeView:(MENewsSwipeView *)view didScroll:(UIScrollView*)scrollView;
@end

@interface MENewsSwipeView : UIScrollView

@property id<MENewsSwipeDelegate> swipeDelegate;
@property id<UIWebViewDelegate> webViewDelegate;

@property (nonatomic) UIImage  *dividerImage;
@property (nonatomic) UIImage  *headerImage,*footerImage;
@property MENewsSwipeDirection animateDirection;

@property(nonatomic) CGFloat horizontalPadding, verticalPadding, height, maxSwipeDestination;
@property(nonatomic) UIView* toolBar;
@property(nonatomic,readonly) MENewsContentView *webView;

- (void) setPage:(NSString*)html withHeaderText:(NSString*)aHeaderText withFooterText:(NSString*)aFooterText;
- (void) updatePageWithHtml:(NSString*)html;
- (void) updatePageWithRequest:(NSURLRequest*)request;
- (void) beginLoading;
- (void) endLoading;
- (void) stopLoading;

@end
