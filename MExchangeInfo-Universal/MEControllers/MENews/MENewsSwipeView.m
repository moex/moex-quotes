//
//  MENewsScrollView.m
//  MExchange
//
//  Created by denn on 8/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//  Some ideas has been taken from http://idevrecipes.com/
//

#import "MENewsSwipeView.h"
#import "MELabel.h"
#import "MEUIPreferences.h"
#import "MENewsContentView.h"

@interface MENewsSwipeView() <UIScrollViewDelegate, UIWebViewDelegate>

@property (nonatomic) UIView* headerView, *footerView;
@property (nonatomic) MELabel *headerLabel, *footerLabel;
@property (nonatomic) UIImageView *headerImageView, *footerImageView; 
@property (nonatomic) NSString *headerCaption,*footerCaption;
@property (nonatomic) MENewsContentView *webView;

@end

@implementation MENewsSwipeView
{
    UIImageView *dividerImageHeaderView, *dividerImageFooterView;
    
    BOOL _headerLoaded;
    BOOL _footerLoaded;
    
    NSString                *currentHtml;
    UIActivityIndicatorView *currentSpinner;
    
    UIView *toolBarShield;
    UIWebView *previousView;
    
    BOOL is_fixedHeight;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self setPreferecnces];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setPreferecnces];
    }
    return self;
}

- (void) setPreferecnces{
    
    self.alwaysBounceVertical = YES;
    self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    self.delegate = self;
    
    self.showsHorizontalScrollIndicator=NO;
    self.showsVerticalScrollIndicator = YES;
        
    _horizontalPadding = ME_NEWS_SWIPE_HORIZONTAL_PADDING;
    _verticalPadding = ME_NEWS_SWIPE_VERTICAL_PADDING;
    _height = ME_NEWS_SWIPE_HEIGHT;
    _animateDirection = ME_NEWS_SWIPE_CHANGE;    
    _maxSwipeDestination = ME_NEWS_SWIPE_MAX_DESTINATION;
}

- (void) setToolBar:(UIView *)toolBar{
    _toolBar = toolBar;
    if (_toolBar!=0) {
        toolBarShield = [[UIView alloc] initWithFrame:CGRectMake(0, _toolBar.frame.origin.y-_toolBar.frame.size.height, _toolBar.frame.size.width, _toolBar.frame.size.height)];
        toolBarShield.backgroundColor = _toolBar.backgroundColor;
        toolBarShield.alpha = .0;
        [_toolBar.superview insertSubview:toolBarShield belowSubview:_toolBar];
        self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _toolBar.frame.size.height, 0);
    }
}

- (void) scaleImage:(UIImageView*)image withScale:(CGFloat) percentage{    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    image.layer.transform = CATransform3DMakeScale(percentage, percentage, 1);
    [CATransaction commit];
}

CGFloat degreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};


- (void) rotateImageView:(UIImageView*)imageView withDirection:(int)direction
{
    [imageView.layer removeAllAnimations];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: direction * M_PI * 2.0 /* full rotation*/ * 2. * ME_UI_ANIMATION_DURATION ];
    rotationAnimation.duration = ME_UI_ANIMATION_DURATION;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    [imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}
    
- (void) runSpinner:(UIView*)view{
    
    if (_headerLoaded || _footerLoaded) {
        return;
    }
    
    if (currentSpinner && currentSpinner.isAnimating) {
        return;
    }
    
    if (!currentSpinner) {
        currentSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        currentSpinner.frame =  view.frame;
        
        [view.superview addSubview:currentSpinner]; 
        if ([view isKindOfClass:[UIImageView class]]) {
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                view.alpha = 0.0;
            } completion:^(BOOL finished){
                currentSpinner.alpha = 0.0;
                currentSpinner.hidden = NO;
                [currentSpinner startAnimating];
                [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                    currentSpinner.alpha = 1.0;
                }];
            }];
        }
        else{
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                view.alpha = 0.3;
            }];
            [currentSpinner startAnimating];
        }
    }    
}

- (void) beginLoading{
    if (_webView) {
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            _webView.alpha = 0.3;
        }];
    }
}

- (void) endLoading{
    if (_webView) {
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            _webView.alpha = 1.0;
        }];
    }
}

- (void) scrollBack:(UIImageView*)imageView withCaptionView:(UIView*)view{
    [self endLoading];
    [self scaleImage:imageView withScale:1.0];

    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        if (currentSpinner && currentSpinner.isAnimating) {
            currentSpinner.alpha = 0.0;
            if (imageView) {
                imageView.alpha=1.0;
            }
        }
        
    } completion:^(BOOL finished){
        
        if (fabs(self.contentOffset.y)>=_maxSwipeDestination)
            return ;
        
        [currentSpinner stopAnimating];
        [currentSpinner removeFromSuperview];
        currentSpinner=nil;
        imageView.alpha=1.0;
    }];
}

- (void) captionSizes:(UIView*)captionView withDivider:(UIImageView*)divider withDistance:(CGFloat)distance{
    if (fabsf(distance)>=_height) {
        if (distance<=0) 
            captionView.frame = CGRectMake(captionView.frame.origin.x, distance, captionView.frame.size.width, fabsf(distance));                    
        else
            captionView.frame = CGRectMake(captionView.frame.origin.x, captionView.frame.origin.y, captionView.frame.size.width, fabsf(distance));                    
    }
    else if (distance<0) 
            captionView.frame = CGRectMake(captionView.frame.origin.x, -_height, captionView.frame.size.width, _height);                    
    
        
    for (UIView *view in captionView.subviews) {
        if (view == divider) {
            if (distance>0) {
                divider.frame=CGRectMake(0, 0, divider.frame.size.width, divider.frame.size.height);
            }
            else
                divider.frame=CGRectMake(0, captionView.frame.size.height-divider.frame.size.height, divider.frame.size.width, divider.frame.size.height);

        }
        else            
            view.center=CGPointMake(view.center.x, captionView.frame.size.height/2.);
    }
}

// scrollViewDidScroll is where we can tell that the header or footer are fully
// in view or out of view. This is how we accomplish the animation when you pull
// up/down to see the previous/next page.
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (self.swipeDelegate && [self.swipeDelegate respondsToSelector:@selector(newsSwipeView:didScroll:)]) {
        [self.swipeDelegate newsSwipeView:self didScroll:scrollView];
    }
    
    CGFloat delta = scrollView.contentOffset.y;
    UIView  *_captionView ;
    CGFloat max_distance = _maxSwipeDestination;
            
    CGFloat _distance = delta;
    
    if (delta <= 0){
        _captionView = _headerView;
        _distance=-_distance;
        toolBarShield.alpha=0.0;
        self.scrollIndicatorInsets = UIEdgeInsetsMake(-delta, 0, _toolBar.frame.size.height, 0);
    }
    else{
        _captionView = _footerView;
        _distance = delta+scrollView.frame.size.height-scrollView.contentSize.height+_toolBar.frame.size.height;

        self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _toolBar.frame.size.height, 0);
        if (_distance>_verticalPadding) {
            self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _distance+_toolBar.frame.size.height-_verticalPadding, 0);
        }
        
        if (_distance<0)
            return;
    }

    //self.contentInset = UIEdgeInsetsMake(-delta, self.contentInset.left, self.contentInset.bottom, self.contentInset.right);

    // Everything we want to do is only applicable if the user is in the middle of dragging
    if (!scrollView.dragging || !scrollView.isTracking) {
        return;
    }

    CGFloat percentage = MIN(1, (_distance/max_distance));
    
    // The user is dragging down, we are loading/unloading the header/previous page view
    if (delta < 0)
    {
        // If the header is hidden, then there is no previous page and nothing for us to do
        if (!_headerView) return;
                
        [self scaleImage:self.headerImageView withScale:percentage];
                
        [self captionSizes:_headerView withDivider:dividerImageHeaderView withDistance:-_distance];

        // If the user has pulled down more than the height of the header
        if (_distance >= max_distance)
        {
            // The header is already loaded, nothing for us to do
            if (_headerLoaded) return;
            
            if (_headerImageView) {
                [self rotateImageView:_headerImageView withDirection:1];
            }else if (_headerView){
                [self runSpinner:_headerView];
            }
            
            // The header has been loaded
            _headerLoaded = YES;
            [self beginLoading];
        }
        else // The user has pulled down less than the height of the header
        {
            // If the header isn't already loaded, nothing for us to do
            if (!_headerLoaded) return;

            if (_headerLoaded) {
                [self rotateImageView:_headerImageView withDirection:-1];
            }
            
            // The header has been unloaded
            _headerLoaded = NO;
            [self scrollBack:_headerImageView withCaptionView:_headerView];
        }
    }
    else // The user is dragging up, we are loading/unloading the footer/next page view
    {
        
        // If the footer is hidden, then there is no next page and nothing for us to do
        if (!_footerView) return;
                
        
        [self scaleImage:self.footerImageView withScale:percentage];
        
        // If the user has pulled up more than the height of the
        
        [self captionSizes:_footerView withDivider:dividerImageFooterView withDistance:_distance];

        if (delta >= 0){
            toolBarShield.alpha = 1.0;
        }
        else{
            toolBarShield.alpha = 0.0;
        }

        if (_distance<=_verticalPadding) {
            //_footerView.alpha = 0.0;
        }
        else
            _footerView.alpha = 1.0;
                    
        if (_distance > max_distance)
        {
            // The footer is already loaded, nothing for us to do
            if (_footerLoaded) return;
            
            if (_footerImageView) {
                [self rotateImageView:_footerImageView withDirection:-1];
            }else if (_footerView){
                [self runSpinner:_footerView];
            }
            
            // The footer has been loaded
            _footerLoaded = YES;
            [self beginLoading];
        }
        else // The user has pulled up less than the height of the footer
        {
            // If the footer isn't already loaded, nothing for us to do
            if (!_footerLoaded) return;
            
            if (_footerImageView) {
                [self rotateImageView:_footerImageView withDirection:1];
            }
            
            // The footer has been unloaded
            _footerLoaded = NO;
            [self scrollBack:_footerImageView withCaptionView:_footerView];
        }
    }
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (_headerLoaded || _footerLoaded)
        [self scrollViewDidEndDragging:scrollView willDecelerate:NO];
}

// The scroll view sends this message when the user’s finger touches up after dragging content
// This where we start switching to the previous/next page
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//    NSLog(@"SCROLLED: offset=%f  insets=%f", scrollView.contentOffset.y, scrollView.contentInset.top);
//    self.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, self.contentInset.left, self.contentInset.bottom, self.contentInset.right);
    if (self.swipeDelegate && [self.swipeDelegate respondsToSelector:@selector(newsSwipeView:didSwiping:)]) {
        // If the header is loaded, then the user wants to go to the previous page
        if (_headerLoaded)
        {
            [self.swipeDelegate newsSwipeView:self didSwiping:ME_NEWS_SWIPE_PULLDOWN];
            _headerLoaded = NO;
        }
        else if (_footerLoaded) // If the footer is loaded, then the user wants to go to the next page
        {
            [self.swipeDelegate newsSwipeView:self didSwiping:ME_NEWS_SWIPE_PULLUP];
            _footerLoaded = NO;
        }
        else{
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                toolBarShield.alpha = 0.0;
                _footerView.alpha = 0.0;
            }];
            [self endLoading];
        }
    }
    else{
        _headerLoaded = _footerLoaded = NO;
    }
}

- (BOOL) scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    return YES;
}

- (void) setPage:(NSString *)html withHeaderText:(NSString *)aHeaderText withFooterText:(NSString *)aFooterText{
    
    _headerLoaded = NO;
    _footerLoaded = NO;
    
    if (html) {
        
        if (currentHtml==html)
            return;

        self.bounces = NO;

        _headerCaption = aHeaderText;
        _footerCaption = aFooterText;

        currentHtml = html;
        
        previousView = _webView;
                
        _webView = [[MENewsContentView alloc] initWithFrame:self.bounds];
        _webView.scrollView.scrollEnabled = NO;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;

        _webView.backgroundColor = [UIColor lightGrayColor];
                
        is_fixedHeight = NO;

        _webView.delegate = self;
        //_webView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_webView];

        //[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[web]-0-|" options:0 metrics:nil views:@{@"web":_webView}]];
        //[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[web]-0-|" options:0 metrics:nil views:@{@"web":_webView}]];
        
        [_webView loadHTMLString:currentHtml baseURL:nil];
    }
    else{
        if (_webView) 
            [_webView removeFromSuperview];
        _webView = nil;
    }
}

- (void) stopLoading{
    [_webView stopLoading];
}

- (void) updatePageWithHtml:(NSString*)html{
    _animateDirection = ME_NEWS_SWIPE_CHANGE;
    [self setPage:html withHeaderText:_headerCaption withFooterText:_footerCaption];
}

- (void) updatePageWithRequest:(NSURLRequest*)request{

    if (!request)
        return;
    
    _animateDirection = ME_NEWS_SWIPE_CHANGE;
    previousView = _webView;
    is_fixedHeight = YES;

    _webView = [[MENewsContentView alloc] initWithFrame:self.bounds];
    //_webView.frame = CGRectMake(0, 0, 320, 504);

    _webView.scrollView.scrollEnabled = YES;
    
    _webView.delegate = self;

    _webView.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-_toolBar.frame.size.height);
    self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height+_toolBar.frame.size.height-_verticalPadding);

    [_webView loadHTMLString:currentHtml baseURL:nil];
    
    [self addSubview:_webView];
    
    [_webView loadRequest:request];
}


- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked){
        _animateDirection = ME_NEWS_SWIPE_CHANGE;
        is_fixedHeight = YES;
    
        _webView.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-_toolBar.frame.size.height);
        self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height+_toolBar.frame.size.height-_verticalPadding);
        
        _webView.scrollView.scrollEnabled = YES;
    }
    
    if (self.webViewDelegate && [self.webViewDelegate respondsToSelector:@selector(webView:shouldStartLoadWithRequest:navigationType:)]) {
        [self.webViewDelegate webView:_webView shouldStartLoadWithRequest:request navigationType:navigationType];
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    if (self.webViewDelegate && [self.webViewDelegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.webViewDelegate webViewDidStartLoad:webView];
    }
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    CGRect mWebViewFrame = webView.frame;
    CGFloat realHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.height;"] floatValue];
    
    realHeight = realHeight<=0?webView.scrollView.contentSize.height:realHeight;

    NSLog(@"Did load %i", _animateDirection);
    
    NSLog(@"WEB view height: %f <> %f zoom=%f isfixed=%i ", realHeight, webView.scrollView.contentSize.height, webView.scrollView.zoomScale, is_fixedHeight);
    

    CGFloat shiftView = previousView.frame.origin.y;

    mWebViewFrame.size.height = realHeight+_toolBar.frame.size.height+_verticalPadding;
    
    if (!is_fixedHeight) {
        _webView.frame = mWebViewFrame;
        self.contentSize = CGSizeMake(_webView.frame.size.width, mWebViewFrame.size.height);        
    }

    if (previousView) {
        //_webView.alpha = 0.0;
        if (_animateDirection==ME_NEWS_SWIPE_PULLDOWN) {
            shiftView = -_webView.scrollView.contentSize.height-_headerView.frame.size.height;
            _webView.frame = CGRectMake(0, shiftView, _webView.frame.size.width, _webView.frame.size.height);
        }
        else if (_animateDirection==ME_NEWS_SWIPE_PULLUP) {
            shiftView = previousView.scrollView.contentSize.height+_footerView.frame.size.height;
            _webView.frame = CGRectMake(0, shiftView, _webView.frame.size.width, _webView.frame.size.height);
        }
    }
    //else _webView.alpha = 0.0;
    
    _footerView.alpha = 1.0;
    
    if (toolBarShield)
        toolBarShield.alpha = 0.0;
    
    void (^_completion)(BOOL) = ^(BOOL finished){
        
        if (previousView) [previousView removeFromSuperview];
        
        if (currentSpinner) {
            [currentSpinner stopAnimating];
            [currentSpinner removeFromSuperview];
        }
        
        currentSpinner = nil;
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            _webView.alpha = 1.0;
            //if (previousView)
            //    previousView.alpha = 0.0;
        }];
        
        self.headerCaption = _headerCaption;
        self.footerCaption = _footerCaption;
        self.dividerImage  = _dividerImage;
        
    };
    
    if (previousView) {
        [self endLoading];
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            if (_animateDirection!=ME_NEWS_SWIPE_CHANGE) {
                previousView.frame = CGRectMake(0, -shiftView, previousView.frame.size.width, previousView.frame.size.height);                
                _webView.frame = CGRectMake(0, 0, _webView.frame.size.width, _webView.frame.size.height);
            }

            if (_animateDirection == ME_NEWS_SWIPE_PULLDOWN) {
                _headerView.frame = CGRectMake(0, -shiftView-_headerView.frame.size.height, _headerView.frame.size.width, _headerView.frame.size.height);
            }
            else if (_animateDirection==ME_NEWS_SWIPE_PULLUP) {
                _footerView.frame = CGRectMake(0, -_footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
            }
            
        } completion:^(BOOL finished){
            _completion(finished);            
        }];
    }
    else{
        _completion(YES);
    }
    
    self.bounces = YES;
    self.contentOffset = CGPointMake(0, 0);

    if (self.webViewDelegate && [self.webViewDelegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [self.webViewDelegate webViewDidFinishLoad:webView];
    }
}



- (void) setHeaderCaption:(NSString *)aHeaderCaption{
    _headerCaption = aHeaderCaption;
    if (_headerCaption) {
        self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, _height)];
        self.headerLabel.text = _headerCaption;
    }
    else{
        self.headerView = nil;
    }
}

- (void) setFooterCaption:(NSString *)aFooterCaption{
    _footerCaption = aFooterCaption;
    if (_footerCaption) {
        self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, _height)];
        self.footerLabel.text = _footerCaption;
    }
    else{
        self.footerView = nil;
    }
}

- (UIImageView*) newImageView:(UIView*)view withImage:(UIImage*)image{
    if (image) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(_horizontalPadding, _verticalPadding+view.frame.size.height/2.-image.size.height, image.size.width, image.size.height)];
        imageView.image = image;
        
        [view addSubview:imageView];
        
        return imageView;
    }
    return nil;
}

- (MELabel*) newLabel:(UIView*)view withImageView:(UIImageView*)imageView{
    
    CGFloat labelPadding_x = imageView?(imageView.frame.size.width+2*_horizontalPadding):_horizontalPadding;
    MELabel  *label = [[MELabel alloc] initWithFrame:CGRectMake(labelPadding_x, _verticalPadding, view.frame.size.width-labelPadding_x-_horizontalPadding, view.frame.size.height-2*_verticalPadding)];
    
    label.numberOfLines = ME_NEWS_CAPTION_LINES;
    
    [view addSubview:label];
    
    return label;
}

// The header view is what appears when you pull the
// scroll view down to get to the previous page
-(void) setHeaderView:(UIView*)newValue
{
    if (_headerView != newValue)
    {
        // Standard setter code
        [_headerView removeFromSuperview];
        _headerView = newValue;
        
        if (!newValue){ 
            _headerImageView = nil;
            _headerLabel = nil;
            return;
        }
        
        // Place the header above the scroll view
        _headerView.frame = CGRectMake(0, -_headerView.frame.size.height, _headerView.frame.size.width, _headerView.frame.size.height);
        
        _headerImageView = [self newImageView:_headerView withImage:_headerImage];
        _headerImageView.backgroundColor = [UIColor clearColor];
        _headerLabel = [self newLabel:_headerView withImageView:_headerImageView];
        _headerLabel.backgroundColor = [UIColor clearColor];
        
        [self scaleImage:_headerImageView withScale:1.0];
        [self addSubview:_headerView];   
    }
}

// The footer view is what appears when you pull the
// scroll view up to get to the next page
-(void) setFooterView:(UIView*)newValue
{
    if (_footerView != newValue)
    {
        // Standard setter code
        [_footerView removeFromSuperview];
        _footerView = newValue;
        
        if (!newValue) {
            _footerLabel = nil;
            _footerImageView = nil;
            return;     
        }
        
        // Place the footer below the scroll view
        _footerView.frame = CGRectMake(0, self.contentSize.height-2*_toolBar.frame.size.height+_verticalPadding, _footerView.frame.size.width, _footerView.frame.size.height);
        
        _footerImageView = [self newImageView:_footerView withImage:_footerImage];
        _footerImageView.backgroundColor = [UIColor clearColor];
        _footerLabel = [self newLabel:_footerView withImageView:_footerImageView];
        _footerLabel.backgroundColor = [UIColor clearColor];
        
        [self scaleImage:_footerImageView withScale:1.0];
        _footerView.alpha = 0.0;
        
        [self addSubview:_footerView];
    }    
}

- (void) setDividerImage:(UIImage *)aDividerImage{
    
    _dividerImage = aDividerImage;
    
    if (!aDividerImage) {
        if (dividerImageHeaderView) 
            [dividerImageHeaderView removeFromSuperview];
        if (dividerImageFooterView) 
            [dividerImageFooterView removeFromSuperview];
        return;
    }
    
    if (_headerView) {                
        
        if (dividerImageHeaderView) 
            [dividerImageHeaderView removeFromSuperview];
        
        dividerImageHeaderView = [[UIImageView alloc] initWithImage:aDividerImage];
        
        dividerImageHeaderView.frame = CGRectMake(0, _headerView.frame.size.height-1, _headerView.frame.size.width, aDividerImage.size.height);
        [_headerView addSubview:dividerImageHeaderView];
    }
    if (_footerView) {
        
        if (dividerImageFooterView) 
            [dividerImageFooterView removeFromSuperview];
        
        dividerImageFooterView = [[UIImageView alloc] initWithImage:aDividerImage];
        dividerImageFooterView.frame = CGRectMake(0, 0, _footerView.frame.size.width, aDividerImage.size.height);
        
        [_footerView addSubview:dividerImageFooterView];
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
