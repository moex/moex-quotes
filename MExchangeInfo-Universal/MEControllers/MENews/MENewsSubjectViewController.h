//
//  MENewsSubjectViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MENewsModel.h"
#import "MENewsSubjectView.h"

@class MENewsSubjectViewController;

@protocol MENewsSubjectViewControllerProtocol <NSObject>
- (void) newsSubjectDidUpdateItem:(MENewsItem*) item;
@optional
- (void) newsSubjectViewDidEndChange:(MENewsItem*) item withDirection:(MENewsSubjectSwitchDirection)direction;
@end


@interface MENewsSubjectViewController : UIViewController
@property (strong, nonatomic) MENewsItem *newsItem;
@property (strong, nonatomic) id<MENewsSubjectViewControllerProtocol> updateDelegate;
@property (nonatomic) UINavigationItem *masterNavigationItem;
@end
