//
//  MEDetailViewController.m
//  MExchangeInfo-Universal
//
//  Created by denn on 10/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//
// BLURRED: http://uncorkedstudios.com/blog/ios-7-background-effects-and-split-view-controllers
//

#import "MENewsListViewController.h"
#import "MEAlertsView.h"
#import "MENewsModel.h"
#import "MENewsSubjectViewController.h"
#import "MENewsItemViewCell.h"
#import "MENewsListHeaderView.h"
#import "MEUIPreferences.h"
#import "METableView.h"
#import "MESettings.h"

#import <ObjectiveSugar/ObjectiveSugar.h>
#import <Crashlytics/Crashlytics.h>


static NSString *newsItemCellID = ME_NEWSLIST_ITEM_CELL;
static NSString *newsHeaderID = ME_NEWSLIST_HEADER;

@interface MENewsListViewController() <UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, MENewsDelegate, MENewsSubjectViewControllerProtocol, UIAlertViewDelegate>
@property (nonatomic, strong) UIBarButtonItem *activityIndicator;
@property (nonatomic, strong)     NSIndexPath *lastSelectedIndexPath;
@property (nonatomic, strong)     MENewsSubjectViewController *newsSubjectViewController;
@end

@implementation MENewsListViewController
{
    BOOL isAppeared;
    MENewsModel *newsList;    
    BOOL isNewsContentPresented;
}

@synthesize lastSelectedIndexPath = _lastSelectedIndexPath;

- (NSIndexPath*) lastSelectedIndexPath{
    
    if (!_lastSelectedIndexPath) {
        _lastSelectedIndexPath = ((METableView*)self.tableView).lastSelectedIndexPath;
        if (newsList && [newsList sections]>((METableView*)self.tableView).lastSelectedIndexPath.section) {
            if ([newsList rowsForSection:_lastSelectedIndexPath.section]>_lastSelectedIndexPath.row) {
                return _lastSelectedIndexPath;
            }
            return nil;
        }
    }
    
    return _lastSelectedIndexPath;
}

- (void) setLastSelectedIndexPath:(NSIndexPath *)lastSelectedIndexPath{
    _lastSelectedIndexPath = lastSelectedIndexPath;
    ((METableView*)self.tableView).lastSelectedIndexPath = _lastSelectedIndexPath;
}

- (UIBarButtonItem*) activityIndicator{
    if (!_activityIndicator) {
        UIActivityIndicatorView *activityIndicator =
        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activityIndicator = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    }    
    return _activityIndicator;
}

- (void)hideActivityIndicator {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // we are done processing earthquakes, stop our activity indicator
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView stopAnimating];
        if (self.newsSubjectViewController.masterNavigationItem)
            self.newsSubjectViewController.masterNavigationItem.rightBarButtonItem = nil;
        else
        self.navigationItem.rightBarButtonItem = nil;
    });
}

- (void) showActivityIndicator{    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView startAnimating];
        if (self.newsSubjectViewController.masterNavigationItem)
            self.newsSubjectViewController.masterNavigationItem.rightBarButtonItem = self.activityIndicator;
        else
        self.navigationItem.rightBarButtonItem = self.activityIndicator;
    });
}

#pragma mark - MENewsdelegate

- (void) didNewsError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];

        [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_NETWORK_LOST];
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:error.localizedFailureReason
                                  message:error.localizedDescription
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"Cancel button", @"")
                                  otherButtonTitles:NSLocalizedString(@"Repeat", @""), nil];
        
        alertView.cancelButtonIndex = 0;
        alertView.delegate = self;
        [alertView show];
    });
    [newsList unsubscribe];
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        return;
    }
    [newsList subscribe];
}


- (void) deferredReload{    
    if (newsList.items.count==0) 
        return;    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];        
    });
}

- (void) reload:(BOOL)toSelection{    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.tableView.isDecelerating || self.tableView.isDragging || self.tableView.isEditing || self.tableView.isTracking)
            return;
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReload) object:nil];
        [self performSelector:@selector(deferredReload) withObject:nil afterDelay:ME_UI_UPDATE_RATE];
    });    
}

- (void) didNewsRestore:(MENewsModel *)list{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.newsSubjectViewController.newsItem = [newsList newsForIndexPath:self.lastSelectedIndexPath];
    }
    [self reload:NO];
    [self hideActivityIndicator];
}

- (void) willNewsUpdate:(MENewsModel *)list{
    [self showActivityIndicator];
}

- (void) didNewsUpdate:(MENewsModel *)news{    
    [self reload:NO];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (self.lastSelectedIndexPath) {
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    self.newsSubjectViewController.newsItem = [self->newsList newsForIndexPath:self.lastSelectedIndexPath];
                } @catch (NSException *exception) {
                    NSLog(@"MENewsListViewController:Error %@", exception);
                    [[MEAlertsView sharedInstance] pushMessageText: NSLocalizedString(@"Connection problem", @"Connection problem") withStatus:ME_ALERT_WARN];
                    [self hideActivityIndicator];
                }
            });
        }
    }
    [self hideActivityIndicator];
}

- (void) newsSubjectDidUpdateItem:(MENewsItem *)item{    
    NSIndexPath *indexPath =  [newsList indexPathForNews:item];
    if (indexPath) {
        self.lastSelectedIndexPath = indexPath;
    }
    [self reload: YES];
}


#pragma mark - Scroll delagate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self reload:YES];
}

//- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
//    [self reload:NO];
//}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self reload:YES];
    }
}


#pragma mark - Managing the detail item

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated{
    isAppeared = YES;
    
    [super viewDidAppear:animated];
    if (!isNewsContentPresented) {
        [newsList subscribe];
    }    
    isNewsContentPresented = NO;
    
    if (newsList.items.count>0) {
        [self reload:YES];        
    }
    
    [Answers logContentViewWithName:@"News List View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{}];
}

- (void) viewDidDisappear:(BOOL)animated{
    isAppeared = NO;
    [super viewDidDisappear:animated];
    if (!isNewsContentPresented) {
        [newsList unsubscribe];        
    }
}


- (void)loginSuccessed:(NSNotification*)event{

    if (isAppeared) {
        if (newsList) {
            [newsList subscribe];
        }
    }
}


- (void)flushCache:(NSNotification*)event{
    if (isAppeared) {
        
        [newsList unsubscribe];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self reload:NO];
            if (self->newsList) {
                [self->newsList subscribe];
            }
        });
    }
}

- (MENewsSubjectViewController*) newsSubjectViewController{
    if (!_newsSubjectViewController) {
        // iphone
        _newsSubjectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"newsSubjectViewID"];
        _newsSubjectViewController.updateDelegate = self;
    }

    return _newsSubjectViewController;
}

- (void) viewDidLoad{
    [super viewDidLoad];
    
    self.detailController = self.newsSubjectViewController;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
        
    [self.tableView registerClass:[MENewsItemViewCell class] forCellReuseIdentifier:newsItemCellID];
    [self.tableView registerClass:[MENewsListHeaderView class] forHeaderFooterViewReuseIdentifier:newsHeaderID];
    
    newsList = [MENewsModel sharedInstance];
    newsList.delegate = self;
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flushCache:) name:ME_SETTINGS_FLUSH_CACHE_NOTIFICATION object:nil];
}

#pragma mark - UITableView source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [newsList sections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [newsList rowsForSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MENewsItem *item = [newsList newsForIndexPath:indexPath];
        
    MENewsItemViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:newsItemCellID forIndexPath:indexPath];
        
    cell.captionLabel.text = [NSString stringWithFormat:@"%@", item.caption];
    cell.shortCutLabel.text = item.cachedShortcut;

    if (item.isRead.boolValue)
        cell.captionLabel.textColor = ME_UI_GREY_FONT_COLOR;
    else
        cell.captionLabel.textColor = [UIColor blackColor];

    cell.timeLabel.text = [NSDateFormatter localizedStringFromDate:item.publicTime dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];

    if (!self.tableView.isDragging & !self.tableView.isDecelerating) {
        //
        // to be faster do not redraw while scrolling
        //
        
        if (indexPath.row == [newsList rowsForSection:indexPath.section]-1)
            cell.devider.alpha = 0.0;
        else
            cell.devider.alpha = 1.0;        

        if (self.lastSelectedIndexPath && [self.lastSelectedIndexPath isEqual:indexPath])
            [self setLastSubjectTitle];
        
        if ([self.lastSelectedIndexPath isEqual:indexPath]) {
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
        
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 28;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{    
    MENewsListHeaderView *header = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:newsHeaderID];
    MENewsSection *newsSection = [newsList sectionForSection:section];    
    header.captionLabel.text = newsSection.caption;        
    return header;
}

#pragma mark - Table view delegate

- (void) setLastSubjectTitle{
    if (self.lastSelectedIndexPath) {
        @try {
            MENewsSection *newsSection = [newsList sectionForSection:self.lastSelectedIndexPath.section];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                self.newsSubjectViewController.navigationItem.title = newsSection.caption;
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@ %s:%i", exception, __FILE__, __LINE__);
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        // Navigation logic may go here. Create and push another view controller.
                
        self.lastSelectedIndexPath = indexPath;
        
        MENewsItem *item = [newsList newsForIndexPath:indexPath];
        
        isNewsContentPresented = YES;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.navigationController pushViewController:self.newsSubjectViewController animated:YES];
        }
        
        [self setLastSubjectTitle];
        
        self.newsSubjectViewController.newsItem = item;        
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

@end
