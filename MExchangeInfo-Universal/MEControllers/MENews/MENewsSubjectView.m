//
//  MENewsSubjectVeiw.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/21/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsSubjectView.h"
#import "MEUIPreferences.h"
#import "MEAlertsView.h"

#define __ME_NEWS_SUBJECT_LAST_NEWS_TITLE NSLocalizedString(@"This is the last news...", @"");

@interface MENewsCloseTitleView: UIView
@property(nonatomic,strong) UILabel *captionLabel;
@property(nonatomic,strong) UILabel *shortCutLabel;
@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UIImageView *directionImageView;
@property(nonatomic,strong) UIImageView *devider;
@end

@implementation MENewsCloseTitleView

- (id) initWithFrame:(CGRect)frame inTop:(BOOL)inTop{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 10)];
        self.shortCutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 10)];
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        if (inTop)
            self.directionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate_previous"]];
        else
            self.directionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate_next"]];
        
        self.devider = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"thin_grey_line.png"]];
        
        self.devider.translatesAutoresizingMaskIntoConstraints = NO;
        self.captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.shortCutLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.directionImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.captionLabel.textColor = [UIColor blackColor];
        self.captionLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:16];
        self.captionLabel.numberOfLines = 2;
        self.captionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        self.shortCutLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.shortCutLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        self.shortCutLabel.minimumScaleFactor=1;
        self.shortCutLabel.adjustsFontSizeToFitWidth = YES;
        self.shortCutLabel.numberOfLines = 3;
        self.shortCutLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        self.shortCutLabel.text = __ME_NEWS_SUBJECT_LAST_NEWS_TITLE;
        
        self.timeLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.timeLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        
        [self addSubview:self.captionLabel];
        [self addSubview:self.timeLabel];
        [self addSubview:self.shortCutLabel];
        [self addSubview:self.directionImageView];
        [self addSubview:self.devider];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(imageWidth)]-10-[caption]-5-[time(50)]-5-|" options:0 metrics:@{@"imageWidth": [NSNumber numberWithFloat:self.directionImageView.bounds.size.width]} views:@{@"image": self.directionImageView, @"caption":self.captionLabel, @"time":self.timeLabel}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[shortCut]-5-|" options:0 metrics:nil views:@{@"shortCut":self.shortCutLabel}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[devider]-0-|" options:0 metrics:nil views:@{@"devider":self.devider}]];
        
        if (inTop)
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=5)-[caption]-2-[shortCut]-(>=5)-[devider(1)]-0-|" options:0 metrics:nil views:@{@"devider":self.devider, @"caption":self.captionLabel, @"shortCut": self.shortCutLabel}]];
        else
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[devider(1)]-(>=5)-[caption]-2-[shortCut]-(>=10)-|" options:0 metrics:nil views:@{@"devider":self.devider, @"caption":self.captionLabel, @"shortCut": self.shortCutLabel}]];
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionLabel attribute:NSLayoutAttributeBaseline relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.timeLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.captionLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.directionImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.captionLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

@end

@implementation MENewsSubjectToolBar
@end

@interface MENewsSubjectView() <UIWebViewDelegate, UIScrollViewDelegate>
@property(strong,nonatomic) UIWebView *newsContentView;
@property(strong,nonatomic) MENewsSubjectToolBar *toolBar;
@end

@implementation MENewsSubjectView
{
    MENewsCloseTitleView *previousItem;
    MENewsCloseTitleView *nextItem;
    MENewsItem *currentNewsItem;
    MENewsSubjectSwitchDirection switchDirection;
    
    NSLayoutConstraint *toolBarPositionConstraint;
    CGPoint            lastOffset;
    
    UIBarButtonItem *nextBarItem;
    UIBarButtonItem *previousBarItem;
    UIBarButtonItem *safariBarItem;
    
    MENewsModel *newsModel;
}

- (void) layoutSubviews{
    [super layoutSubviews];
    previousItem.frame = CGRectMake(0, -self.closeNewsTitleHeight, self.bounds.size.width, self.closeNewsTitleHeight);
    nextItem.frame = CGRectMake(0, self.scrollView.contentSize.height, self.bounds.size.width, self.closeNewsTitleHeight);
}

- (void) config{
    
    newsModel = [MENewsModel sharedInstance];
    
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    switchDirection = ME_NEWS_SUBJECT_UNDEFINED;
    self.closeNewsTitleHeight = 90;
    self.slideVelocity = 2.0;
    self.closeNewsTitleOffset = 24;
    
    self.scalesPageToFit = YES;
    self.scrollView.multipleTouchEnabled = NO;
    self.userInteractionEnabled = YES;
    
    self.scrollView.showsVerticalScrollIndicator=YES;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    
    self.scrollView.bounces = YES;
    self.scrollView.alwaysBounceHorizontal = NO;
    
    self.delegate = self;
    
    previousItem = [[MENewsCloseTitleView alloc] initWithFrame:CGRectMake(0, -self.closeNewsTitleHeight, self.bounds.size.width, self.closeNewsTitleHeight) inTop:YES];
    nextItem = [[MENewsCloseTitleView alloc] initWithFrame:CGRectMake(0, self.scrollView.contentSize.height, self.bounds.size.width, self.closeNewsTitleHeight) inTop:NO];
    
    [self.scrollView addSubview:previousItem];
    [self.scrollView addSubview:nextItem];
    
    self.toolBar = [[MENewsSubjectToolBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.toolBar];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[toolBar]-0-|" options:0 metrics:0 views:@{@"toolBar": self.toolBar}]];
    toolBarPositionConstraint = [NSLayoutConstraint constraintWithItem:self.toolBar attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self addConstraint:toolBarPositionConstraint];
    
    lastOffset = self.scrollView.contentOffset;
    
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    nextBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate_next"] style:UIBarButtonItemStylePlain target:self action:@selector(toolBarNext:)];
    previousBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate_previous"] style:UIBarButtonItemStylePlain target:self action:@selector(toolBarPrevious:)];
    safariBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate_open_safari"] style:UIBarButtonItemStylePlain target:self action:@selector(toolBarSafari:)];
    
    nextBarItem.enabled = previousBarItem.enabled = safariBarItem.enabled = NO;
    
    NSArray *items = [NSArray arrayWithObjects:flexiableItem, nextBarItem, flexiableItem, previousBarItem, flexiableItem, flexiableItem, safariBarItem, flexiableItem, nil];
    
    [self.toolBar setItems:items];
    
    [self setToolbarHidden:YES animated:NO];
}

- (void) toolBarNext:(id)sender{
    [self setToolbarHidden:YES animated:YES];
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectViewDidEndChange:withDirection:)]) {
        [self.newsSubjectDelegate newsSubjectViewDidEndChange:self withDirection:ME_NEWS_SUBJECT_NEXT];
    }
}

- (void) toolBarPrevious:(id)sender{
    [self setToolbarHidden:YES animated:YES];
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectViewDidEndChange:withDirection:)]) {
        [self.newsSubjectDelegate newsSubjectViewDidEndChange:self withDirection:ME_NEWS_SUBJECT_PREVIOUS];
    }
}

- (void) toolBarSafari:(id)sender{
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectView:shouldStartLoadWithRequest:navigationType:)]) {
        NSString *url = [NSString stringWithFormat:@"%@/n%@",ME_NEWS_BASEURL,currentNewsItem.newsId];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        [self.newsSubjectDelegate newsSubjectView:self shouldStartLoadWithRequest:request navigationType:UIWebViewNavigationTypeLinkClicked];
    }
}

-(void) setToolbarHidden:(BOOL)hidden animated:(BOOL) animated{
    dispatch_async(dispatch_get_main_queue(), ^{        
        
        if (hidden)
            self->toolBarPositionConstraint.constant=self.toolBar.bounds.size.height;
        else
            self->toolBarPositionConstraint.constant=0.0;
        
        if (self.toolBar.hidden && !hidden){
            self.toolBar.alpha = 0.0;
            self.toolBar.hidden = NO;
        }
        
        [self setNeedsUpdateConstraints];
        if (animated) {
            
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                self.toolBar.alpha = hidden?0.0:1.;
                [self layoutIfNeeded];
            } completion:^(BOOL f){
                if (hidden && !self.toolBar.hidden)
                    self.toolBar.hidden = YES;
            }];
            
        }else{
            [self layoutIfNeeded];
            self.toolBar.alpha = hidden?0.0:1.;
            self.toolBar.hidden = hidden;
        }
    });
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self config];
    }
    return self;
}

- (NSString*) renderHtml:(MENewsItem*)aNews{
    if (aNews && aNews.content) {
        NSString *htmlString = [NSMutableString stringWithString:aNews.content];
        if (aNews.publicTime) {
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<!--updatetime-->"
                                                               withString:[NSDateFormatter localizedStringFromDate:aNews.publicTime
                                                                                                         dateStyle:NSDateFormatterNoStyle
                                                                                                         timeStyle:NSDateFormatterMediumStyle]];
        }
        return htmlString;
    }
    return nil;
}

- (void) setCloseNewsTitle:(MENewsCloseTitleView*)titleView forItem:(MENewsItem*)item{
    if (item) {
        titleView.captionLabel.text = item.caption;
        titleView.shortCutLabel.text = item.shortcut;
        titleView.timeLabel.text = [NSDateFormatter localizedStringFromDate:item.publicTime dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
        if (item.isRead)
            titleView.captionLabel.textColor = ME_UI_GREY_FONT_COLOR;
        else
            titleView.captionLabel.textColor = [UIColor blackColor];
    }else{
        titleView.captionLabel.textColor = ME_UI_GREY_FONT_COLOR;
        titleView.captionLabel.text = __ME_NEWS_SUBJECT_LAST_NEWS_TITLE;
        titleView.shortCutLabel.text = @"";
        titleView.timeLabel.text = @"";
    }
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [webView stopLoading];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_NETWORK_LOST];
    });
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectView:shouldStartLoadWithRequest:navigationType:)]) {
        return [self.newsSubjectDelegate newsSubjectView:self shouldStartLoadWithRequest:request navigationType:navigationType];
    }
    return YES;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    self.scrollView.scrollEnabled = YES;
    
    if (currentNewsItem.next)
        nextBarItem.enabled = YES;
    
    if (currentNewsItem.previous)
        previousBarItem.enabled = YES;
    
    safariBarItem.enabled = YES;
    
    [self setCloseNewsTitle:previousItem forItem:currentNewsItem.previous];
    [self setCloseNewsTitle:nextItem forItem:currentNewsItem.next];
    [self layoutSubviews];
    
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectViewDidFinishLoad:)]) {
        [self.newsSubjectDelegate newsSubjectViewDidFinishLoad:self];
    }
    
    [self setToolbarHidden:NO animated:YES];
}

- (void) setNewsItem:(MENewsItem *)newsItem{
    
    currentNewsItem=newsItem;
    self.scrollView.scrollEnabled = NO;
    NSString *html = [self renderHtml:newsItem];
    if (html)
        [self loadHTMLString:html baseURL:nil];
}


- (void) scaleImage:(UIImageView*)image withScale:(CGFloat) percentage{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    image.layer.transform = CATransform3DMakeScale(percentage, percentage, 1);
    [CATransaction commit];
}

- (void) rotateImageView:(UIImageView*)imageView withDirection:(int)direction
{
    [imageView.layer removeAllAnimations];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: direction * M_PI * 2.0 /* full rotation*/ * 2. * ME_UI_ANIMATION_DURATION ];
    rotationAnimation.duration = ME_UI_ANIMATION_DURATION;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    [imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat max_distance = self.closeNewsTitleHeight+self.closeNewsTitleOffset;
    CGPoint offset = scrollView.contentOffset;
    CGFloat delta=offset.y-self.scrollView.contentSize.height+self.bounds.size.height;
    CGFloat bottomDelta=0.;
    
    if (offset.y<0) {
        delta = ABS(offset.y);
        if (ABS(offset.y)>=self.closeNewsTitleHeight)
            previousItem.frame = CGRectMake(0, -ABS(offset.y), self.bounds.size.width,  ABS(offset.y));
        else{
            previousItem.frame = CGRectMake(0, -self.closeNewsTitleHeight, self.bounds.size.width,  self.closeNewsTitleHeight);
            delta=0.0;
        }
        
        CGFloat percentage = MIN(1, (delta/max_distance));
        
        if (percentage==1 && switchDirection==ME_NEWS_SUBJECT_UNDEFINED) {
            [self rotateImageView:previousItem.directionImageView withDirection:-1];
            
            if (currentNewsItem.previous)
                switchDirection = ME_NEWS_SUBJECT_PREVIOUS;
            else
                switchDirection = ME_NEWS_SUBJECT_UNDEFINED;
        }
        else if(percentage==0 && switchDirection==ME_NEWS_SUBJECT_PREVIOUS){
            [self rotateImageView:previousItem.directionImageView withDirection:1];
            switchDirection = ME_NEWS_SUBJECT_UNDEFINED;
        }
    }
    else if (offset.y>0){
        bottomDelta = delta=offset.y-self.scrollView.contentSize.height+self.bounds.size.height;
        
        if (delta>=self.closeNewsTitleHeight){
            nextItem.frame = CGRectMake(0, self.scrollView.contentSize.height, self.bounds.size.width,  delta);
        }
        else{
            nextItem.frame = CGRectMake(0, self.scrollView.contentSize.height, self.bounds.size.width,  self.closeNewsTitleHeight);
            delta=0.0;
        }
        CGFloat percentage = MIN(1, (delta/max_distance));
        if (percentage==1 && switchDirection==ME_NEWS_SUBJECT_UNDEFINED) {
            [self rotateImageView:nextItem.directionImageView withDirection:-1];
            if (currentNewsItem.next)
                switchDirection = ME_NEWS_SUBJECT_NEXT;
            else
                switchDirection = ME_NEWS_SUBJECT_UNDEFINED;
        }
        else if(percentage==0 && switchDirection==ME_NEWS_SUBJECT_NEXT){
            [self rotateImageView:nextItem.directionImageView withDirection:1];
            switchDirection = ME_NEWS_SUBJECT_UNDEFINED;
        }
    }
    
    CGFloat direction = (lastOffset.y-offset.y);
    if (offset.y>0) {
        if (direction<0) {
            if (toolBarPositionConstraint.constant==0.)
                [self setToolbarHidden:YES animated:YES];
        }
        else if (direction>0 && bottomDelta<0){
            if (self.toolBar.hidden)
                [self setToolbarHidden:NO animated:YES];
        }
    } else if (offset.y<0){
        if (self.toolBar.hidden)
            [self setToolbarHidden:NO animated:YES];
    }
    
    lastOffset = self.scrollView.contentOffset;
}

//- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
//    CGFloat delta=scrollView.contentOffset.y;
//
//    if (delta>0) {
//        delta=scrollView.contentOffset.y-self.scrollView.contentSize.height+self.bounds.size.height;
//    }
//    else{
//        delta+=self.closeNewsTitleHeight;
//    }
//    if (velocity.y<-self.slideVelocity && delta>0 && currentNewsItem.previous) {
//        switchDirection = ME_NEWS_SUBJECT_PREVIOUS;
//    }
//    else if (velocity.y>self.slideVelocity && delta>0 && currentNewsItem.next){
//        switchDirection = ME_NEWS_SUBJECT_NEXT;
//    }
//}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectViewWillBeginChange:)]) {
        [self.newsSubjectDelegate newsSubjectViewWillBeginChange:self];
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (switchDirection == ME_NEWS_SUBJECT_PREVIOUS) {
        self.scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom, self.scrollView.contentInset.right);
    }
    else if (switchDirection == ME_NEWS_SUBJECT_NEXT){
        [self setToolbarHidden:YES animated:YES];
        self.scrollView.contentInset = UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, scrollView.contentOffset.y, self.scrollView.contentInset.right);
    }
    
    if (self.newsSubjectDelegate && [self.newsSubjectDelegate respondsToSelector:@selector(newsSubjectViewDidEndChange:withDirection:)]) {
        [self.newsSubjectDelegate newsSubjectViewDidEndChange:self withDirection:switchDirection];
    }
}

@end
