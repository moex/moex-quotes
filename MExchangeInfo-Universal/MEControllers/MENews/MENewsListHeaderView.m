//
//  MENewsListHeaderView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsListHeaderView.h"
#import "MEUIPreferences.h"

@implementation MENewsListHeaderView

- (id) initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
                
        static UIImage *_bg = nil; if(!_bg) _bg = [UIImage imageNamed:@"light_grey_panel"];
        
        self.backgroundView = [[UIImageView alloc] initWithImage:_bg];
        
        self.backgroundView.layer.shouldRasterize = YES;
        self.backgroundView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.captionLabel = [[UILabel alloc] initWithFrame: self.contentView.bounds];
        
        self.captionLabel.textAlignment = NSTextAlignmentCenter;
        self.captionLabel.backgroundColor = [UIColor clearColor];
        self.captionLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.captionLabel.font = [UIFont fontWithName:ME_UI_FONT size:13];
        self.captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.captionLabel.layer.shouldRasterize = YES;
        self.captionLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        [self.contentView addSubview:self.captionLabel];
        
        [self.contentView addConstraint:[NSLayoutConstraint 
                                         constraintWithItem:self.captionLabel 
                                         attribute:NSLayoutAttributeCenterX 
                                         relatedBy:NSLayoutRelationEqual 
                                         toItem:self.contentView 
                                         attribute:NSLayoutAttributeCenterX 
                                         multiplier:1 
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint 
                                         constraintWithItem:self.captionLabel 
                                         attribute:NSLayoutAttributeCenterY 
                                         relatedBy:NSLayoutRelationEqual 
                                         toItem:self.contentView 
                                         attribute:NSLayoutAttributeCenterY 
                                         multiplier:1 
                                         constant:0]];
    }
    
    return self;
}

@end
