//
//  MENewsWebView.h
//  MExchange
//
//  Created by denn on 8/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MENewsContentView : UIWebView
@end
