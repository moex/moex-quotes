//
//  MENewsWebView.m
//  MExchange
//
//  Created by denn on 8/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MENewsContentView.h"


@interface MENewsContentView()
@end

@implementation MENewsContentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self pref];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self pref];
    }
    return self;
}

- (void) pref{

    [self hideBackgroundShadow:YES];
    
    self.scalesPageToFit = YES;
    self.scrollView.multipleTouchEnabled = NO;
    self.userInteractionEnabled = YES;
    
    self.scrollView.showsVerticalScrollIndicator=YES;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    
    self.scrollView.bounces = YES;
    self.scrollView.alwaysBounceHorizontal = YES;
    self.scrollView.scrollEnabled = NO;
}


- (void) hideBackgroundShadow: (BOOL) isHidden{
    if ([[self subviews] count] > 0) {
        // hide the shadows
        for (UIView* shadowView in [[[self subviews] objectAtIndex:0] subviews]) {
            [shadowView setHidden:isHidden];
        }
        // show the content
        [[[[[self subviews] objectAtIndex:0] subviews] lastObject] setHidden:NO];
    }
    if (isHidden) {
        self.backgroundColor = [UIColor whiteColor];        
    }
}


@end
