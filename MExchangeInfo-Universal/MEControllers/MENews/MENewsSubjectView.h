//
//  MENewsSubjectVeiw.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/21/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MENewsModel.h"

typedef enum {
    ME_NEWS_SUBJECT_PREVIOUS,
    ME_NEWS_SUBJECT_NEXT,
    ME_NEWS_SUBJECT_UNDEFINED,
} MENewsSubjectSwitchDirection;



@interface MENewsSubjectToolBar : UIToolbar
@end


@class MENewsSubjectView;

@protocol MENewsSubjectScrollProtocol <NSObject>
- (BOOL) newsSubjectView:(MENewsSubjectView *)subjectView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;

@optional
- (void) newsSubjectViewDidFinishLoad:(MENewsSubjectView *)subjectView;
- (void) newsSubjectViewWillBeginChange:(MENewsSubjectView *)subjectView;
- (void) newsSubjectViewDidEndChange:(MENewsSubjectView *)subjectView withDirection:(MENewsSubjectSwitchDirection)direction;
@end

@interface MENewsSubjectView : UIWebView
@property(strong,readonly,nonatomic) MENewsSubjectToolBar *toolBar;
@property(strong,nonatomic) MENewsItem *newsItem;
@property(assign,nonatomic) CGFloat closeNewsTitleOffset;
@property(assign,nonatomic) CGFloat closeNewsTitleHeight;
@property(assign,nonatomic) CGFloat slideVelocity;
@property(nonatomic,strong) id<MENewsSubjectScrollProtocol> newsSubjectDelegate;
@end
