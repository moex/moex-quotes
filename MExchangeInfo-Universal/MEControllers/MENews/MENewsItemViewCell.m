//
//  MENewsItemViewCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsItemViewCell.h"
#import "MEUIPreferences.h"

@implementation MENewsItemViewCell


- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
        
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        self.captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 10)];
        self.shortCutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 10)];
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.shortCutLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.captionLabel.textColor = [UIColor blackColor];
        self.captionLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:16];
        //
        //  it turned the very expencive operation
        //
        //  self.captionLabel.minimumScaleFactor=0.9;
        //  self.captionLabel.adjustsFontSizeToFitWidth = YES;
        self.captionLabel.numberOfLines = 2;
        self.captionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        self.captionLabel.layer.shouldRasterize = YES;
        self.captionLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;

        self.shortCutLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.shortCutLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        //self.shortCutLabel.minimumScaleFactor=0.8;
        //self.shortCutLabel.adjustsFontSizeToFitWidth = YES;
        self.shortCutLabel.numberOfLines = 3;
        self.shortCutLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        //
        // to improve performance
        //
        self.shortCutLabel.layer.shouldRasterize = YES;
        self.shortCutLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.shortCutLabel.text = @"";
        
        self.timeLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.timeLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        
        [self.contentView addSubview:self.captionLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.shortCutLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[caption]-5-[time(70)]-0-|" options:0 metrics:nil views:@{@"caption":self.captionLabel, @"time":self.timeLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[shortCut]-0-|" options:0 metrics:nil views:@{@"shortCut":self.shortCutLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[caption]-2-[shortCut]-5-|" options:0 metrics:nil views:@{@"caption":self.captionLabel, @"shortCut": self.shortCutLabel}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.timeLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.captionLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;

        [self addDevider];
    }
    return self;
}

@end
