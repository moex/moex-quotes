    //
    //  MENewsSubjectViewController.m
    //  MExchangeInfo-Universal
    //
    //  Created by denis svinarchuk on 1/20/14.
    //  Copyright (c) 2014 Moscow Exchange. All rights reserved.
    //

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "MENewsSubjectViewController.h"
#import "MEUIPreferences.h"
#import "MEAlertsView.h"

#import <Crashlytics/Crashlytics.h>

@interface MENewsSubjectViewController () <MENewsItemDelegate, UIWebViewDelegate, MENewsSubjectScrollProtocol>
@property (strong, nonatomic)  MENewsSubjectView *newsSubjectView;
@property (nonatomic,readonly) UIActivityIndicatorView *subjectActivity;
@property (atomic,weak) MENewsItem  *currentNewsItem;
@property (nonatomic,weak)     MENewsModel *newsModel;
@end

@implementation MENewsSubjectViewController
{
    MENewsSubjectSwitchDirection lastDirection;
    
    NSLayoutConstraint *topConstraint;
    NSLayoutConstraint *bottomConstraint;
    BOOL isTheFirstUpdate;
    BOOL isAppeared;
}

@synthesize subjectActivity=_subjectActivity;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
            // Custom initialization
    }
    return self;
}

- (UIActivityIndicatorView*) subjectActivity{
    if (_subjectActivity == nil) {
        _subjectActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _subjectActivity.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        _subjectActivity.tintColor = [UIColor grayColor];
        [self.view addSubview:_subjectActivity];
    }
    
    _subjectActivity.frame = self.view.bounds;
    
    [_subjectActivity bringSubviewToFront:self.view];
    return _subjectActivity;
}


- (void) showActivity{
    [self.subjectActivity startAnimating];
}

- (void) hideActivity{
    [self.subjectActivity stopAnimating];
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isTheFirstUpdate = YES;
    isAppeared=YES;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showActivity];
    
    [Answers logContentViewWithName:@"News Content View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{}];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    isAppeared = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.newsModel = [MENewsModel sharedInstance];
    self.newsModel.itemDelegate = self;
    
    lastDirection = ME_NEWS_SUBJECT_UNDEFINED;
        // Do any additional setup after loading the view.
}

- (void) addNewsSubjectViewFromDirection:(MENewsSubjectSwitchDirection)direction{
    NSNumber *topoffset = [NSNumber numberWithInt:0];
    
    MENewsSubjectView *currentSubjectView = [[MENewsSubjectView alloc] initWithFrame:self.view.bounds];
    
    currentSubjectView.newsSubjectDelegate = self;
    currentSubjectView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *c_topConstraint = [NSLayoutConstraint constraintWithItem:currentSubjectView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1 constant:topoffset.floatValue];
    NSLayoutConstraint *c_bottomConstraint = [NSLayoutConstraint constraintWithItem:currentSubjectView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    if (!self.newsSubjectView || direction==ME_NEWS_SUBJECT_UNDEFINED) {
        [self.view addSubview:currentSubjectView];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[web]-0-|" options:0 metrics:nil views:@{@"web": currentSubjectView}]];
        
        if (self.newsSubjectView) {
            [self.newsSubjectView removeFromSuperview];
            [self.view removeConstraint:topConstraint];
            [self.view removeConstraint:bottomConstraint];
        }
        
        self.newsSubjectView = currentSubjectView;
        topConstraint = c_topConstraint;
        bottomConstraint = c_bottomConstraint;
        
        [self.view addConstraint:c_topConstraint];
        [self.view addConstraint:c_bottomConstraint];
        
        return;
    }
    
    if (direction == ME_NEWS_SUBJECT_PREVIOUS) {
        bottomConstraint.constant = self.view.bounds.size.height;
        topConstraint.constant = self.view.bounds.size.height;
    }
    else{
        topConstraint.constant = -self.view.bounds.size.height;
        bottomConstraint.constant = -self.view.bounds.size.height;
    }
    
    UIView *oldVew = self.newsSubjectView;
    NSLayoutConstraint *old_topConstraint = topConstraint;
    NSLayoutConstraint *old_bottomConstraint = bottomConstraint;
    
    [self.view insertSubview:currentSubjectView belowSubview:self.newsSubjectView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[web]-0-|" options:0 metrics:nil views:@{@"web": currentSubjectView}]];
    [self.view addConstraint:c_topConstraint];
    [self.view addConstraint:c_bottomConstraint];
    
    topConstraint = c_topConstraint;
    bottomConstraint = c_bottomConstraint;
    
    currentSubjectView.alpha = 0.0;
    self.newsSubjectView = currentSubjectView;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION
                         animations:^{
            oldVew.alpha = 0.6;
            self.newsSubjectView.alpha = 1.0;
            [self.view layoutIfNeeded];
        }
                         completion:^(BOOL f){
            self->lastDirection = ME_NEWS_SUBJECT_UNDEFINED;
            [self.view removeConstraint:old_topConstraint];
            [self.view removeConstraint:old_bottomConstraint];
            [oldVew removeFromSuperview];
        }
         ];
    });
}

- (void) setNewsItem:(MENewsItem *)newsItem{
    
    if (newsItem==nil || [_newsItem.newsId isEqualToNumber:newsItem.newsId])
        return;
    
    _newsItem = newsItem;
    [self startLoad];
    [self.newsModel requestContentByNewsId:_newsItem.newsId];
}

#pragma mark - MENewsItemDelegate

- (void) didNewsItemError:(NSError *)error{
    [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_NETWORK_LOST];
}

- (void) didNewsItemUpdate:(MENewsItem *)news{
    
    if (news && news.newsId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self->isTheFirstUpdate = NO;
            
            if ([self.currentNewsItem.newsId isEqualToNumber:self->_newsItem.newsId]){
                [self stopLoad];
                    //return ;
            }
            
            self.currentNewsItem = self->_newsItem;
            
            if (self.currentNewsItem) {
                NSString *caption = self.currentNewsItem.newsSection.caption;
                
                [self addNewsSubjectViewFromDirection:self->lastDirection];
                self.newsSubjectView.newsItem = self.currentNewsItem;
                
                self.navigationItem.title = caption;
                
                if (self.currentNewsItem.isRead.boolValue==NO) {
                    self.currentNewsItem.isRead = @YES;
                    [self.currentNewsItem save];
                }
                
                if (self.updateDelegate && [self.updateDelegate respondsToSelector:@selector(newsSubjectDidUpdateItem:)]) {
                    [self.updateDelegate newsSubjectDidUpdateItem:self.currentNewsItem];
                }
            }
        });
    }
}


#pragma mark - MENewsSubjectScrollProtocol

- (void) startLoad{
    if (isTheFirstUpdate) {
        return;
    }
    [self showActivity];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self.newsSubjectView.alpha = 0.2;
        }];
    });
}

- (void) stopLoad{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self.newsSubjectView.alpha = 1.;
        } completion:^(BOOL finished) {
            [self hideActivity];
        }];
    });
}

- (void) newsSubjectViewDidFinishLoad:(MENewsSubjectView *)subjectView{
    [self stopLoad];
    [self hideActivity];
}

- (void) newsSubjectViewDidEndChange:(MENewsSubjectView *)subjectView withDirection:(MENewsSubjectSwitchDirection)direction{
    
    lastDirection = direction;
    switch (direction) {
        case ME_NEWS_SUBJECT_PREVIOUS:
            if (self.currentNewsItem.previous)
                self.newsItem = self.currentNewsItem.previous;
            break;
        case ME_NEWS_SUBJECT_NEXT:
            if (self.currentNewsItem.next)
                self.self.newsItem = self.currentNewsItem.next;
            break;
        default:
            return;
    }
    
    if (self.updateDelegate && [self.updateDelegate respondsToSelector:@selector(newsSubjectViewDidEndChange:withDirection:)]) {
        [self.updateDelegate newsSubjectViewDidEndChange:self.newsItem withDirection:direction];
    }
}

- (BOOL) newsSubjectView:(MENewsSubjectView *)subjectView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL: request.URL];
        return NO;
    }
    return YES;
}


#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

@end
