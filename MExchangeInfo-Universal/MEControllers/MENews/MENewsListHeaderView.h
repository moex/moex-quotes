//
//  MENewsListHeaderView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ME_NEWSLIST_HEADER @"ME_NEWSLIST_HEADER"

@interface MENewsListHeaderView : UITableViewHeaderFooterView
@property(nonatomic,strong) UILabel *captionLabel;
@end
