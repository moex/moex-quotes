//
//  MENewsItemViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEBaseViewCell.h"

#define ME_NEWSLIST_ITEM_CELL @"ME_NEWSLIST_ITEM_CELL"

@interface MENewsItemViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel *captionLabel;
@property(nonatomic,strong) UILabel *shortCutLabel;
@property(nonatomic,strong) UILabel *timeLabel;
@end
