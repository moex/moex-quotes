//
//  METickerLevel2Table.h
//  MExchange
//
//  Created by denn on 5/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METableView.h"
#import "MELevel2.h"
#import "METicker.h"
#import "METickerInfoProtocol.h"

@interface MELevel2TableView : METableView
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,strong) id<METickerInfoProtocol> infoDelegate;
@property (readonly) METicker* ticker;
- (void) scrollToSplitPosition;
- (void) update;
@end
