//
//  MEWatchListViewController.m
//  MExchangeInfo-Universal
//
//  Created by denn on 28.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEWatchListViewController.h"
#import "MEDispatcher.h"
#import "MEWatchListModel.h"
#import "MESearchListModel.h"
#import "MESearchBarViewController.h"
#import "MEUIPreferences.h"
#import "MEWatchListSectionHeaderView.h"
#import "MEWatchListViewCell.h"
#import "MEAlertsView.h"
#import "NSAttributedString+MEFormatedString.h"
#import "METurnoversModel.h"
#import "MESettings.h"
#import "MEActionSheet.h"
#import "MEChartViewController.h"
#import "MESearchViewCell.h"
#import "METickerCardDetailController.h"
#import "METPresentAnimationController.h"
#import "METDismissAnimationController.h"
#import "MEConfig.h"
#import <TSMessage.h>

#import <Crashlytics/Crashlytics.h>

static NSString *CellIdentifier = ME_WATCHLIST_MARKET_CELL;
static NSString *CellWideIdentifier = ME_WATCHLIST_MARKET_CELL_WIDE;
static NSString *HeaderIdentifier = ME_WATCHLIST_MARKET_HEADER;
static NSString *HeaderWideIdentifier = ME_WATCHLIST_MARKET_HEADER_WIDE;

@interface MEWatchListViewController () <UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate>
@property (nonatomic,weak)    MEWatchListModel  *watchList;

@property (nonatomic,strong)  MESearchBarViewController *searchBarController;
@property (nonatomic,strong)  MEActionSheet *rowActionSheet;
@property (nonatomic,strong)  MEChartViewController *chartViewController;
@property (nonatomic,strong)  METickerInfoViewController *currentTickerInfo;

@property (nonatomic,assign,readonly) BOOL isLockedForReload;
@property (nonatomic,assign) BOOL isActivityShown;
@property (nonatomic,assign) BOOL tickerIsChanging;

@property (nonatomic,readonly) UIActivityIndicatorView *searchActivity;
@property (nonatomic,strong)   NSIndexPath *lastSelectedIndexPath;
@property (nonatomic,strong)   UIActivityIndicatorView *connectingActivity;
@property (nonatomic,strong)   UIAlertView   *alertView;
@property (nonatomic,strong)   UIAlertView   *alertReferencesView;
@property (nonatomic,strong)   UIView *watchListContainerView;

@property (nonatomic,strong)   UIButton              *tickerCardButton;

@property (nonatomic,strong) METickerCardDetailController *tickerCardsViewController;

- (void) showActivity;
- (void) hideActivity;

@end

@interface MEWatchListViewController () <MEWatchListModelProtocol, MESearchListModelProtocol, MESearchBarControllerProtocol, MEActionSheetDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    MESearchListModel *searchList;
    
    MEDispatcher  *dispatcher;
    
    BOOL isSearchBarShowing, isSearchBarOpened, isRotating;
    BOOL isAppeared;
    BOOL isNotAlertShown;
    BOOL isNotAlertReferencesShown;
    BOOL isCurrentellyUpdated;
    BOOL isEditingRow;
    BOOL isDemoShown;
    
    UIInterfaceOrientation lastOrientation;
    NSMutableDictionary *cachedHeaderHeaders;
    
    UILongPressGestureRecognizer *longPress;
    
}
@end

@implementation MEWatchListViewController
{
    UIEdgeInsets originalInsets;
}

@synthesize searchActivity=_searchActivity, lastSelectedIndexPath=_lastSelectedIndexPath;

- (UIButton*) tickerCardButton{
    
    if (_tickerCardButton) {
        return _tickerCardButton;
    }
    
    _tickerCardButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    UIImage *image = [UIImage imageNamed:@"button_ticker_info"];
    [_tickerCardButton setImage: image forState:UIControlStateNormal];
    [_tickerCardButton setAdjustsImageWhenHighlighted:YES];
    [_tickerCardButton setAdjustsImageWhenDisabled:NO];
    [_tickerCardButton setShowsTouchWhenHighlighted: YES];
    
    [_tickerCardButton addTarget:self action:@selector(tickerCardButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    return _tickerCardButton;
};

- (METickerCardDetailController*) tickerCardsViewController{
    if (!_tickerCardsViewController) {
        if (UIUserInterfaceIdiomPhone==UI_USER_INTERFACE_IDIOM())
            _tickerCardsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tickerCardItemsCID"];
        else{
            _tickerCardsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tickerCardItemsNID"];
        }
    }
    return _tickerCardsViewController;
}

- (void) tickerCardDoneHandler:(id)sender{
    [self.tickerCardsViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) tickerCardButtonHandler:(UIButton*)sender{
    [self tickerCardPushController:nil];
}

- (void) tickerCardPushController:(NSNotification*)event{
    if (UIUserInterfaceIdiomPhone==UI_USER_INTERFACE_IDIOM()) {
        if (self.navigationController.topViewController==self.tickerCardsViewController) {
            [self.navigationController popToViewController:self animated:YES];
        }
        else
            [self.navigationController pushViewController:self.tickerCardsViewController animated:YES];        
    }
    else{
        self.tickerCardsViewController.modalPresentationStyle = UIModalPresentationCustom;
        self.tickerCardsViewController.transitioningDelegate = self;
        [self presentViewController:self.tickerCardsViewController animated:YES completion:nil];
    }
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    return [[METPresentAnimationController alloc] init];
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [[METDismissAnimationController alloc] init];
}


- (UIAlertView*) alertView{
    //
    //
    // TODO: make common class to write alert and log to alert logger...
    //
    if (!_alertView){
        _alertView = [[UIAlertView alloc]
                       initWithTitle:NSLocalizedString(@"User demo alert title", nil)
                       message:nil
                       delegate:self
                       cancelButtonTitle:NSLocalizedString(@"Cancel button", nil)
                       otherButtonTitles:NSLocalizedString(@"Do not show again button", nil), nil];
        _alertView.cancelButtonIndex=0;
    }
    
    return _alertView;
}

//- (UIAlertView*) alertSearchEmptyView {
//    //
//    if (!_alertSearchEmptyView){
//        _alertSearchEmptyView = [[UIAlertView alloc]
//                      initWithTitle:NSLocalizedString(@"Search result", nil)
//                      message:NSLocalizedString(@"No pattern-like securities found", nil)
//                      delegate:self
//                      cancelButtonTitle:NSLocalizedString(@"Cancel button", nil)
//                      otherButtonTitles:nil];
//        _alertSearchEmptyView.cancelButtonIndex=0;
//    }
//    
//    return _alertSearchEmptyView;
//}

- (UIAlertView*) alertReferencesView{
    if (!_alertReferencesView){
        _alertReferencesView = [[UIAlertView alloc]
                      initWithTitle:NSLocalizedString(@"Markets reference update error", nil)
                      message:nil
                      delegate:self
                      cancelButtonTitle:NSLocalizedString(@"Cancel button", nil)
                      otherButtonTitles:NSLocalizedString(@"Do not show again button", nil), nil];
        _alertReferencesView.cancelButtonIndex=0;
    }
    
    return _alertReferencesView;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView==self.alertView) {
        if (buttonIndex==1) {
            //
            // Do not show alert
            //
            isNotAlertShown = YES;
        }
    }
    else if (alertView==self.alertReferencesView){
        if (buttonIndex==1) {
            //
            // Do not show alert
            //
            isNotAlertReferencesShown = YES;
        }
    }
}

- (UIActivityIndicatorView*) connectingActivity{
    if (_connectingActivity == nil) {
        _connectingActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _connectingActivity.autoresizesSubviews = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _connectingActivity.color = [UIColor grayColor];
        [self.view addSubview:_connectingActivity];
    }
    
    [_connectingActivity bringSubviewToFront:_connectingActivity.superview];
    _connectingActivity.frame = self.tableView.bounds;
    _connectingActivity.center = self.tableView.center;
    
    return _connectingActivity;
}

- (void) setIsActivityShown:(BOOL)isActivityShown{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self->_isActivityShown==isActivityShown) {
            return;
        }
        
        self->_isActivityShown = isActivityShown;
        
        if (self->_isActivityShown) {
            [self showActivity];
        }
        else
            [self hideActivity];
        
    });
}

- (void) showActivity{
    [self.connectingActivity startAnimating];
}

- (void) hideActivity{
    [self.connectingActivity stopAnimating];
}

- (BOOL) isLockedForReload{
    return self.tableView.isDecelerating || self.tableView.isDragging || self.tableView.isEditing || isEditingRow || isRotating;
}

#pragma mark - Action sheet

- (MEActionSheet*) rowActionSheet{
    if (_rowActionSheet) {
        return _rowActionSheet;
    }
    _rowActionSheet = [[MEActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel button", @"")
                                              buttonTitles:@[NSLocalizedString(@"Ticker info", @""), NSLocalizedString(@"Chart", @"")]];
    return _rowActionSheet;
}

- (void) actionSheet:(MEActionSheet *)acthionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [self tableView: self.tableView didSelectRowAtIndexPath:self.lastSelectedIndexPath];
    }
}

- (MEChartViewController*) chartViewController{
    if (!_chartViewController) {
        _chartViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"chartViewControllerID"];
        _chartViewController.parentController = self;
        _chartViewController.chartView.titleView.textLabel.text = [self.watchList tickerForIndexPath:self.lastSelectedIndexPath].hint;
    }
    return _chartViewController;
}


- (METickerInfoViewController*) currentTickerInfo{
    
    if (!_currentTickerInfo) {
        _currentTickerInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"watchListDetailViewID"];
    }
    return _currentTickerInfo;
}

#pragma mark - Activity

- (UIActivityIndicatorView*) searchActivity{
    if (_searchActivity == nil) {
        _searchActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _searchActivity.autoresizesSubviews = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.searchBarController.searchTableView addSubview:_searchActivity];
    }
    
    CGRect rect = self.searchBarController.searchTableView.bounds;
    rect.origin.y = 0;
    rect.size.height = 150;
    _searchActivity.frame = rect;
    
    [_searchActivity bringSubviewToFront:_searchActivity.superview];
    return _searchActivity;
}

- (void) showSearchActivity{
    [self.searchActivity startAnimating];
}

- (void) hideSearchActivity{
    [self.searchActivity stopAnimating];
}

- (NSIndexPath*) lastSelectedIndexPath{
    
    if (_lastSelectedIndexPath) {
        return _lastSelectedIndexPath;
    }
    
    _lastSelectedIndexPath = ((METableView*)self.tableView).lastSelectedIndexPath;
    
    return _lastSelectedIndexPath;
}

- (void) setLastSelectedIndexPath:(NSIndexPath *)lastSelectedIndexPath{
    _lastSelectedIndexPath = lastSelectedIndexPath;
    ((METableView*)self.tableView).lastSelectedIndexPath = lastSelectedIndexPath;
}

- (void) viewWillAppear:(BOOL)animated{
    
    if (    lastOrientation == UIInterfaceOrientationUnknown )
        lastOrientation = [UIApplication sharedApplication].statusBarOrientation;

    [super viewWillAppear:animated];
    
    [self rotationLayoutIfNeed];
    lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tickerIsChanging  = NO;

    if (!self.alertView.isVisible && isNotAlertShown==NO) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (!self->dispatcher.config.isAuthorized){
                [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"User not authorized mode", @"") withStatus:ME_ALERT_WARN];
            }
            else if ([self->dispatcher.config.login.domain isEqualToString:GIC_GUEST_DOMAIN]) {
                if (!self->isDemoShown){
                    [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"User demo mode", @"") withStatus:ME_ALERT_WARN_NEED_SHOW];
                    self->isDemoShown = YES;
                }
            }
        });
    }
    
    [self.watchList subscribe];
    isAppeared = YES;    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (self.lastSelectedIndexPath)
            [self tableView: self.tableView didSelectRowAtIndexPath:self.lastSelectedIndexPath];
    }    
    else{
        self.watchList.activeTicker = [self.watchList tickerForIndexPath:self.lastSelectedIndexPath];
    }
    
    [Answers logContentViewWithName:@"Watch List View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{}];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    isAppeared = NO;
    
    [self.alertView dismissWithClickedButtonIndex:0 animated:YES];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.watchList unsubscribe];
    }
    else if (!self.currentTickerInfo.isAppeared) {
        [self.watchList unsubscribe];
    }    
}

- (MESearchBarViewController*) searchBarController{
    if (!_searchBarController) {
        _searchBarController = [[MESearchBarViewController alloc] init];
        _searchBarController.tableView = self.tableView;
        _searchBarController.delegate = self;
        _searchBarController.searchTableView.dataSource = self;
        _searchBarController.searchTableView.delegate = self;
    }
    
    return _searchBarController;
}

- (void)loginSuccessed:(NSNotification*)event{
    [self.watchList subscribe];
}

- (void)viewDidLoad
{
    
    lastOrientation = UIInterfaceOrientationUnknown;
    
    [super viewDidLoad];
    
    isDemoShown = NO;
    
    self.detailController = self.currentTickerInfo;
    
    dispatcher = [MEDispatcher sharedInstance];
    
    self.watchList = [MEWatchListModel sharedInstance];
    self.watchList.delegate = self;
    
    cachedHeaderHeaders  = [[NSMutableDictionary dictionary] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.restorationIdentifier = @"ME_WATCH_LIST_RESTORATION_ID";
    
    [self.tableView registerClass:[MESearchViewCell class] forCellReuseIdentifier:CellIdentifierSearch];
    [self.tableView registerClass:[MEWatchListViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self.tableView registerClass:[MEWatchListViewWideCell class] forCellReuseIdentifier:CellWideIdentifier];
    [self.tableView registerClass:[MEWatchListSectionHeaderView class] forHeaderFooterViewReuseIdentifier:HeaderWideIdentifier];
    [self.tableView registerClass:[MEWatchListSectionHeaderView class] forHeaderFooterViewReuseIdentifier:HeaderIdentifier];
    
    self.tableView.tableHeaderView = self.searchBarController.view;
    self.tableView.autoresizesSubviews = YES;
    self.tableView.clipsToBounds = YES;
    [self.tableView setContentMode:UIViewContentModeScaleAspectFit];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    originalInsets = self.tableView.contentInset;    
    searchList = [[MESearchListModel alloc] init];
    searchList.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    
    longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    [self.tableView addGestureRecognizer:longPress];    
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:self.tickerCardButton]];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tickerCardPushController:) name:ME_TICKER_CARD_SHOW_NOTIFICATION object:nil];
}

- (void) longPressAction:(UILongPressGestureRecognizer*)gesture{
    
    CGPoint pressPont = [gesture locationInView:self.tableView];
    NSIndexPath *pressIndexPath = [self.tableView indexPathForRowAtPoint:pressPont];
    
    if (self.tableView.isEditing)
        return;
    
    if (pressIndexPath) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            [self tableView: self.tableView didSelectRowAtIndexPath:pressIndexPath];
        else
            self.lastSelectedIndexPath = pressIndexPath;
    }
    
    self.watchList.activeTicker = [self.watchList tickerForIndexPath:self.lastSelectedIndexPath];
}

- (void) rotationLayoutIfNeed{   
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation != lastOrientation) {
        [cachedHeaderHeaders removeAllObjects];
        [UIView transitionWithView:self.tableView duration:ME_UI_ANIMATION_DURATION / 4.0
                           options:UIViewAnimationOptionTransitionCrossDissolve 
                        animations:^{
                            [self deferredReloadData];
                            [self.tableView layoutIfNeeded];
                        } 
                        completion:nil];
    }    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation  = [[UIApplication sharedApplication] statusBarOrientation];
    
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;

    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];

    //[self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self _willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self _didRotateFromInterfaceOrientation:fromOrientation];
    }];

}

- (void) _willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    isRotating = YES;
    //[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    _connectingActivity.center= self.view.center;
    _connectingActivity.bounds = self.tableView.bounds;

    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

    [self rotationLayoutIfNeed];
    
    lastOrientation = orientation;
}

- (void) _didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    //[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    isRotating = NO;
}

- (void) tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadData) object:nil];
    });
    isEditingRow = YES;
}

- (void) tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    isEditingRow = NO;
    [self reload];
}

- (void) reload{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadData) object:nil];
        
        if (self.tableView.isEditing || self->isEditingRow) {
            return;
        }

        if (!self->isAppeared) {
            return;
        }
        
        if (((METableView*)self.tableView).messageText!=nil) {
            if ([self.tableView numberOfSections]>0) {
                ((METableView*)self.tableView).messageText=nil;
            }
        }
        
        [self performSelector:@selector(deferredReloadData) withObject:nil afterDelay:ME_UI_UPDATE_RATE];
    });
}


- (void) deferredReloadData{
    [self.tableView reloadData];                        
}

#pragma Watchlist model

- (void) didWatchList:(MEWatchListModel *)watchlist ticker:(METicker *)ticker fault:(NSError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        self.isActivityShown = NO;
        
        if (ticker) {
            ticker.hasAccess = YES;
            ticker.tradingStatusBoolean = NO;
        }

        if (self.currentTickerInfo && [self.currentTickerInfo respondsToSelector:@selector(didTicker:fault:)] && self.lastSelectedIndexPath) {
            if (!self.tableView.isDecelerating && !self.tableView.isDragging) {
                [self.currentTickerInfo didTicker:[self.watchList tickerForIndexPath:self.lastSelectedIndexPath] fault:error];
            }
        }
        
        double delayInSeconds = .2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus: ME_ALERT_WARN];
        });
        
//        self.alertReferencesView.message = error.localizedDescription;
//        if (!self.alertReferencesView.isVisible && !isNotAlertReferencesShown) {
//            [self.alertReferencesView show];
//        }
        
        [self reload];
    });
}

- (void) willWatchListUpdate:(MEWatchListModel *)watchlist{
    self.isActivityShown = YES;
}

- (void) didWatchListUpdate:(MEWatchListModel *)watchlist{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        self.isActivityShown = NO;

        if (self.watchList.activeTicker==nil) {
            self.watchList.activeTicker = [self.watchList tickerForIndexPath:self.lastSelectedIndexPath];
        }

        [self.currentTickerInfo didTickerUpdate:[self.watchList tickerForIndexPath:self.lastSelectedIndexPath] withChangedAttributes:nil];
        
        if (self.tableView.isEditing || self.tickerIsChanging)
            return;
        
        ((METableView*)self.tableView).messageText = nil;
        
        if (
            self.currentTickerInfo && self.currentTickerInfo.isAppeared &&
            [self.currentTickerInfo respondsToSelector:@selector(didTickerUpdate:withChangedAttributes:)] &&
            self.lastSelectedIndexPath
            )
            
            [self.currentTickerInfo didTickerUpdate:[self.watchList tickerForIndexPath:self.lastSelectedIndexPath] withChangedAttributes:nil];
        
        if (self.isLockedForReload || self->isSearchBarShowing) return;
        
        if ([self.watchList sections]==0)
            return;
        
        [self reload];
    });
}

- (void) didWatchListUpdate:(MEWatchListModel *)watchlist atIndexPaths:(NSArray *)indexPaths withChangedAttributes:(NSDictionary *)attributes{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        self.isActivityShown = NO;

        if (self.tableView.isEditing || self.tickerIsChanging)
            return;
        
        if (self.currentTickerInfo  &&
            [self.currentTickerInfo respondsToSelector:@selector(didTickerUpdate:withChangedAttributes:)] &&
            self.lastSelectedIndexPath &&
            [indexPaths containsObject:self.lastSelectedIndexPath]
            )
        {
            [self.currentTickerInfo didTickerUpdate:[self.watchList tickerForIndexPath:self.lastSelectedIndexPath] withChangedAttributes:attributes];
        }
                
        if (self.chartViewController) {
            [self.chartViewController update];
        }
        
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && self.chartViewController.isShown) {
            return;
        }
        
        if (self.currentTickerInfo && self.currentTickerInfo.isAppeared && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            return;
        
        if (self.isLockedForReload || self->isSearchBarShowing) return;
        
        if ([self.watchList sections]==0)
            return;
                
        if (!self->isAppeared) {
            return;
        }
        
        [self deferredReloadWithIndexPaths:indexPaths withAttributes:attributes];
        
        if (((METableView*)self.tableView).messageText == nil) {
            ((METableView*)self.tableView).messageText = nil;
        }
    });
}

- (void) deferredReloadWithIndexPaths:(NSArray *)indexPaths withAttributes:(NSDictionary *)attributes{
    NSMutableArray *updated_indexPaths = [[NSMutableArray alloc] init];
    
    NSArray *visible_indices = [self.tableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in indexPaths) {
        if ([visible_indices containsObject:indexPath]) {
            [updated_indexPaths addObject:indexPath];
        }
    }
    
    if (updated_indexPaths == nil || updated_indexPaths.count==0){
        if (!isCurrentellyUpdated && self.tableView.visibleCells.count==0) {
            [self.tableView reloadData];
            isCurrentellyUpdated = YES;
        }
        return ;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->isCurrentellyUpdated) {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadDataWithPaths:) object:updated_indexPaths];
            self->isCurrentellyUpdated = NO;
        }
        [self performSelector:@selector(deferredReloadDataWithPaths:) withObject:updated_indexPaths afterDelay:ME_UI_UPDATE_RATE];
    });
}


- (void) deferredReloadDataWithPaths:(NSMutableArray *)updated_indexPaths{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadDataWithPaths:) object:updated_indexPaths];
        
        if (self.tableView.isEditing || self->isEditingRow) {
            return;
        }
        
        if (!self->isCurrentellyUpdated) {
            //[self.tableView reloadRowsAtIndexPaths:updated_indexPaths withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView reloadData];
        }
        
        self->isCurrentellyUpdated = YES;
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma SearchList model

- (void) didSearchListReceived:(MESearchListModel *)tickers{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *message = nil;
        
        if (tickers.sections == 0){
            message = NSLocalizedString(@"No pattern-like securities found", nil);
        }
        else {
            message = nil;
        }
    
        self.searchBarController.searchTableView.messageText = message;
        
        [self hideSearchActivity];
        [self.searchBarController.searchTableView reloadData];
    });
}

- (void) didSearchList:(MESearchListModel *)watchlist fault:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideSearchActivity];
        [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus: ME_ALERT_WARN];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchBarController.searchTableView) {
        return [searchList sections];
    }
    // Return the number of sections.
    return 1;//self.watchList.sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchBarController.searchTableView) {
        return [searchList rowsForSection:section];
    }
    // Return the number of rows in the section.
    return [self.watchList rowsForSection:section];
}

- (MEWatchListViewCell*) tableView:(UITableView *)tableView watchCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    MEWatchListViewCell *mcell;
    
    
    if (UIInterfaceOrientationIsLandscape(orientation)){
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            mcell = [tableView dequeueReusableCellWithIdentifier:CellWideIdentifier forIndexPath:indexPath];        
        else
            mcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    else {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            mcell = [tableView dequeueReusableCellWithIdentifier:CellWideIdentifier forIndexPath:indexPath];
        else
            mcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }

    
    return mcell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    UILabel *_ticker;
    UILabel *_board;
    UIImageView *_devider;
    METicker *ticker;
    
    if (tableView == self.searchBarController.searchTableView) {
        
        MESearchViewCell *scell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifierSearch];
        cell = scell;
        
        ticker = [searchList tickerForIndexPath:indexPath];
        _ticker = scell.tickerLabel;
        _board = scell.boardLabel;
        _devider = scell.devider;
        
        if ([self.watchList indexPathForTicker:ticker]) {
            _ticker.alpha = 0.5;
        }
        else{
            _ticker.alpha = 1.0;
        }        
    }
    else{
        MEWatchListViewCell *mcell = [self tableView:tableView watchCellForRowAtIndexPath:indexPath];
        
        _ticker = mcell.tickerLabel;
        _board = mcell.boardLabel;
        
        ticker = [self.watchList tickerForIndexPath:indexPath];
        
        NSAttributedString *_changePrice = [NSAttributedString changePrice:ticker.lastPrice andChange:ticker.change];
        
        mcell.changeLabel.attributedText = _changePrice;
        
        mcell.priceLabel.attributedText = [NSAttributedString coloredPrice:ticker.lastPrice andPrevPrice:ticker.prevPrice];
        
        if ([mcell isKindOfClass:[MEWatchListViewWideCell class]]){
            MEWatchListViewWideCell *wcell = (MEWatchListViewWideCell*)mcell;
            
            wcell.bidLabel.attributedText = [NSAttributedString coloredPrice:ticker.bid andPrevPrice:ticker.prevBid];
            
            if (ticker.bidDepth==nil || [ticker.bidDepth number].integerValue==0)
                wcell.bidDepthLabel.text = @"-";
            else
                wcell.bidDepthLabel.text = [ticker.bidDepth string];
            
            wcell.offerLabel.attributedText = [NSAttributedString coloredPrice:ticker.offer andPrevPrice:ticker.prevOffer];
            
            if (ticker.offerDepth==nil || [ticker.offerDepth number].integerValue==0)
                wcell.offerDepthLabel.text = @"-";
            else
                wcell.offerDepthLabel.text = [ticker.offerDepth string];
            
            static UIFont *updateTimeFont = nil; if(!updateTimeFont) updateTimeFont = [wcell.updateTimeLabel.font copy];
            wcell.updateTimeLabel.attributedText = [NSAttributedString coloredDate:ticker.time withBaseFontSize:updateTimeFont.pointSize];
        }
        
        _devider = mcell.devider;
        
        cell = mcell;
        
//        if (ticker.isDelayed) {
//            //mcell.state = ME_TICKER_IS_DELAYED;
//            mcell.state = ME_TICKER_HAS_NO_PERMISSIONS;
//        }
//        else
        if (!dispatcher.config.isAuthorized || !ticker.hasAccess) {
            mcell.state = ME_TICKER_HAS_NO_PERMISSIONS;
        }
        else
        if (!ticker.tradingStatusBoolean){
            mcell.state = ME_TICKER_TRADE_CLOSED;
        }
        else
        if (!ticker.hasLevel2){
            mcell.state = ME_TICKER_HAS_NO_LEVEL2;
        }
        else
        if (ticker.hasLevel2){
            mcell.state = ME_TICKER_HAS_LEVEL2;
        }
        else
        if (ticker.lastError!=nil){
            mcell.state = ME_TICKER_ALERT;
        }
    }
    
//    if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight
//        ||
//        [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft
//        ||
//        tableView == self.searchBarController.searchTableView
//        ) {
        _ticker.text = ticker.hint?ticker.hint:ticker.caption;
//    }
//    else {
//        _ticker.text = ticker.name;
//    }
    
    _board.text = ticker.board.caption;
    
    if (!ticker.hasAccess)
        cell.contentView.alpha = 0.4;
    else
        cell.contentView.alpha = 1.0;
    
    if (indexPath.row==[tableView numberOfRowsInSection:indexPath.section]-1)
        _devider.alpha = 0.0;
    else
        _devider.alpha = 1.0;
    
    if ([self.lastSelectedIndexPath isEqual:indexPath]) {
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62;
}

#pragma Scroll view delegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.tableView){
        [self reload];
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (scrollView == self.searchBarController.searchTableView) {
        if ([self.searchBarController.searchTableView numberOfSections]>0){
            if ([self.searchBarController.searchTableView numberOfRowsInSection:0]>0) {
                [self.searchBarController hideKeyboard];
            }
        }
    }

    if (scrollView.contentInset.top==0) {
        if (scrollView.contentOffset.y>=44.0f) {
            //
            // hide if content is < bounds            
            //
            if (self.tableView.contentSize.height<=self.tableView.bounds.size.height) {
                scrollView.bounces=NO;
                [UIView animateWithDuration:ME_UI_ANIMATION_DURATION 
                                 animations:^{
                                     scrollView.contentInset = UIEdgeInsetsMake(-44.0, 0, 0, 0);
                                     scrollView.contentOffset = CGPointMake(0, 44.0f);                
                                 }
                                 completion:^(BOOL finished) {
                                     scrollView.bounces=YES;
                                 }
                 ];
            }
        }
    }
    else if (scrollView.contentInset.top<=-44.0){    
        if (scrollView.contentOffset.y<=0.0f) {
            scrollView.bounces = NO;
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION 
                             animations:^{
                                 scrollView.contentOffset = CGPointMake(0, 0.0f);                
                                 scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                             }
                             completion:^(BOOL finished) {
                                 scrollView.bounces=YES;
                             }
             ];
        }
    }
}


#pragma MESearchBarController delegate
- (void) searchBarDidShow:(MESearchBarViewController *)searchBar{
    isSearchBarShowing = NO;
}

- (void) searchBarWillShow:(MESearchBarViewController *)searchBar{
    isSearchBarShowing = YES;
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if (self.tableView.contentInset.top==-44.0f) {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);            
    }
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if (!searchBar) {
        if (searchList.sections>0) {
            [searchList removeAllTickers];
            [self.searchBarController.searchTableView reloadData];
        }
    }
    
    NSString *text = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *message = nil;
    
    if (text.length>=3) {
        [self showSearchActivity];
        [searchList searchTickers:text];
    }
    else {
        message = NSLocalizedString(@"Search pattern should have at least 3 symbols", nil);
    }
    
    self.searchBarController.searchTableView.messageText = message;
}

- (void) searchBarEditButtonClicked:(UISearchBar *)searchBar withState:(MESearchBarButtonState)state{
    if (state == ME_SEARCH_BAR_BUTTON_EDIT) {
        [self.tableView setEditing:YES animated:YES];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        
        longPress.enabled = NO;
    }
    else if (state == ME_SEARCH_BAR_BUTTON_DONE) {
        [self.tableView setEditing:NO animated:YES];
        longPress.enabled = YES;
    }
    else{
        longPress.enabled = YES;
    }
    [self hideSearchActivity];
}


- (void) searchBarCancel:(UISearchBar *)searchBar{
    [self hideSearchActivity];
}

#pragma UITableView delegate

- (void) tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.tableView == tableView) {
        self.lastSelectedIndexPath = indexPath;
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}


- (void) pushViewControllerWithTicker:(METickerInfoViewController*) tickerInfo{
    
    if (isAppeared) {
        self.tickerIsChanging  = YES;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self.navigationController pushViewController:tickerInfo animated:YES];
    }
    
    [UIView transitionWithView:tickerInfo.view
                      duration:ME_UI_ANIMATION_DURATION
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.currentTickerInfo.navigationItem.title = self.watchList.activeTicker.caption;
                        [self.currentTickerInfo update];
                    }
                    completion:^(BOOL finished) {
                        self.tickerIsChanging = NO;
                    }];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.tableView == tableView){
        
        self.lastSelectedIndexPath =  indexPath;
        
        [self.watchList setActiveTicker:[self.watchList tickerForIndexPath:self.lastSelectedIndexPath]];
        
        if (!dispatcher.config.isAuthorized)
            return;
        
        [self pushViewControllerWithTicker:self.currentTickerInfo];
    }
    else if (tableView==self.searchBarController.searchTableView) {
        
        METicker *ticker = [searchList tickerForIndexPath:indexPath];
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            [self.searchBarController hideSearchTableView];
            [self.searchBarController hideKeyboard];
        }];
        [self.watchList addTicker:ticker];
        [self.tableView reloadData];        
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"editing : %@ ---> %@", indexPath, @(editingStyle));
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        @try {
            // Delete the row from the data source
            
            [cachedHeaderHeaders removeAllObjects];
            
            [self.watchList removeTicker:[self.watchList tickerForIndexPath:indexPath]];

            [tableView beginUpdates];
            
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [tableView endUpdates];
        }
        @catch (NSException *exception) {
            NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
            [tableView reloadData];
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    NSIndexPath *toIndexPathReal = toIndexPath;
    if (toIndexPathReal.row>=[self.watchList rowsForSection:toIndexPathReal.section]) {
        toIndexPathReal = [NSIndexPath indexPathForRow:[self.watchList rowsForSection:toIndexPath.section]-1 inSection:toIndexPath.section];
    }
    
    METicker *fromTicker = [self.watchList tickerForIndexPath:fromIndexPath];
    METicker *toTicker   = [self.watchList tickerForIndexPath:toIndexPathReal];
    
    if (!fromTicker || !toIndexPath) {
        return;
    }
    
    [self.watchList moveTicker:fromTicker withTicker:toTicker];
    
    [cachedHeaderHeaders removeAllObjects];
    
    [self reload];
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}



@end
