//
//  METickerMainInfoView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METickerMainInfoView.h"
#import "METhermometer.h"
#import "MEDispatcher.h"
#import "NSAttributedString+MEFormatedString.h"
#import "MEUIPreferences.h"
#import "MEWatchListModel.h"
#import <TSMessage.h>

@interface METickerMainInfoView() <UIGestureRecognizerDelegate>
@property(nonatomic,strong) UIImageView* topPanel;
@property(nonatomic,strong) UILabel *tickerName;
@property(nonatomic,strong) UIImageView *statusImageView;
@property(nonatomic,strong) UILabel *boardName;
@property(nonatomic,strong) UILabel *lastPrice;
@property(nonatomic,strong) UILabel *lastPriceCaption;
@property(nonatomic,strong) UILabel *change;
@property(nonatomic,strong) UILabel *valtoday;
@property(nonatomic,strong) METhermometer *depthThermo;

@property(nonatomic,strong) UILabel *bidLabel;
@property(nonatomic,strong) UILabel *offerLabel;
@property(nonatomic,strong) UILabel *bid;
@property(nonatomic,strong) UILabel *offer;

@property(nonatomic,strong) UIImageView *backLight;

@property(nonatomic,readonly) MEWatchListModel *watchList;
@property(nonatomic,readonly) METicker         *ticker;

@end

@implementation METickerMainInfoView
{
    MEDispatcher *dispatcher;
    
    UIImage *backLightImgWhite;
    UIImage *backLightImgRed;
    UIImage *backLightImgGreen;
    
    UIImageView *bottomLine;
    UIImage *delayedImage;
    UIImage *notradingImage;
    NSLayoutConstraint *statusImageViewWidth;
    
    BOOL isDelayedMessageShown;
}

@synthesize watchList=_watchList;

- (id) initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (!self) {
        return self;
    }
    
    isDelayedMessageShown = NO;
    
    delayedImage = [UIImage imageNamed:@"icon_15min"];
    notradingImage = [UIImage imageNamed:@"watchlist_icon_kirpich_dark"];
    
    self.topPanel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"grey_panel.png"]];
    
    self.statusImageView = [[UIImageView alloc] initWithImage:delayedImage];
    self.statusImageView.hidden = YES;
    
    self.boardName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    self.tickerName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    
    self.lastPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    self.lastPriceCaption = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    
    self.lastPriceCaption.lineBreakMode = NSLineBreakByWordWrapping;
    self.lastPriceCaption.adjustsFontSizeToFitWidth = NO;
    self.lastPriceCaption.numberOfLines = 3;
    
    self.change = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    self.valtoday = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
    
    self.topPanel.translatesAutoresizingMaskIntoConstraints = NO;
    self.statusImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.boardName.translatesAutoresizingMaskIntoConstraints = NO;
    self.tickerName.translatesAutoresizingMaskIntoConstraints = NO;
    self.lastPrice.translatesAutoresizingMaskIntoConstraints = NO;
    self.lastPriceCaption.translatesAutoresizingMaskIntoConstraints = NO;
    self.change.translatesAutoresizingMaskIntoConstraints = NO;
    self.valtoday.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.topPanel];
    [self.topPanel addSubview:self.statusImageView];
    [self.topPanel addSubview:self.boardName];
    [self.topPanel addSubview:self.tickerName];
    
    self.tickerName.textColor = self.boardName.textColor = ME_UI_LIGHT_GREY_FONT_COLOR;
    self.tickerName.font = [UIFont fontWithName:ME_UI_FONT size:14];
    self.tickerName.textAlignment = NSTextAlignmentRight;
    self.boardName.font = [UIFont fontWithName:ME_UI_FONT size:12];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[topPanel]-0-|" options:0 metrics:nil views:@{@"topPanel": self.topPanel}]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.topPanel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    
    statusImageViewWidth = [NSLayoutConstraint constraintWithItem:self.statusImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
                                                         toItem:nil  attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1 constant:delayedImage.size.width];

    [self.topPanel addConstraint:statusImageViewWidth];

    [self.topPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[delayed]-5-[board]-0-[tickerName(>=50)]-10-|" options:0 metrics:nil views:@{@"board": self.boardName, @"tickerName": self.tickerName, @"delayed":self.statusImageView}]];
    [self.topPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[board]-0-|" options:0 metrics:nil views:@{@"board": self.boardName}]];
    [self.topPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[tickerName]-0-|" options:0 metrics:nil views:@{@"tickerName": self.tickerName}]];
    
    
    [self.topPanel addConstraint:[NSLayoutConstraint constraintWithItem:self.statusImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.boardName  attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];

    
    [self addSubview:self.lastPrice];
    [self addSubview:self.lastPriceCaption];
    [self addSubview:self.change];
    [self addSubview:self.valtoday];
    [self addSubview:self.depthThermo];
    
    self.lastPrice.minimumScaleFactor=0.6;
    self.lastPrice.adjustsFontSizeToFitWidth=YES;
    
    self.lastPriceCaption.numberOfLines=3;
    self.lastPriceCaption.minimumScaleFactor=0.5;
    self.lastPriceCaption.adjustsFontSizeToFitWidth=YES;
    self.lastPriceCaption.lineBreakMode = NSLineBreakByWordWrapping;
    self.lastPriceCaption.font = [UIFont fontWithName:ME_UI_FONT size:10];
    self.lastPriceCaption.textColor = ME_UI_GREY_FONT_COLOR;
    
    self.change.minimumScaleFactor=0.5;
    self.change.adjustsFontSizeToFitWidth=YES;
    self.change.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:12];
    
    self.valtoday.minimumScaleFactor=0.5;
    self.valtoday.adjustsFontSizeToFitWidth=YES;
    self.valtoday.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:12];
    self.lastPrice.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:30];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        self.lastPrice.textAlignment = NSTextAlignmentCenter;
        self.lastPrice.adjustsFontSizeToFitWidth = YES;
        self.lastPrice.minimumScaleFactor = 0.6;
//        self.valtoday.textAlignment = NSTextAlignmentCenter;
//        
//        self.bidLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 10, 10)];
//        self.offerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        self.bid = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 10, 10)];
//        self.offer = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        
//        [self addSubview:self.bid];
//        self.bid.translatesAutoresizingMaskIntoConstraints = NO;
//        [self addSubview:self.bidLabel];
//        self.bidLabel.translatesAutoresizingMaskIntoConstraints=NO;
//        
//        [self addSubview:self.offer];
//        self.offer.translatesAutoresizingMaskIntoConstraints = NO;
//        [self addSubview:self.offerLabel];
//          self.offerLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        
//        self.bidLabel.font =  self.offerLabel.font = [UIFont fontWithName:ME_UI_FONT size:10];
//        self.bidLabel.textAlignment = self.offerLabel.textAlignment = NSTextAlignmentRight;
//        self.bid.font =  self.offer.font = [UIFont fontWithName:ME_UI_FONT size:14];
//        self.bid.minimumScaleFactor=self.offer.minimumScaleFactor = 0.5;
//        self.bid.adjustsFontSizeToFitWidth = self.offerLabel.adjustsFontSizeToFitWidth = YES;
//        self.bid.textAlignment = self.offer.textAlignment = NSTextAlignmentLeft;
//        self.bidLabel.textColor = self.offerLabel.textColor = ME_UI_GREY_FONT_COLOR;
//        
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPrice attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:120]];
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topPanel]-10-[price(>=20)]" options:0 metrics:nil views:@{@"price": self.lastPrice, @"topPanel": self.topPanel}]];
//        
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[priceCaption(<=100)]-(5)-[price(>=50)]" options:0 metrics:nil views:@{@"priceCaption":self.lastPriceCaption, @"price": self.lastPrice}]];
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPriceCaption attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.change attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeTop multiplier:1 constant:2]];
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valtoday attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeBottom multiplier:1 constant:2]];
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valtoday attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeRight multiplier:1 constant:5]];
//        
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.change attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bid attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.bidLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.valtoday attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offer attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.offerLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offer attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.bid attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
//        
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(5)-[change]-5-[bidLabel]-(5)-[bid]-(>=10)-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"change": self.change, @"bidLabel":self.bidLabel, @"bid":self.bid}]];
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(5)-[valtoday]-5-[offerLabel]-(5)-[offer]-(>=10)-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"valtoday": self.valtoday, @"offerLabel":self.offerLabel, @"offer":self.offer}]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPrice attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:-1]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topPanel]-10-[price]" options:0 metrics:nil views:@{@"price": self.lastPrice, @"topPanel": self.topPanel}]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[priceCaption]-(>=5)-[price]" options:0 metrics:nil views:@{@"priceCaption":self.lastPriceCaption, @"price": self.lastPrice}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPriceCaption attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=5)-[change]-10-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"change": self.change}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.change attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeTop multiplier:1 constant:2]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=5)-[valtoday]-10-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"valtoday": self.valtoday}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valtoday attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeBottom multiplier:1 constant:2]];

    }
    else{
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPrice attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:-1]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topPanel]-10-[price]" options:0 metrics:nil views:@{@"price": self.lastPrice, @"topPanel": self.topPanel}]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[priceCaption]-(>=5)-[price]" options:0 metrics:nil views:@{@"priceCaption":self.lastPriceCaption, @"price": self.lastPrice}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastPriceCaption attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=5)-[change]-10-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"change": self.change}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.change attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeTop multiplier:1 constant:2]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=5)-[valtoday]-10-|" options:0 metrics:nil views:@{@"price": self.lastPrice, @"valtoday": self.valtoday}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valtoday attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.lastPrice attribute:NSLayoutAttributeBottom multiplier:1 constant:2]];
    }
    
    backLightImgGreen = [UIImage imageNamed: @"podsvetka_green.png"];
    backLightImgRed = [UIImage imageNamed:@"podsvetka_red.png" ];
    backLightImgWhite = [UIImage imageNamed:@"podsvetka_white.png"];
    
    self.backLight = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.topPanel.bounds.size.height, backLightImgWhite.size.width, backLightImgWhite.size.height)];
    self.backLight.translatesAutoresizingMaskIntoConstraints = NO;
    self.backLight.image=backLightImgWhite;
    
    [self.topPanel addSubview:self.backLight];
    [self.backLight.superview bringSubviewToFront:self.backLight];
    [self.topPanel.superview bringSubviewToFront:self.topPanel];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.backLight attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.topPanel attribute:NSLayoutAttributeBottom multiplier:1 constant:-1]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.backLight attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.topPanel attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    
    UIImage *thin_grey_line = [UIImage imageNamed:@"thin_grey_line"];
    bottomLine = [[UIImageView alloc] initWithImage:thin_grey_line];
    [self addSubview:bottomLine];
    bottomLine.translatesAutoresizingMaskIntoConstraints=NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomLine]-0-|" options:0 metrics:nil views:@{@"bottomLine": bottomLine}]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:bottomLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:thin_grey_line.size.height<=1?0:0.5]];
    
    //
    // Thermometer
    //
    
    self.depthThermo = [[METhermometer alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    self.depthThermo.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.depthThermo];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.depthThermo attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:bottomLine attribute:NSLayoutAttributeTop multiplier:1 constant:-5]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topPanel]-(>=5)-[depthThermo(30)]" options:0 metrics:nil views:@{@"depthThermo": self.depthThermo, @"topPanel": self.topPanel}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[depthThermo(>=10)]-10-|" options:0 metrics:nil views:@{@"depthThermo": self.depthThermo}]];
    
    
    self.backgroundColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTopPanel:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    tap.delegate = self;

    self.topPanel.userInteractionEnabled  = YES;
    [self.topPanel addGestureRecognizer:tap];
    
    return self;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void) tapOnTopPanel:(UIGestureRecognizer*)gesture{
    if (self.ticker.isDelayed){
        [self showDelaiedInfo];
    }
}


- (MEWatchListModel*) watchList{
    if (!_watchList) {
        _watchList = [MEWatchListModel sharedInstance];
    }
    return _watchList;
}

- (METicker*) ticker{
    return self.watchList.activeTicker;
}

- (void) changeBackLight: (UIImage *)newImage{
    if (![self.backLight.image isEqual:newImage]) {
        dispatch_async(dispatch_get_main_queue(), ^{        
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:ME_UI_ANIMATION_DURATION];
            [UIView setAnimationBeginsFromCurrentState:YES];
            self.backLight.alpha = 0.0;
            [UIView commitAnimations];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:ME_UI_ANIMATION_DURATION];
            self.backLight.image = newImage;
            self.backLight.alpha = 1.0;
            [UIView commitAnimations];
        });
    }
}

- (void) setRootController:(UIViewController *)rootController{
    _rootController = rootController;
}

- (void) showDelaiedInfo {
    [TSMessage dismissActiveNotification];
    [TSMessage showNotificationInViewController:self.rootController
                                          title:NSLocalizedString(@"Connection info", nil)
                                       subtitle:self.ticker.delayDescription
                                          image:[UIImage imageNamed:@"icon_15min_button"]
                                           type:TSMessageNotificationTypeMessage
                                       duration:TSMessageNotificationDurationAutomatic
                                       callback:nil
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}

- (void) update{
    
    if (!self.ticker) {
        return ;
    }
    
    if (self.ticker.isDelayed){
        
        //if (self.statusImageView.hidden){
            
            self.statusImageView.hidden = NO;
            self.statusImageView.image = delayedImage;
            statusImageViewWidth.constant = delayedImage.size.width;

            if (!isDelayedMessageShown){
                [self showDelaiedInfo];
                isDelayedMessageShown = YES;
            }
        //}
    }
    else if (!self.ticker.tradingStatusBoolean){
        self.statusImageView.hidden = NO;
        self.statusImageView.image = notradingImage;
        statusImageViewWidth.constant = notradingImage.size.width;
    }
    else if (self.statusImageView.image!=nil){
        self.statusImageView.hidden = YES;
        statusImageViewWidth.constant = 0;
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.boardName.text = self.ticker.board.caption;
        self.tickerName.text = self.ticker.name;
        self.lastPriceCaption.text = [self fieldByName:@"LAST"].hint;
        
        self.lastPrice.attributedText = [NSAttributedString coloredPrice:self.ticker.lastPrice andPrevPrice:self.ticker.prevPrice];
        self.change.attributedText = [NSAttributedString changePrice:self.ticker.lastPrice andChange:self.ticker.change];
        self.valtoday.attributedText = [NSAttributedString compoundValToday:[self.ticker.valtoday number]];
        
        self.depthThermo.bidCaptionLabel.text = [self fieldByName:@"BIDDEPTHT"].caption;
        self.depthThermo.offerCaptionLabel.text = [self fieldByName:@"OFFERDEPTHT"].caption;
        [self.depthThermo setTimeUpdate:self.ticker.time];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            self.bidLabel.text = [self fieldByName:@"BID"].caption;
            self.offerLabel.text = [self fieldByName:@"OFFER"].caption;
        }
        
        float trend = [self.ticker.change number].floatValue;
        
        if (trend==0.0)
            [self changeBackLight:self->backLightImgWhite];
        else if (trend<0)
            [self changeBackLight:self->backLightImgRed];
        else if (trend>0)
            [self changeBackLight:self->backLightImgGreen];
        
        [self updateBidOfferDepths];
    });
}

- (void) updateBidOfferDepths{        
    [self.depthThermo setBidDepth:self.ticker.bidDepth withOfferDepth:self.ticker.offerDepth];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.bid.attributedText = [NSAttributedString coloredPrice:self.ticker.bid andPrevPrice:self.ticker.prevBid];
        self.offer.attributedText = [NSAttributedString coloredPrice:self.ticker.offer andPrevPrice:self.ticker.prevOffer];
    }        
}

- (void) setNeedsUpdateConstraints{
    [super setNeedsUpdateConstraints];
    [self.depthThermo setNeedsUpdateConstraints];
}

- (void) layoutIfNeeded{
    [super layoutIfNeeded];
    [self.depthThermo layoutIfNeeded];
}

- (GIField*) fieldByName:(NSString*)fieldName{
    @try {
        return self.ticker.securitySubscription.structure.stream.fields[fieldName];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        return nil;
    }
}


@end
