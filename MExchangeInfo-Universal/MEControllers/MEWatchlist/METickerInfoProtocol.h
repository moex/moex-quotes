//
//  METickerInfoProtocol.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 05/01/15.
//  Copyright (c) 2015 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol METickerInfoProtocol <NSObject>
- (void) tickerInfoDidScroll:(UIScrollView *)scrollView;
@end
