//
//  MESearchViewCell.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 08/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEBaseViewCell.h"
static NSString *CellIdentifierSearch = @"CellSearch";

@interface MESearchViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel *tickerLabel;
@property(nonatomic,strong) UILabel *boardLabel;
@end
