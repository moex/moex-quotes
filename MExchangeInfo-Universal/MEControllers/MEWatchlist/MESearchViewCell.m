//
//  MESearchViewCell.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 08/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESearchViewCell.h"
#import "MEUIPreferences.h"

@implementation MESearchViewCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [self initWithreuseIdentifier:reuseIdentifier];
    [self addDevider];
    return self;
}

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        // Initialization code
        self.tickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 320, 10)];
        self.boardLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.boardLabel.minimumScaleFactor=0.5;
        self.boardLabel.adjustsFontSizeToFitWidth = YES;
        
        self.tickerLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
       
        self.boardLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.boardLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        self.boardLabel.minimumScaleFactor=0.5;
        self.boardLabel.adjustsFontSizeToFitWidth = YES;

        [self.contentView addSubview:self.tickerLabel];
        [self.contentView  addSubview:self.boardLabel];
        
        self.tickerLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.boardLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[ticker]" options:0 metrics:0 views:@{@"ticker": self.tickerLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[board]" options:0 metrics:0 views:@{@"board": self.boardLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[ticker]" options:0 metrics:0 views:@{@"ticker": self.tickerLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[ticker]-(>=2)-[board]" options:0 metrics:0 views:@{@"board": self.boardLabel, @"ticker": self.tickerLabel}]];
    }
    return self;
}

@end
