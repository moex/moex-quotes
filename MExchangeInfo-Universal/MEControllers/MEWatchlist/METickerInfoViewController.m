//
//  METickerInfoViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/20/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METickerInfoViewController.h"
#import "METickerInfoViewCell.h"
#import "MESettings.h"
#import "MEUIPreferences.h"
#import "MEMenuDefaults.h"
#import "MEWatchListModel.h"
#import "MEChartViewController.h"
#import "MELevel2Model.h"
#import "MELevel2TableViewController.h"
#import "MEChartViewController.h"
#import "METickerInfoTableViewController.h"
#import "METickerCardDetailController.h"

#import <Crashlytics/Crashlytics.h>


static NSString *tickerInfoReuseIdenifierCell = @"tickerInfoReuseIdenifierCell";

static const NSInteger tickerInfoPageNumber = 0;
static const NSInteger chartViewPageNumber = 1;
static const NSInteger level2PageNumber = 2;

@interface METickerInfoViewController () <UIGestureRecognizerDelegate, UINavigationControllerDelegate, METickerInfoProtocol>

@property (nonatomic,strong         ) UIButton              *tickerCardButton;

@property (nonatomic,strong         ) MEChartViewController *chartViewController;
@property (nonatomic,readonly       ) MEWatchListModel      *watchList;
@property (nonatomic,weak           ) MESettings            *settings;
@property (nonatomic,assign         ) BOOL                  showHintName;
@property (nonatomic,readonly       ) METicker             *ticker;
@property (nonatomic,weak           ) METicker             *tickerLastTime;

@property (nonatomic,strong) METickerCardDetailController *tickerCardsViewController;

@end

@implementation METickerInfoViewController
{
    BOOL isSimultaneouslyWithGestureRecognizer;
    BOOL isCurrentellyUpdated;
    
    NSMutableDictionary *cachedCells;
    
    NSLayoutConstraint *tableViewWidth, *tableViewPosition, *tickerLevel2Width;
    
    UIPanGestureRecognizer *panTableView;
    UIPanGestureRecognizer *panLevel2View;
    
    UIButton *buttonTickerInfo, *buttonLevel2, *buttonChart;
    
    float lastVelocityPan;
    
    NSInteger lastVeiwedPage;
    CGPoint lastOffset;
}

- (MEWatchListModel*) watchList{
    return [MEWatchListModel sharedInstance];
}

- (METicker*) ticker{
    return self.watchList.activeTicker;
}

- (METickerCardDetailController*) tickerCardsViewController{
    if (!_tickerCardsViewController) {
        _tickerCardsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tickerCardItemsCID"];
    }
    return _tickerCardsViewController;
}

- (void) update{
    [self.level2ViewController.tableView update];
    [self reloadData];
}

#pragma MEWatchListModelProtocol

- (void) deferredReload{
    [self reloadData];
}

- (void) reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReload) object:nil];
        
        [self.tickerInfoView update];
        
        if (self.tickerTableInfoViewController) {
            [self.tickerTableInfoViewController.tableView update];
        }
        
        if (self.chartViewController) {
            [self.chartViewController update];
        }
        
        self->isCurrentellyUpdated = YES;
    });
}

- (void) didTickerUpdate:(METicker *)aTicker withChangedAttributes:(NSDictionary *)attributes{
    
    if (!self.isAppeared)
        return;
    
    if (self.ticker==nil) {
        return;
    }
    
    if (attributes && attributes.count==0) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->isCurrentellyUpdated) {            
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReload) object:nil];
        }
        self->isCurrentellyUpdated = NO;
        [self performSelector:@selector(deferredReload) withObject:nil afterDelay:ME_UI_UPDATE_RATE];
    });
    
}

- (void) didTicker:(METicker *)ticker fault:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.ticker.rows.count == 0) {
            NSString *mess = self.ticker.lastError?self.ticker.lastError.localizedDescription:error.localizedDescription;
            self.tickerTableInfoViewController.tableView.messageText = mess;
        }
        else {
            self.tickerTableInfoViewController.tableView.messageText = error.localizedDescription;
        }
    });
}

#pragma  UIViewController overrides

- (UIButton*) tickerCardButton{
    
    if (_tickerCardButton) {
        return _tickerCardButton;
    }
    
    _tickerCardButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    UIImage *image = [UIImage imageNamed:@"button_ticker_info"];
    [_tickerCardButton setImage: image forState:UIControlStateNormal];
    [_tickerCardButton setAdjustsImageWhenHighlighted:YES];
    [_tickerCardButton setAdjustsImageWhenDisabled:NO];
    [_tickerCardButton setShowsTouchWhenHighlighted: YES];
    
    [_tickerCardButton addTarget:self action:@selector(tickerCardButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    return _tickerCardButton;
};

- (void) tickerCardButtonHandler:(UIButton*)sender{
    [self tickerCardPushController:nil];
}


- (void) tickerCardPushController:(NSNotification*)event{
    if (self.navigationController.topViewController==self.tickerCardsViewController) {
        if (UIUserInterfaceIdiomPhone==UI_USER_INTERFACE_IDIOM()) {
            [self.navigationController popToViewController:self animated:YES];
        }
        else
            [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        self.tickerCardsViewController.navigationItem.leftBarButtonItem.title = self.navigationItem.title;
        [self.navigationController pushViewController:self.tickerCardsViewController animated:YES];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];

    self.isAppeared = YES;
    [self reloadData];
    
    lastVeiwedPage = self.currentPage;

    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && self.currentPage==chartViewPageNumber) {
        //[self setToolbarHidden:YES animated:NO];
        [self hideToolbar];
    }
    else{
        //[self setToolbarHidden:NO animated:NO];
        [self showToolbar];
    }
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self checkCurrentPage];
    
    self.tickerInfoView.rootController = self;

    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    self.isAppeared = YES;
    
    if (self.currentPage!=chartViewPageNumber) {
        [self performSelector:@selector(hideToolbar) withObject:nil afterDelay:ME_DEFERRED_SCROLL_DELAY];
    }
    
    self.tickerTableInfoViewController.tableView.infoDelegate = self;
    self.level2ViewController.tableView.infoDelegate = self;
    
    [Answers logContentViewWithName:@"Ticker Info View"
                        contentType:@""
                          contentId:@""
                   customAttributes:@{                                    
                                      @"Ticker Info: ticker": self.ticker.ID == nil ? @"N/A" : self.ticker.ID,
                                      }];

}


- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.isAppeared= NO;
    
    self.tickerLastTime = self.ticker;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) return YES;
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    lastVelocityPan = 1.0;
    
    self.settings = [MESettings sharedInstance];
    self.showHintName = self.settings.showHintName.boolValue;
    
    NSNumber *topoffset = [NSNumber numberWithInt:0];
    
    self.tickerInfoView = [[METickerMainInfoView alloc] initWithFrame:CGRectMake(0, 0, 320, 128)];
    self.tickerInfoView.rootController = self;
    
    self.tickerInfoView.translatesAutoresizingMaskIntoConstraints=NO;
    
    [self.view addSubview:self.tickerInfoView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[info]-0-|" options:0 metrics:nil views:@{@"info":self.tickerInfoView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(offset)-[info(height)]"
                                                                      options:0
                                                                      metrics:@{@"offset":topoffset, @"height": [NSNumber numberWithInt:ME_UI_TICKERINFO_HEIGHT]}
                                                                        views:@{@"info":self.tickerInfoView, @"topGuide":self.topLayoutGuide}]];
    
    [self.view.superview sendSubviewToBack:self.view];
    
    [self addChildViewController: _tickerTableInfoViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"tickerInfoTableControllerID"]];
    [self addChildViewController: _chartViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"chartViewControllerID"]];
    [self addChildViewController: _level2ViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"level2TableControllerID"]];
    
    cachedCells = [[NSMutableDictionary alloc] init];
    // Do any additional setup after loading the view.
    
    self.navigationController.delegate = self;
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:self.tickerCardButton]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    buttonTickerInfo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [buttonTickerInfo setImage:[UIImage imageNamed:@"icon_info"] forState:UIControlStateNormal];
    [buttonTickerInfo setImage:[UIImage imageNamed:@"icon_info_selected"] forState:UIControlStateSelected];
    [buttonTickerInfo addTarget:self action:@selector(toolBarHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonTickerInfo.selected = YES;
    
    buttonLevel2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [buttonLevel2 setImage:[UIImage imageNamed:@"icon_level2"] forState:UIControlStateNormal];
    [buttonLevel2 setImage:[UIImage imageNamed:@"icon_level2_selected"] forState:UIControlStateSelected];
    [buttonLevel2 addTarget:self action:@selector(toolBarHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonLevel2.selected = NO;
    
    buttonChart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [buttonChart setImage:[UIImage imageNamed:@"icon_graph"] forState:UIControlStateNormal];
    [buttonChart setImage:[UIImage imageNamed:@"icon_graph_selected"] forState:UIControlStateSelected];
    [buttonChart addTarget:self action:@selector(toolBarHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonChart.selected = NO;
    
    NSArray *items = [NSArray
                      arrayWithObjects:flexiableItem,
                      [[UIBarButtonItem alloc] initWithCustomView:buttonTickerInfo],
                      flexiableItem,
                      [[UIBarButtonItem alloc] initWithCustomView:buttonChart],
                      flexiableItem,
                      [[UIBarButtonItem alloc] initWithCustomView:buttonLevel2],
                      flexiableItem, nil];
    
    [self.toolBar setItems:items];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tickerCardPushController:) name:ME_TICKER_CARD_SHOW_NOTIFICATION object:nil];
    }
    
    [self.tickerInfoView.superview sendSubviewToBack:self.tickerInfoView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    tap.delegate = self;
    self.tickerInfoView.userInteractionEnabled = YES;
    [self.tickerInfoView addGestureRecognizer:tap];
}

- (void) tapHandler:(UITapGestureRecognizer*)gesture{
    NSLog(@"tapppp %@", gesture);
    [self toggleToolBar];
}

- (void) checkCurrentPage{
    buttonTickerInfo.selected=buttonLevel2.selected = buttonChart.selected = NO;
    
    
    if (self.currentPage==tickerInfoPageNumber) {
        buttonTickerInfo.selected = YES;
    }
    else if (self.currentPage==chartViewPageNumber){
        buttonChart.selected = YES;
    }
    else if (self.currentPage==level2PageNumber){
        buttonLevel2.selected = YES;
    }
}

- (void) toolBarHandler:(UIButton*)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
    if (sender==buttonTickerInfo) {
        self.currentPage = tickerInfoPageNumber;
    }
    else if (sender==buttonChart){
        self.currentPage = chartViewPageNumber;
    }
    else if (sender==buttonLevel2){
        self.currentPage = level2PageNumber;
    }
    
    [self checkCurrentPage];
}

- (void) setCurrentPage:(NSInteger)currentPage{
    [super setCurrentPage:currentPage];
    [self checkCurrentPage];
}

- (void) loginSuccessed:(NSNotification*)event{
    [self reloadData];
}

- (BOOL) shouldHideWhenRotation{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return NO;
    }
    return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    //[self reloadData];
    //[self.chartViewController.chartView redraw];
}


- (void) pageWillChange:(NSInteger)page{
    [super pageWillChange:page];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            //[self setToolbarHidden:NO animated:YES];
            [self showToolbar];
        }
    }
}

- (void) pageDidChange:(NSInteger)page{
    self.pageScrollView.scrollEnabled = YES;
    [super pageDidChange:page];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
    if (page==tickerInfoPageNumber || page==level2PageNumber) {
        [self performSelector:@selector(hideToolbar) withObject:nil afterDelay:ME_DEFERRED_SCROLL_DELAY];
    }
    else{
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
                //[self setToolbarHidden:NO animated:YES];
                [self showToolbar];
            }
            else {
                //[self setToolbarHidden:YES animated:YES];
                [self hideToolbar];
            }
        }
    }
}

- (void) hideToolbar{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [self setToolbarHidden:YES animated:YES];        
    }
}

- (void) showToolbar{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showToolbar) object:nil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [self setToolbarHidden:NO animated:YES];
    }
}

- (void) tickerInfoWillBeginDragging:(UIScrollView *)scrollView{
    if (self.currentPage==tickerInfoPageNumber || self.currentPage==level2PageNumber) {
        //[self setToolbarHidden:NO animated:YES];
        [self showToolbar];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
        [self performSelector:@selector(hideToolbar) withObject:nil afterDelay:ME_DEFERRED_SCROLL_DELAY];
    }
}

- (void) tickerInfoDidScroll:(UIScrollView *)scrollView{
    
    CGPoint offset = scrollView.contentOffset;
    CGFloat direction = (lastOffset.y-offset.y);
    
    if (scrollView.isTracking) {
                
        if (offset.y==0.0f) 
            return;        
                
        if (self.currentPage==tickerInfoPageNumber || self.currentPage==level2PageNumber) {

            BOOL show=NO;
            
            if (offset.y>0) {
                
                if (direction<0 && !self.toolbarHidden){

                    //[self setToolbarHidden:YES animated:YES];
                    [self hideToolbar];
                }
                else if (direction>0 && self.toolbarHidden){
                    show=YES;
                }
                
            }
            else if (self.toolbarHidden){
                show=YES;
            }
            
            if (show) {
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showToolbar) object:nil];
                [self performSelector:@selector(showToolbar) withObject:nil afterDelay:0.05];

                
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideToolbar) object:nil];
                [self performSelector:@selector(hideToolbar) withObject:nil afterDelay:ME_DEFERRED_SCROLL_DELAY];
            }
        }
    }
    
    lastOffset = scrollView.contentOffset;
}

@end
