//
//  METickerInfoViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/20/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEWatchListModel.h"
#import "METickerMainInfoView.h"
#import "METableView.h"
#import "MELevel2TableView.h"
#import "MEPageViewController.h"
#import "METickerInfoViewDefaults.h"
#import "METickerInfoProtocol.h"

@class METickerInfoTableViewController;
@class MEChartViewController;
@class MELevel2TableViewController;

@protocol METickerInfoModelProtocol <NSObject>
- (void) didTickerUpdate:(METicker *)ticker withChangedAttributes:(NSDictionary*)attributes;
@optional
- (void) didTicker:(METicker*)ticker fault:(NSError*)error;
@end

@interface METickerInfoViewController : MEPageViewController <METickerInfoModelProtocol>

@property (nonatomic)     BOOL isAppeared;

@property (strong, nonatomic ) METickerMainInfoView            *tickerInfoView;
@property (nonatomic,readonly) METickerInfoTableViewController *tickerTableInfoViewController;
@property (nonatomic,readonly) MELevel2TableViewController     *level2ViewController;
@property (nonatomic,readonly) MEChartViewController           *chartViewController;

- (void) update;

@end
