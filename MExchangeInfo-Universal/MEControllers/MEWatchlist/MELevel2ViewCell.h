//
//  MELevel2ViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEBaseViewCell.h"

static NSString *ME_BID_LEVEL2_CELL=@"bidLevel2Cell";
static NSString *ME_OFFER_LEVEL2_CELL=@"offerLevel2Cell";

@interface MELevel2ViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel  *price;
@property(nonatomic,strong) UILabel  *qty;
@property(nonatomic,strong) UISlider *qtyTherm;
@end
