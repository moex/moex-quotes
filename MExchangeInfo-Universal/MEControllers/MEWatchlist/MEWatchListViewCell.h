//
//  MEWatchListViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/16/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEBaseViewCell.h"


#define ME_WATCHLIST_MARKET_CELL @"marketWatchListCell"
#define ME_WATCHLIST_MARKET_CELL_WIDE @"marketWatchListCellWide"


typedef enum {
    ME_TICKER_HAS_NO_PERMISSIONS,
    ME_TICKER_HAS_LEVEL2,
    ME_TICKER_HAS_NO_LEVEL2,
    ME_TICKER_TRADE_CLOSED,
    ME_TICKER_IS_DELAYED,
    ME_TICKER_ALERT
} METickerInfoState;


@interface MEWatchListViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel *tickerLabel;
@property(nonatomic,strong) UILabel *boardLabel;
@property(nonatomic,strong) UILabel *priceLabel;
@property(nonatomic,strong) UILabel *changeLabel;
@property(nonatomic,strong) UIImageView *stateImage;
@property(nonatomic) METickerInfoState state;

@end


@interface MEWatchListViewWideCell : MEWatchListViewCell
@property(nonatomic,strong) UILabel *updateTimeLabel;
@property(nonatomic,strong) UILabel *bidLabel;
@property(nonatomic,strong) UILabel *bidDepthLabel;
@property(nonatomic,strong) UILabel *offerLabel;
@property(nonatomic,strong) UILabel *offerDepthLabel;
@end

