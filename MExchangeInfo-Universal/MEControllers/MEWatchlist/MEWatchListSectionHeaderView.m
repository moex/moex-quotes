//
//  MEWatchListSectionHeaderView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 13.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEWatchListSectionHeaderView.h"
#import "MEUIPreferences.h"
#import "NSAttributedString+MEFormatedString.h"

@interface MEWatchListSectionHeaderView()
@property(nonatomic,strong) NSString *engineName;
@property(nonatomic,strong) NSString *marketName;
@end

@implementation MEWatchListSectionHeaderView
{
    UIImage *sectionHeaderBgImage;
    UIImage *subSectionHeaderBgImage;
    
    UIImageView *titleView;
    UIImageView *subTitleView;
    
    NSMutableArray *titleAutoLayer, *subTitleAutoLayer;
}

- (void) setShouldShowTitle:(BOOL)shouldShowTitle{
    _shouldShowTitle = shouldShowTitle;
    if (_shouldShowTitle) {
        
        titleView.frame = titleView.bounds;
        _height = titleView.bounds.size.height;
        
        if (_shouldShowSubTitle) {
            _height+=subTitleView.bounds.size.height;
            subTitleView.frame = CGRectMake(0.0, titleView.bounds.size.height, subTitleView.bounds.size.width, subTitleView.bounds.size.height);
        }
        
        self.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, _height);
        [self addSubview:titleView];
    }
    else{
        
        _height = subTitleView.bounds.size.height;
        
        if (_shouldShowSubTitle)
            subTitleView.frame = subTitleView.bounds;
        else
            _height = 0.0;
        
        self.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, _height);
        [titleView removeFromSuperview];
    }
}

- (void) setShouldShowSubTitle:(BOOL)shouldShowSubTitle{
    _shouldShowSubTitle = shouldShowSubTitle;
    
    if (_shouldShowSubTitle) {
        
        _height = subTitleView.bounds.size.height;
        if (_shouldShowTitle) {
            subTitleView.frame = CGRectMake(0.0, titleView.bounds.size.height, subTitleView.bounds.size.width, subTitleView.bounds.size.height);
            _height+=titleView.bounds.size.height;
        }
        else
            subTitleView.frame = subTitleView.bounds;
        
        self.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, _height);
        [self addSubview:subTitleView];
    }
    else{
        if (_shouldShowTitle) {
            _height = titleView.bounds.size.height;
            self.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, _height);
        }
        else{
            _height = 0.0;
        }
        [subTitleView removeFromSuperview];
    }
}

- (id) initWithReuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        
        _height = 0.;
        
        sectionHeaderBgImage = [UIImage imageNamed:@"narrow_black_panel"];
        subSectionHeaderBgImage = [UIImage imageNamed:@"light_grey_panel_shadow"];
        
        titleView = [[UIImageView alloc] initWithImage:sectionHeaderBgImage];
        subTitleView = [[UIImageView alloc] initWithImage:subSectionHeaderBgImage];
        subTitleView.backgroundColor = [UIColor whiteColor];
        
        titleView.backgroundColor = [UIColor whiteColor];
        
        titleView.autoresizesSubviews = YES;
        titleView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        subTitleView.autoresizesSubviews = YES;
        subTitleView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        //self.shouldShowTitle = YES;
        //self.shouldShowSubTitle = YES;
        
        //_titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _subTitleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _valtodayLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _valtodayTotalLabel = [[UILabel alloc] initWithFrame:self.bounds];
        
        _subTitleLabel.translatesAutoresizingMaskIntoConstraints = _titleLabel.translatesAutoresizingMaskIntoConstraints = _valtodayLabel.translatesAutoresizingMaskIntoConstraints = _valtodayTotalLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        _titleLabel.textColor = ME_UI_MENU_FONT_COLOR;
        _titleLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        
        _subTitleLabel.textColor = ME_UI_GREY_FONT_COLOR;
        _subTitleLabel.textAlignment = NSTextAlignmentLeft;
        _subTitleLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        
        _valtodayLabel.textColor = [UIColor blackColor];
        _valtodayLabel.textAlignment = NSTextAlignmentRight;
        _valtodayLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:14];
        
        _valtodayTotalLabel.textColor = ME_UI_MENU_FONT_COLOR;
        _valtodayTotalLabel.textAlignment = NSTextAlignmentRight;
        _valtodayTotalLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
        
        _valtodayLabel.text = @"";
        _valtodayTotalLabel.text = @"";
        
        [titleView addSubview:_titleLabel];
        [titleView addSubview:_valtodayTotalLabel];
        
        [subTitleView addSubview:_subTitleLabel];
        [subTitleView addSubview:_valtodayLabel];
        
        NSDictionary *views = @{@"title":_titleLabel, @"valtodayTotal": _valtodayTotalLabel};
        NSDictionary *subTitleViews = @{@"subtitle":_subTitleLabel, @"valtoday":_valtodayLabel};
        
        //
        titleAutoLayer = [NSMutableArray arrayWithArray: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[title(>=100)]" options:0 metrics:0 views:views]];
        [titleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[valtodayTotal]-10-|" options:0 metrics:0 views:views]];
        [titleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[title]-2-|" options:0 metrics:0 views:views]];
        [titleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[valtodayTotal]-2-|" options:0 metrics:0 views:views]];
        
        subTitleAutoLayer = [NSMutableArray arrayWithArray: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[subtitle(>=100)]" options:0 metrics:0 views:subTitleViews]];
        [subTitleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[valtoday]-10-|" options:0 metrics:0 views:subTitleViews]];
        [subTitleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[subtitle]-2-|" options:0 metrics:0 views:subTitleViews]];
        [subTitleAutoLayer addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[valtoday]-2-|" options:0 metrics:0 views:subTitleViews]];
        
        [titleView addConstraints:titleAutoLayer];
        [subTitleView addConstraints:subTitleAutoLayer];
        
        //self.frame = titleView.bounds;
        
        NSLog(@" *** MEWatchListSectionHeaderView: initWithReuseIdentifier: %@", reuseIdentifier);
        
    }
    return self;
}


- (void) setSector:(MEMarketSection *)sector{
    if (_sector == nil || _sector!=sector) {
        _sector = sector;        
    }
    self.valtodayTotalLabel.attributedText = [NSAttributedString compoundValToday:[_sector.engineValtoday number] withAbvColor:ME_UI_LIGHT_GREY_FONT_COLOR andFont:nil];
    self.valtodayLabel.attributedText = [NSAttributedString compoundValToday:[_sector.marketValtoday number]];
}


@end
