    //
    //  MEWatchListViewCell.m
    //  MExchangeInfo-Universal
    //
    //  Created by denis svinarchuk on 12/16/13.
    //  Copyright (c) 2013 Moscow Exchange. All rights reserved.
    //

#import "MEWatchListViewCell.h"
#import "MEUIPreferences.h"
#import "MEConfig.h"

@implementation MEWatchListViewCell
{
    UIImage *tickerLevel2StateImage, *tickerNoLevel2StateImage, *tickerHasNoPermitionsStateImage, *tickerTradeClosedStateImage, *tickerAlertStateImage, *tickerIsDelayed, *tickerInfo;
        //UIImageView *delayedImageView;
}


- (void) setAccessoryImage: (UIImage*)image{
    
    if (image!=nil){
        UIImageView* accessoryImageView = [[UIImageView alloc] initWithFrame:
                                           CGRectMake(0, 0, image.size.width , image.size.height)];
        accessoryImageView.contentMode = UIViewContentModeLeft;
        accessoryImageView.image = image;
        CGRect rect = accessoryImageView.frame;
        accessoryImageView.frame = rect;
        self.accessoryView = accessoryImageView;
    }
    else {
        self.accessoryView = nil;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}

- (void) setState:(METickerInfoState)state{
        //delayedImageView.image  = nil;
    
    switch (state) {
        case ME_TICKER_HAS_LEVEL2:
                //[self setAccessoryImage:tickerLevel2StateImage];
            //[self setAccessoryImage:nil];
            [self setAccessoryImage:tickerInfo];
            break;
        case ME_TICKER_HAS_NO_LEVEL2:
                //[self setAccessoryImage:tickerNoLevel2StateImage];
            //[self setAccessoryImage:nil];
            [self setAccessoryImage:tickerInfo];
            break;
        case ME_TICKER_IS_DELAYED:
                //delayedImageView.image  = tickerIsDelayed;
            break;
        case ME_TICKER_HAS_NO_PERMISSIONS:
            [self setAccessoryImage:tickerHasNoPermitionsStateImage];
            break;
        case ME_TICKER_TRADE_CLOSED:
            [self setAccessoryImage:tickerTradeClosedStateImage];
            break;
        case ME_TICKER_ALERT:
            [self setAccessoryImage:tickerAlertStateImage];
            break;
        default:
            break;
    }
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [self initWithreuseIdentifier:reuseIdentifier];
    [self addDevider];
    return self;
}

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        tickerLevel2StateImage = [UIImage imageNamed:@"watchlist_icon_levelII"];
        tickerNoLevel2StateImage = [UIImage imageNamed:@"watchlist_icon_levelII_light"];
        tickerHasNoPermitionsStateImage = [UIImage imageNamed:@"watchlist_icon_lock"];
        tickerTradeClosedStateImage = [UIImage imageNamed:@"watchlist_icon_kirpich"];
        tickerAlertStateImage = [UIImage imageNamed:@"watchlist_icon_tiker_alert"];
        tickerIsDelayed = [UIImage imageNamed:@"icon_15min_accessory"];
        tickerInfo = [UIImage imageNamed:@"disclosure_icon"];
        
            //delayedImageView = [[UIImageView alloc] initWithImage:nil];
        
        [self setState:ME_TICKER_HAS_NO_LEVEL2];
        
            // Initialization code
        self.tickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 320, 10)];
        self.boardLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.changeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.tickerLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:20];
        self.priceLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:24];
        self.changeLabel.font = [UIFont fontWithName:ME_UI_FONT size:10];
        self.changeLabel.minimumScaleFactor = 0.5;
        self.changeLabel.adjustsFontSizeToFitWidth = YES;
        
        self.boardLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.boardLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        self.boardLabel.minimumScaleFactor=0.5;
        self.boardLabel.adjustsFontSizeToFitWidth = YES;
        
            //[self.contentView addSubview:delayedImageView];
        [self.contentView addSubview:self.tickerLabel];
        [self.contentView addSubview:self.boardLabel];
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.changeLabel];
        
            //delayedImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.tickerLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.boardLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.changeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.priceLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.priceLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-1]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[price]" options:0 metrics:0 views:@{@"price": self.priceLabel}]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.changeLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.changeLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-1]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[price]-(>=2)-[change]" options:0 metrics:0 views:@{@"change": self.changeLabel, @"price": self.priceLabel}]];
        
            //[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[imageView]" options:0 metrics:0 views:@{@"imageView": delayedImageView}]];
            //[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[imageView]-5-[ticker(<=180)]-(>=2)-[price]" options:0 metrics:0 views:@{@"imageView": delayedImageView, @"ticker": self.tickerLabel, @"price": self.priceLabel}]];
        
        //NSMutableString *tp = [NSMutableString stringWithFormat:@"H:|-5-[ticker(<=180)]-(>=2)-[price]"];
        NSMutableString *tp = [NSMutableString stringWithFormat:@"H:|-15-[ticker]-(>=10)-[price]"];
        
//        if (self.accessoryView == nil ) {
//            [tp appendFormat:@"-(10)-|"];
//        }
//
        [self.contentView addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:tp
          options:0 metrics:0 views:@{@"ticker": self.tickerLabel, @"price": self.priceLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[board]-(>=8)-[change]" options:0 metrics:0 views:@{@"board": self.boardLabel,@"change": self.changeLabel}]];
        
            //[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:delayedImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
            //                                                                toItem:self.tickerLabel  attribute:NSLayoutAttributeCenterY
            //                                                            multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.tickerLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.priceLabel  attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.boardLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.changeLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
    }
    return self;
}

@end

const int ACCESORY_MARGIN = -10;
const int LABEL_MARGIN = -10;

@implementation MEWatchListViewWideCell


- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame;
    frame = self.textLabel.frame;
    frame.origin.x += LABEL_MARGIN;
    frame.size.width -= 2 * LABEL_MARGIN;
    self.textLabel.frame = frame;
    
    frame = self.detailTextLabel.frame;
    frame.origin.x += LABEL_MARGIN;
    frame.size.width -= 2 * LABEL_MARGIN;
    self.detailTextLabel.frame = frame;
    
    if (self.accessoryType != UITableViewCellAccessoryNone)
    {
        float estimatedAccesoryX = MAX(self.textLabel.frame.origin.x + self.textLabel.frame.size.width, self.detailTextLabel.frame.origin.x + self.detailTextLabel.frame.size.width);
        
        for (UIView *subview in self.subviews) {
            if (subview != self.textLabel &&
                subview != self.detailTextLabel &&
                subview != self.backgroundView &&
                subview != self.contentView &&
                subview != self.selectedBackgroundView &&
                subview != self.imageView &&
                subview.frame.origin.x > estimatedAccesoryX) {
                frame = subview.frame;
                frame.origin.x -= ACCESORY_MARGIN;
                subview.frame = frame;
                break;
            }
        }
    }
}

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithreuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        BOOL    hasWide = (fmax(SCREEN_WIDTH, SCREEN_HEIGHT)>480);
        BOOL    hasSuperWide = (fmax(SCREEN_WIDTH, SCREEN_HEIGHT)>586);
        CGFloat priceX = (hasWide?-250:-90);
        CGFloat priceFont = hasSuperWide?32:24;
        
        self.priceLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:priceFont];
        
        self.bidLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 10, 10)];
        
        self.bidDepthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.offerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.offerDepthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.updateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        
        self.changeLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:12];
        
        [self.contentView addSubview:self.bidLabel];
        self.bidLabel.translatesAutoresizingMaskIntoConstraints=NO;
        
        [self.contentView addSubview:self.bidDepthLabel];
        self.bidDepthLabel.translatesAutoresizingMaskIntoConstraints=NO;
        
        [self.contentView addSubview:self.offerLabel];
        self.offerLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:self.offerDepthLabel];
        self.offerDepthLabel.translatesAutoresizingMaskIntoConstraints=NO;
        
        [self.contentView addSubview:self.updateTimeLabel];
        
        self.updateTimeLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.updateTimeLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        self.updateTimeLabel.textColor = ME_UI_GREY_FONT_COLOR;
        
        self.bidLabel.font =  self.offerLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        self.offerDepthLabel.font = self.bidDepthLabel.font = self.offerLabel.font;
        
        if (!hasWide) {
            self.bidDepthLabel.hidden = self.bidLabel.hidden = self.offerDepthLabel.hidden = self.offerLabel.hidden = YES;
        }
        
        [self.contentView removeConstraints:self.contentView.constraints];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.priceLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.priceLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:priceX]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.tickerLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.tickerLabel.superview attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=6)-[change]" options:0 metrics:0 views:@{@"change": self.changeLabel, @"price": self.priceLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(>=6)-[time]" options:0 metrics:0 views:@{@"time": self.updateTimeLabel, @"price": self.priceLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[price]-5-|" options:0 metrics:0 views:@{@"price": self.priceLabel}]];
        
            //[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[ticker]-(>=4)-[price]" options:0 metrics:0 views:@{@"ticker": self.tickerLabel,@"price": self.priceLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[ticker]" options:0 metrics:0 views:@{@"ticker": self.tickerLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[board]" options:0 metrics:0 views:@{@"board": self.boardLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[ticker]-(>=4)-[board]" options:0 metrics:0 views:@{@"board": self.boardLabel, @"ticker": self.tickerLabel}]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[ticker]-(>=10)-[price]" options:0 metrics:0 views:@{@"ticker": self.tickerLabel, @"price": self.priceLabel}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.changeLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.tickerLabel attribute:NSLayoutAttributeTop
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.updateTimeLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.boardLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.changeLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bidLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bidDepthLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.updateTimeLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.offerDepthLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bidDepthLabel attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1 constant:0]];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.bidLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-70]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bidDepthLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.bidDepthLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-70]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.offerDepthLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-2]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.offerDepthLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.offerDepthLabel.superview attribute:NSLayoutAttributeRight multiplier:1 constant:-2]];
        
    }
    return self;
}


@end


