//
//  MESearchBarViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/12/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METableView.h"

typedef enum {
    ME_SEARCH_BAR_BUTTON_CANCEL,
    ME_SEARCH_BAR_BUTTON_EDIT,
    ME_SEARCH_BAR_BUTTON_DONE,
    ME_SEARCH_BAR_BUTTON_UNKNOWN
}MESearchBarButtonState;

@class MESearchBarViewController;

@protocol MESearchBarControllerProtocol <NSObject>
/**
 *  Delegate when search bar will drag down an show
 *  
 *  @param searchBar search bar.
 */
- (void) searchBarWillShow:(MESearchBarViewController *)searchBar;

/**
 *  Delegate when search bar draged down and has shown.
 *  
 *  @param searchBar search bar.
 */
- (void) searchBarDidShow:(MESearchBarViewController *)searchBar;

/**
 *  @see UISearchBarDelegate description.
 *  
 */
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar;

/**
 *  @see UISearchBarDelegate description.
 *  
 */
- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar;

/**
 *  Delegate when edit button clicked on search bar panel;
 *  
 *  @param searchBar search bar.
 *  @param state     state.
 */
- (void) searchBarEditButtonClicked:(UISearchBar*)searchBar withState:(MESearchBarButtonState)state;
- (void) searchBarCancel:(UISearchBar*)searchBar;

@end

@interface MESearchBarViewController : UIViewController
@property(nonatomic,strong) UISearchBar* searchBar;
@property(nonatomic,strong) UISegmentedControl *scopeBar;
@property(nonatomic,strong) METableView* searchTableView;
@property(nonatomic,weak) UITableView* tableView;

@property(nonatomic,strong) id<MESearchBarControllerProtocol> delegate;

- (void) hideKeyboard;
- (void) hideSearchTableView;

@end
