//
//  METickerInfoTableView.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 13/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METableView.h"
#import "METicker.h"
#import "METickerInfoProtocol.h"

@interface METickerInfoTableView : METableView
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,strong) id<METickerInfoProtocol> infoDelegate;
- (void) update;
@end
