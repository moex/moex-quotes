//
//  METickerMainInfoView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METicker.h"

@interface METickerMainInfoView : UIView
- (void) update;
@property (nonatomic) UIViewController *rootController;
@end
