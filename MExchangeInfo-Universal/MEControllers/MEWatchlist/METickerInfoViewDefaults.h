//
//  METickerInfoViewDefaults.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 12/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#ifndef MExchange_MosQuotes_METickerInfoViewDefaults_h
#define MExchange_MosQuotes_METickerInfoViewDefaults_h

#define ME_UI_TICKERINFO_HEIGHT 128
#define ME_UI_TOOLBAR_HEIGHT    44

#endif
