//
//  METickerInfoViewCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METickerInfoViewCell.h"
#import "MEUIPreferences.h"
#import "METickerInfoViewDefaults.h"
#import "MESettings.h"

@implementation METickerInfoViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [self initWithreuseIdentifier:reuseIdentifier];
    [self addDevider];
    return self;
}

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        _conetntInsets = UIEdgeInsetsMake(5, 10, 7, 10);
        
        self.accessoryType = UITableViewCellAccessoryNone;
        
        // Initialization code
        self.rowCaption = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 320, 10)];
        self.rowValue = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.rowCaption.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowCaption.textColor = [UIColor colorWithRed:145./256. green:145./256. blue:145./256. alpha:1.];
        self.rowCaption.font = [UIFont fontWithName:ME_UI_FONT size:14];
    
        self.rowCaption.minimumScaleFactor = 0.7;
        self.rowCaption.adjustsFontSizeToFitWidth = YES;
        self.rowCaption.numberOfLines = 10;
        self.rowCaption.textAlignment = NSTextAlignmentLeft;

        self.rowCaption.layer.shouldRasterize = YES;
        self.rowCaption.layer.rasterizationScale = [UIScreen mainScreen].scale;

        self.rowValue.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowValue.font = [UIFont fontWithName:ME_UI_FONT size:18];
        self.rowValue.textAlignment = NSTextAlignmentLeft;

        self.rowValue.minimumScaleFactor = 0.5;
        self.rowValue.adjustsFontSizeToFitWidth = YES;
        self.rowValue.numberOfLines = 3;

        self.rowValue.layer.shouldRasterize = YES;
        self.rowValue.layer.rasterizationScale = [UIScreen mainScreen].scale;

        [self.contentView addSubview:self.rowCaption];
        [self.contentView addSubview:self.rowValue];
        
        [self.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:@"H:|-(left)-[caption]-(8)-[value(120)]-(right)-|"
                                          options:0 metrics:@{@"left": [NSNumber numberWithFloat:_conetntInsets.left], @"right": [NSNumber numberWithFloat:_conetntInsets.right]}
                                          views:@{@"caption": self.rowCaption, @"value": self.rowValue}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-(>=top)-[caption]-(>=bottom)-|"
                                          options:0
                                          metrics:@{@"top": [NSNumber numberWithFloat:_conetntInsets.top], @"bottom":[NSNumber numberWithFloat:_conetntInsets.bottom]}
                                          views:@{@"caption": self.rowCaption}]];
        [self.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-(>=top)-[value]-(>=bottom)-|"
                                          options:0
                                          metrics:@{@"top": [NSNumber numberWithFloat:_conetntInsets.top], @"bottom":[NSNumber numberWithFloat:_conetntInsets.bottom]}
                                          views:@{@"value": self.rowValue}]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowCaption attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowValue attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

@implementation METickerInfoDoubleViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [self initWithreuseIdentifier:reuseIdentifier];
    [self addDevider];
    return self;
}

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        float fontCaption = 12;
        float fontValue  = 18;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            fontValue = 24;
            fontCaption = 14;
        }
        
        _conetntInsets = UIEdgeInsetsMake(5, 10, 7, 10);
        
        self.accessoryType = UITableViewCellAccessoryNone;
        
        // Initialization code
        self.rowCaptionLeft = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 320, 10)];
        self.rowValueLeft = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.rowCaptionLeft.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowCaptionLeft.textColor = [UIColor colorWithRed:145./256. green:145./256. blue:145./256. alpha:1.];
        self.rowCaptionLeft.font = [UIFont fontWithName:ME_UI_FONT size:fontCaption];
        
        self.rowCaptionLeft.minimumScaleFactor = 0.8;
        self.rowCaptionLeft.adjustsFontSizeToFitWidth = YES;
        self.rowCaptionLeft.numberOfLines = 10;
        self.rowCaptionLeft.textAlignment = NSTextAlignmentRight;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            self.rowCaptionLeft.textAlignment = NSTextAlignmentLeft;
        }

        self.rowCaptionLeft.layer.shouldRasterize = YES;
        self.rowCaptionLeft.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.rowValueLeft.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowValueLeft.font = [UIFont fontWithName:ME_UI_FONT size:fontValue];
        self.rowValueLeft.textAlignment = NSTextAlignmentRight;
        
        self.rowValueLeft.minimumScaleFactor = 0.5;
        self.rowValueLeft.adjustsFontSizeToFitWidth = YES;
        self.rowValueLeft.numberOfLines = 3;
        
        self.rowValueLeft.layer.shouldRasterize = YES;
        self.rowValueLeft.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        [self.contentView addSubview:self.rowCaptionLeft];
        [self.contentView addSubview:self.rowValueLeft];
        
        
        self.rowCaptionRight = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 320, 10)];
        self.rowValueRight = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.rowCaptionRight.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowCaptionRight.textColor = self.rowCaptionLeft.textColor;
        self.rowCaptionRight.font = self.rowCaptionLeft.font;
        
        self.rowCaptionRight.minimumScaleFactor = self.rowCaptionLeft.minimumScaleFactor;
        self.rowCaptionRight.adjustsFontSizeToFitWidth = YES;
        self.rowCaptionRight.numberOfLines = self.rowCaptionLeft.numberOfLines;
        self.rowCaptionRight.textAlignment = NSTextAlignmentLeft;
        
        self.rowCaptionRight.layer.shouldRasterize = YES;
        self.rowCaptionRight.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.rowValueRight.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowValueRight.font = self.rowValueLeft.font;
        self.rowValueRight.textAlignment = NSTextAlignmentLeft;
        
        self.rowValueRight.minimumScaleFactor = self.rowValueLeft.minimumScaleFactor;
        self.rowValueRight.adjustsFontSizeToFitWidth = YES;
        self.rowValueRight.numberOfLines = self.rowValueLeft.numberOfLines;
        
        self.rowValueRight.layer.shouldRasterize = YES;
        self.rowValueRight.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        [self.contentView addSubview:self.rowCaptionRight];
        [self.contentView addSubview:self.rowValueRight];
        
//        if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM()) {
//            [self.contentView addConstraint:[NSLayoutConstraint
//                                             constraintWithItem:self.contentView
//                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                             toItem:self.rowValueLeft
//                                             attribute:NSLayoutAttributeTrailing
//                                             multiplier:1 constant:8]];
//            
//            [self.contentView addConstraint:[NSLayoutConstraint
//                                             constraintWithItem:self.contentView
//                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                             toItem:self.rowCaptionRight
//                                             attribute:NSLayoutAttributeLeading
//                                             multiplier:1 constant:-8]];
//            
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"H:|-(left)-[caption]-(8)-[value(160)]"
//                                              options:0 metrics:@{@"left": [NSNumber numberWithFloat:_conetntInsets.left], @"right": [NSNumber numberWithFloat:_conetntInsets.right]}
//                                              views:@{@"caption": self.rowCaptionLeft, @"value": self.rowValueLeft}]];
//            
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"H:[caption]-(8)-[value(160)]-(right)-|"
//                                              options:0 metrics:@{@"left": [NSNumber numberWithFloat:_conetntInsets.left], @"right": [NSNumber numberWithFloat:_conetntInsets.right]}
//                                              views:@{@"caption": self.rowCaptionRight, @"value": self.rowValueRight}]];
//
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"V:|-[caption]-|"
//                                              options:0 metrics: nil
//                                              views:@{@"caption": self.rowCaptionRight}]
//             ];
//
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"V:|-[caption]-|"
//                                              options:0 metrics: nil
//                                              views:@{@"caption": self.rowCaptionLeft}]
//             ];
//
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"V:|-[value]-|"
//                                              options:0 metrics: nil
//                                              views:@{@"value": self.rowValueLeft}]
//             ];
//            
//            [self.contentView addConstraints:[NSLayoutConstraint
//                                              constraintsWithVisualFormat:@"V:|-[value]-|"
//                                              options:0 metrics: nil
//                                              views:@{@"value": self.rowValueRight}]
//             ];
//
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowCaptionLeft attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//            
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowValueLeft attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//            
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowCaptionRight attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//            
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowValueRight attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
//        }
//        else{
        
//            BOOL printHint = [[MESettings sharedInstance] showHintName].boolValue;
//            float h = printHint?50:30;

            //
            // x
            //
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowCaptionLeft
                                             attribute:NSLayoutAttributeTrailing
                                             multiplier:1 constant:8]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowCaptionLeft
                                             attribute:NSLayoutAttributeLeading
                                             multiplier:1 constant:-8]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowValueLeft
                                             attribute:NSLayoutAttributeTrailing
                                             multiplier:1 constant:8]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowValueLeft
                                             attribute:NSLayoutAttributeLeading
                                             multiplier:1 constant:-4]];


            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowCaptionRight
                                             attribute:NSLayoutAttributeLeading
                                             multiplier:1 constant:-8]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowCaptionRight
                                             attribute:NSLayoutAttributeTrailing
                                             multiplier:1 constant:8]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.contentView
                                             attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowValueRight
                                             attribute:NSLayoutAttributeLeading
                                             multiplier:1 constant:-8]];
         
//            [self.contentView addConstraint:[NSLayoutConstraint
//                                             constraintWithItem:self.rowValueRight
//                                             attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual
//                                             toItem:self.contentView
//                                             attribute:NSLayoutAttributeRight
//                                             multiplier:1 constant:-4]];

            
            //
            // y
            //

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowCaptionLeft
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1 constant:4]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowValueLeft
                                             attribute:NSLayoutAttributeHeight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:nil
                                             attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1 constant:fontValue+4]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowCaptionLeft
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowValueLeft
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1 constant:-4]];


            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowCaptionRight
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.rowValueRight
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1 constant:-4]];
            

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowValueRight
                                             attribute:NSLayoutAttributeHeight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:nil
                                             attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1 constant:fontValue+4]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowValueLeft
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1 constant:-4]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowCaptionRight
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1 constant:4]];

            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.rowValueRight
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1 constant:-4]];

//        }

        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
