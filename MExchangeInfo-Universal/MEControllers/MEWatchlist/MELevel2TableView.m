//
//  METickerLevel2Table.m
//  MExchange
//
//  Created by denn on 5/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MELevel2TableView.h"
#import "MEUIPreferences.h"
#import "MELevel2ViewCell.h"
#import "MELevel2Model.h"
#import "MESettings.h"
#import "MEWatchListModel.h"

#define __ME_LEVEL2_IS_EMPTY NSLocalizedString(@"Level2 has nothing to update for the ticker", @"")

static NSString *ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID=@"ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID";
static NSString *ME_LEVEL2SPLITER_TABLEVIEW_HEADERVIEW_ID=@"ME_LEVEL2SPLITER_TABLEVIEW_HEADERVIEW_ID";

@interface MELevel2TableViewSectionHeaderView : UITableViewHeaderFooterView
@property(strong,nonatomic) UILabel *bidLabel;
@property(strong,nonatomic) UILabel *offerLabel;
@property(nonatomic,assign) CGFloat height;
@end

@implementation MELevel2TableViewSectionHeaderView
{
    UIView *bidOfferViewSplitter;
    UIFont *bidOfferFontSplitter;
    NSInteger updateCounter;
}

- (id) initWithReuseIdentifier:(NSString *)reuseIdentifier{
    
    static NSMutableDictionary *reusedHeaderViews = nil; if (!reusedHeaderViews) reusedHeaderViews = [NSMutableDictionary dictionaryWithCapacity:2];
    
    id view = [reusedHeaderViews valueForKey:reuseIdentifier];
    if (view) {
        self = view;
        return self;
    }
    
    
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        _height = 0;
        
        if (reuseIdentifier == ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID) {
            self.frame = CGRectMake(0, 0, self.bounds.size.width, 0);
            [reusedHeaderViews setValue:self forKey:reuseIdentifier];
            return self;
        }
        
        UIImageView *_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"devider_fanciful"]];
        
        _height = _bg.bounds.size.height;
        
        bidOfferFontSplitter = [UIFont fontWithName:ME_UI_FONT size:10];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:1. alpha:0.8];
        
        _bg.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_bg];
        
        [self.contentView addConstraint:[NSLayoutConstraint 
                                         constraintWithItem:_bg
                                         attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual 
                                         toItem:self.contentView 
                                         attribute:NSLayoutAttributeCenterX 
                                         multiplier:1 constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint 
                                         constraintWithItem:_bg 
                                         attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual 
                                         toItem:self.contentView 
                                         attribute:NSLayoutAttributeCenterY 
                                         multiplier:1 constant:0]];

        
        self.bidLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,  4, 110, 12)];
        self.offerLabel = [[UILabel alloc] initWithFrame:CGRectMake(195, 4, 110, 12)];
        
        self.bidLabel.textAlignment = NSTextAlignmentRight;
        self.offerLabel.textAlignment = NSTextAlignmentLeft;
        
        self.bidLabel.font = self.offerLabel.font = bidOfferFontSplitter;
        
        self.offerLabel.textColor = [UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101/255.0 alpha:1.0];
        self.bidLabel.textColor = self.offerLabel.textColor;
        
        self.bidLabel.text = @"Sell";// [currentBook.structure.stream.fields[@"BID"] caption];
        self.offerLabel.text = @"Buy";//  [currentBook.structure.stream.fields[@"OFFER"] caption];
        
        self.bidLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.offerLabel.translatesAutoresizingMaskIntoConstraints=NO;
        
        [self addSubview:self.bidLabel];
        [self addSubview:self.offerLabel];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:-50]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:50]];
        
        self.frame = CGRectMake(0, 0, self.bounds.size.width, _bg.bounds.size.height);
    }
    
    [reusedHeaderViews setValue:self forKey:reuseIdentifier];
    
    
    return self;
}

@end

@interface MELevel2TableView() <UITableViewDataSource, UITableViewDelegate, MELevel2ModelProtocol>
@property (nonatomic,readonly) MELevel2Model *level2Model;
@property(nonatomic,readonly) NSString *lastError;
@property(nonatomic,readonly) MEWatchListModel *watchList;
//@property(readonly) METicker         *ticker;
@end

@implementation MELevel2TableView
{
    NSInteger updateCounter;    
    BOOL _isScrolling;    
    NSTimeInterval timeBegining;
    BOOL isCurrentellyUpdated;
}

@synthesize level2Model=_level2Model, ticker=_ticker, watchList=_watchList;

- (MEWatchListModel*) watchList{
    if (!_watchList) {
        _watchList = [MEWatchListModel sharedInstance];
    }
    return _watchList;
}

- (METicker*) ticker{
    return self.watchList.activeTicker;
}

- (NSString*) lastError{
    NSString *err= self.level2Model.lastError;
    if (err) {
        return err;
    }
    
    return __ME_LEVEL2_IS_EMPTY;
}

#pragma mark - Level2 Model protocol

- (void) didLevel2Model:(MELevel2Model *)level2 fault:(NSError *)error{    
    dispatch_async(dispatch_get_main_queue(), ^{                
        self.level2Model.lastError = error.localizedDescription;        
        self.messageText = self.lastError;
        [self reloadData];
    });
}

- (void) didLevel2ModelUpdate:(MELevel2Model *)level2{

    dispatch_async(dispatch_get_main_queue(), ^{        
        NSString *mess = self.messageText;
        NSUInteger count = [self.level2Model allRows];
                
        if (mess && count>0) {
            self.messageText = nil;
        }
        else if (mess == nil && count ==0){
            self.messageText = self.lastError;
        }
        
        [self reloadData];                
    });
}


- (MELevel2Model*) level2Model{
    if (!_level2Model) {
        _level2Model = [MELevel2Model sharedInstance];
        _level2Model.delegate = self;
    }
    return _level2Model;
}

- (void) resetTicker{
    self.level2Model.ticker = self.ticker;
    [self.level2Model subscribe];        
    [self.level2Model beginTransaction];   
    updateCounter=0;
}

- (void) setIsActive:(BOOL)isActive{
    _isActive = isActive;
        
    if (self.ticker==nil) {
        return;
    }
        
    if (_isActive) {        
        [self resetTicker];
        dispatch_async(dispatch_get_main_queue(), ^{            
            [self reloadData];
        });
    }
    else{
        updateCounter = 0;
        [self.level2Model unsubscribe];
    }
}

#pragma mark - UIView/UITableView

- (void) deferredReloadData{
    
    if (self.isDragging) 
        return;        
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadData) object:nil];    
    [super reloadData];  
    
    
    isCurrentellyUpdated = YES;
    
    if (updateCounter==0) {
        [self scrollToSplitPosition:NO];
    }
    
    updateCounter++;
}

- (void) update{
    [self resetTicker];
    [self deferredReloadData];
}

- (void) reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        //@synchronized(self.level2Model){
            if (self.isActive && !self.isDecelerating && !self.isDragging) {
                
                if (self->isCurrentellyUpdated) {
                    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredReloadData) object:nil];                    
                }
                self->isCurrentellyUpdated = NO;
                [self performSelector:@selector(deferredReloadData) withObject:nil afterDelay:ME_UI_UPDATE_RATE];
            }
            else if (self.isActive){
                [self.level2Model commitTransaction];
            }
        //}
    });
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    
    self = [super initWithFrame:frame style:style];
    if (self) {
        // Initialization code
        self.dataSource = self;
        self.delegate = self;
        
        updateCounter = 0;
        _isScrolling = NO;
        
        [self registerClass:[MELevel2TableViewSectionHeaderView class] forHeaderFooterViewReuseIdentifier:ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID];
        [self registerClass:[MELevel2TableViewSectionHeaderView class] forHeaderFooterViewReuseIdentifier:ME_LEVEL2SPLITER_TABLEVIEW_HEADERVIEW_ID];
        [self registerClass:[MELevel2ViewCell class] forCellReuseIdentifier:ME_BID_LEVEL2_CELL];
        [self registerClass:[MELevel2ViewCell class] forCellReuseIdentifier:ME_OFFER_LEVEL2_CELL];
        
        self.enableTopShadowOnly=YES;
        
        self.messageText = @"";//__ME_LEVEL2_IS_EMPTY;
        
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.allowsSelection = NO;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];

    }
    return self;
}

- (void) loginSuccessed:(NSNotification*)event{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.level2Model subscribe];
    });
}


#pragma mark - UITableView datasource

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return 40;
    }
    return 28;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    @synchronized(self.level2Model){
        
        MELevel2TableViewSectionHeaderView *hv;
        
        MELevel2Type type = [self.level2Model level2TypeForSection:section];
        
        if (type==MELEVEL2_OFFER)
            hv=[tableView dequeueReusableHeaderFooterViewWithIdentifier:ME_LEVEL2SPLITER_TABLEVIEW_HEADERVIEW_ID];
        else
            hv=[tableView dequeueReusableHeaderFooterViewWithIdentifier:ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID];
        
        GIField *field = [self.level2Model level2Book].bidAskField;
        if (field.enumType) {
            hv.bidLabel.text = [field.enumType.values[@"B"] hint];
            hv.offerLabel.text = [field.enumType.values[@"S"] hint];
        }
        return hv;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    @synchronized(self.level2Model){
        
        MELevel2TableViewSectionHeaderView *hv;
        if (section==1)
            hv=[tableView dequeueReusableHeaderFooterViewWithIdentifier:ME_LEVEL2SPLITER_TABLEVIEW_HEADERVIEW_ID];
        else
            hv=[tableView dequeueReusableHeaderFooterViewWithIdentifier:ME_LEVEL2_TABLEVIEW_HEADERVIEW_ID];
        
        return hv.height;
    }
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @synchronized(self.level2Model){
        
        MELevel2ViewCell *_cell;
        NSString *_cellId;
        NSArray  *rows;
        MELevel2 *level2 = [self.level2Model level2ForIndexPath:indexPath];
        
        if (indexPath.section==0) {
            _cellId = ME_BID_LEVEL2_CELL;
        }
        else if (indexPath.section==1){
            _cellId = ME_OFFER_LEVEL2_CELL;
        }
        
        _cell =  (MELevel2ViewCell*)[tableView dequeueReusableCellWithIdentifier:_cellId forIndexPath:indexPath];
        
        
        UILabel  *_price = _cell.price;
        UILabel  *_qty = _cell.qty;
        UISlider *pv   = _cell.qtyTherm;
        
        @try {
            
            if (indexPath.section==0) {
                
                if (indexPath.row==(rows.count-1))
                    _price.textColor = [UIColor colorWithRed:100.0/255.0 green:158.0/255.0 blue:0.0 alpha:1];
                
            }
            else if (indexPath.section==1 && indexPath.row==0)
                _price.textColor = [UIColor colorWithRed:207.0/255.0 green:34.0/255.0 blue:0.0 alpha:1.0];
            
            else
                _price.textColor = [UIColor blackColor];
            
            if (indexPath.row==(rows.count-1))
                _cell.devider.alpha = 0.0;
            
            else
                _cell.devider.alpha = 1.0;
            
            _price.text = [level2.price string];
            _qty.text = [level2.qty string];
            
            float ratio = level2.ratio;
            
            if (ratio>=0.0){
                [pv setValue:ratio animated:NO];                                     
            }
            
            if (indexPath.section==0) {
                ratio=1-ratio;
            }            
            [pv setValue:ratio animated:NO];                                     
        }
        @catch (NSException *exception) {
            NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        }
        
        NSIndexPath *lastRows=[[self indexPathsForVisibleRows] lastObject];
        
        if (indexPath.section>=lastRows.section && indexPath.row>=lastRows.row) {
            [self.level2Model commitTransaction];
        }
        
        return _cell;

    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @synchronized(self.level2Model){
        return [self.level2Model rowsForSection:section];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    @synchronized(self.level2Model){
        [self.level2Model beginTransaction];
        return [self.level2Model sections];
    }
}

#pragma mark - UIScrolView delegate

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _isScrolling = YES;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [self cancelScrollTpSplitposition];
    
    if (_isScrolling){
        [self reloadData];
        [self scrollToSplitPositionAfterDelay:ME_DEFERRED_SCROLL_DELAY];
    }
    _isScrolling = NO;
}

- (void) scrollToSplitPosition{
    [self scrollToSplitPosition:YES];
}

- (void) scrollToSplitPosition:(BOOL)animate{
    CGFloat y_offset = (self.contentSize.height-self.bounds.size.height)/2.0;
    if (self.contentSize.height<=self.bounds.size.height) {
        return;
    }
    
    CGPoint _center = CGPointMake(0, y_offset);
    if (animate) {
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self.contentOffset = _center;
        }];        
    }
    else {
        self.contentOffset = _center;
    }
}

- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (self.contentSize.height<=self.bounds.size.height) {
        [self reloadData];
        [self scrollToSplitPosition:YES];
        return;
    }
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (_isScrolling){
        [self reloadData];
        [self scrollToSplitPositionAfterDelay:ME_DEFERRED_SCROLL_DELAY];
    }
    _isScrolling = NO;
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.infoDelegate tickerInfoDidScroll:scrollView];
}


- (void) cancelScrollTpSplitposition{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollToSplitPosition) object:nil];
}

- (void) scrollToSplitPositionAfterDelay:(NSTimeInterval)delay{
    [self cancelScrollTpSplitposition];
    if (delay>0) {
        [self performSelector:@selector(scrollToSplitPosition) withObject:nil afterDelay:delay];        
    }
    else{
        [self scrollToSplitPosition:NO];
    }
}

- (BOOL) scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    return NO;
}

@end
