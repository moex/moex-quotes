//
//  AMPageViewController.h
//  Atom
//
//  Created by denis svinarchuk on 17/07/14.
//  Copyright (c) 2014 Atom-M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METickerInfoViewDefaults.h"

@interface MEScrollView : UIScrollView
@end

@interface MEPageViewController : UIViewController
@property (readonly, nonatomic) MEScrollView *pageScrollView;
@property (readonly, nonatomic) UIToolbar    *toolBar;
@property (assign, nonatomic  ) NSInteger    currentPage;
@property (readonly,nonatomic)  BOOL         toolbarHidden;

- (BOOL) shouldHideWhenRotation;
- (void) setToolbarHidden:(BOOL)hidden animated:(BOOL) animated;
- (void) toggleToolBar;
- (void) pageWillChange:(NSInteger)page;
- (void) pageDidChange:(NSInteger)page;
- (void) setCurrentPage:(NSInteger)currentPage animate:(BOOL)animate;
@end
