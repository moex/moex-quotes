//
//  METickerInfoTableView.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 13/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerInfoTableView.h"
#import "METickerInfoViewCell.h"
#import "MESettings.h"
#import "MEUIPreferences.h"
#import "MEWatchListModel.h"
#import "NSAttributedString+MEFormatedString.h"

@interface METickerInfoTableView() <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak           ) MESettings       *settings;
@property (nonatomic,readonly       ) MEWatchListModel *watchList;
@property (nonatomic,readonly       ) METicker         *ticker;
@property (nonatomic,assign         ) BOOL             showHintName;
@property (nonatomic,assign         ) BOOL             isReloading;
@end

//static inline NSString* _attributedKey(NSString* name, NSIndexPath *indexPath){
//    return [NSString stringWithFormat:@"%li:%li-%@", (long)indexPath.row,(long)indexPath.section, name];
//}

@implementation METickerInfoTableView
{
    NSMutableDictionary *cachedCells;
    UILongPressGestureRecognizer *longPress;
}
@synthesize watchList=_watchList;

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    
    self = [super initWithFrame:frame style:style];
    if (self) {
        // Initialization code

        cachedCells = [[NSMutableDictionary alloc] init];

        self.settings = [MESettings sharedInstance];
        self.showHintName = self.settings.showHintName.boolValue;

        self.dataSource = self;
        self.delegate = self;
        
        [self registerClass:[METickerInfoDoubleViewCell class] forCellReuseIdentifier:ME_TICKER_DOUBLE_INFO_CELL];
        
        self.enableTopShadowOnly=YES;
        self.messageText = @"";
        
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.allowsSelection = NO;

        longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
        [self addGestureRecognizer:longPress];
                
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChanged:) name:ME_SETTINGS_CHANGED_NOTIFICATION object:nil];
    }
    return self;
}

- (MEWatchListModel*) watchList{
    if (!_watchList) {
        _watchList = [MEWatchListModel sharedInstance];
    }
    return _watchList;
}

- (METicker*) ticker{
    return self.watchList.activeTicker;
}

- (void) longPressAction:(UILongPressGestureRecognizer*)gesture{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self.settings setShowHintName:[NSNumber numberWithBool:![self.settings showHintName].boolValue]];
        self.showHintName = self.settings.showHintName.boolValue;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded){
        //[self update];
    }
}

- (void) settingsChanged: (NSNotification*)event{
    self.isReloading = YES;
    [cachedCells removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self
                          duration:ME_UI_ANIMATION_DURATION
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self reloadData];
                        }
                        completion:^(BOOL finished) {
                            self.isReloading = NO;
                        }];
    });
}

- (void) update{
    if ([self checkRows]) {
        if (self.isReloading==NO)
            [self reloadData];
    }
}

- (void) deferredMessage{
    self.messageText = [NSString stringWithFormat:NSLocalizedString(@"Ticker has nothing to update for that time", @"")];
}

- (BOOL) checkRows{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(deferredMessage) object:nil];    
    if (self.ticker.rows.count==0) {
        if (self.ticker.lastError) {
            self.messageText = self.ticker.lastError.localizedDescription;
            return NO;
        }
        else{
            [self performSelector:@selector(deferredMessage) withObject:nil afterDelay:5.0];            
            return NO;
        }
    }
    else{
        self.messageText = nil;
    }
    return YES;
}


#pragma UITableView DataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    if (!self.ticker)
        return 0;
    
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!self.ticker)
        return 0;
    
    return (NSInteger)ceilf(self.ticker.rows.count/2.0);
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    METickerInfoDoubleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ME_TICKER_DOUBLE_INFO_CELL forIndexPath:indexPath];
    
    @try {
        MEValue *valueLeft  = [self.ticker.rows objectAtIndex:indexPath.row*2];
        NSInteger next  = indexPath.row*2+1;
        MEValue *valueRight = next>=self.ticker.rows.count?nil:[self.ticker.rows objectAtIndex:next];
        
        BOOL printHint = self.showHintName;
        
        cell.rowCaptionLeft.text = printHint?valueLeft.field.hint:valueLeft.field.caption;
        cell.rowCaptionRight.text = valueRight?printHint?valueRight.field.hint:valueRight.field.caption:@"";
        
        void (^printValue)(MEValue* value, UILabel *rowLabel) = ^(MEValue* value, UILabel *rowLabel){
            if ([value.value isNull] || [[value.value string] isEqualToString:@"00:00:00"] )
                rowLabel.text = @"-";
            else{
                
                if (
                    !value.field.hasPercent &&
                    (value.field.type == GI_STOMP_FIELD_INT || value.field.type == GI_STOMP_FIELD_FLOAT || value.field.type == GI_STOMP_FIELD_FIXED))   {
                    if ([value.value floatValue]>1000000.0f) {
                        rowLabel.attributedText = [NSAttributedString compoundValToday:value.value withAbvColor:[UIColor grayColor]  andFont:rowLabel.font];
                    }
                    else
                        rowLabel.text = [value.value string];
                }
                else{
                    NSString *_string_value = [value.value string];
                    if ([value.field.name isEqualToString:@"TRADINGSTATUS"]) {
                        _string_value = [value.field.enumType.values[_string_value] hint];
                    }
                    else{
                        if (value.field.type==GI_STOMP_FIELD_ENUM && value.field.enumType) {
                            if (printHint)
                                _string_value = [value.field.enumType.values[_string_value] hint];
                            else
                                _string_value = [value.field.enumType.values[_string_value] caption];
                        }
                        else {
                            rowLabel.lineBreakMode = NSLineBreakByWordWrapping;
                        }
                    }
                    
                    if (value.field.hasPercent) {
                        _string_value = [NSString stringWithFormat:@"%@%%",_string_value];
                    }
                    
                    rowLabel.text = _string_value;
                }
            }
            
            if ([value.field.name isEqualToString:@"TRADINGSTATUS"] && [rowLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length==0) {
                rowLabel.text = [value.value string];
            }
            
            if (value.field.isHidden) {
                rowLabel.textColor = [UIColor greenColor];
            }
        };
        
        printValue(valueLeft, cell.rowValueLeft);
        printValue(valueRight, cell.rowValueRight);
        
        if (indexPath.row == (NSInteger)ceilf(self.ticker.rows.count/2.0)-1)
            cell.devider.alpha = 0.0;
        else
            cell.devider.alpha = 1.0;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return self.showHintName?80.0:60.0;
    
//    BOOL printHint = self.showHintName;
//
//    if (!printHint) {
//        return 44.0;
//    }
//    
//    NSString *k = _attributedKey(@"", indexPath);
//    
//    NSNumber *height = [cachedCells valueForKey:k];
//    
//    if (height)
//        return [height floatValue];
//    
//    METickerInfoDoubleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ME_TICKER_DOUBLE_INFO_CELL];
//    
//    MEValue *value = [self.ticker.rows objectAtIndex:indexPath.row];
//    
//    cell.rowCaption.text = printHint?value.field.hint:value.field.caption;
//    CGFloat fheight = [cell.rowCaption sizeThatFits:CGSizeMake(cell.rowCaption.bounds.size.width, 1000.)].height+cell.conetntInsets.bottom+cell.conetntInsets.top;
//    
//    if (fheight<44.)
//        fheight = 44.;
//    else
//        fheight+=cell.conetntInsets.bottom+cell.conetntInsets.top;
//    
//    height = [NSNumber numberWithFloat:fheight];
//    
//    [cachedCells setValue:height forKey:k];
//    
//    return fheight;
}

#pragma Scroll view delegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self update];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.infoDelegate tickerInfoDidScroll:scrollView];
}

@end
