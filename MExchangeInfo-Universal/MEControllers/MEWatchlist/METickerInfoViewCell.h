//
//  METickerInfoViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEBaseViewCell.h"

static NSString *ME_TICKER_INFO_CELL=@"tickerInfoCell";
static NSString *ME_TICKER_DOUBLE_INFO_CELL=@"tickerInfoDoubleCell";

@interface METickerInfoViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel *rowCaption;
@property(nonatomic,strong) UILabel *rowValue;
@property(readonly,nonatomic) UIEdgeInsets conetntInsets;
@end

@interface METickerInfoDoubleViewCell : MEBaseViewCell
@property(nonatomic,strong) UILabel *rowCaptionLeft;
@property(nonatomic,strong) UILabel *rowValueLeft;
@property(nonatomic,strong) UILabel *rowCaptionRight;
@property(nonatomic,strong) UILabel *rowValueRight;
@property(readonly,nonatomic) UIEdgeInsets conetntInsets;
@end
