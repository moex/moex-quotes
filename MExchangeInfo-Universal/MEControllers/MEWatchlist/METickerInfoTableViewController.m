//
//  METickerInfoTableViewController.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 12/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerInfoTableViewController.h"

@interface METickerInfoTableViewController ()
@property (strong, nonatomic)   METickerInfoTableView *tableView;
@end

@implementation METickerInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView = [[METickerInfoTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.tableView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:nil views:@{@"table":self.tableView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(height)-[table]-0-|"
                                                                      options:0
                                                                      metrics:@{@"height": [NSNumber numberWithInt:ME_UI_TICKERINFO_HEIGHT]}
                                                                        views:@{@"table":self.tableView, @"topGuide": self.topLayoutGuide}]];
}



@end
