//
//  METickerInfoTableViewController.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 12/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METableView.h"
#import "METickerInfoViewDefaults.h"
#import "METickerInfoTableView.h"

@interface METickerInfoTableViewController : UIViewController
@property (readonly, nonatomic)   METickerInfoTableView *tableView;
@end
