//
//  MELevel2ViewCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MELevel2ViewCell.h"
#import "MEUIPreferences.h"

@implementation MELevel2ViewCell

- (UIImage*) bidOfferProgressImage: (NSString*) name{
    return [[UIImage imageNamed: name] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code     
        
        self.price = [[UILabel alloc] init];
        self.qty = [[UILabel alloc] init];
        self.qtyTherm = [[UISlider alloc] init];
        
        self.price.translatesAutoresizingMaskIntoConstraints = NO;
        self.qty.translatesAutoresizingMaskIntoConstraints = NO;
        self.qtyTherm.translatesAutoresizingMaskIntoConstraints = NO;
        self.price.font = [UIFont fontWithName:ME_UI_FONT size:14];
        self.qty.font = [UIFont fontWithName:ME_UI_FONT size:13];
        self.qty.textColor = ME_UI_GREY_FONT_COLOR;
        
        self.price.textAlignment = NSTextAlignmentRight;
        
        self.price.text = @"PRICE";
        self.qty.text = @"QTY";
        self.qtyTherm.value = 0.5;
        self.qtyTherm.userInteractionEnabled = NO;        
        
        self.price.layer.shouldRasterize = YES;
        self.price.layer.rasterizationScale = [UIScreen mainScreen].scale;

        self.qty.layer.shouldRasterize = YES;
        self.qty.layer.rasterizationScale = [UIScreen mainScreen].scale;

        self.qtyTherm.layer.shouldRasterize = YES;
        self.qtyTherm.layer.rasterizationScale = [UIScreen mainScreen].scale;

        [self addSubview:self.price];
        [self addSubview:self.qty];
        [self addSubview:self.qtyTherm];
        
        NSDictionary *views = @{@"qty": self.qty, @"price": self.price, @"qtyTherm":self.qtyTherm};
        
        static UIImage *_blank = nil; if (!_blank) _blank = [self bidOfferProgressImage:@"progress_background"];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.price attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.price attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.qtyTherm attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.qty attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];

        if ([reuseIdentifier isEqualToString:ME_BID_LEVEL2_CELL]) {
            self.qty.textAlignment = NSTextAlignmentLeft;

            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(>=5)-[qtyTherm(96)]-5-[price]-5-[qty]" options:0 metrics:nil views:views]];
            
            [self.qtyTherm setMinimumTrackImage: _blank forState: UIControlStateNormal];
            [self.qtyTherm setMaximumTrackImage: [self bidOfferProgressImage:@"progress_bid"] forState: UIControlStateNormal];
            [self.qtyTherm setThumbImage: [UIImage imageNamed:@"progress_bid_left"] forState:UIControlStateNormal];

        }
        else{
            self.qty.textAlignment = NSTextAlignmentRight;

            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[qty]-5-[price]-5-[qtyTherm(96)]-(>=5)-|" options:0 metrics:nil views:views]];
            [self.qtyTherm setMaximumTrackImage: _blank forState: UIControlStateNormal];
            [self.qtyTherm setMinimumTrackImage: [self bidOfferProgressImage:@"progress_offer"] forState: UIControlStateNormal];
            [self.qtyTherm setThumbImage:[UIImage imageNamed:@"progress_offer_right"] forState:UIControlStateNormal];
        }        
    }
    return self;
}

@end
