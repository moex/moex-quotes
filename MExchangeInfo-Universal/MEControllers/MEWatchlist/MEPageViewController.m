//
//  MEPageViewController.m
//  MosQuotes
//
//  Created by denis svinarchuk on 17/07/14.
//  Copyright (c) 2014 MOEX. All rights reserved.
//

#import "MEPageViewController.h"
#import "MEUIPreferences.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation MEScrollView
@end


@interface MEPageViewController () <UIScrollViewDelegate>

@property (assign) NSUInteger page;
@property (assign) BOOL pageControlUsed;
@property (assign) BOOL pagesLoaded;
@property (assign) BOOL pageIsChanging;

@property (strong, nonatomic) MEScrollView  *pageScrollView;
@property (strong, nonatomic) UIToolbar     *toolBar;

@property (strong, nonatomic) UIView *containerView;
@property (assign, nonatomic) CGRect  containerFrame;

@end

@implementation MEPageViewController
{
    NSLayoutConstraint *constraintToolBarPosition;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

@synthesize pageScrollView=_pageScrollView;
@synthesize pageControlUsed = _pageControlUsed;
@synthesize page = _page;
@synthesize containerView = _containerView;
@synthesize toolBar = _toolBar;

- (void) scrollToCurrentPageAnimate:(BOOL)animate{
    // update the scroll view to the appropriate page
    CGRect frame = self.pageScrollView.frame;
    frame.origin.x = frame.size.width * _currentPage;
    frame.origin.y = 0;
    [self.pageScrollView scrollRectToVisible:frame animated:animate];
}

- (void)setCurrentPage:(NSInteger)currentPage{
    [self setCurrentPage:currentPage animate:YES];
}

- (void) setCurrentPage:(NSInteger)currentPage animate:(BOOL)animate{
    _currentPage = currentPage;
    
    self.pageIsChanging  = YES;
    
    @try {

        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:_currentPage];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
        
        [self scrollToCurrentPageAnimate:animate];

        _pageControlUsed = YES;
    }
    @catch (NSException *exception) {
    }
}

- (BOOL) toolbarHidden{
    return self.toolBar.alpha<=0.0f;
    
}

- (UIToolbar*) toolBar{
    if (!_toolBar) {
        float pc_height=ME_UI_TOOLBAR_HEIGHT;
        
        _toolBar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, self.view.bounds.size.height-pc_height, self.view.bounds.size.width, pc_height)];
        _toolBar.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_toolBar];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[toolBar]-0-|"
                                                                          options:0
                                                                          metrics:0
                                                                            views:@{@"toolBar": _toolBar}]];
        
        constraintToolBarPosition = [NSLayoutConstraint constraintWithItem: _toolBar
                                                                 attribute: NSLayoutAttributeBottom
                                                                 relatedBy: NSLayoutRelationEqual
                                                                    toItem: self.view
                                                                 attribute: NSLayoutAttributeBottom
                                                                multiplier: 1
                                                                  constant: 0];
        [self.view addConstraint:constraintToolBarPosition];
    }
    return _toolBar;
}

- (UIView*) containerView{
    if (!_containerView) {
        CGRect frame = self.view.bounds;
        frame.size.height-=self.toolBar.bounds.size.height;
        _containerView = [[UIView alloc] initWithFrame:frame];
        _containerView.translatesAutoresizingMaskIntoConstraints = NO;
        _containerView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_containerView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[containerView]-0-|"
                                                                          options:0
                                                                          metrics:0
                                                                            views:@{@"containerView": _containerView}]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[containerView]-0-[toolBar]"
                                                                          options:0
                                                                          metrics:0
                                                                            views:@{@"containerView": _containerView, @"toolBar": self.toolBar}]];
        
    }
    return _containerView;
}


- (MEScrollView*) pageScrollView{        
    if (_pageScrollView) {
        return _pageScrollView;
    }    
    _pageScrollView = [[MEScrollView alloc] initWithFrame:self.containerView.bounds];
    _pageScrollView.backgroundColor = [UIColor clearColor];
    _pageScrollView.autoresizesSubviews = YES;
    _pageScrollView.autoresizingMask = ~ UIViewAutoresizingNone;
    [self.containerView addSubview:_pageScrollView];
    
    return _pageScrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentPage = 0;
    
    self.pageIsChanging = NO;
    
    [self containerView];
    
    if (self.pageScrollView) {
        [self.pageScrollView setPagingEnabled:YES];
        [self.pageScrollView setScrollEnabled:YES];
        [self.pageScrollView setShowsHorizontalScrollIndicator:NO];
        [self.pageScrollView setShowsVerticalScrollIndicator:NO];
        [self.pageScrollView setDelegate:self];
        
        self.pageScrollView.alwaysBounceVertical = NO;
        self.pageScrollView.directionalLockEnabled = YES;
    }
    
	// Do any additional setup after loading the view, typically from a nib.
}


//- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
//	return NO;
//}

- (BOOL)shouldAutorotate {
    return YES;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//	return YES;
//}

- (void) reframeIfNeed{
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
    self.pageScrollView.contentSize = CGSizeMake(self.pageScrollView.frame.size.width * [self.childViewControllers count], self.pageScrollView.frame.size.height);
    NSUInteger page = 0;
    for (viewController in self.childViewControllers) {
        CGRect frame = self.pageScrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        viewController.view.frame = frame;
        page++;
    }
    
    CGRect frame = self.pageScrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
    [self.pageScrollView scrollRectToVisible:frame animated:NO];
}

- (BOOL) shouldHideWhenRotation{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
        
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
	[viewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if ([self shouldHideWhenRotation]) {        
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            [self setToolbarHidden:YES animated:YES];
        }
        else{
            [self setToolbarHidden:NO animated:YES];
        }
    }
    
    self.pageScrollView.contentSize = CGSizeMake(_pageScrollView.bounds.size.width * [self.childViewControllers count], _pageScrollView.bounds.size.height);
    
    [self.view layoutIfNeeded];
    for (UIViewController *vc in self.childViewControllers) {
        [vc didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    }
    
    [self scrollToCurrentPageAnimate:NO];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    if (self.pageScrollView==nil) {
        return;
    }

    if (!self.pagesLoaded) {
        for (int i =0; i < [self.childViewControllers count]; i++) {
            [self loadScrollViewWithPage:i];
        }
        self.pagesLoaded = YES;

        self.currentPage = 0;
        _page = 0;
        
        self.pageIsChanging = NO;
    }
	    
    if (self.childViewControllers.count>0) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewWillAppear:animated];
        }
    }    
    
    [self reframeIfNeed];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
    if (self.pageScrollView==nil) {
        return;
    }

	if ([self.childViewControllers count]) {
		UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
		if (viewController.view.superview != nil) {
			[viewController viewDidAppear:animated];
		}
	}
}

- (void)toggleToolBar{
    if (self.toolBar.alpha==0)
        [self setToolbarHidden:NO animated:YES];
    else
        [self setToolbarHidden:YES animated:YES];
}

- (void) viewDidLayoutSubviews{
    if (_page==self.currentPage) {                
        if (self.pageIsChanging==NO) {            
            [super viewDidLayoutSubviews];
            [self reframeIfNeed];        
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.pageScrollView==nil) {
        return;
    }

	if ([self.childViewControllers count]) {
		UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
		if (viewController.view.superview != nil) {
			[viewController viewWillDisappear:animated];
		}
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    if (self.pageScrollView==nil) {
        return;
    }

    if (self.childViewControllers.count>0) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidDisappear:animated];
        }        
    }
	[super viewDidDisappear:animated];
}

- (void)loadScrollViewWithPage:(int)page {
    
    if (page < 0)
        return;
    
    if (page >= [self.childViewControllers count])
        return;
    
	// replace the placeholder if necessary
    UIViewController *controller = [self.childViewControllers objectAtIndex:page];
    if (controller == nil) {
		return;
    }
	
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.pageScrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        if (self.pageScrollView) {
            controller.view.autoresizesSubviews = YES;
            controller.view.autoresizingMask = ~ UIViewAutoresizingNone;
            [self.pageScrollView addSubview:controller.view];             
        }
    }
}

- (void)previousPage {
	if (_page - 1 > 0) {
        
		// update the scroll view to the appropriate page
		CGRect frame = self.pageScrollView.frame;
		frame.origin.x = frame.size.width * (_page - 1);
		frame.origin.y = 0;
		
		UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
		UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page - 1];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		        
		[self.pageScrollView scrollRectToVisible:frame animated:YES];
		
		self.currentPage = _page - 1;
		// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
		_pageControlUsed = YES;
	}
}

- (void)nextPage {
	if (_page + 1 > self.childViewControllers.count) {
		
		// update the scroll view to the appropriate page
		CGRect frame = self.pageScrollView.frame;
		frame.origin.x = frame.size.width * (_page + 1);
		frame.origin.y = 0;
		
		UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
		UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page + 1];
		
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        
		[self.pageScrollView scrollRectToVisible:frame animated:NO];

		self.currentPage = _page + 1;
		// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
		_pageControlUsed = YES;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
	UIViewController *newViewController = [self.childViewControllers objectAtIndex:self.currentPage];
        
    if (oldViewController!=newViewController)
        [oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
	
	_page = self.currentPage;    
    
    self.pageIsChanging = NO;
    
    [self pageDidChange:_page];
    
}


-(void) setToolbarHidden:(BOOL)hidden animated:(BOOL) animated{
    dispatch_async(dispatch_get_main_queue(), ^{        
        
        if (self.toolBar.alpha == 0.0 && hidden){
            return ;
        }
        else if (self.toolBar.alpha == 1.0 && !hidden){
            return;
        }
        
        if (hidden)
            self->constraintToolBarPosition.constant=self.toolBar.bounds.size.height;
        else
            self->constraintToolBarPosition.constant=0.0;        
        
        [self.view setNeedsUpdateConstraints];
        
        if (animated) {
            float duration = ME_UI_ANIMATION_DURATION;
            [UIView animateWithDuration:duration animations:^{
                self.toolBar.alpha = hidden?0.0:1.;
                [self.view layoutIfNeeded];  
            } completion:nil];
            
        }else{
            [self.view layoutIfNeeded];
            self.toolBar.alpha = hidden?0.0:1.;
        }
    });
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender {
        
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    
    if (_pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    NSInteger page = self.currentPage;
    @try {
        CGFloat pageWidth = self.pageScrollView.frame.size.width;
        int page = floor((self.pageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (self.currentPage != page) {
            UIViewController *oldViewController = [self.childViewControllers objectAtIndex:self.currentPage];
            UIViewController *newViewController = [self.childViewControllers objectAtIndex:page];
            [oldViewController viewWillDisappear:YES];
            [newViewController viewWillAppear:YES];
            self.currentPage = page;
            [oldViewController viewDidDisappear:YES];
            
            [newViewController.view setNeedsUpdateConstraints];
            [newViewController.view layoutIfNeeded];
            
            [newViewController viewDidAppear:YES];
            _page = page;
        }
    }
    @catch (NSException *exception) {
        self.currentPage = page;
    }
}

- (void) pageWillChange:(NSInteger)page{
}

- (void) pageDidChange:(NSInteger)page{
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, self.containerView.frame.size.height);
    _pageControlUsed = NO;
    self.pageIsChanging = YES;
    
    [self pageWillChange:self.currentPage];
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControlUsed = NO;
}


@end
