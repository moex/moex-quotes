//
//  MEWatchListSectionHeaderView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 13.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEMarketSection.h"

#define ME_WATCHLIST_MARKET_HEADER @"marketWatchListHeader"
#define ME_WATCHLIST_MARKET_HEADER_WIDE @"marketWatchListHeaderWide"

@interface MEWatchListSectionHeaderView : UITableViewHeaderFooterView

@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *subTitleLabel;
@property(nonatomic,strong) UILabel *valtodayTotalLabel;
@property(nonatomic,strong) UILabel *valtodayLabel;

/**
 *  Show title. Default is NO.
 */
@property(nonatomic) BOOL shouldShowTitle;

/**
 *  Show sub title title. Default is NO.
 */
@property(nonatomic) BOOL shouldShowSubTitle;
@property(nonatomic) CGFloat height;
@property(nonatomic,strong) MEMarketSection *sector;

@property (nonatomic,weak) UITableView *rootTableView;

@end
