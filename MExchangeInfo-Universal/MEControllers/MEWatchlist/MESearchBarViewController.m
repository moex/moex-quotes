//
//  MESearchBarViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/12/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MESearchBarViewController.h"
#import "MEUIPreferences.h"
#import "MESettings.h"
#import "GIStompSubscriptions.h"

#define ME_SEARCH_SCOPE_ALL NSLocalizedString(@"All tickers", @"Search bar scope all tickers")
#define ME_SEARCH_SCOPE_PRIMARY NSLocalizedString(@"Primary boards only", @"Search bar scope primary boards only")
#define ME_SEARCHBAR_EDIT_BUTTON_TITLE NSLocalizedString(@"Edit", @"Edit button title on search bar panel")
#define ME_SEARCHBAR_CANCEL_BUTTON_TITLE NSLocalizedString(@"Cancel", @"Cancel button title on search bar panel")
#define ME_SEARCHBAR_DONE_BUTTON_TITLE NSLocalizedString(@"Done", @"Done button title on search bar panel")

@interface MESearchBarTableView : METableView
@property    (nonatomic) CGFloat currentAlpha;
@end

@implementation MESearchBarTableView


- (instancetype) initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self){
        _currentAlpha = 0.9;
    }
    return self;
}

- (void) setMessageText:(NSString *)messageText {
    [super setMessageText:messageText];
    if (messageText == nil) {
        self.currentAlpha = 0.9;
    }
    else {
        self.currentAlpha = 0.95;
    }
}

- (void) setCurrentAlpha:(CGFloat)currentAlpha{
    _currentAlpha = currentAlpha;
    [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION animations:^{
        self.backgroundColor = [UIColor whiteColor];
        self.alpha = self->_currentAlpha;
    }];
}

- (void) reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
            if ([self.dataSource tableView:self numberOfRowsInSection:0]>0) {
                [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION animations:^{
                    self.alpha = 1.0;
                    self.backgroundColor = [UIColor whiteColor];
                }];
            }
            else{
                [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION animations:^{
                    self.backgroundColor = [UIColor whiteColor];
                    self.alpha = self->_currentAlpha;
                }];
            }
        }
        [super reloadData];
    });
}

@end


@interface MESearchBarViewController () <UISearchBarDelegate>
@property(nonatomic,strong) UIButton *editButton;
@property(nonatomic) MESearchBarButtonState curentState;
@property(nonatomic) MESearchBarButtonState lastState;
@end

@implementation MESearchBarViewController
{
    NSString  *lastSearch;
    BOOL shouldBeginEditing;
    
    NSTimer *typeTimer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self __init__];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self __init__];
    }
    
    return self;
}

- (id) init{
    self = [super init];
    
    if (self) {
        [self __init__];
    }
    
    return self;
}

- (void) __init__{
    shouldBeginEditing = YES;
    self.searchTableView = [[MESearchBarTableView alloc] initWithFrame:CGRectMake(0, 0, 320, 0) style:UITableViewStylePlain];
    self.searchTableView.backgroundColor = [UIColor clearColor];
    self.searchTableView.tableHeaderView.frame = CGRectMake(0, 0, 320, 44);
    self.searchTableView.autoresizesSubviews = YES;
    self.searchTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.searchTableView.editing = NO;
    self.searchTableView.messageTextView.textAlignment = NSTextAlignmentCenter;
}

- (void) loadView{
    
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.searchBar.translucent=YES;
    self.searchBar.placeholder = NSLocalizedString(@"Search a ticker", @"");
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchBar.delegate = self;
    
    self.editButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.editButton.showsTouchWhenHighlighted = YES;
    self.editButton.backgroundColor = [UIColor clearColor];
    [self.editButton setTitle:ME_SEARCHBAR_EDIT_BUTTON_TITLE forState:UIControlStateNormal];
    [self.editButton setTitleColor: [UIColor grayColor] forState:UIControlStateNormal];
    self.editButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.scopeBar  = [[UISegmentedControl alloc] initWithFrame:CGRectMake(0,0,320,44)];
    self.scopeBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scopeBar insertSegmentWithTitle:ME_SEARCH_SCOPE_ALL atIndex:0 animated:NO];
    [self.scopeBar insertSegmentWithTitle:ME_SEARCH_SCOPE_PRIMARY atIndex:0 animated:NO];
    self.scopeBar.tintColor = [UIColor grayColor];
    
    self.scopeBar.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:ME_SEARCH_ONLY_PRIMARY_BOARD];
    
    [self.scopeBar addTarget:self action:@selector(scopeAction:) forControlEvents:UIControlEventValueChanged];
    self.scopeBar.alpha = 0.0;
    
    [self.view addSubview:self.editButton];
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.scopeBar];
    
    NSDictionary *views = @{@"searchBar":self.searchBar, @"editButton":self.editButton, @"scopeBar": self.scopeBar};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[searchBar]" options:0 metrics:0 views:views]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.editButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.searchBar attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    if ([GIC_current_lang() hasPrefix:@"ru"]) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[searchBar]-5-[editButton(95)]-10-|" options:0 metrics:0 views:views]];
    }
    else {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[searchBar]-5-[editButton(70)]-10-|" options:0 metrics:0 views:views]];
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-9-[scopeBar]-9-|" options:0 metrics:0 views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[searchBar]-2-[scopeBar]" options:0 metrics:0 views:views]];
    
    _curentState = ME_SEARCH_BAR_BUTTON_UNKNOWN;
    self.lastState = self.curentState = ME_SEARCH_BAR_BUTTON_EDIT;
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self hideSearchTableView];
    [self hideKeyboard];
}

- (BOOL) isKeyboardOnScreen
{
    BOOL isKeyboardShown = NO;
    
    NSArray *windows = [UIApplication sharedApplication].windows;
    if (windows.count > 1) {
        NSArray *wSubviews =  [windows[1]  subviews];
        if (wSubviews.count) {
            CGRect keyboardFrame = [wSubviews[0] frame];
            CGRect screenFrame = [windows[1] frame];
            if (keyboardFrame.origin.y+keyboardFrame.size.height == screenFrame.size.height) {
                isKeyboardShown = YES;
            }
        }
    }
    
    return isKeyboardShown;
}

- (void) hideKeyboard{
    if ([self isKeyboardOnScreen]){
        [self.searchBar resignFirstResponder];
        //self.lastState = self.curentState = ME_SEARCH_BAR_BUTTON_EDIT;
    }
}

- (void) setCurentState:(MESearchBarButtonState)curentState{
    
    if (curentState==_curentState)
        return;
    
    if (curentState == ME_SEARCH_BAR_BUTTON_EDIT)
        [self setEditButtonSetTitle:ME_SEARCHBAR_EDIT_BUTTON_TITLE];
    else if(curentState == ME_SEARCH_BAR_BUTTON_CANCEL)
        [self setEditButtonSetTitle:ME_SEARCHBAR_CANCEL_BUTTON_TITLE];
    else
        [self setEditButtonSetTitle:ME_SEARCHBAR_DONE_BUTTON_TITLE];
    
    _curentState = curentState;    

    if (_curentState != ME_SEARCH_BAR_BUTTON_DONE && self.tableView.editing){
        [self.tableView setEditing:NO animated:YES];
        _lastState = ME_SEARCH_BAR_BUTTON_EDIT;
    }
}

- (void) setEditButtonSetTitle:(NSString*)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self.editButton.alpha = 0.0;
        }];
        [self.editButton setTitle:title forState:UIControlStateNormal];
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self.editButton.alpha = 1.0;
        }];
    });
}

- (void) hideSearchTableView{
        
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (!self.searchTableView.isHidden) {
            self.tableView.scrollEnabled = YES;
            self.tableView.alpha = 0.5;
        }

        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION/2.
                         animations:^{
                             
                             if (!self.searchTableView.isHidden) {
                                 [self.view layoutIfNeeded];                                 
                                 self.tableView.alpha = 1.0;
                                 self.scopeBar.alpha = 0.0;                                 
                             }
                             self.searchTableView.alpha = 0.0;                             
                         }
                         completion:^(BOOL finished) {
                             self.searchTableView.hidden = YES;
                             [self.searchTableView removeFromSuperview];
                         }
         ];
        

        if (!self.searchTableView.isHidden) {
            [UIView transitionWithView:self.tableView
                              duration:ME_UI_ANIMATION_DURATION
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self showHeaderwithNewRect:CGRectMake(0, 0, 320, 44)];
                                [self.tableView setTableHeaderView:self.view];
                            }
                            completion:nil];
        }
        
        self.curentState = self.lastState;
    });
}

- (void) editButtonAction: (id) sender{
    
    if(self.curentState == ME_SEARCH_BAR_BUTTON_CANCEL){
        [self.searchBar resignFirstResponder];
        [self hideSearchTableView];
        self.curentState = self.lastState;
        if (self.delegate)
            [self.delegate searchBarCancel:self.searchBar];
        return;
    }
    
    if (self.delegate)
        [self.delegate searchBarEditButtonClicked:self.searchBar withState:_curentState];
    
    if (_curentState == ME_SEARCH_BAR_BUTTON_EDIT)
        self.curentState = ME_SEARCH_BAR_BUTTON_DONE;
    else
        self.curentState = ME_SEARCH_BAR_BUTTON_EDIT;
}

- (void) scopeAction: (id) sender{
    
    UISegmentedControl  *_scope = (UISegmentedControl*)sender;
    [[NSUserDefaults standardUserDefaults] setInteger:_scope.selectedSegmentIndex forKey:ME_SEARCH_ONLY_PRIMARY_BOARD];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.delegate) {
        [self.delegate performSelector:@selector(searchBarTextDidEndEditing:) withObject:self.searchBar];
        [self.searchTableView reloadData];
    }
    
}

#pragma Search bar

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if (self.delegate) {
        [self.delegate performSelector:@selector(searchBarTextDidEndEditing:) withObject:self.searchBar];
        [self.searchTableView reloadData];
    }
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    self.lastState = self.curentState;
    self.searchTableView.hidden = NO;

    if (!lastSearch)
        searchBar.text = lastSearch;
    
    self.tableView.scrollEnabled = NO;
    
    [self showHeaderwithNewRect:CGRectMake(0, 0, 320, 88)];
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        self.scopeBar.alpha = 1.0;
    }];
    
    self.curentState = ME_SEARCH_BAR_BUTTON_CANCEL;
    
    self.searchTableView.frame=CGRectMake(0, self.view.bounds.size.height, self.tableView.frame.size.width, self.tableView.frame.size.height-self.view.bounds.size.height);
    
    self.searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.searchTableView.separatorColor = [UIColor clearColor];
    
    self.searchTableView.alpha = 0.5;
    [self.tableView addSubview:self.searchTableView];
    
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        self.searchTableView.alpha = 1.0;
    }];
    
    if (self.delegate) {
        [self.delegate searchBarTextDidBeginEditing:self.searchBar];
    }
    
    [self.searchTableView reloadData];
}

- (void) searchBarDidEditingTimer:(NSTimer*)timer{
    [self.delegate performSelector:@selector(searchBarTextDidEndEditing:) withObject:self.searchBar];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(![searchBar isFirstResponder]) {
        shouldBeginEditing = NO;
    }
    
    lastSearch = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (self.delegate && lastSearch.length>=3) {
        
        if (typeTimer)
            [typeTimer invalidate];
        
        typeTimer = [NSTimer scheduledTimerWithTimeInterval:ME_DEFERRED_ACTION_DELAY target:self selector:@selector(searchBarDidEditingTimer:) userInfo:nil repeats:NO];
    }
    else{
        [self.delegate performSelector:@selector(searchBarTextDidEndEditing:) withObject:nil];
    }
}

- (BOOL) searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    if (!shouldBeginEditing) {
        shouldBeginEditing = YES;
        return NO;
    }
    return YES;
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
    [self searchBarShouldBeginEditing:searchBar];
    self.curentState = ME_SEARCH_BAR_BUTTON_CANCEL;
}


- (void) showHeaderwithNewRect:(CGRect)frame{
    
    if (self.delegate) {
        [self.delegate performSelector:@selector(searchBarWillShow:) withObject:self];
    }
    
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION
                     animations:^{
                         self.tableView.alpha = 0.5;
                         self.searchTableView.alpha = 1.0;
                     }];

    self.view.frame = frame;

    [UIView transitionWithView:self.tableView
                      duration:ME_UI_ANIMATION_DURATION/2.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.tableView setTableHeaderView:self.view];
                    }
                    completion:^(BOOL finished) {
                    }];

    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION
                     animations:^{
                         [self.view layoutIfNeeded];
                         [self.editButton layoutIfNeeded];
                         self.tableView.alpha = 1.0;
                     }
                     completion:^(BOOL f){
                         if (self.delegate) {
                             [self.delegate performSelector:@selector(searchBarDidShow:) withObject:self];
                         }
                     }
     ];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
