//
//  MELevel2TableViewController.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 12/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELevel2TableViewController.h"

#import <Crashlytics/Crashlytics.h>

@interface MELevel2TableViewController ()
@property (strong,nonatomic) MELevel2TableView *tableView;
@end

@implementation MELevel2TableViewController


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableView.isActive = YES;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView scrollToSplitPosition];
    
    if (self.tableView.ticker != nil )
        [Answers logContentViewWithName:@"Ticker Level2 View"
                            contentType:nil
                              contentId:nil
                       customAttributes:@{
                           @"Ticker Level2: ticker": self.tableView.ticker.ID,
                       }];

}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.tableView.isActive = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView = [[MELevel2TableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.tableView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:nil views:@{@"table":self.tableView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(height)-[table]-0-|"
                                                                      options:0
                                                                      metrics:@{@"height": [NSNumber numberWithInt:ME_UI_TICKERINFO_HEIGHT]}
                                                                        views:@{@"table":self.tableView, @"topGuide": self.topLayoutGuide}]];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.tableView scrollToSplitPosition];
}

@end
