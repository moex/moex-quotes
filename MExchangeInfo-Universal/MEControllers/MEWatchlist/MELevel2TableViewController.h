//
//  MELevel2TableViewController.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 12/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MELevel2TableView.h"
#import "METickerInfoViewDefaults.h"

@interface MELevel2TableViewController : UIViewController
@property (readonly,nonatomic) MELevel2TableView *tableView;
@end
