//
//  MEWatchListViewController.h
//  MExchangeInfo-Universal
//
//  Created by denn on 28.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METickerInfoViewController.h"
#import "METableView.h"
#import "MEListMasterViewController.h"

@interface MEWatchListViewController : MEListMasterViewController
- (void) _willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

- (void) _didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;

@end
