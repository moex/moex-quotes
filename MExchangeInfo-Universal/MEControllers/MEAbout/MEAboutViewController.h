//
//  MEAboutViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/4/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEButtonLogin.h"

@interface MEAboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *appInfoTextView;
@property (weak, nonatomic) IBOutlet UITextView *appCopyRightTextView;
@property (weak, nonatomic) IBOutlet UILabel *versionLavel;
@property (weak, nonatomic) IBOutlet UILabel *versionReleaseDateLabel;
@property (weak, nonatomic) IBOutlet MEButtonLogin *launchTechSupportEmailButton;

- (IBAction)techSupportAction:(id)sender;
@end
