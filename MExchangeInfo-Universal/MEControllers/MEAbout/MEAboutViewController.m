//
//  MEAboutViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/4/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEAboutViewController.h"
#import "MEUIPreferences.h"
#import "MEConfig.h"

@interface MEAboutViewController ()

@end

@implementation MEAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (BOOL)shouldAutorotate { return YES; }

- (UIInterfaceOrientationMask) supportedInterfaceOrientations{ return UIInterfaceOrientationMaskPortrait; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self.launchTechSupportEmailButton setTitle:NSLocalizedString(@"Tech support button", @"") forState:UIControlStateNormal];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];

    self.appCopyRightTextView.text = [NSString stringWithFormat:NSLocalizedString(@"Copyright text", @""),@([components year])];
    self.appInfoTextView.text = NSLocalizedString(@"Application info", @"");

    self.appInfoTextView.textColor = ME_UI_DARK_GREY_FONT_COLOR;
    self.appInfoTextView.font = [UIFont fontWithName:ME_UI_FONT size:17];
    self.appCopyRightTextView.textColor = ME_UI_GREY_FONT_COLOR;
    self.appCopyRightTextView.font = [UIFont fontWithName:ME_UI_FONT size:12];
    
    self.versionLavel.text = [NSString stringWithFormat:NSLocalizedString(@"Version: %@", @""), ME_APP_VESRION_INFO];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *rd = [dateFormatter dateFromString:ME_APP_RELEASE_DATE];
    self.versionReleaseDateLabel.text = [NSDateFormatter localizedStringFromDate:rd dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)techSupportAction:(id)sender {

    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceType = UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?@"iPad":@"iPhone";
    NSString *buildVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    NSString *bodyInfo = [NSString stringWithFormat:@"device-type: %@\n sys-info: %@\n sys-version: %@\n build: %@%@\n\n%@\n\n",
                          deviceType,
                          [device systemName],
                          [device systemVersion],
                          ME_APP_BUILD,
                          buildVersion,
                          NSLocalizedString(@"Add here some description of your problem...", @"")
                          ];
    
#if DEBUG == 1

    NSString *emailInfo = [NSString stringWithFormat: @"mailto:%@?bcc=%@&subject=[%@] Tech support request for: version:%@&body=%@",
                           ME_APP_TECH_SUPPORT_EMAIL,
                           ME_APP_AUTHOR_SUPPORT_EMAIL,
                           ME_APP_NAME,
                           ME_APP_VERSION,
                           bodyInfo];
#else
    
    NSString *emailInfo = [NSString stringWithFormat: @"mailto:%@?subject=[%@] Tech support request for: version:%@&body=%@",
                           ME_APP_TECH_SUPPORT_EMAIL,
                           ME_APP_NAME,
                           ME_APP_VERSION,
                           bodyInfo];
#endif
        
    NSURL *url = [NSURL URLWithString: [emailInfo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[UIApplication sharedApplication] openURL: url];
}
@end
