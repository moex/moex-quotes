//
//  MEButtonAbout.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/4/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEButtonAbout.h"
#import "MEUIPreferences.h"

@implementation MEButtonAbout

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) awakeFromNib{
    [super awakeFromNib];
    [self setReversesTitleShadowWhenHighlighted:YES];
    [self setAdjustsImageWhenHighlighted:NO];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 8.0;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [ME_UI_LIGHT_GREY_FONT_COLOR CGColor];
    self.layer.borderWidth = 0.5;
}

@end
