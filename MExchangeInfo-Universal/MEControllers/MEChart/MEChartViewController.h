//
//  MEChartViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 3/6/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MESChartView/MESChartView.h>
#import "METickerInfoViewDefaults.h"
#import "METicker.h"

@protocol MEChartViewShieldProtocol <NSObject>
@optional
- (void) cancel:(UIGestureRecognizer*)gesture;
@end

@interface MEChartViewController : UIViewController <MEChartViewShieldProtocol>

@property (nonatomic,strong) UIView *shieldView;
@property (nonatomic,readonly) MESChartView *chartView;
@property (nonatomic,strong) UIViewController *parentController;
@property (nonatomic,readonly) BOOL isShown;
- (void) update;
@end
