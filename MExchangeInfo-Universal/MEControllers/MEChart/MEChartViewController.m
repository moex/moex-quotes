//
//  MEChartViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 3/6/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEChartViewController.h"
#import "MEUIPreferences.h"
#import <MESChartView/MESChartView.h>
#import "MECandlesModel.h"
#import "METickerInfoViewController.h"
#import "NSAttributedString+MEFormatedString.h"
#import "NSDate+MEComparing.h"
#import "MESettings.h"
#import <Crashlytics/Crashlytics.h>

#define ME_UI_CANDLE_FILL_COLOR [UIColor colorWithRed:98./255. green:164./255 blue:209./255 alpha:1.]
#define ME_UI_CHART_ANIMATION     (ME_UI_ANIMATION_DURATION/2.)


@interface MEIntervalButton : UIButton
@property (nonatomic,strong) NSString *interval;
@end

@implementation MEIntervalButton
@end

@interface METimeTitleView : UIView
@property (nonatomic,readonly) UILabel *timeTextLabel;
@end

@implementation METimeTitleView

@synthesize timeTextLabel=_timeTextLabel;

- (UILabel*) timeTextLabel{
    if (_timeTextLabel) {
        return _timeTextLabel;
    }
    
    _timeTextLabel = [[UILabel alloc] initWithFrame:CGRectInset(self.bounds, 5, 5)];
    _timeTextLabel.autoresizingMask = ~UIViewAutoresizingNone;
    _timeTextLabel.text = @"TEXT";
    _timeTextLabel.adjustsFontSizeToFitWidth = YES;
    _timeTextLabel.minimumScaleFactor = 0.7;
    _timeTextLabel.textAlignment = NSTextAlignmentCenter;
    _timeTextLabel.font = [UIFont fontWithName:ME_UI_FONT size:16];
    _timeTextLabel.textColor = [UIColor whiteColor];
    
    [self addSubview:_timeTextLabel];
    
    return _timeTextLabel;
}

@end

@interface MEChartViewController () <UIViewControllerTransitioningDelegate, MESChartDataSource, MESChartDelegate, MESChartFormater, MECandlesModelProtocol, UIGestureRecognizerDelegate>

@property (nonatomic,strong  ) MESChartView            *chartView;

@property (nonatomic,strong  ) UIView                  *controlsView;
@property (nonatomic,strong  ) UIView                  *intervalsView;
@property (nonatomic,strong  ) METimeTitleView         *timeTitleView;
@property (nonatomic,strong  ) UIButton                *chartTypeButton;
@property (nonatomic,assign  ) MESChartSeriesType      seriesType;

@property (nonatomic,strong  ) UIButton                *closeButton;

@property (nonatomic,strong  ) UIImageView             *graphDeviderImageView;
@property (nonatomic,strong  ) UIImageView             *volumeDeviderImageView;
@property (nonatomic,strong  ) UIImageView             *leftDeviderImageView;
@property (nonatomic,strong  ) UIImageView             *rightDeviderImageView;
@property (nonatomic,strong  ) NSNumberFormatter       *numberFormatter;
@property (nonatomic,strong  ) NSString                *currentInterval;

@property (nonatomic,strong  ) UIActivityIndicatorView *activityIndicator;
@property (nonatomic,strong  ) UITextView              *messageText;
@property (nonatomic,strong  ) UIView                  *touchDeferredShield;

@property (nonatomic,readonly) METicker                *ticker;
@property (nonatomic,strong  ) MESChartZone            *volumeZone;

@property (atomic,readonly   ) MECandlesModel          *candlesModel;

@property (nonatomic,strong  ) NSDate                  *lastTickDate;

@end

@implementation MEChartViewController
{
    UIInterfaceOrientation lastOrientation;
    BOOL intervalsUpdated;
    CGAffineTransform savedTransoform;
    CGRect savedFrame;
    NSLayoutConstraint *constraintTopHeight, *constraintBottomHeight;
    
    UITapGestureRecognizer *touchDeferredShieldGesture;
    
    BOOL  isRotating;
    BOOL  hasNotAccess;
    BOOL  hasScroll;
    
    UIView *viewSnapshot;
    
    METicker *lastTicker;
}

@synthesize seriesType=_seriesType, currentInterval=_currentInterval, candlesModel=_candlesModel;


- (UIView*) touchDeferredShield{
    if (!_touchDeferredShield) {
        _touchDeferredShield = [[UIView alloc] initWithFrame:self.view.bounds];
        _touchDeferredShield.autoresizingMask = ~UIViewAutoresizingNone;
        [self.view addSubview:_touchDeferredShield];
        
        touchDeferredShieldGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchDeferredCalback:)];
        touchDeferredShieldGesture.numberOfTapsRequired = 1;
        touchDeferredShieldGesture.numberOfTouchesRequired = 1;
        touchDeferredShieldGesture.delegate = self;
        [_touchDeferredShield addGestureRecognizer:touchDeferredShieldGesture];
    }
    return _touchDeferredShield;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (gestureRecognizer==touchDeferredShieldGesture) {
        [self disableTouchDeferredShield];
    }
    return YES;
}

- (void) touchDeferredCalback:(UITapGestureRecognizer*)gesture{
}

- (void) disableTouchDeferredShield{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->touchDeferredShieldGesture.enabled = NO;
        self.touchDeferredShield.hidden = YES;
    });
}


- (UIImageView*) graphDeviderImageView{
    if (_graphDeviderImageView) {
        return _graphDeviderImageView;
    }
    
    _graphDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xAxisDevider"]];
    [self.chartView addSubview:_graphDeviderImageView];
    
    return _graphDeviderImageView;
}

- (UIImageView*) volumeDeviderImageView{
    if (_volumeDeviderImageView) {
        return _volumeDeviderImageView;
    }
    
    _volumeDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xAxisDevider"]];
    [self.chartView addSubview:_volumeDeviderImageView];
    
    return _volumeDeviderImageView;
}

- (UIImageView*) leftDeviderImageView{
    if (_leftDeviderImageView) {
        return _leftDeviderImageView;
    }
    
    _leftDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yAxisDevider"]];
    [self.chartView addSubview:_leftDeviderImageView];
    
    return _leftDeviderImageView;
}

- (UIImageView*) rightDeviderImageView{
    if (_rightDeviderImageView) {
        return _rightDeviderImageView;
    }
    
    _rightDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yAxisDevider"]];
    [self.chartView addSubview:_rightDeviderImageView];
    
    return _rightDeviderImageView;
}


- (NSString*) currentInterval{
    if (_currentInterval) {
        return _currentInterval;
    }
    
    _currentInterval = [[NSUserDefaults standardUserDefaults] stringForKey:@"MEChartViewController-currentInterval"];
    
    if (!_currentInterval) {
        _currentInterval = @"M1";
    }
    
    return _currentInterval;
}

- (void) setCurrentInterval:(NSString *)currentInterval{
    _currentInterval = currentInterval;
    [[NSUserDefaults standardUserDefaults] setObject:_currentInterval forKey:@"MEChartViewController-currentInterval"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) rotateSetup:(UIInterfaceOrientation)orientation{
    if (UIInterfaceOrientationIsPortrait(orientation) && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        self.chartView.titleView.hidden = YES;
        self.chartView.headerView.hidden = YES;
    }
    else{
        self.chartView.titleView.hidden = NO;
        self.chartView.headerView.hidden = NO;
    }
}

- (UIActivityIndicatorView*) activityIndicator{
    
    if (_activityIndicator == nil) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicator.color = [UIColor grayColor];
        [self.view addSubview:_activityIndicator];
        _activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_activityIndicator.superview addConstraint:[NSLayoutConstraint constraintWithItem:_activityIndicator
                                                                                  attribute:NSLayoutAttributeCenterX
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.view
                                                                                  attribute:NSLayoutAttributeCenterX
                                                                                 multiplier:1
                                                                                   constant:0]];
        
        [_activityIndicator.superview addConstraint:[NSLayoutConstraint constraintWithItem:_activityIndicator
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.chartView
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                multiplier:1
                                                                                  constant:0]];

    }
        
    return _activityIndicator;
}

- (UITextView*) messageText{
    if (!_messageText) {
        _messageText = [[UITextView alloc] initWithFrame:CGRectInset(self.chartView.frame, 0., 0.)];
        
        _messageText.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        
        _messageText.font = [UIFont fontWithName:ME_UI_FONT size:16];
        _messageText.editable = NO;
        _messageText.scrollEnabled = NO;
        _messageText.userInteractionEnabled = NO;
        _messageText.dataDetectorTypes = UIDataDetectorTypeLink|UIDataDetectorTypeAddress;
        
        _messageText.textColor = ME_UI_GREY_FONT_COLOR;
        _messageText.textAlignment = NSTextAlignmentCenter;
        
        [self.view addSubview:_messageText];
        
        _messageText.scrollEnabled=NO;
        _messageText.alpha=0.0;
    }
    
    return _messageText;
}

- (void) loadView{
    [super loadView];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    //
    // Chart panel
    //
    
    if (!_chartView) {
        [MESChartView setAnimationDuration:ME_UI_CHART_ANIMATION];
        _chartView = [[MESChartView alloc] initWithFrame:self.view.bounds];
    }
    
    _chartView.dataSource = self;
    _chartView.formater = self;
    _chartView.delegate = self;
    
    self.chartView.backgroundColor = [UIColor whiteColor];
    [self rotateSetup:[UIApplication sharedApplication].statusBarOrientation];
    
    self.chartView.backgroundColor = [UIColor whiteColor];
    self.chartView.gridLayer.yAxis.font = [UIFont fontWithName:ME_UI_FONT size:10];
    self.chartView.gridLayer.yAxis.textColor = [UIColor darkGrayColor];
    self.chartView.gridLayer.yAxis.ticks = 8;
    self.chartView.gridLayer.yAxis.enableEdges = YES;
    
    self.chartView.gridLayer.xAxis.width = 0.0;
    self.chartView.gridLayer.lineWidth = 0.3;
    self.chartView.gridLayer.yLayer.lineWidth = 0.2;
    self.chartView.gridLayer.xLayer.lineWidth = 0.3;
    self.chartView.gridLayer.xAxis.font = [UIFont fontWithName:ME_UI_FONT size:11];
    self.chartView.gridLayer.xAxis.textColor = [UIColor grayColor];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        self.chartView.gridLayer.xAxis.ticks = 8;
    else
        self.chartView.gridLayer.xAxis.ticks = 6;
    
    
    self.chartView.gridLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4].CGColor;
    self.chartView.gridLayer.yLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2].CGColor;
    self.chartView.gridLayer.adjustXGridToOverMidnight = YES;
    
    self.chartView.crossHairLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6].CGColor;
    self.chartView.crossHairLayer.lineWidth = 0.5;
    
    self.chartView.graphLayer.lineWidth = 3.;
    self.chartView.graphLayer.gradientColor = [UIColor colorWithRed:0.7 green:0.1 blue:0.1 alpha:0.3].CGColor;
    self.chartView.graphLayer.strokeColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:.5].CGColor;
    self.chartView.graphLayer.enableGradient = YES;
    
    self.chartView.candlesLayer.grouthCanvas.fillColor = [UIColor whiteColor].CGColor;
    self.chartView.candlesLayer.fallingCanvas.fillColor = ME_UI_CANDLE_FILL_COLOR.CGColor;
    self.chartView.candlesLayer.grouthCanvas.strokeColor = self.chartView.candlesLayer.fallingCanvas.strokeColor = self.chartView.graphLayer.strokeColor = self.chartView.graphLayer.gradientColor = self.chartView.candlesLayer.fallingCanvas.fillColor;
    self.chartView.candlesLayer.grouthCanvas.lineWidth = .5;
    self.chartView.candlesLayer.fallingCanvas.lineWidth = .5;
    
    
    self.chartView.selectionLayer.fillColor =  self.chartView.candlesLayer.fallingCanvas.fillColor;//[UIColor redColor].CGColor;
    self.chartView.selectionLayer.strokeColor = [UIColor blueColor].CGColor;
    self.chartView.selectionLayer.opacity = 0.3;
    
    self.chartView.candlesLayer.widthBetween = 1;
    self.chartView.headerView.backgroundColor = [UIColor colorWithCGColor:self.chartView.candlesLayer.grouthCanvas.fillColor];
    self.chartView.titleView.backgroundColor = [UIColor clearColor];
    
    self.chartView.zonePadding = 1;
    self.chartView.edgePadding = 10.0;
    
    [self.view addSubview:self.chartView];
    self.chartView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.chartView.backgroundImage = [UIImage imageNamed:@"chartBackground"];
    [self.view addSubview:self.chartView];
    self.chartView.translatesAutoresizingMaskIntoConstraints = NO;
    
    //
    // Controls panel
    //
    CGRect cframe = self.view.bounds;
    UIImage *cimage = [UIImage imageNamed:@"chartFilterBackgound"];
    NSNumber *cheight = [NSNumber numberWithFloat:cimage.size.height];
    cframe.size.height = cimage.size.height;
    _controlsView = [[UIView alloc] initWithFrame:self.view.bounds];
    _controlsView.backgroundColor = [UIColor colorWithPatternImage:cimage];
    _controlsView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_controlsView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[chart]-0-|" options:0 metrics:nil views:@{@"chart":self.chartView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[controls]-0-|" options:0 metrics:nil views:@{@"controls":_controlsView}]];
    
    [self.view addConstraint:constraintTopHeight=[NSLayoutConstraint constraintWithItem:_controlsView
                                                                              attribute:NSLayoutAttributeTop
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.topLayoutGuide
                                                                              attribute:NSLayoutAttributeBottom
                                                                             multiplier:1
                                                                               constant:ME_UI_TICKERINFO_HEIGHT]];
        
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[controls(cheight)]-0-[chart]-0-|"
                                                                      options:0
                                                                      metrics:@{@"cheight": cheight, @"height": [NSNumber numberWithInt:ME_UI_TICKERINFO_HEIGHT]}
                                                                        views:@{@"chart":self.chartView, @"controls":_controlsView}]];
    
    _intervalsView = [[UIView alloc] initWithFrame:_controlsView.bounds];
    _intervalsView.translatesAutoresizingMaskIntoConstraints=NO;
    
    _chartTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _chartTypeButton.adjustsImageWhenHighlighted=YES;
    _chartTypeButton.showsTouchWhenHighlighted=YES;
    _chartTypeButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_controlsView addSubview:_intervalsView];
    [_controlsView addSubview:_chartTypeButton];
    
    [_controlsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[intervals]-1-[button(40)]-5-|" options:0 metrics:nil views:@{@"intervals": _intervalsView, @"button": _chartTypeButton}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[button]-0-|" options:0 metrics:nil views:@{@"button":_chartTypeButton}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[intervals]-0-|" options:0 metrics:nil views:@{@"intervals":_intervalsView}]];
    
    [_chartTypeButton addTarget:self action:@selector(changeType:) forControlEvents:UIControlEventTouchUpInside];
    
    _timeTitleView = [[METimeTitleView alloc] initWithFrame:_controlsView.bounds];
    _timeTitleView.alpha = 0.0;
    _timeTitleView.translatesAutoresizingMaskIntoConstraints = NO;
    [_controlsView addSubview:_timeTitleView];
    
    [_controlsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[timeTitleView]-0-|" options:0 metrics:nil views:@{@"timeTitleView":_timeTitleView}]];
    [_controlsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[timeTitleView]-|" options:0 metrics:nil views:@{@"timeTitleView":_timeTitleView}]];
    
   self.seriesType = MESCHART_LINE;
}

- (void) closeChartHandler:(UIButton*)sender{
    [self cancel:nil];
}

- (void) changeType:(id)sender{
    static NSArray *types = nil;
    static int currentIndex = 0;
    
    if(!types) types = @[[NSNumber numberWithInt:MESCHART_LINE], [NSNumber numberWithInt:MESCHART_CANDLE], [NSNumber numberWithInt:MESCHART_OHLC]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        currentIndex++;
        if (currentIndex==3) {
            currentIndex=0;
        }
        MESChartSeriesType type= (MESChartSeriesType)[[types objectAtIndex:currentIndex] integerValue];
        [self redraw:[UIApplication sharedApplication].statusBarOrientation];
        self.seriesType = type;        
    });
}

- (void) changeButtonTypeSetLine{
    [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_lines"] forState:UIControlStateNormal];
    [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_linesPressed.png"] forState:UIControlStateSelected];
    [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_linesPressed.png"] forState:UIControlStateHighlighted];
}

- (void) setSeriesType:(MESChartSeriesType)seriesType{
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [self changeButtonTypeSetLine];
        _chartTypeButton.enabled = NO;
        _chartTypeButton.alpha = 0.0;
        return;
    }
    
    self.candlesModel.seriesType = _seriesType = seriesType;
    
    [self changeButtonTypeSetLine];
    _chartTypeButton.enabled = YES;
    _chartTypeButton.alpha = 1.0;
    
    if (_seriesType==MESCHART_LINE) {
        [self changeButtonTypeSetLine];
    }
    else if (_seriesType==MESCHART_CANDLE){
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_candles"] forState:UIControlStateNormal];
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_candlesPressed"] forState:UIControlStateSelected];
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_candlesPressed"] forState:UIControlStateHighlighted];
    }
    else if (_seriesType==MESCHART_OHLC){
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_bar"] forState:UIControlStateNormal];
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_barPressed"] forState:UIControlStateSelected];
        [_chartTypeButton setImage:[UIImage imageNamed:@"Icon_barPressed"] forState:UIControlStateHighlighted];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_seriesType] forKey:@"MEChartViewController-seriesType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self rotateSetup:[UIApplication sharedApplication].statusBarOrientation];
    
    if (_seriesType == MESCHART_LINE) {
        self.chartView.graphLayer.opacity = 1.0;
        self.chartView.candlesLayer.grouthCanvas.opacity = 0.0;
        self.chartView.candlesLayer.fallingCanvas.opacity = 0.0;
    }
    else{
        self.chartView.graphLayer.opacity = 0.0;
        self.chartView.candlesLayer.grouthCanvas.opacity = 1.0;
        self.chartView.candlesLayer.fallingCanvas.opacity = 1.0;
    }
}

- (MESChartSeriesType) seriesType{
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return MESCHART_LINE;
    }
    self.candlesModel.seriesType =  _seriesType = (MESChartSeriesType)[[NSUserDefaults standardUserDefaults] integerForKey:@"MEChartViewController-seriesType"];
    return _seriesType;
}

- (METicker*) ticker{
    METicker *t = [[MEWatchListModel sharedInstance] activeTicker];
    
    if (t!=lastTicker) {
        [self hideMessage];
        lastTicker=t;
    }
    
    return t;
}

- (void) update{
    
    if (!self.isShown) {
        return;
    }
    
    self.chartView.titleView.textLabel.textAlignment = NSTextAlignmentLeft;
    
    if (self.ticker==nil) {
        return;
    }
    
    METicker *ticker = self.ticker;
    
    if (self.candlesModel.ticker!=self.ticker) {
        self.candlesModel.ticker = ticker;
        [self unsubscribe];
        [self subscribe];
    }
    
    UILabel  *label = self.chartView.titleView.textLabel;
    UILabel  *priceLable = self.chartView.titleView.priceLabel;
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:ticker.name?ticker.name:@"-"];
    NSAttributedString *space = [[NSAttributedString alloc] initWithString:@" "];
    if (text) {
        
        NSString *hint_of_ticker;
        if (ticker.issuer && ticker.issuer.ID)
            hint_of_ticker = ticker.issuer.caption;
        else
            hint_of_ticker = ticker.hint;
        
        NSAttributedString *hint = [[NSAttributedString alloc]
                                    initWithString:hint_of_ticker
                                    attributes: @{
                                                  NSFontAttributeName: [UIFont fontWithName:ME_UI_FONT size:label.font.pointSize*.7],
                                                  NSForegroundColorAttributeName: ME_UI_GREY_FONT_COLOR
                                                  }
                                    ];
        
        [text appendAttributedString:space];
        [text appendAttributedString:hint];
        
        label.attributedText = text;
    }
    
    NSMutableAttributedString *price = [[NSMutableAttributedString alloc] initWithAttributedString: [NSAttributedString coloredPrice:ticker.lastPrice andPrevPrice:ticker.prevPrice]];
    NSAttributedString *change = [NSAttributedString changePrice:ticker.lastPrice andChange:ticker.change];
    
    if (price) {
        [price appendAttributedString:space];
        [price appendAttributedString:change];
        priceLable.attributedText = price;
    }
    
}

- (MECandlesModel*) candlesModel{
    if (_candlesModel) {
        return _candlesModel;
    }
    
    _candlesModel = [MECandlesModel sharedInstance];
    _candlesModel.ticker = self.ticker;
    _candlesModel.delegate = self;
    
    return _candlesModel;
}

- (void) viewWillAppear:(BOOL)animated{
    
    [self.activityIndicator stopAnimating];
    
    [super viewWillAppear:animated];
        
    [self rotateSetup:[UIApplication sharedApplication].statusBarOrientation];
    [self resetPath:[UIApplication sharedApplication].statusBarOrientation];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.candlesModel.size = 127;
    }
    else
        self.candlesModel.size = 72;
    
    _isShown = YES;
    
    [self contraintUpdateIfNeed:[UIApplication sharedApplication].statusBarOrientation];
}

- (void) unsubscribe{
    [self.candlesModel unsubscribe];
}

- (void) subscribe{
    [self.candlesModel subscribeIntervals];
    [self.candlesModel setCurrentInterval:self.currentInterval];
    [self.candlesModel subscribe];
    
    NSString *type = @"bar";
    
    switch (self.seriesType) {
        case MESCHART_BAR:
            break;
            
        case MESCHART_LINE:
            type = @"line";
            break;
            
        case MESCHART_OHLC:
            type = @"ohcl";
            break;
            
        case MESCHART_CANDLE:
            type = @"candle";
            break;
            
        default:
            break;
    }
    
    //NSLog(@" ticker : %@", self.ticker);
    if (self.ticker != nil)
        [Answers logContentViewWithName:@"Chart View"
                            contentType:nil
                              contentId:nil
                       customAttributes:@{
                           @"Chart View: ticker": self.ticker.ID,
                           @"Chart View: interval": self.currentInterval,
                           @"Chart View: type": type,
                       }];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self subscribe];
    
    savedTransoform = self.view.transform;
    savedFrame = self.view.frame;
    
    [self update];
}

- (void) viewWillDisappear:(BOOL)animated{
    _isShown = NO;
    [super viewWillDisappear:animated];
    [self unsubscribe];
    lastTicker = self.ticker;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
}

- (void) loginSuccessed:(NSNotification*)event{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideMessage];
        [self.candlesModel setCurrentInterval:self.currentInterval];
        [self.candlesModel subscribe];
    });
}

- (void) redrawDecor{
    
    CGRect f = self.chartView.frame;
    f.size.width = self.view.bounds.size.width;
    self.chartView.frame = f;
    
    CGRect rect =CGRectMake(self.chartView.rootLayer.frame.origin.x, self.chartView.rootLayer.frame.origin.y+0.5+self.chartView.graphLayer.bounds.size.height, self.chartView.graphLayer.bounds.size.width, 1);
    
    self.graphDeviderImageView.frame = rect;
    rect.origin.y +=self.chartView.zonePadding+[self volumeZoneHeight]-self.volumeZone.grid.xAxis.width;
    self.volumeDeviderImageView.frame = rect;
    
    rect =CGRectMake(self.chartView.rootLayer.frame.origin.x-1, self.chartView.rootLayer.frame.origin.y, 1, self.chartView.graphLayer.bounds.size.height);
    self.leftDeviderImageView.frame = rect;
    rect.origin.x += self.chartView.graphLayer.bounds.size.width+1.5;
    
    self.rightDeviderImageView.frame = rect;
    
//    [UIView animateWithDuration:ME_UI_CHART_ANIMATION animations:^{
//        self.chartView.alpha = 1.0;
//    }];
}

- (void) redraw:(UIInterfaceOrientation)orientation{    
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self flushXLabels];        
        [self.chartView redraw];
    });
}

- (void) viewDidLayoutSubviews{
    [self redrawDecor];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    isRotating = NO;
    [self redraw:[UIApplication sharedApplication].statusBarOrientation];
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];    
}

- (void) resetPath:(UIInterfaceOrientation)orientation{
    if (orientation == UIInterfaceOrientationIsPortrait(orientation) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.chartView.graphLayer.opacity = 1.0;
        self.chartView.candlesLayer.grouthCanvas.opacity = 0.0;
        self.chartView.candlesLayer.fallingCanvas.opacity = 0.0;
    }
    else{
        self.chartView.graphLayer.opacity = 0.0;
        self.chartView.candlesLayer.grouthCanvas.opacity = 1.0;
        self.chartView.candlesLayer.fallingCanvas.opacity = 1.0;
    }
}

- (void) contraintUpdateIfNeed:(UIInterfaceOrientation)orientation{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        if (UIInterfaceOrientationIsLandscape(orientation)) {
            constraintTopHeight.constant = 0;
        }
        else{
            constraintTopHeight.constant = ME_UI_TICKERINFO_HEIGHT;
        }
    }
    
    [self.view setNeedsUpdateConstraints];
    [self.chartView setNeedsUpdateConstraints];    
    
    [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration 
                     animations:^{
                         [self.view layoutIfNeeded];
                         [self.chartView layoutIfNeeded];
                     }     
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration{
    isRotating = YES;
        
    [super willRotateToInterfaceOrientation:orientation duration:duration];
    
    [self rotateSetup:orientation];
    if (_seriesType != MESCHART_LINE) {
        [self resetPath:orientation];
    }

    [self contraintUpdateIfNeed:orientation];
    [self redraw:[UIApplication sharedApplication].statusBarOrientation];
}

- (CGRect) restoredViewFrame:(UIInterfaceOrientation)deviceOrientation{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect frame ;
    
    switch (deviceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            frame = CGRectMake(0, screenRect.size.height/2., screenRect.size.width, screenRect.size.height/2.);
            break;
        case UIDeviceOrientationLandscapeRight:
        case UIDeviceOrientationLandscapeLeft:
            frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
            break;
        default:
            frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
            break;
    }
    
    return frame;
}


#pragma mark - SChart data

- (MESChartSeries*) meSChartView:(MESChartView *)chart seriesAtIndex:(NSInteger)index{
    MESChartSeries *series = [[MESChartSeries alloc] init];
    if (index == 0) {
        self.seriesType = _seriesType; // reset to current
        series.type = self.seriesType;
        series.zoneIndex = 0;
    }
    else if (index==1) {
        series.type = MESCHART_BAR;
        series.zoneIndex = 1;
    }
    return series;
}

- (NSInteger) numberOfSeriesInSChartView:(MESChartView *)chart{
    return self.candlesModel.series;
}

- (NSInteger) meSChartView:(MESChartView *)chart numberOfChartDataForSeriesAtIndex:(NSInteger)index{
    return [self.candlesModel dataCountAtSeries:index];
}

- (MESChartData*) meSChartView:(MESChartView *)chart chartDataAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    MESChartData *data = [self.candlesModel dataAtIndex:dataIndex forSeriesIndex:seriesIndex];
    return data;
}

- (MESChartDataRange*) dataRangeInSChartView:(MESChartView *)chart atSeriesIndex:(NSInteger)index{
    return [self.candlesModel dataRangeAtSeriesIndex:index];
}

#pragma mark - SChart delegate

- (NSNumberFormatter*) numberFormatter{
    if (!_numberFormatter) {
        
        _numberFormatter = [[NSNumberFormatter alloc] init];
        
        [_numberFormatter allowsFloats];
        [_numberFormatter setLocale:[NSLocale currentLocale]];
        [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [_numberFormatter setAlwaysShowsDecimalSeparator:NO];
    }
    
    NSInteger precision = self.ticker.lastPrice.precision; if (precision<0) precision=2;
    
    [_numberFormatter setMaximumFractionDigits:precision];
    [_numberFormatter setMinimumFractionDigits:precision];
    return _numberFormatter;
}

- (void) meSChartView:(MESChartView *)chart yAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    if (seriesIndex==1) {
        label.attributedText = [NSAttributedString compoundValToday:(NSNumber*)value];
        label.textAlignment = NSTextAlignmentRight;
        
        CGSize size = label.bounds.size;
        CGRect gridF = self.chartView.gridLayer.bounds;
        size = [label sizeThatFits:CGSizeMake(gridF.size.width, label.bounds.size.height)];
        label.frame = CGRectMake(gridF.size.width - size.width + self.chartView.gridLayer.yAxis.labelPadding+self.chartView.edgePadding, label.frame.origin.y, size.width, label.bounds.size.height);
        label.backgroundColor = [UIColor colorWithWhite:1 alpha:0.7];
        return;    }
    label.text = [self.numberFormatter stringFromNumber:value];
}

#pragma mark - SChart formater

- (void) flushXLabels{
    for (UIView *label in self.chartView.subviews) {
        if ([label isKindOfClass:[UILabel class]]) {
            if (label.tag >1000) {
                [label removeFromSuperview];
            }
        }
    }
}

- (void) meSChartView:(MESChartView *)chart xAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    NSDate *time = value; //[MECandlesModel dateFromString:[NSString stringWithFormat:@"%@", value]];
    
    if (time==nil) {
        label.text = @"-";
        return;
    }
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:time];
    label.textAlignment = NSTextAlignmentLeft;
    
    NSInteger tag=tickIndex+1000*(seriesIndex+1);
    UILabel *dateLabel = (UILabel*) [label.superview viewWithTag:tag];
    
    if (seriesIndex==1) {
        
        
        if (!dateLabel) {
            dateLabel = [[UILabel alloc] init];
            dateLabel.backgroundColor = [UIColor clearColor];
            dateLabel.font = [UIFont fontWithName:label.font.fontName size:label.font.pointSize/1.1];
            dateLabel.textColor = [UIColor colorWithCGColor:CGColorCreateCopyWithAlpha(label.textColor.CGColor, 0.9)];
            dateLabel.tag = tag;
            dateLabel.textAlignment = label.textAlignment;
            [label.superview addSubview:dateLabel];
        }
        
        CGRect rect =label.frame;
        rect.origin.y += rect.size.height;
        dateLabel.frame = rect;
        dateLabel.text = @"";
    }
    
    
    NSString *timetext;
    if (components.hour==0) {
        timetext = [NSDateFormatter localizedStringFromDate:time dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
    }
    else{
        if ((tickIndex==0 || ![[self.lastTickDate dateOnly] isEqualToDate:[time dateOnly]]) && dateLabel) {
            dateLabel.text = [NSDateFormatter localizedStringFromDate:time dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
        }
        timetext = [NSDateFormatter localizedStringFromDate:time dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
    }
    label.text = timetext;
    
    self.lastTickDate = time;
}

- (void) meSChartView:(MESChartView *)chart headerView:(UILabel *)label atLeftFingerData:(MESChartData *)leftData atRightFingerData:(MESChartData *)rightData{
    
    void(^bold_value)(UILabel *label, NSString* timestr, NSString *value) = ^(UILabel *label, NSString* timestr, NSString *value) {
        NSMutableAttributedString *attrtext=[[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@:  ", timestr]];
        NSMutableAttributedString *attrvolume=[[NSMutableAttributedString alloc] initWithString: value];
        
        [attrvolume addAttribute:NSFontAttributeName value:[UIFont fontWithName:ME_UI_BOLD_FONT size:self.timeTitleView.timeTextLabel.font.pointSize] range:NSMakeRange(0, attrvolume.length)];
        
        [attrtext appendAttributedString:attrvolume];
        label.attributedText = attrtext;
    };

    NSDateFormatterStyle(^date_style)(NSDate *date) = ^NSDateFormatterStyle(NSDate *date) {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:date];
        NSDateFormatterStyle st =NSDateFormatterShortStyle;
        if (components.hour==0) {
            st=NSDateFormatterNoStyle;
        }
        return st;
    };
    
    NSNumber *value;
    if (leftData && rightData) {
        CGFloat trend = 0.;
        NSNumber *numberTrend;
        if ([leftData.yValue isKindOfClass:[MESChartOHLC class]]) {
            MESChartOHLC *ohlc_left = leftData.yValue;
            MESChartOHLC *ohlc_right = rightData.yValue;
            trend = ohlc_right.floatValue - ohlc_left.floatValue;
            value = [NSNumber numberWithFloat:ohlc_left.floatValue];
        }
        else{
            value = leftData.yValue;
        }
        
        if (trend>0) {
            label.backgroundColor = ME_UI_TREND_POSITIVE_COLOR_ON_CHART;//[UIColor colorWithRed:0.1 green:0.8 blue:0.01 alpha:0.7];
            self.timeTitleView.timeTextLabel.textColor = ME_UI_TREND_POSITIVE_COLOR_ON_GREY;
        }
        else if (trend<0){
            label.backgroundColor = ME_UI_TREND_NEGATIVE_COLOR_ON_CHART;// [UIColor colorWithRed:0.8 green:0.1 blue:0.01 alpha:0.7];
            self.timeTitleView.timeTextLabel.textColor = ME_UI_TREND_NEGATIVE_COLOR_ON_GREY;
        }
        else{
            self.timeTitleView.timeTextLabel.textColor=[UIColor whiteColor];
            label.backgroundColor = ME_UI_TREND_NEUTRAL_COLOR; // [UIColor colorWithRed:0.01 green:0.1 blue:0.8 alpha:0.7];
        }
        
        numberTrend = [NSNumber numberWithFloat:trend];
        NSNumber *trendInPerc = [NSNumber numberWithFloat: (trend/value.floatValue)*100.];
        
        NSString *sign = trend>0.?@"+":@"";
        
        NSString *valuestr=[NSString stringWithFormat:@"%@%@ %@%@%%", sign, [self.numberFormatter stringFromNumber:numberTrend], sign, [self.numberFormatter stringFromNumber:trendInPerc]];
        if (self.chartView.headerView.hidden) {
            self.timeTitleView.timeTextLabel.text = valuestr;
        }
        else{
            label.text = valuestr;
            NSDate *lefttime = leftData.xValue;
            NSDate *righttime = rightData.xValue ;
            NSDateFormatterStyle st =date_style(lefttime);
            NSString *timestr = [NSString stringWithFormat:@"%@ - %@ ",
                                 [NSDateFormatter localizedStringFromDate:lefttime dateStyle:NSDateFormatterShortStyle timeStyle:st],
                                 [NSDateFormatter localizedStringFromDate:righttime dateStyle:NSDateFormatterShortStyle timeStyle:st]];
            self.timeTitleView.timeTextLabel.text = timestr;
        }
        
    }
    else if (leftData){
        self.timeTitleView.timeTextLabel.textColor = [UIColor whiteColor];
        NSString *value_string;
        if ([leftData.yValue isKindOfClass:[MESChartOHLC class]]) {
            MESChartOHLC *ohlc_left = leftData.yValue;
            value = [NSNumber numberWithFloat:ohlc_left.floatValue];
            if (self.seriesType==MESCHART_OHLC || self.seriesType==MESCHART_CANDLE) {
                value_string = [NSString stringWithFormat:@"%@/%@",
                                [self.numberFormatter stringFromNumber:ohlc_left.open],
                                [self.numberFormatter stringFromNumber:ohlc_left.close]
                                ];
            }
            else{
                value_string = [self.numberFormatter stringFromNumber:ohlc_left.close];
            }
        }
        else{
            value = leftData.yValue;
            value_string = [self.numberFormatter stringFromNumber:value];
        }
        
        label.backgroundColor = [UIColor colorWithRed:0.01 green:0.1 blue:0.8 alpha:0.7];
        
        NSString *valuestr=[self.numberFormatter stringFromNumber:value];
        NSString *timestr = [NSDateFormatter localizedStringFromDate:leftData.xValue dateStyle:NSDateFormatterShortStyle timeStyle:date_style(leftData.xValue)];
        
        if (self.chartView.headerView.hidden) {
            bold_value(self.timeTitleView.timeTextLabel, timestr, valuestr);
        }
        else{
            label.text = value_string;
            // volume:
            MESChartData *volumeData=[self.candlesModel dataAtIndex:leftData.index forSeriesIndex:1];
            bold_value(self.timeTitleView.timeTextLabel, timestr, [[NSAttributedString compoundValToday:volumeData.yValue] string]);
        }
    }
}

- (NSInteger) numberOfZonesInSChartView:(MESChartView *)chart{
    return 1;
}

- (CGFloat) volumeZoneHeight{
    CGFloat height = 0.0;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            height = 60.;
        }
        else{
            height = 30.;
        }
    }
    else{
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            height = 80.;
        }
        else{
            height = 30.;
        }
    }
    return height;
}

- (MESChartZone*) volumeZone{
    
    if (_volumeZone) {
        _volumeZone.height = [self volumeZoneHeight];
        return _volumeZone;
    }
    
    _volumeZone = [[MESChartZone alloc] init];
    _volumeZone.height = [self volumeZoneHeight];
    _volumeZone.grid = [MESChartGrid layer];
    _volumeZone.grid.xAxis.width = 15.0;
    _volumeZone.grid.yLayer.lineWidth = 0.0;
    _volumeZone.grid.yAxis.font = [UIFont fontWithName:ME_UI_FONT size:10];
    
    _volumeZone.graph = [MESChartCanvas layer];
    _volumeZone.graph.fillColor = ME_UI_CANDLE_FILL_COLOR.CGColor;
    _volumeZone.graph.strokeColor = [UIColor clearColor].CGColor;
    _volumeZone.graph.enableGradient = NO;
    _volumeZone.graph.lineWidth = 0.0;
    _volumeZone.grid.adjustXGridToOverMidnight = YES;
    
    return _volumeZone;
}

- (MESChartZone*) meSChartView:(MESChartView *)chart zoneAtIndex:(NSInteger)zoneIndex{
    // always == 0
    self.volumeZone.grid.xLayer.lineWidth = self.chartView.gridLayer.xLayer.lineWidth;
    self.volumeZone.grid.yAxis.font = self.chartView.gridLayer.yAxis.font;
    self.volumeZone.grid.yAxis.textColor = self.chartView.gridLayer.yAxis.textColor;
    self.volumeZone.grid.xAxis.font = self.chartView.gridLayer.xAxis.font;
    self.volumeZone.grid.xAxis.textColor = self.chartView.gridLayer.xAxis.textColor;
    self.volumeZone.grid.xAxis.ticks = self.chartView.gridLayer.xAxis.ticks;
    self.volumeZone.grid.yAxis.ticks = self.chartView.gridLayer.yAxis.ticks;
    return self.volumeZone;
}


#pragma mark - Candles protocol

- (void) willCandlesModelUpdateData:(MECandlesModel *)candles{
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self.activityIndicator startAnimating];
    });
}

- (void) didCandlesModel:(MECandlesModel *)candles accessDeny:(NSError *)error{
    hasNotAccess = YES;
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self.activityIndicator stopAnimating];

        [self redraw:[UIApplication sharedApplication].statusBarOrientation];        

        if (error && [UIApplication sharedApplication].applicationState == UIApplicationStateActive){
            self.messageText.text = [NSString stringWithFormat:NSLocalizedString(@"Ticker %@ has not access to candles.", @""), self.candlesModel.ticker.name];
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                self.messageText.alpha = 1.0;
                self.chartView.alpha = 0.0;
            }];
        }
    });
}

- (void) didCandlesModel:(MECandlesModel *)candles fault:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];

        [self redraw:[UIApplication sharedApplication].statusBarOrientation];
        
        if (error && [UIApplication sharedApplication].applicationState == UIApplicationStateActive){
            self.messageText.text = [NSString stringWithFormat:NSLocalizedString(@"Candles for %@ could not response.", @""), self.candlesModel.ticker.name];
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                self.messageText.alpha = 1.0;
                self.chartView.alpha = 0.0;
            }];
        }
        
        for (MEIntervalButton *button in self.intervalsView.subviews) {
            if ([button.interval isEqualToString:candles.currentInterval]) {
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button.userInteractionEnabled = YES;
            }
        }
    });
}

- (void) hideMessage{
    dispatch_async(dispatch_get_main_queue(), ^{                
        if (self.messageText.alpha>=0.9) {
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                self.messageText.alpha = 0.0;
                self.chartView.alpha = 1.0;
            }];
        }
    });
}

- (void) didCandlesModelUpdateData:(MECandlesModel *)candles{
    
    hasNotAccess = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [self hideMessage];
    
        [self redraw:[UIApplication sharedApplication].statusBarOrientation];
        
        [self.activityIndicator stopAnimating];
        
        for (MEIntervalButton *button in self.intervalsView.subviews) {
            if ([button.interval isEqualToString:candles.currentInterval]) {
                [button setTitleColor:ME_UI_CANDLE_FILL_COLOR forState:UIControlStateNormal];
                button.userInteractionEnabled = NO;
            }
            else{
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button.userInteractionEnabled = YES;
            }
        }
    });
}

- (void) didCandlesModelUpdateIntervals:(MECandlesModel *)candles{
    
    if (intervalsUpdated || isRotating) {
        return;
    }
    
    intervalsUpdated = YES;
    
    int i=0;
    MEIntervalButton *button, *prevbutton;
    for (NSString *interval in candles.intervals) {
        button = [[MEIntervalButton alloc] initWithFrame:self.intervalsView.bounds];
        button.adjustsImageWhenHighlighted=YES;
        button.showsTouchWhenHighlighted=YES;
        button.alpha = 0.8;
        button.translatesAutoresizingMaskIntoConstraints = NO;
        button.interval = interval;
        button.titleLabel.adjustsFontSizeToFitWidth = YES;
        button.titleLabel.minimumScaleFactor = 0.7;
        button.titleLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
        
        if ([self.candlesModel.currentInterval isEqualToString:interval]) {
            [button setTitleColor:ME_UI_CANDLE_FILL_COLOR forState:UIControlStateNormal];
            button.userInteractionEnabled = NO;
        }
        
        [button setTitle:NSLocalizedString(interval, @"") forState:UIControlStateNormal];
        
        [self.intervalsView addSubview:button];
        [button addTarget:self action:@selector(changeInterval:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.intervalsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[button]-0-|" options:0 metrics:nil views:@{@"button":button}]];
        
        if (prevbutton) {
            [self.intervalsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prevbutton]-5-[button]" options:0 metrics:nil views:@{@"button":button, @"prevbutton":prevbutton}]];
        }
        else{
            [self.intervalsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(5)-[button]" options:0 metrics:nil views:@{@"button":button}]];
        }
        
        i++; prevbutton=button;
    }
    [self.intervalsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[button]-(>=5)-|" options:0 metrics:nil views:@{@"button":button}]];
}

- (void) changeInterval:(MEIntervalButton*) intervalButton{
    
    if ([self.currentInterval isEqualToString:intervalButton.interval] && self.messageText.alpha==0.0) {
        return ;
    }
    
    [self.candlesModel unsubscribe];
    
    self.currentInterval = intervalButton.interval;
    intervalButton.userInteractionEnabled = NO;    
    [self.candlesModel setCurrentInterval:self.currentInterval];
    [self.candlesModel subscribe];
}


#pragma mark - delegate

- (void) meSChartView:(MESChartView *)chart beganTouchWithPoints:(NSArray *)points withDataIndex:(NSArray *)indxs atSeriesindex:(NSInteger)seriesIndex{
    if ([self.view.superview isKindOfClass:[UIScrollView class]]) {
        UIScrollView *sv = (UIScrollView *)self.view.superview;
        hasScroll = sv.scrollEnabled;
        sv.scrollEnabled = NO;            
    }
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        self.intervalsView.alpha = 0.0;
        self.timeTitleView.alpha = 1.0;
    }];
}

- (void) meSChartView:(MESChartView *)chart finishedTouchWithPoints:(NSArray *)points withDataIndex:(NSArray *)indxs atSeriesindex:(NSInteger)seriesIndex{
    
    if (hasScroll) {
        UIScrollView *sv = (UIScrollView *)self.view.superview;
        sv.scrollEnabled = YES;
    }
    
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        self.intervalsView.alpha = 1.0;
        self.timeTitleView.alpha = 0.0;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
