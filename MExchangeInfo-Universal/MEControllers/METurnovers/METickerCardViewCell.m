//
//  METickerCardViewCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/17/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerCardViewCell.h"
#import "MEUIPreferences.h"
#import "NSDate+MEComparing.h"
#import "METickerCardsModel.h"
#import <ObjectiveSugar/ObjectiveSugar.h>

@interface METickerCardViewCell()
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UITextView *valueText;
@property(nonatomic,strong) UILabel *valueInfoLabel;
@end

@implementation METickerCardViewCell
{
    NSLayoutConstraint *infoConstraint;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) setValue:(id)value{
    _value=value;    
    
    NSString *value_str;

    
    if ([_value isKindOfClass:[NSDate class]]) {
        value_str = [NSDateFormatter localizedStringFromDate:(NSDate*) _value
                                               dateStyle:NSDateFormatterMediumStyle
                                               timeStyle:NSDateFormatterNoStyle];
    }
    else if ([self validateDateString:[_value description]]){
        value_str = [NSDate dateStringToFormatter:(NSString*)_value];
    }
    else
        value_str = [[GIValueFormatter sharedObject] string: _value];

    self.valueLabel.text = value_str;
    self.valueLabel.hidden = NO;
    self.valueText.hidden = YES;
    _firstUrl = nil;
    self.accessoryType = UITableViewCellAccessoryNone;
}

-(void) setUrls:(id)urls{

    _urls=urls;
    
    NSArray *many = [[[GIValueFormatter sharedObject] string: _urls] componentsSeparatedByString:@";"];
    
    if (many.count>1) {
        __block NSMutableString *m=[[NSMutableString alloc] init];
        [many each:^(NSString *o) {
            [m appendFormat:@"%@\n",[o stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }];            
        self.valueText.text = m;
        [self.valueText sizeToFit];
        
        self.valueText.hidden = NO;
        self.valueLabel.hidden = YES;
        _firstUrl = [many firstObject];
        
        self.accessoryType = UITableViewCellAccessoryNone;

    }else{
        _firstUrl = _urls;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.userInteractionEnabled = YES;

        self.valueText.hidden = YES;
        self.valueLabel.hidden = NO;
        
        self.valueLabel.text = _firstUrl;
        [self.valueLabel sizeToFit];
    }
}

- (void) setInfo:(NSString *)info{
    if (info) {
        if ([self validateDateString:info])
            _info = [NSDate dateStringToFormatter:(NSString*)info];
        else
            _info = info;
        self.valueInfoLabel.text = _info ;    
        self.valueInfoLabel.hidden = NO;
        [self.valueInfoLabel sizeToFit];
        infoConstraint.constant=14;
    }
    else{
        infoConstraint.constant=0;
        self.valueInfoLabel.text=@"";
        self.valueInfoLabel.hidden = YES;
        [self.valueInfoLabel sizeToFit];
    }
}


- (void) setCaption:(NSString *)caption{
    self.nameLabel.text= _caption=caption;
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 44)];
        self.valueInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        self.valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueInfoLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
        self.nameLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.nameLabel.minimumScaleFactor=0.8;
        self.nameLabel.adjustsFontSizeToFitWidth = YES;
        self.nameLabel.numberOfLines = 4;
    
        self.valueLabel.font = [UIFont fontWithName:ME_UI_FONT size:15];
        self.valueLabel.minimumScaleFactor=0.7;
        self.valueLabel.adjustsFontSizeToFitWidth = YES;
        self.valueLabel.numberOfLines = 6;
        self.valueLabel.textAlignment = NSTextAlignmentLeft;
        
        self.valueInfoLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.valueInfoLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        self.valueInfoLabel.textAlignment = NSTextAlignmentLeft;
        
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.valueLabel];
        [self.contentView addSubview:self.valueInfoLabel];
        
        
        self.valueText = [[UITextView alloc] initWithFrame:self.valueLabel.bounds];
        self.valueText.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueText.backgroundColor = [UIColor clearColor];
        self.valueText.scrollEnabled = NO;
        self.valueText.editable = NO;
        self.valueText.dataDetectorTypes = UIDataDetectorTypeLink|UIDataDetectorTypeAddress;
        self.valueText.font = [UIFont fontWithName:ME_UI_FONT size:14];
        self.valueText.contentMode = UIViewContentModeScaleToFill;
        self.valueText.textAlignment = NSTextAlignmentLeft;
        
        [self.contentView addSubview:self.valueText];
        self.valueText.backgroundColor = [UIColor clearColor];
        [self.valueText.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[text(<=120)]-4-|" options:0 metrics:nil views:@{@"text":self.valueText}]];
        [self.valueText.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[name]-10-[text]-0-|" options:0 metrics:nil views:@{@"text":self.valueText, @"name": self.nameLabel}]];
                
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[name]-4-|" options:0 metrics:nil views:@{@"name":self.nameLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(4)-[value(>=14)]-2-[info(<=14)]-(4)-|" options:0 metrics:nil views:@{@"value":self.valueLabel, @"info":self.valueInfoLabel}]];
        
        [self.contentView addConstraint:infoConstraint=[NSLayoutConstraint 
                                        constraintWithItem:self.valueInfoLabel 
                                        attribute:NSLayoutAttributeHeight 
                                        relatedBy:NSLayoutRelationLessThanOrEqual 
                                        toItem:nil attribute:NSLayoutAttributeNotAnAttribute 
                                         multiplier:1 constant:14]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[name(120)]-10-[value]-10-|" options:0 metrics:nil views:@{@"name":self.nameLabel, @"value":self.valueLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[name]-10-[info]-10-|" options:0 metrics:nil views:@{@"name":self.nameLabel, @"info":self.valueInfoLabel}]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.valueLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.valueText attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        self.contentView.layer.shouldRasterize=YES;
        self.contentView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.valueLabel.layer.shouldRasterize=YES;
        self.valueLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.valueText.layer.shouldRasterize=YES;
        self.valueText.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.valueInfoLabel.layer.shouldRasterize=YES;
        self.valueInfoLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.nameLabel.layer.shouldRasterize=YES;
        self.nameLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        [self addDevider];
    }
    
    return self;
}

- (BOOL) validateDateString:(NSString*)date{
    NSString *dateRegEx = @"^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$";
    NSPredicate *dateTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", dateRegEx];
    if ([dateTest evaluateWithObject:date] == YES)
        return YES;
    return NO;
}

@end
