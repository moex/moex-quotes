//
//  METurnoversSectorsPalette.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/22/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ME_TURNOVERS_PALETTE @[\
[UIColor colorWithRed:99./255.  green:177./255. blue:229./255. alpha:1.],\
[UIColor colorWithRed:159./255. green:193./255. blue:57./255. alpha:1.],\
[UIColor colorWithRed:247./255. green:148./255. blue:30./255. alpha:1.],\
[UIColor colorWithRed:112./255. green:111./255. blue:96./255. alpha:1.],\
[UIColor colorWithRed:255./255. green:206./255. blue:0./255. alpha:1.],\
[UIColor colorWithRed:42./255.  green:111./255. blue:173./255. alpha:1.],\
[UIColor colorWithRed:255./255. green:56./255.  blue:56./255. alpha:1.],\
[UIColor colorWithRed:245./255. green:144./255. blue:181./255. alpha:1.]\
]
