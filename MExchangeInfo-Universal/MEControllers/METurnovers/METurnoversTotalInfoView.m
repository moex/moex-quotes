//
//  METurnoversTotalInfoView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/14/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//
//
//  TODO: pie chart see: http://stackoverflow.com/questions/18614376/calayer-clip-cgcontextaddarc-making-a-donut-slide-pie-chart
//
//


#import "METurnoversTotalInfoView.h"
#import "MEUIPreferences.h"
#import <MEinfoCXConnector/GIStompBody.h>
#import "NSAttributedString+MEFormatedString.h"
#import "METurnoversSectorsPalette.h"

@interface METurnoversTotalInfoView() <MEPieChartViewDataSource,MEPieChartViewDelegate>
@property(nonatomic,strong) MEPieChartView *pieChartView;
@property(nonatomic,strong) UILabel *captionLabel;
@property(nonatomic,strong) UILabel *captionValTodayLabel;
@property(nonatomic,strong) UILabel *captionValTodayUSDLabel;
@property(nonatomic,strong) UILabel *captionNumtradesLabel;
@property(nonatomic,strong) UILabel *valTodayLabel;
@property(nonatomic,strong) UILabel *valTodayUSDLabel;
@property(nonatomic,strong) UILabel *numtradesLabel;
@end

@implementation METurnoversTotalInfoView
{
    METurnoversInfo *info;
    NSNumber        *pieSize;
    CGFloat         pieLineWidth;
    UIImageView *bottomLine;
}


- (void) configure{
    
    pieLineWidth = 6.0;
    pieSize = [NSNumber numberWithFloat:120.];
    
    self.pieChartView = [[MEPieChartView alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    //self.pieChartView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
    
    self.captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    self.captionValTodayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 20)];
    self.captionValTodayUSDLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 100, 20)];
    self.captionNumtradesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 100, 20)];
    
    self.valTodayLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 20, 100, 20)];
    self.valTodayUSDLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 40, 100, 20)];
    self.numtradesLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 60, 100, 20)];
    
    self.captionNumtradesLabel.adjustsFontSizeToFitWidth = self.captionValTodayLabel.adjustsFontSizeToFitWidth =self.captionValTodayUSDLabel.adjustsFontSizeToFitWidth = self.captionLabel.adjustsFontSizeToFitWidth = YES;
    self.captionValTodayLabel.minimumScaleFactor = self.captionNumtradesLabel.minimumScaleFactor = self.valTodayUSDLabel.minimumScaleFactor = self.captionLabel.minimumScaleFactor = 0.5;
    
    self.captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.captionNumtradesLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.captionValTodayUSDLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.captionValTodayLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.valTodayLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.valTodayUSDLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.numtradesLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.pieChartView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.numtradesLabel.textAlignment = self.valTodayUSDLabel.textAlignment = self.valTodayLabel.textAlignment = NSTextAlignmentRight;
    
    self.captionLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
    self.captionLabel.textColor = ME_UI_GREY_FONT_COLOR;
    self.captionValTodayLabel.font = self.captionValTodayUSDLabel.font = self.captionNumtradesLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
    self.captionValTodayLabel.textColor = self.captionValTodayUSDLabel.textColor = self.captionNumtradesLabel.textColor = ME_UI_GREY_FONT_COLOR;
    
    self.valTodayLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:16];
    self.valTodayUSDLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
    self.numtradesLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
    
    self.valTodayUSDLabel.textColor = self.numtradesLabel.textColor = ME_UI_GREY_FONT_COLOR;
    
    [self addSubview:self.captionLabel];
    [self addSubview:self.captionValTodayLabel];
    [self addSubview:self.captionValTodayUSDLabel];
    [self addSubview:self.captionNumtradesLabel];
    [self addSubview:self.valTodayLabel];
    [self addSubview:self.valTodayUSDLabel];
    [self addSubview:self.numtradesLabel];
    [self addSubview:self.pieChartView];
    
    UIImage *thin_grey_line = [UIImage imageNamed:@"thin_grey_line"];
    bottomLine = [[UIImageView alloc] initWithImage:thin_grey_line];
    [self addSubview:bottomLine];
    bottomLine.translatesAutoresizingMaskIntoConstraints=NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomLine]-0-|" options:0 metrics:nil views:@{@"bottomLine": bottomLine}]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:bottomLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:thin_grey_line.size.height<=1?0:0.5]];
    
    self.backgroundColor = [UIColor whiteColor];
}


#pragma mark -    PieChartViewDelegate

-(CGFloat)centerCircleRadius{
    return pieSize.floatValue/2.0-pieLineWidth;
}


#pragma mark - PieChartViewDataSource

-(NSUInteger)numberOfSlicesInPieChartView:(MEPieChartView *)pieChartView{
    return [info.turnovers rowsForSection:0];
}

-(UIColor *)pieChartView:(MEPieChartView *)pieChartView colorForSliceAtIndex:(NSUInteger)paletteIndex{
    NSInteger  index = paletteIndex % ME_TURNOVERS_PALETTE.count;
    return ME_TURNOVERS_PALETTE[index];
}

-(double)pieChartView:(MEPieChartView *)pieChartView valueForSliceAtIndex:(NSUInteger)index{
    METurnoverItem *item = [info.turnovers turnoverForIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    NSInteger rows = [info.turnovers rowsForSection:0];
    
    double sum=0.;
    for (int i=0; i<rows; i++) {
        METurnoverItem *r = [info.turnovers turnoverForIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        sum += [r.valtoday.value number].doubleValue;
    }
    
    double ratio = (([item.valtoday.value number].doubleValue>0?[item.valtoday.value number].doubleValue:0.0)/sum)/rows;
    
    return ratio;
}

- (id) initWithPieInFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self configure];
        self.pieChartView.delegate = self;
        self.pieChartView.datasource = self;
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.self.pieChartView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:pieSize.floatValue]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.pieChartView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:-10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.pieChartView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[caption(>=10)]-10-[pie(height)]"
                                                                     options:0
                                                                     metrics:@{@"height":pieSize}
                                                                       views:@{
                                                                               @"pie": self.pieChartView, @"caption": self.captionLabel
                                                                               }]];
        
        self.valTodayLabel.textAlignment = NSTextAlignmentCenter;
        self.captionValTodayLabel.textAlignment = NSTextAlignmentCenter;
        self.captionValTodayLabel.numberOfLines = 2;
        self.captionValTodayUSDLabel.numberOfLines = 2;
        self.captionNumtradesLabel.numberOfLines = 2;
        
        self.valTodayLabel.adjustsFontSizeToFitWidth = YES;
        self.valTodayLabel.minimumScaleFactor = 0.5;
        self.valTodayLabel.numberOfLines = 1;
        
        self.valTodayLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:26];
        self.captionValTodayLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.pieChartView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayLabel attribute:NSLayoutAttributeBaseline relatedBy:NSLayoutRelationEqual toItem:self.pieChartView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:pieSize.floatValue-pieLineWidth*2.0-5]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.valTodayLabel attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:pieSize.floatValue-pieLineWidth*2.0-10.]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.valTodayLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:-5.]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[caption]-20-[valtodayUSD]-5-[captionValtodayUSD]-10-[numtrades]-5-[captionNumtrades]-(>=10)-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{
                                                                               @"caption": self.captionLabel,
                                                                               @"valtodayUSD": self.valTodayUSDLabel,
                                                                               @"captionValtodayUSD": self.captionValTodayUSDLabel,
                                                                               @"numtrades": self.numtradesLabel,
                                                                               @"captionNumtrades": self.captionNumtradesLabel
                                                                               }]];
        
        self.captionValTodayUSDLabel.textAlignment = self.valTodayUSDLabel.textAlignment = NSTextAlignmentLeft;
        self.captionValTodayUSDLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        self.valTodayUSDLabel.textColor = ME_UI_DARK_GREY_FONT_COLOR;
        
        self.captionNumtradesLabel.textAlignment = self.numtradesLabel.textAlignment = NSTextAlignmentLeft;
        self.captionNumtradesLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        self.numtradesLabel.textColor = ME_UI_DARK_GREY_FONT_COLOR;
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayUSDLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:20.]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayUSDLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.valTodayUSDLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.numtradesLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.valTodayUSDLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionNumtradesLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.valTodayUSDLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionNumtradesLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-4]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayUSDLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-4]];
        
    }
    
    return self;
}


- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self configure];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[caption]-5-[valtoday]-5-[valtodayUSD]-5-[numtrades]-(>=10)-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{
                                                                               @"caption": self.captionLabel,
                                                                               @"valtoday": self.valTodayLabel,
                                                                               @"valtodayUSD": self.valTodayUSDLabel,
                                                                               @"numtrades": self.numtradesLabel
                                                                               }]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayUSDLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.numtradesLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.valTodayUSDLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.numtradesLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.valTodayLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayUSDLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.valTodayUSDLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionNumtradesLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.numtradesLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayUSDLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionNumtradesLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.valTodayLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:-5]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionValTodayUSDLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.valTodayUSDLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:-5]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.captionNumtradesLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.numtradesLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:-5]];
    }
    
    return self;
}

- (void) update:(METurnoversInfo*)anInfo{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->info = anInfo;
        
        if (self->info.total==nil) {
            return ;
        }
        self.captionLabel.text = self->info.total.caption;
        self.captionValTodayLabel.text = self->info.total.valtoday.field.caption;
        self.captionValTodayUSDLabel.text = self->info.total.valtodayUsd.field.caption;
        self.captionNumtradesLabel.text = self->info.total.numtrades.field.caption;
        
        self.valTodayLabel.attributedText =  [NSAttributedString compoundValToday:[self->info.total.valtoday.value number]];
        self.valTodayUSDLabel.attributedText = [NSAttributedString compoundValToday:[self->info.total.valtodayUsd.value number]];
        self.numtradesLabel.text = [self->info.total.numtrades.value string];
        
        if (self.pieChartView) {
            [self.pieChartView reloadData];
        }
    });
}

@end
