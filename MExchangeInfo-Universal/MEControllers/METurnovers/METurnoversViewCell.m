//
//  METurnoversViewCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoversViewCell.h"
#import "MEUIPreferences.h"

@interface METurnoversViewCell()
@property(nonatomic,strong) UIImageView *ratioBarItemViewImage;
@end

@implementation METurnoversViewCell
{
    CGFloat ratio;
    NSLayoutConstraint *ratioConstarint;
    CAGradientLayer *gradient;
}

- (void) setValueRatio:(CGFloat)valueRation{
    ratio = valueRation;
}

- (void)setGradientToView:(UIView *)view
{
    if (!gradient) {
        gradient = [CAGradientLayer layer];
        gradient.frame = view.bounds;
        gradient.colors = @[
                            (id)[[UIColor colorWithRed:1. green:1.0 blue:1. alpha:1.] CGColor],
                            (id)[[UIColor colorWithRed:1. green:1. blue:1. alpha:0.5] CGColor],
                            (id)[[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:0.6] CGColor],
                            (id)[[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.6] CGColor],
                            (id)[[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.6] CGColor],
                            (id)[[UIColor colorWithRed:.4 green:.4 blue:.4 alpha:0.7] CGColor]
                            ];
        gradient.startPoint=CGPointMake(0, 0);
        gradient.endPoint=CGPointMake(1., 0);
        [view.layer insertSublayer:gradient atIndex:0];
    }
    else{
        gradient.frame = view.bounds;
    }
}



- (void) layoutSubviews{
    [super layoutSubviews];
    
    if (!_hasRatioBar)
        return;
    
    
    CGFloat newRatio = self.contentView.bounds.size.width*ratio-15.;
    
    if (newRatio<=5.)
        newRatio = 5.0;
    
    ratioConstarint.constant=newRatio;
    [self.contentView setNeedsUpdateConstraints];
    [self.contentView layoutIfNeeded];
    //[self setGradientToView:self.ratioBarItemViewImage];
    self.ratioBarItemViewImage = [self roundCornersOnView:self.ratioBarItemViewImage radius:2.5];
}

-(void) setHasRatioBar:(BOOL)hasRatioBar{
    _hasRatioBar = hasRatioBar;
    if (_hasRatioBar)
        _ratioBarItemViewImage.alpha = _ratioBarAlpha;
    else
        _ratioBarItemViewImage.alpha = 0.0;
}

- (void) setPaletteIndex:(NSInteger)paletteIndex{
    _paletteIndex=paletteIndex;
    
    NSInteger  index = paletteIndex % ME_TURNOVERS_PALETTE.count;
    self.ratioBarItemViewImage.backgroundColor = ME_TURNOVERS_PALETTE[index];
}

-(UIImageView *)roundCornersOnView:(UIImageView *)view radius:(float)radius {
    UIRectCorner corner = UIRectCornerTopRight | UIRectCornerBottomRight;
    UIImageView *roundedView = view;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = roundedView.bounds;
    maskLayer.path = maskPath.CGPath;
    roundedView.layer.mask = maskLayer;
    return roundedView;
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        NSNumber *ratioHeight = [NSNumber numberWithFloat:5.0];
        self.hasRatioBar = YES;
        self.ratioBarAlpha = 1.;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;            
        }

        self.ratioBarItemViewImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, ratioHeight.floatValue)];
        
        self.ratioBarItemViewImage.backgroundColor = [UIColor clearColor];
        self.ratioBarItemViewImage.translatesAutoresizingMaskIntoConstraints = NO;
        self.ratioBarItemViewImage.alpha = _ratioBarAlpha;
        self.ratioBarItemViewImage = [self roundCornersOnView:self.ratioBarItemViewImage radius:ratioHeight.floatValue/2.];

        [self.contentView addSubview:self.ratioBarItemViewImage];
        
        [self.ratioBarItemViewImage.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(8)-[ratio]" options:0 metrics:0 views:@{@"ratio": self.ratioBarItemViewImage}]];

        ratioConstarint=[NSLayoutConstraint
         constraintWithItem:self.ratioBarItemViewImage
         attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
         toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
        
        [self.ratioBarItemViewImage.superview addConstraint:ratioConstarint];
        
        self.captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.valtodayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.numtradesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.updateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        
        self.captionLabel.font = [UIFont fontWithName:ME_UI_FONT size:14];
        self.valtodayLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:16];
        self.valtodayLabel.minimumScaleFactor = self.captionLabel.minimumScaleFactor=0.5;
        self.valtodayLabel.adjustsFontSizeToFitWidth = self.captionLabel.adjustsFontSizeToFitWidth = YES;
        self.numtradesLabel.textAlignment = self.valtodayLabel.textAlignment = NSTextAlignmentRight;
        
        self.numtradesLabel.font = self.updateTimeLabel.font = [UIFont fontWithName:ME_UI_FONT size:11];
        self.numtradesLabel.textColor = self.updateTimeLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.numtradesLabel.minimumScaleFactor = self.updateTimeLabel.minimumScaleFactor=0.5;
        self.numtradesLabel.adjustsFontSizeToFitWidth = self.updateTimeLabel.adjustsFontSizeToFitWidth = YES;
        
        self.captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.valtodayLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.numtradesLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.updateTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:self.valtodayLabel];
        [self.contentView addSubview:self.captionLabel];
        [self.contentView addSubview:self.numtradesLabel];
        [self.contentView addSubview:self.updateTimeLabel];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[caption]-(>=10)-[valtoday(>=100)]-10-|" options:0 metrics:0 views:@{@"caption": self.captionLabel, @"valtoday": self.valtodayLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[caption]" options:0 metrics:0 views:@{@"caption": self.captionLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[valtoday]" options:0 metrics:0 views:@{@"valtoday": self.valtodayLabel}]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[time]-(>=10)-[numtrades]-10-|" options:0 metrics:0 views:@{@"time": self.updateTimeLabel, @"numtrades": self.numtradesLabel}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[caption]-5-[time]-5-[ratio(ratioHeight)]-5-|" options:0 metrics:@{@"ratioHeight": ratioHeight} views:@{@"caption": self.captionLabel, @"time": self.updateTimeLabel, @"ratio": self.ratioBarItemViewImage}]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.numtradesLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.updateTimeLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addDevider];
    }
    
    return self;
}

@end
