//
//  METurnovers3SplitController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnovers3SplitController.h"
#import "METurnoversViewController.h"

@interface METurnovers3SplitController ()
@end

@implementation METurnovers3SplitController
- (NSString*) leftSegueID{
    return ME_TURNOVERS_LEFT_SEGUE_ID;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _isApeared = YES;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _isApeared = NO;
}

@end
