//
//  METurnoversViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEArrowedViewCell.h"
#import "METurnoversSectorsPalette.h"

#define ME_TURNOVERS_SHORT_CELL_ID @"ME_TURNOVERS_SHORT_CELL_ID"

@interface METurnoversViewCell : MEArrowedViewCell
@property(nonatomic,strong) UILabel *captionLabel;
@property(nonatomic,strong) UILabel *valtodayLabel;
@property(nonatomic,strong) UILabel *numtradesLabel;
@property(nonatomic,strong) UILabel *updateTimeLabel;

@property(nonatomic,assign) CGFloat valueRatio;
@property(nonatomic,assign) BOOL hasRatioBar;
@property(nonatomic,assign) CGFloat ratioBarAlpha;

@property(nonatomic,assign) NSInteger paletteIndex;

@end
