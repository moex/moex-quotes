//
//  METurnoversViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoversViewController.h"
#import "METurnoversSector.h"
#import "METurnoversViewCell.h"
#import "METurnoverItem.h"
#import "NSAttributedString+MEFormatedString.h"
#import "METableView.h"
#import "MEUIPreferences.h"
#import "METurnoversModel.h"
#import "METurnovers3SplitController.h"
#import "MEAlertsView.h"
#import "MESettings.h"

#import <Crashlytics/Crashlytics.h>


#define __ME_TURNOVERS_LASTINDEX_PATH @"__ME_TURNOVERS_LASTINDEX_PATH"

static NSString *turnoversShortCellID = ME_TURNOVERS_SHORT_CELL_ID;

@interface METurnoversViewController () <METurnoversModelProtocol, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property(nonatomic,assign)  NSInteger currentPaletteIndex;
@property(nonatomic,strong) METurnoversTableView *tableView;
@property(nonatomic,strong) METurnoversTotalInfoView *totalinfoView;
@property(nonatomic,strong) METurnoversSector *turnoversSector;
@property(nonatomic,strong) NSString *engineName, *marketName;
@property(nonatomic,strong) UIBarButtonItem *activityIndicator;
@property(nonatomic,strong) UIAlertView *alertView;
@end

@implementation METurnoversViewController
{
    METurnoversInfo *currentInfo;
    METurnoversViewController *currentTurnoversInfoController;
    BOOL isNotSelected;  
    BOOL isAppeared;
    
    METurnovers3SplitController *_3c;
}

- (UIBarButtonItem*) activityIndicator{
    if (!_activityIndicator) {
        UIActivityIndicatorView *activityIndicator =
        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activityIndicator = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    }
    
    return _activityIndicator;
}

- (void)hideActivityIndicator {
    dispatch_async(dispatch_get_main_queue(), ^{
        // we are done processing earthquakes, stop our activity indicator
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView stopAnimating];
        if ([self.parentViewController isKindOfClass:[METurnovers3SplitController class]]) {
            self.parentViewController.navigationItem.rightBarButtonItem = nil;
        }
        else
            self.navigationItem.rightBarButtonItem = nil;
    });
}

- (void) showActivityIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView startAnimating];
        if ([self.parentViewController isKindOfClass:[METurnovers3SplitController class]]) {
            self.parentViewController.navigationItem.rightBarButtonItem = self.activityIndicator;
        }
        else
            self.navigationItem.rightBarButtonItem = self.activityIndicator;
    });
}

- (NSString*) contextName{
    if (_engineName && _marketName) {
        return [NSString stringWithFormat:@"%@-%@-%@",__ME_TURNOVERS_LASTINDEX_PATH, _engineName, _marketName];
    }
    else if (_engineName) {
        return [NSString stringWithFormat:@"%@-%@",__ME_TURNOVERS_LASTINDEX_PATH, _engineName];
    }
    return __ME_TURNOVERS_LASTINDEX_PATH;
}

- (METurnoversSector*) turnoversSector{
    
    if (_turnoversSector) {
        _turnoversSector.delegate  = self;
        return _turnoversSector;
    }
    
    _turnoversSector = [[METurnoversModel sharedInstance] sectorWithDelegate:self withEngineName:self.engineName withMarketName:self.marketName];

    return _turnoversSector;
}

- (void) subscribe {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(unsubscribe) object:nil];

    if (self.turnoversSector) {
        [self.turnoversSector subscribe];
    }
}

- (void) unsubscribe {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(unsubscribe) object:nil];
    
    if (self.turnoversSector) {
        [self.turnoversSector unsubscribe];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    
    isAppeared = YES;
    [super viewWillAppear:animated];
    self.tableView.restorationIdentifier = [self contextName];
    isNotSelected = NO;
    [self subscribe];
}

- (void)loginSuccessed:(NSNotification*)event{
    if (isAppeared) {
        [self subscribe];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

    BOOL doUnsibscribe = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (_3c.isApeared) {
            doUnsibscribe = NO;
        }
    }
    
    isAppeared = NO;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(unsubscribe) object:nil];
    if (doUnsibscribe)
        [self performSelector:@selector(unsubscribe) withObject:nil afterDelay:5];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _3c = (METurnovers3SplitController*)self.navigationController.topViewController;
    }
    
    NSString *engine = self.turnoversSector.engineName == nil ? @"total" : self.turnoversSector.engineName;
    NSString *market = self.turnoversSector.marketName == nil ? @"total" : self.turnoversSector.marketName;
    
    [Answers logContentViewWithName:@"Turnovers View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{
                                      @"Turnovers: engine": engine,
                                      @"Turnovers: market": market
                                      }];
}

- (void) update:(METurnoversInfo *)info{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [self.tableView reloadData];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            NSIndexPath *lastSelectedIndexPath = ((METableView*)self.tableView).lastSelectedIndexPath;
            
            if (!self->isNotSelected) {
                self->isNotSelected = YES;
                
                if (![self isKindOfClass:[METurnoversBoardsViewController class]]) {
                    [self tableView:self.tableView didSelectRowAtIndexPath:lastSelectedIndexPath];
                }
            }
        }
        
    });
    
    [self.totalinfoView update:info];

    [self hideActivityIndicator];
    
}

- (UIAlertView*) alertView{
    if (_alertView) {
        return _alertView;
    }
    
    _alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration error", @"")
                                       message:NSLocalizedString(@"Access denied for the turnovers", @"")
                                      delegate:self
                             cancelButtonTitle:NSLocalizedString(@"Close button", @"")
                             otherButtonTitles:nil];
    
    _alertView.cancelButtonIndex = 0;
    
    return _alertView;
}

- (void) didTurnovers:(METurnoversInfo *)turnoversInfo fault:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error.code == GI_STOMP_LOGIN_FAILED) {
            self.alertView.title = error.localizedFailureReason;
            self.alertView.message = error.localizedDescription;
            [self.alertView show];
        }
        else {
            [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_WARN];
        }
    });
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self hideActivityIndicator];
    self.alertView = nil;
}

- (void) willTurnoversUpdate:(METurnoversInfo *)turnoversInfo{
    [self showActivityIndicator];
}

- (void) didTurnoversRestore:(METurnoversInfo *)turnoversInfo{
    if (!turnoversInfo)
        return;
    
    currentInfo = turnoversInfo;
    [self update:currentInfo];
}

- (void) didTurnoversUpdate:(METurnoversInfo *)turnoversInfo{
    
    if (!isAppeared) {
        return;
    }
    
    @try {
        if (!turnoversInfo)
            return;
        
        currentInfo = turnoversInfo;
        [self update:currentInfo];
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) awakeFromNib{
    [super awakeFromNib];
    
    //NSNumber *topoffset =  [NSNumber numberWithInt:UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone?0:24];
    NSNumber *topoffset =  [NSNumber numberWithInt:0];
    NSNumber *infoHeight = [NSNumber numberWithInt:105.];

    //
    // Total info
    //
    if ([self isKindOfClass:[METurnoversSectorsViewController class]]){
        infoHeight = [NSNumber numberWithFloat:165.];
        self.totalinfoView = [[METurnoversTotalInfoView alloc] initWithPieInFrame:CGRectMake(0, 0, 320, infoHeight.floatValue)];
    }
    else
        self.totalinfoView = [[METurnoversTotalInfoView alloc] initWithFrame:CGRectMake(0, 0, 320, infoHeight.floatValue)];

    self.totalinfoView.translatesAutoresizingMaskIntoConstraints = NO;

    //
    // Turnovers
    //
    self.tableView = [[METurnoversTableView alloc] initWithFrame:CGRectMake(0, 128, 320, 400) style:UITableViewStylePlain];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[METurnoversViewCell class] forCellReuseIdentifier:turnoversShortCellID];
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.allowsSelection = YES;
    
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.totalinfoView];

    NSString *constraintsString;
    
    NSNumber *rightLine = [NSNumber numberWithInt:0];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) 
        constraintsString = @"V:[top]-(offset)-[info(infoHeight)]-0-[table]-0-|";    
    else{
        rightLine = [NSNumber numberWithFloat:1.0];
        constraintsString = @"V:|-(offset)-[info(infoHeight)]-0-[table]-0-|";
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[info]-(rightLine)-|"  options:0 metrics:@{@"rightLine": rightLine} views:@{@"info":self.totalinfoView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-(rightLine)-|" options:0 metrics:@{@"rightLine": rightLine} views:@{@"table":self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintsString
                                                                      options:0
                                                                      metrics:@{@"offset":topoffset, @"infoHeight":infoHeight}
                                                                        views:@{@"info":self.totalinfoView, @"table": self.tableView, @"top": self.topLayoutGuide}]];
    self.tableView.clipsToBounds = NO;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.view.backgroundColor = ME_UI_LIGHT_GREY_ARROW_COLOR;
    }
    else{
        self.view.backgroundColor = [UIColor blackColor];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.navigationController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0.3];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 52;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [self.turnoversSector sections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.turnoversSector rowsForSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    METurnoversViewCell *cell = [tableView dequeueReusableCellWithIdentifier:turnoversShortCellID forIndexPath:indexPath];
    NSInteger rows = [self.turnoversSector rowsForSection:indexPath.section];
    
    if (rows==1)
        cell.hasRatioBar = NO;
    else
        cell.hasRatioBar = YES;
    
    METurnoverItem *item = [self.turnoversSector turnoverForIndexPath:indexPath];
    
    cell.captionLabel.text = item.caption;
    cell.valtodayLabel.attributedText = [NSAttributedString compoundValToday:[item.valtoday.value number]];
    cell.numtradesLabel.text = [item.numtrades.value string];
    cell.updateTimeLabel.attributedText = [NSAttributedString coloredDate:item.updatetime withBaseFontSize:12];
    
    cell.valueRatio = [item.valueRatio floatValue];
    
    if ([self isKindOfClass:[METurnoversSectorsViewController class]]) {
        cell.paletteIndex = indexPath.row;
    }
    else{
        cell.paletteIndex = self.currentPaletteIndex;
    }
    
    if (indexPath.row<rows-1)
        cell.devider.alpha = 1.0;
    else
        cell.devider.alpha = 0.0;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        if ([self isKindOfClass:[METurnoversBoardsViewController class]]) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }        
    }
    NSIndexPath *lastSelectedIndexPath = ((METableView*)self.tableView).lastSelectedIndexPath;
    if ([lastSelectedIndexPath isEqual:indexPath]) {
        [self.tableView selectRowAtIndexPath:lastSelectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];    
    }
    
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *segueName;
    
    @try {
        
        if (tableView != self.tableView) {
            return;
        }
                
        ((METableView*)self.tableView).lastSelectedIndexPath = indexPath;
        
        if ([self isKindOfClass:[METurnoversSectorsViewController class]]) {
            segueName = ME_TURNOVERS_MIDDLE_SEGUE_ID;
        }
        else if([self isKindOfClass:[METurnoversMarketsViewController class]]){
            segueName = ME_TURNOVERS_RIGHT_SEGUE_ID;
        }

//        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
//            METurnoverItem *item = [self.turnoversSector turnoverForIndexPath:indexPath];
//            self.navigationController.navigationItem.title = item.caption;            
//        }
//        else
            [self performSegueWithIdentifier:segueName sender:self];
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *lastSelectedIndexPath =  ((METableView*)self.tableView).lastSelectedIndexPath;
    
    METurnoverItem *item = [self.turnoversSector turnoverForIndexPath:lastSelectedIndexPath];
    METurnoversViewController *vc = segue.destinationViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) 
        vc.navigationItem.title = item.caption;            
    
    if ([self isKindOfClass:[METurnoversSectorsViewController class]]) {
        vc.engineName = item.name;
        vc.currentPaletteIndex = lastSelectedIndexPath.row;
    }
    else if([self isKindOfClass:[METurnoversMarketsViewController class]]){
        vc.engineName = self.engineName;
        vc.marketName = item.name;
        vc.tableView.allowsSelection = NO;
        vc.currentPaletteIndex = self.currentPaletteIndex;
    }
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];        
    [self update:currentInfo];
}

@end

@implementation METurnoversSectorsViewController
@end

@implementation METurnoversMarketsViewController
@end

@implementation METurnoversBoardsViewController
@end
