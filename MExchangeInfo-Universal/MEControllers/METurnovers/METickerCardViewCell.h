//
//  METickerCardViewCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/17/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEBaseViewCell.h"


#define ME_TICKERCARD_ITEM_CELL @"ME_TICKERCARD_ITEM_CELL"

@interface METickerCardViewCell : MEBaseViewCell
@property (nonatomic, strong)   id        value;
@property (nonatomic, strong)   id        urls;
@property (nonatomic, strong)   NSString *caption;
@property (nonatomic, strong)   NSString *info;
@property (nonatomic, readonly) NSString *firstUrl;
@end
