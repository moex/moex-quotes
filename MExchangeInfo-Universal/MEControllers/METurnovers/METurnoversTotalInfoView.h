//
//  METurnoversTotalInfoView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/14/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METurnoversSector.h"
#import "MEPieChartView.h"

@interface METurnoversTotalInfoView : UIView

@property(nonatomic,strong,readonly) MEPieChartView *pieChartView;
@property(nonatomic,strong,readonly) UILabel *captionLabel;
@property(nonatomic,strong,readonly) UILabel *captionValTodayLabel;
@property(nonatomic,strong,readonly) UILabel *captionValTodayUSDLabel;
@property(nonatomic,strong,readonly) UILabel *captionNumtradesLabel;
@property(nonatomic,strong,readonly) UILabel *valTodayLabel;
@property(nonatomic,strong,readonly) UILabel *valTodayUSDLabel;
@property(nonatomic,strong,readonly) UILabel *numtradesLabel;

- (id) initWithPieInFrame:(CGRect)frame ;

- (void) update:(METurnoversInfo*)info;

@end
