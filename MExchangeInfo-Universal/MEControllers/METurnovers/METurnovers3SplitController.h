//
//  METurnovers3SplitController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "ME3SplitViewController.h"

@interface METurnovers3SplitController : ME3SplitViewController
@property (atomic,assign) BOOL isApeared;
@end
