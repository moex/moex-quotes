//
//  METurnoversViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METurnoversTableView.h"
#import "METurnoversTotalInfoView.h"
#import "METurnoverItem.h"

#define ME_TURNOVERS_LEFT_SEGUE_ID @"turnoversLeftSegueID"
#define ME_TURNOVERS_MIDDLE_SEGUE_ID @"turnoversMiddleSegueID"
#define ME_TURNOVERS_RIGHT_SEGUE_ID @"turnoversRightSegueID"

@interface METurnoversViewController : UIViewController
@property (strong, nonatomic, readonly) METurnoversTableView *tableView;
@property (strong, nonatomic, readonly) METurnoversTotalInfoView *totalinfoView;
@property (strong, nonatomic, readonly) METurnoversSector *turnoversSector;
@end

@interface METurnoversSectorsViewController : METurnoversViewController
@end

@interface METurnoversMarketsViewController : METurnoversViewController
@end

@interface METurnoversBoardsViewController : METurnoversViewController
@end
