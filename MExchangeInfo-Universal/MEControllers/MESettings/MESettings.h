//
//  MESettings.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// To add new setting, just add a few code lines:
// 1. A new propety
// 2. A new row in ME_DEFAULT_SETTINGS and localize it (optional). The ME_DEFAULT_SETTINGS key must by the same name of property.
//

#define ME_SEARCH_ONLY_PRIMARY_BOARD @"searchOnlyPrimaryBoard"

#define ME_SETTINGS_CHANGED_NOTIFICATION @"ME_SETTINGS_CHANGED_NOTIFICATION"
#define ME_SETTINGS_FLUSH_CACHE_NOTIFICATION @"ME_SETTINGS_FLUSH_CACHE_NOTIFICATION"
#define ME_LOGIN_ERROR_NOTIFICATION @"ME_LOGIN_ERROR_NOTIFICATION"
#define ME_LOGIN_SUCCESS_NOTIFICATION @"ME_LOGIN_SUCCESS_NOTIFICATION"

#define ME_TICKER_CARD_SHOW_NOTIFICATION @"ME_TICKER_CARD_SHOW_NOTIFICATION"


#ifdef DEBUG
#define __DEBUG_YES_TO_NO @YES
#else
#define __DEBUG_YES_TO_NO @NO
#endif

@interface MEFlushCache : NSObject
@property(nonatomic, assign) NSInteger size;
- (void) execute;
@end

#define ME_DEFAULT_SETTINGS __settins__()

/**
 *  Application settings.
 */
@interface MESettings : NSObject

/**
 *  Close connection while app is inactive;
 */
@property(nonatomic, strong) NSNumber *closeInActiveConnection;

/**
 *  Show Helper views at the start of app.
 */
//@property(nonatomic, strong) NSNumber *showHelper;

/**
 *  Show hint column name instead of caption.
 */
@property(nonatomic, strong) NSNumber *showHintName;

/**
 *  Show turnovers information on the watch list screen.
 */
//@property(nonatomic, strong) NSNumber *showTurnovers;

/**
 *  Show alert messages history.
 */
//@property(nonatomic, strong) NSNumber *showAlertMessagesHistory;

/**
 *  Show warnings.
 */
@property(nonatomic, strong) NSNumber *showWarns;


@property(nonatomic,strong) NSNumber* newsKeepDays;

@property(nonatomic,strong) MEFlushCache *flushCache;

/**
 *  Number of available settings.
 */
- (NSUInteger) count;

/**
 *  Set new preperty value.
 *
 *  @param value new value.
 *  @param number number by order.
 */
- (void) setValue:(id)value forSettingNo:(NSInteger)number;
- (void) setValue:(id)value forSettingName:(NSString*)name;

/**
 *  Creat one shared instance.
 *
 *  @return shared application settings.
 */
+ (id) sharedInstance;


//
// Helper methods
//

/**
 *  Get boolean value of property.
 *
 *  @param number number by order.
 *
 *  @return YES/NO.
 */
- (BOOL) boolForSettingNo:(NSInteger)number;

/**
 *  Get boolean value.
 *
 *  @param name property name.
 *
 *  @return YES/NO.
 */
- (BOOL) boolForSettingName:(NSString*)name;

/**
 *  Get property object.
 *
 *  @param number number by order.
 *
 *  @return property.
 */
- (id) valueForSettingNo:(NSInteger)number;
- (id) sourceForSettingNo:(NSInteger)number;

/**
 *  Get property object.
 *
 *  @param name property name.
 *
 *  @return property.
 */
- (id) valueForSettingName:(NSString*)name;


/**
 *  Get property localized caption.
 *
 *  @param number number by order.
 *
 *  @return localized string.
 */
- (NSString*) captionForSettingNo:(NSUInteger)number;
- (NSString*) typeForSettingNo:(NSUInteger)number;

/**
 *  Get property localized caption by name.
 *
 *  @param name name.
 *
 *  @return localized string.
 */
- (NSString*) captionForSettingName:(NSString*)name;

@end
