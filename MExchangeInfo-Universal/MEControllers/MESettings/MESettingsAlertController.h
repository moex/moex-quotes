//
//  MESettingsAlertController.h
//  MExchange-MosQuotes
//
//  Created by denn on 28.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MESettingsAlertController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
- (IBAction)cancelHandler:(UIButton *)sender;
- (IBAction)okHandler:(UIButton *)sender;

@end
