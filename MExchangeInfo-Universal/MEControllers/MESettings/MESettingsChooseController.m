//
//  MESettingsChooseControllerTableViewController.m
//  MExchange-MosQuotes
//
//  Created by denn on 28.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESettingsChooseController.h"
#import "MESettings.h"

@interface MESettingsChooseController ()
@property (nonatomic, readonly) MESettings *settings;
@end

@implementation MESettingsChooseController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (MESettings*) settings{
    return [MESettings sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.selections[0] count];
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];

    NSArray *source =[self.settings sourceForSettingNo:self.settingsNo];
    NSInteger index = [[source objectAtIndex:0] indexOfObject:[self.settings valueForSettingNo:self.settingsNo]];

    cell.textLabel.text = [[self.selections objectAtIndex:1] objectAtIndex:indexPath.row];
    if(index==indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.settings setValue:[NSNumber numberWithInt:[[[self.selections objectAtIndex:0]objectAtIndex:indexPath.row] intValue]] forSettingNo:self.settingsNo];
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
