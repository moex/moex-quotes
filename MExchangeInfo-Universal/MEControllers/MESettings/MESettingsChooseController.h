//
//  MESettingsChooseControllerTableViewController.h
//  MExchange-MosQuotes
//
//  Created by denn on 28.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MESettingsChooseController : UITableViewController
@property (nonatomic,strong) NSArray *selections;
@property (nonatomic,assign) NSInteger settingsNo;
@end
