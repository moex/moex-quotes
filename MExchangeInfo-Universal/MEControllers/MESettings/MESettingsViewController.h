//
//  MESettingsViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MESettingsChooseController.h"
#import "MESettingsAlertController.h"

@interface MESettingsViewController : UITableViewController
- (IBAction)setPreference:(id)sender;
@end
