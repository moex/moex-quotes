//
//  MESettingsViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MESettingsViewController.h"
#import "MESettings.h"
#import "UIView+UserDefinedPropertyBinding.h"
#import "MEUIPreferences.h"
#import "MENewsModel.h"
#import "METickerCardsModel.h"
#import "MEAppDelegate.h"

#define ME_SWITCH_SETTIN_KEY @"settingPreferenceKEY"

@interface MESettingsViewController () 
@property (nonatomic,assign) CGFloat currentPosition;
@end

@implementation MESettingsViewController
{
    MESettings *settings;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        settings = [MESettings sharedInstance];
    }
    
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
    if (self.tableView.contentSize.height>self.tableView.bounds.size.height) {
        self.tableView.contentOffset = CGPointMake(0, self.currentPosition);        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [settings count];
}

- (UITableViewCell*) tableCellForRowAtIndexPath:(NSIndexPath*)indexPath{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%@",[settings typeForSettingNo:indexPath.row]];
    return [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
}

- (UILabel*) textLabelForCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath{
    UILabel *text   = (UILabel*) [cell viewWithTag:1];
    
    text.text = [settings captionForSettingNo:indexPath.row];
    text.numberOfLines = 10;
    text.lineBreakMode = NSLineBreakByWordWrapping;
    text.adjustsFontSizeToFitWidth = YES;
    text.minimumScaleFactor = 0.5;
    
    text.backgroundColor = [UIColor clearColor];
    
    [text sizeToFit];
    
    return text;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    UITableViewCell *cell = [self tableCellForRowAtIndexPath:indexPath];
//    UILabel *text   = [self textLabelForCell:cell atIndexPath:indexPath];;
//    
//    CGSize size =[text sizeThatFits:cell.contentView.frame.size];
//    CGFloat scale = size.width/text.bounds.size.width;
//    scale = scale*size.height/text.bounds.size.height;
//
//    scale = scale<1.0?1:scale;
//    
//    return 60.0f*scale;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableCellForRowAtIndexPath:indexPath];
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    [self textLabelForCell:cell atIndexPath:indexPath];
    
    NSString *setting_type = [settings typeForSettingNo:indexPath.row];
    
    if (indexPath.row%2) {
        if (!cell.backgroundView) {
            cell.backgroundView = [[UIView alloc] initWithFrame:cell.contentView.bounds];
        }
        cell.backgroundView.backgroundColor = ME_TABLE_EVEN_COLOR;    
    }
    else
        cell.backgroundView.backgroundColor = [UIColor clearColor];
    
    if ([setting_type isEqualToString:@"switch"]) {
        UISwitch *check_switch = (UISwitch*) [cell viewWithTag:2];
        check_switch.on = [settings boolForSettingNo:indexPath.row];
        [check_switch setValue:[NSNumber numberWithInteger:indexPath.row] forUndefinedKey:ME_SWITCH_SETTIN_KEY];
    }
    else if ([setting_type isEqualToString:@"news_list_size"]){
        UILabel *label = (UILabel*) [cell viewWithTag:2];
        NSArray *source =[settings sourceForSettingNo:indexPath.row];
        NSInteger index = [[source objectAtIndex:0] indexOfObject:[settings valueForSettingNo:indexPath.row]];
        label.text = [[source objectAtIndex:1] objectAtIndex:index];
    }
    else if ([setting_type isEqualToString:@"flush_cache"]){
        UIButton *button = (UIButton*) [cell viewWithTag:2];
        [button setTitle:NSLocalizedString(@"Flush button", @"") forState:UIControlStateNormal];
    }
    
    UIImageView *divider  = (UIImageView*)[cell viewWithTag:1001];
    if (indexPath.row==settings.count-1) {
        divider.alpha = 0.0f;
    }
    else{
        divider.alpha = 1.0f;
    }
    
    return cell;
}

//- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 10)];
//    view.backgroundColor = [UIColor clearColor];
//    return view;
//}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
    UILabel *titleNav = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    titleNav.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:16];
    titleNav.textColor = [UIColor whiteColor];
    titleNav.numberOfLines = 3;
    titleNav.adjustsFontSizeToFitWidth = YES;
    titleNav.minimumScaleFactor = 0.7;
    titleNav.text = [settings captionForSettingNo:indexPath.row];
    titleNav.textAlignment = NSTextAlignmentLeft;

    if  ([segue.identifier isEqualToString:@"selectionSegue"]){
        MESettingsChooseController *chooser = (MESettingsChooseController*)segue.destinationViewController;
        chooser.settingsNo = indexPath.row;
        chooser.selections = [settings sourceForSettingNo:indexPath.row];
        chooser.navigationItem.titleView = titleNav;
    }
    else if ([segue.identifier isEqualToString:@"cacheSegue"]){
        MESettingsAlertController *controller= (MESettingsAlertController*) segue.destinationViewController;        
        controller.navigationItem.titleView = titleNav;
    }
    self.currentPosition = self.tableView.contentOffset.y;    
}

- (IBAction)setPreference:(id)sender {
    UISwitch *check_switch = (UISwitch*) sender;
    [settings setValue:[NSNumber numberWithBool:check_switch.on] forSettingNo:[[sender valueForKeyPath:ME_SWITCH_SETTIN_KEY] integerValue]];
}

@end

