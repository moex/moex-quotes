//
//  MESettingsAlertController.m
//  MExchange-MosQuotes
//
//  Created by denn on 28.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESettingsAlertController.h"
#import "MENewsModel.h"
#import "METickerCardsModel.h"
#import "MESettings.h"
#import "MEUIPreferences.h"

@interface MESettingsAlertController ()

@end

@implementation MESettingsAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textView.font = [UIFont fontWithName:ME_UI_FONT size:23];
    self.textView.contentScaleFactor = 1.0;
    self.textView.contentMode = UIViewContentModeScaleAspectFit;
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString *text = NSLocalizedString(@"Do you realy to clean all application data?", @"");
    
    NSAttributedString *attr=[[NSAttributedString alloc] initWithString:text 
                                                             attributes:@{
                                                                          NSFontAttributeName: [UIFont fontWithName:ME_UI_FONT size:18]
                                                                          }];
    
    self.textView.attributedText = attr;
    
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel button", @"") forState:UIControlStateNormal];
    [self.okButton setTitle:NSLocalizedString(@"Ok button", @"") forState:UIControlStateNormal];
}

- (IBAction)cancelHandler:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okHandler:(UIButton *)sender {    
    [NSUserDefaults resetStandardUserDefaults]; 
    
    [[MENewsModel sharedInstance] truncate];
    [[[METickerCardsModel alloc] initWithTicker:nil] truncate];    
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_SETTINGS_FLUSH_CACHE_NOTIFICATION object:self];;

    [self.navigationController popViewControllerAnimated:YES];
}
@end
