//
//  MESettings.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <objc/runtime.h>
#import <objc/message.h>
#import "MESettings.h"

static MESettings *__shared_instance = nil;

@implementation MESettings
{
    objc_property_t *properties;
    NSDictionary *captionBook;
    NSUInteger _count;
    NSMutableDictionary *getters;
}

#pragma Properties definition

- (NSUInteger) count{
    return _count;
}

#pragma Utils
- (void) setValue:(id)value forSettingNo:(NSInteger)number{
    
    if (_count==0 || number>=_count) {
        return;
    }
    
    NSString *property_Name =  [self setterNameForProperty:[NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding]];
    //
    // it crashes under new ios8 and new apple hardware
    //objc_msgSend(self, sel_registerName([property_Name UTF8String]), value);
    
    //
    // replace to
    //
    void (*response)(id, SEL, id) = (void (*)(id, SEL, id)) objc_msgSend;
    SEL setSelector = sel_registerName([property_Name UTF8String]);
    response(self, setSelector, value);
}

- (void) setValue:(id)value forSettingName:(NSString *)name{
    for (int i = 0; i < _count; i++) {
        if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
            [self setValue:value forSettingNo:i];
            return;
        }
    }
}

- (NSString*) captionForSettingName:(NSString *)name{
    for (int i = 0; i < _count; i++) {
        if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
            return [self captionForSettingNo:i];
        }
    }
    return nil;
}

- (NSString*) captionForSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
    @try {
        NSString *ret = [[captionBook valueForKey:name] objectAtIndex:0];
        return ret==nil?name:ret;
    }
    @catch (NSException *exception) {
        return name;
    }
};

- (NSString*) typeForSettingNo:(NSUInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    NSString *name= [NSString stringWithFormat:@"%s", property_getName(properties[number])];
    @try {
        NSString *ret = [[captionBook valueForKey:name] objectAtIndex:2];
        return ret==nil?name:ret;
    }
    @catch (NSException *exception) {
        return name;
    }
};

- (BOOL) boolForSettingNo:(NSInteger)number{
    return [[self valueForSettingNo:number ] boolValue];
}

- (BOOL) boolForSettingName:(NSString *)name{
    for (int i = 0; i < _count; i++) {
        if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
            return [self boolForSettingNo:i];
        }
    }
    return NO;
}


- (id) valueForSettingNo:(NSInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }
    
    NSString *selector = [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];

    
    //
    // instead of
    //id propertyValue = objc_msgSend(self, sel_registerName([selector UTF8String]));

    // use the follow:
    id (*response)(id, SEL) = (id (*)(id, SEL)) objc_msgSend;
    SEL setSelector = sel_registerName([selector UTF8String]);
    id propertyValue = response(self, setSelector);
    
    return propertyValue;
}

- (id) sourceForSettingNo:(NSInteger)number{
    if (_count==0 || number>=_count) {
        return nil;
    }    
    NSString *selector = [NSString stringWithCString:property_getName(properties[number]) encoding:NSUTF8StringEncoding];
    NSArray *a = [captionBook valueForKey:selector];
    return [a objectAtIndex:1];
}

- (id) valueForSettingName:(NSString *)name{
    for (int i = 0; i < _count; i++) {
        if ([[NSString stringWithFormat:@"%s",property_getName(properties[i])] isEqualToString:name]) {
            return [self valueForSettingNo:i];
        }
    }
    return nil;
}


#pragma Constructors
+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!__shared_instance) {
            __shared_instance = [[MESettings alloc] init];
        }
    });
    
    return __shared_instance;
}

id getterMethod(id self, SEL _cmd){
    //return [NSNumber numberWithBool:[[NSUserDefaults standardUserDefaults] boolForKey:NSStringFromSelector(_cmd)]];
    return [[NSUserDefaults standardUserDefaults] valueForKey:NSStringFromSelector(_cmd)];
}

void setterMethod(id self, SEL _cmd, id value){
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:[self getterNameForSetter: NSStringFromSelector(_cmd)]];
    [[NSUserDefaults standardUserDefaults] synchronize];    
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_SETTINGS_CHANGED_NOTIFICATION object:self];;
}

- (NSString*) setterNameForProperty:(NSString*)property_name_string{
    return [NSString stringWithFormat:@"set%@%@:",[[property_name_string substringToIndex:1] capitalizedString],[property_name_string substringFromIndex:1]];
}

- (NSString*) getterNameForSetter:(NSString*)setter_name_string{
    return [getters valueForKey:setter_name_string];
}

- (id) init{
    
    if (__shared_instance!=nil) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {        
        captionBook = ME_DEFAULT_SETTINGS;
        
        getters = [[NSMutableDictionary alloc] init];
        
        unsigned int outCount = 0;
        properties = class_copyPropertyList([self class], &outCount);
        _count = (NSUInteger) outCount;
        
        //
        // Set defaults
        //
        for (int i = 0; i < _count; i++) {
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            
            NSString *property_name_string = [NSString stringWithCString:propName encoding:NSUTF8StringEncoding];
            NSArray *a = [captionBook valueForKey:property_name_string];
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:property_name_string] == nil) {
                
                id obj = [a objectAtIndex:1];
                
                if ([obj isKindOfClass:[NSArray class]]) {
                    NSArray *aobj = obj;
                    aobj = [aobj objectAtIndex:0];
                    if ([aobj isKindOfClass:[NSArray class]]) {
                        aobj = [aobj objectAtIndex:0];
                    }
                    [[NSUserDefaults standardUserDefaults] setValue:aobj forKey:property_name_string];
                }
                else {
                    [[NSUserDefaults standardUserDefaults] setValue:obj forKey:property_name_string];
                }                
            }
            
            
            NSString *getter_name = property_name_string;
            NSString *setter_name = [self setterNameForProperty:property_name_string];
            
            [getters setValue:property_name_string forKey:setter_name];
            
            SEL getter_selector = sel_registerName([getter_name UTF8String]);
            SEL setter_selector = sel_registerName([setter_name UTF8String]);

            class_replaceMethod([self class], getter_selector, (IMP) (getterMethod), method_getTypeEncoding(class_getInstanceMethod([self class], getter_selector)));
            class_replaceMethod([self class], setter_selector, (IMP) (setterMethod), method_getTypeEncoding(class_getInstanceMethod([self class], setter_selector)));
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return self;
}

- (void) dealloc{
    free(properties);
}

static NSDictionary *__settins__(){
    static NSDictionary *d=nil;
    if (!d) {
        d=  @{ \
              @"closeInActiveConnection":   @[NSLocalizedString(@"Close a connection while the app is inactive", @"Text within settings view notes connection state toggler"), @YES, @"switch"],
              @"showHelper":  @[NSLocalizedString(@"Show a helpers screen at the start", @"Text within settings view notes show or not helpers view"), @NO, @"switch"],
              @"showHintName": @[NSLocalizedString(@"Show column hint instead of title (use tap-and-hold to switch quickly)", @"Select how to show columns name: hint or title"), @NO, @"switch"],
              @"showTurnovers": @[NSLocalizedString(@"Show turnovesrs info on Watch List screen", @"Show or hide turnovesr on main screen"), @YES, @"switch"],
              //@"showAlertMessagesHistory": @[NSLocalizedString(@"Show alert messages history", @"Show alert messages history"), __DEBUG_YES_TO_NO, @"switch"],
              @"showWarns": @[NSLocalizedString(@"Show all warning and events", @"Show all warning and events."), @NO, @"switch"],
              @"newsKeepDays": @[NSLocalizedString(@"Remove news from local storage older then, days", @""),
                                 @[
                                     @[@7, @30, @90, @365],
                                     @[
                                         //NSLocalizedString(@"1 day",@""),
                                         NSLocalizedString(@"1 week",@""),
                                         NSLocalizedString(@"1 month",@""),
                                         NSLocalizedString(@"3 months",@""),
                                         NSLocalizedString(@"1 year",@"")
                                         ]], @"news_list_size"],
              @"flushCache": @[NSLocalizedString(@"Clean application cache", @""), @0 , @"flush_cache"],
              };
    }
    return d;
}

@end

