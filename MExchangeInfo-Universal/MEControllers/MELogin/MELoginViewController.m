//
//  MELoginViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/28/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELoginViewController.h"
#import "MEDispatcher.h"
#import "MESettings.h"
#import "MEAppDelegate.h"
#import "MEUIPreferences.h"

#define ME_LOGEDIN_GUEST_PASSWORD_HOLDER NSLocalizedString(@"Now you signed in as a Guest", @"Hold password in guest mode")
#define ME_GUEST_PASSWORD_HOLDER NSLocalizedString(@"Sign in as a Guest user", @"Hold password in guest mode")
#define ME_TYPE_PASSWORD_HOLDER NSLocalizedString(@"Type your MOEX login password", @"Hold password in passport mode")

@interface MELoginViewController () <UITextFieldDelegate, UIAlertViewDelegate, UIWebViewDelegate>
@end

@implementation MELoginViewController
{
    BOOL          hasAuthError;
    MEDispatcher *dispatcher;
    
    UIAlertView  *alertView;
    BOOL          alertIsShown;
    
    UITextField  *lastAciveTextField;
}

- (UIView*) containerView{
    return self.loginContainerView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL) fieldIsEmpty: (UITextField*)textField{
    return [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0;
}

- (void) resetConnectionColor{
    if (hasAuthError || !dispatcher.config.isAuthorized) {
        self.loginButton.backgroundColor = [UIColor colorWithRed:1.0 green:.0 blue:.0 alpha:0.5];
    }
    else if (dispatcher.config.isAuthorized && dispatcher.isRunning){
        self.loginButton.backgroundColor = [UIColor colorWithRed:.0 green:1.0 blue:.0 alpha:0.5];
    }
    else{
        self.loginButton.backgroundColor = [UIColor clearColor];
    }
}

- (void) checkAuth{
    MEConfig *config = [dispatcher config];
    
    if (!config.hasUser || [config.login.domain isEqualToString:GIC_GUEST_DOMAIN]) {
        self.usernameTextField.placeholder = NSLocalizedString(@"Type MOEX login name", @"Hold login name in guest mode");
        self.passwordTextField.enabled = NO;
        
        if (dispatcher.isRunning)
            self.passwordTextField.placeholder = ME_LOGEDIN_GUEST_PASSWORD_HOLDER;
        
        else
            self.passwordTextField.placeholder = ME_GUEST_PASSWORD_HOLDER;
        
    }
    else{
        self.usernameTextField.text = config.login.user;
        self.passwordTextField.text = config.login.password;
        
        self.usernameTextField.placeholder = NSLocalizedString(@"Sign in passport.moex.com", @"");
        self.passwordTextField.placeholder = NSLocalizedString(@"", @"");
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
        [UIView animateWithDuration:0.01 animations:^{
            if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
                self.registerInfoTextView.alpha = 0.0;
            }
            else{
                self.registerInfoTextView.alpha = 1.0;
            }
        }];
    }

    [self resetConnectionColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login error", @"")
                                           message:NSLocalizedString(@"Connection could not be established because of an authentication problem...", @"")
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"Sign in as a Guest", @"")
                                 otherButtonTitles:NSLocalizedString(@"Try new login", @""),
                 nil];
    alertView.cancelButtonIndex = 0;
    alertIsShown = NO;
    
    dispatcher = [MEDispatcher sharedInstance];
    
    self.passwordTextField.delegate = self;
    self.usernameTextField.delegate = self;
    
    [self checkAuth];
    
    [self.loginButton setTitle:NSLocalizedString(@"Sign in", @"") forState:UIControlStateNormal] ;
    [self.registerButton setTitle:NSLocalizedString(@"Register", @"") forState:UIControlStateNormal];
    [self.changePasswordButton setTitle:NSLocalizedString(@"Change password", @"") forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"Recovery password", @"") forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionLogining:) name:ME_DISPATCHER_WILL_CONNECT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed:) name:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginError:) name:ME_LOGIN_ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dispatcher_stoped:) name:ME_DISPATCHER_STOPPED_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dispatcher_error:) name:ME_DISPATCHER_ERROR_EVENT object:nil];
    
    self.registerInfoTextView.delegate = self;
    
    // The magic is loading a request, *not* using loadHTMLString:baseURL:
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"MEloginInstruction" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    NSString *cssFile = [[NSBundle mainBundle] pathForResource:@"MEloginInstruction" ofType:@"css"];
    NSString* cssString = [NSString stringWithContentsOfFile:cssFile encoding:NSUTF8StringEncoding error:nil];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#instruction_css#" withString:cssString];
    
    [self.registerInfoTextView loadHTMLString:htmlString baseURL:nil];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    
    return YES;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
         if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
             [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration animations:^{
                 if(UIInterfaceOrientationIsLandscape(orientation)){
                     self.registerInfoTextView.alpha = 0.0;
                 }
                 else{
                     self.registerInfoTextView.alpha = 1.0;
                 }
             }];
         }
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         
     }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


- (void) dispatcher_stoped:(NSNotification*)event{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetConnectionColor];
        self.navigationItem.title = NSLocalizedString(@"NoConnection", @"");
    });
}

- (void) dispatcher_error:(NSNotification*)event{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetConnectionColor];
        [self hideActivity];
        self.navigationItem.title = NSLocalizedString(@"NoConnection", @"");
    });
}

- (void) connectionLogining:(NSNotification*)event{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showActivity];
    });
}

- (void) loginSuccessed:(NSNotification*)event{
    
    hasAuthError = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self resetConnectionColor];
        [self hideActivity];
        
        NSString *username = self->dispatcher.config.login.user;
        
        self.navigationItem.title = NSLocalizedString(username, @"");
        
        if ([self->dispatcher.config.login.domain isEqualToString:GIC_GUEST_DOMAIN] && self->dispatcher.config.isAuthorized) {
            self.passwordTextField.placeholder = ME_LOGEDIN_GUEST_PASSWORD_HOLDER;
        }
        
        if (self->lastAciveTextField) {
            [self->lastAciveTextField resignFirstResponder];
        }
    });
}

- (void) loginError:(NSNotification*)event{
    hasAuthError = YES;
    
    NSError *error = event.object;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self hideActivity];
        [self resetConnectionColor];
        
        if (!self->alertIsShown) {
            self->alertView.message = error.localizedDescription;
            [self->alertView show];
            self->alertIsShown = YES;
        }
        self.navigationItem.title = NSLocalizedString(@"UnAuthorized", @"");
    });
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        
        [self showActivity];
        
        dispatcher.config.login = [[GIAuth alloc] initWithUser:GIC_GUEST_USER withPassword:GIC_GUEST_PASSWD andDomain:GIC_GUEST_DOMAIN];
        
        self.passwordTextField.text = @"";
        self.passwordTextField.placeholder = ME_GUEST_PASSWORD_HOLDER;
        self.usernameTextField.text = @"";
        self.navigationItem.title = NSLocalizedString(@"Guest", @"");
        
        [dispatcher reconnect];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideActivity];            
            self.navigationItem.title = NSLocalizedString(@"UnAuthorized", @"");
        });
    }
    
    alertIsShown = NO;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField{
    lastAciveTextField=textField;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *text;
    
    if (textField.text.length>0 && string.length==0) {
        text = [textField.text substringToIndex: textField.text.length-1] ;
    }
    else
        text = [NSString stringWithFormat:@"%@%@", textField.text, string];
    
    if (self.usernameTextField == textField) {
        if (text.length>0) {
            self.passwordTextField.enabled = YES;
            self.passwordTextField.placeholder = ME_TYPE_PASSWORD_HOLDER;
        }
        else{
            self.passwordTextField.enabled = NO;
            self.passwordTextField.text = @"";
            self.passwordTextField.placeholder = ME_GUEST_PASSWORD_HOLDER;
        }
    }
    
    lastAciveTextField = textField;
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField==self.usernameTextField) {
        if (textField.text.length>0)
            self.passwordTextField.enabled = YES;
        
        [self.passwordTextField becomeFirstResponder];
        return YES;
    }
    [textField resignFirstResponder];
    
    [self signin:self.loginButton];
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signin:(id)sender {
    
    [self showActivity];
    
    NSString *domain = GIC_DEFAULT_DOMAIN;
    NSString *username = self.usernameTextField.text;
    
    if (self.usernameTextField.text.length==0) {
        domain = GIC_GUEST_DOMAIN;
        username = GIC_GUEST_USER;
    }
    
    GIAuth *login = [[GIAuth alloc] initWithUser:username withPassword:self.passwordTextField.text andDomain:domain];
    MEConfig *config = [dispatcher config];
    config.login = login;
    
    [dispatcher reconnect];
    
}
@end
