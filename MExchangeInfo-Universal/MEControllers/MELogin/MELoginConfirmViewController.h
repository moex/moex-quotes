//
//  MESignUpConfirmViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 03.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEButtonLogin.h"

#define ME_LOGIN_OCNFIRM_VC_ID @"registerConfirmationViewController"

@interface MELoginConfirmViewController : UIViewController

@property (strong, nonatomic) NSString *descriptionText;

@property (weak, nonatomic) UIViewController *signUpViewController;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet MEButtonLogin *closeButton;

- (IBAction)close:(id)sender;

@end
