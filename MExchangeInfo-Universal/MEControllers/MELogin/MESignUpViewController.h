//
//  MESignInViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEButtonLogin.h"
#import "MELoginCommonViewController.h"

@interface MESignUpViewController : MELoginCommonViewController


@property (weak, nonatomic) IBOutlet UIView *registerContainerView;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *usernameErrorBackLight;


@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorBackLight;


@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorBackLight;


@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmTextField;
@property (weak, nonatomic) IBOutlet UIImageView *passwordConfirmErrorBackLight;


@property (weak, nonatomic) IBOutlet MEButtonLogin *signUpButton;
@property (weak, nonatomic) IBOutlet MEButtonLogin *cancelButton;

- (IBAction)signup:(id)sender;
- (IBAction)cancel:(id)sender;
@end
