//
//  MESignUpConfirmViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 03.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELoginConfirmViewController.h"
#import "MELoginCommonViewController.h"
#import "MEUIPreferences.h"

@interface MELoginConfirmViewController ()

@end

@implementation MELoginConfirmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) awakeFromNib{
    [super awakeFromNib];
    [self setModalPresentationStyle:UIModalPresentationFormSheet];
    [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{        
        if (self->_descriptionText) {
            self.descriptionTextView.alpha = 0.0;
            self.descriptionTextView.text = self->_descriptionText;
            self.descriptionTextView.textColor = ME_UI_LIGHT_GREY_FONT_COLOR;
            [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
                self.descriptionTextView.alpha = 1.0;
            }];
        }
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.closeButton setTitle:NSLocalizedString(@"Close button", @"") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    
    MELoginCommonViewController *signupvc = (MELoginCommonViewController*)self.signUpViewController;
    signupvc.view.userInteractionEnabled = NO;
    [self dismissViewControllerAnimated:YES completion:^{        
        [signupvc cancel:self];
    }];
}
@end
