//
//  MESignInViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESignUpViewController.h"
#import "MEPassportAPI.h"
#import "MEUIPreferences.h"
#import "MELoginConfirmViewController.h"

#define ME_LOGIN_REGISTRATION_USERNAME_PH NSLocalizedString(@"New login (minimum 6 characters)", @"Nickname registration text field place holder")
#define ME_LOGIN_REGISTRATION_EMAIL_PH NSLocalizedString(@"Your email", @"User email registration text field place holder")
#define ME_LOGIN_REGISTRATION_PASSWORD_PH NSLocalizedString(@"Password (minimum 6 characters)", @"User password registration text field place holder")
#define ME_LOGIN_REGISTRATION_PASSWORD_CONFIRMATION_PH NSLocalizedString(@"Password confirmation", @"User password confirmation registration text field place holder")

#define ME_LOGIN_REGISTRATION_USERNAME_LIMIT NSLocalizedString(@"The minimum username length should be %i characters", @"User name registration message error text about limit")
#define ME_LOGIN_REGISTRATION_EMAIL_ISEMPTY NSLocalizedString(@"User email is empty", @"User name registration email is empty")
#define ME_LOGIN_REGISTRATION_EMAIL_ISWRONG NSLocalizedString(@"User email has wrong format", @"User name registration email wrong format error message")

@interface MESignUpViewController () <UIAlertViewDelegate, UITextFieldDelegate>
@end

@implementation MESignUpViewController{
    UIAlertView  *alert;
    NSString  *lastFieldError;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel button", @"") forState:UIControlStateNormal];
    [self.signUpButton setTitle:NSLocalizedString(@"Sign up button", @"") forState:UIControlStateNormal];
    
    self.usernameTextField.placeholder = ME_LOGIN_REGISTRATION_USERNAME_PH;
    self.emailTextField.placeholder = ME_LOGIN_REGISTRATION_EMAIL_PH;
    self.passwordTextField.placeholder = ME_LOGIN_REGISTRATION_PASSWORD_PH;
    self.passwordConfirmTextField.placeholder = ME_LOGIN_REGISTRATION_PASSWORD_CONFIRMATION_PH;

    self.usernameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.passwordConfirmTextField.delegate = self;
    
    self.usernameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.passwordConfirmTextField.delegate = self;
    
    self.usernameErrorBackLight.alpha = 0.0;
    self.emailErrorBackLight.alpha = 0.0;
    self.passwordErrorBackLight.alpha = 0.0;
    self.passwordConfirmErrorBackLight.alpha = 0.0;
    
    [self disable_signup];
    
}

- (UIView*)containerView{
    return self.registerContainerView;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self hideActivity];

        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        [self showActivity];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.passport registryUser:self.usernameTextField.text withEmail:self.emailTextField.text andPassword:self.passwordTextField.text];
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) flashError: (UIImageView*) imageView{    
    dispatch_async(dispatch_get_main_queue(), ^{        

    [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION/2. animations:^{
        imageView.alpha=1.0;
    } completion:^(BOOL finished){
        [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION/2. animations:^{
            imageView.alpha=.5;
        } completion:^(BOOL finished){
            [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION/2. animations:^{
                imageView.alpha=1.0;
            } completion:^(BOOL finished){}];
        }];
    }];   
    });
}

- (void) disable_signup{
    self.signUpButton.enabled = NO;
    self.signUpButton.alpha = 0.5;
}

- (void) enable_signup{
    self.signUpButton.enabled = YES;
    self.signUpButton.alpha = 1.;
}

#pragma mark - UITextField delegate

- (BOOL) disablesAutomaticKeyboardDismissal{
    return NO;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        
        if ([self checkForm])
            [self enable_signup];
        else
            [self disable_signup];
        
        if (lastFieldError) {
            [textField resignFirstResponder];
            alert = [self getAlertView];
            alert.message = lastFieldError;
            [alert show];
        }
        else{
            [self signup:self];
        }
        
    }
    return NO; // We do not want UITextField to insert line-breaks.
}


- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *_text=[NSMutableString stringWithString:textField.text];
    
    [_text replaceCharactersInRange:range withString:string];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:ME_UI_HIDESHOW_DURATION];
        
    if (textField == self.usernameTextField){
        if (_text.length<ME_PASSPORT_TEXTLENGTH_REQUIRED){
            self.usernameErrorBackLight.alpha = 1.0;
            [self disable_signup];    
        }
        else
            self.usernameErrorBackLight.alpha = 0.0;
        
    }
    else if (textField == self.emailTextField){
                
        if (![self validateEmail:_text]){
            self.signUpButton.enabled = NO;
            self.emailErrorBackLight.alpha = 1.0;
        }
        else
            self.emailErrorBackLight.alpha = 0.0;
    }
    else {
        if (_text.length<ME_PASSPORT_PASSWORDLENGTH_REQUIRED) {
            if (textField == self.passwordTextField)
                self.passwordErrorBackLight.alpha = 1.0;
            else if (textField == self.passwordConfirmTextField)
                self.passwordConfirmErrorBackLight.alpha = 1.0;
        }
        else{
            BOOL is_matched = NO;
            
            if (textField == self.passwordTextField)
                is_matched = [self.passwordConfirmTextField.text isEqualToString:_text];
            else
                is_matched = [self.passwordTextField.text isEqualToString:_text];
            
            if (!is_matched) {
                self.passwordConfirmErrorBackLight.alpha = 1.0;
                self.passwordErrorBackLight.alpha = 1.0;
            }
            else{
                self.passwordConfirmErrorBackLight.alpha = .0;
                self.passwordErrorBackLight.alpha = .0;
            }
        }
    }
    
    if ((self.usernameErrorBackLight.alpha+self.emailErrorBackLight.alpha+self.passwordErrorBackLight.alpha+self.passwordConfirmErrorBackLight.alpha)<=0.0)
        [self enable_signup];
    else
        [self disable_signup];    

    [UIView commitAnimations];    
    
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField{
    textField.enablesReturnKeyAutomatically=YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    [self.passwordConfirmTextField resignFirstResponder];
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
};


- (BOOL) validateEmail: (NSString*)email{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:email] == YES)
        return YES;
    return NO;
}

- (BOOL) checkForm{
    
    lastFieldError = nil;
    
    if (self.usernameTextField.text.length<ME_PASSPORT_PASSWORDLENGTH_REQUIRED) {
        [self flashError:self.usernameErrorBackLight ];
        lastFieldError = [NSString stringWithFormat:ME_LOGIN_REGISTRATION_USERNAME_LIMIT, ME_PASSPORT_TEXTLENGTH_REQUIRED];
        return NO;
    }
    
    if (![self validateEmail:self.emailTextField.text]) {
        if (self.emailTextField.text.length==0){
            [self flashError:self.emailErrorBackLight];
            lastFieldError = ME_LOGIN_REGISTRATION_EMAIL_ISEMPTY;
        }
        else{
            [self flashError:self.emailErrorBackLight];
            lastFieldError = ME_LOGIN_REGISTRATION_EMAIL_ISWRONG;
        }
        return NO;
    }
    
    if (self.passwordTextField.text.length<ME_PASSPORT_PASSWORDLENGTH_REQUIRED) {
        [self flashError:self.passwordErrorBackLight];
        lastFieldError = [NSString stringWithFormat:ME_LOGIN_REGISTRATION_PASSWORD_LIMIT, ME_PASSPORT_PASSWORDLENGTH_REQUIRED];
        return NO;
    }
    
    if (![self.passwordTextField.text isEqualToString:self.passwordConfirmTextField.text]) {
        [self flashError:self.passwordErrorBackLight];
        [self flashError:self.passwordConfirmErrorBackLight];
        lastFieldError = ME_LOGIN_REGISTRATION_CONFIRAMTION_FAIL;
        return NO;
    }
            
    return YES;
}


#pragma mark - Passport delegate API

- (void) passport:(MEPassportAPI *)passport onErrors:(NSArray *)errors{
    [self hideActivity];
    [super passport:passport onErrors:errors];
}

/*
 * TODO: more interractive and usabel messaging - match responses names with fields of the form and inform user about mismatch.
 *
 */
//- (void) passport:(MEPassportAPI *)passport onInputName:(NSString *)name withError:(NSError *)error{
//    alert = [self getAlertView];
//    alert.message = [NSString stringWithFormat:@"%@: %@", name, error.localizedDescription];
//    [alert show];
//}

- (void) passport:(MEPassportAPI *)passport onComplete:(NSArray *)messages{
    
    [self hideActivity];

    [super passport:passport onComplete:messages];
    
    MELoginConfirmViewController *confirmController = [self.storyboard
                                                       instantiateViewControllerWithIdentifier:ME_LOGIN_OCNFIRM_VC_ID];
    
    confirmController.descriptionText = NSLocalizedString(@"Sign up confirmation text", @"Text describes how to finish passport registration");

    confirmController.signUpViewController = self;
    
    [self presentViewController:confirmController animated:YES completion:nil];        
}

#pragma mark - action

- (IBAction)signup:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self showActivity];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.passport registryUser:self.usernameTextField.text withEmail:self.emailTextField.text andPassword:self.passwordTextField.text];
    });
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        self.view.userInteractionEnabled = YES;
    }];
    
}
@end
