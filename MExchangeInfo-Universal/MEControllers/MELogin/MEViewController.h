//
//  MEViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 25/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEViewController : UIViewController
@property (nonatomic,strong)     UIActivityIndicatorView *connectingActivity;
@property (nonatomic,strong)     UIView *containerView;

- (void) showActivity;
- (void) hideActivity;

@end
