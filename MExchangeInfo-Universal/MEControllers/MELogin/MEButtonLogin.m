//
//  MEButtonLogin.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/30/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEButtonLogin.h"

@implementation MEButtonLogin

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 8.0;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [[UIColor colorWithWhite:1 alpha:0.1] CGColor];
    self.layer.borderWidth = 0.5;
}

@end
