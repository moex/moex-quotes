//
//  MERecoveryViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MERecoveryViewController.h"
#import "MEDispatcher.h"
#import "MEPassportAPI.h"
#import "MEUIPreferences.h"
#import "MELoginConfirmViewController.h"

@interface MERecoveryViewController () <MEPassportAPIDelegate, UIAlertViewDelegate, UITextFieldDelegate>

@end

@implementation MERecoveryViewController{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    GIAuth *login = [[MEDispatcher sharedInstance] config].login;
    
    self.recoveryTextField.placeholder = NSLocalizedString(@"Email or nickname", @"Recover password text field place holder in case when we don't have login");
    self.recoveryTextField.delegate = self;
    
    if (login.user && ![login.user isEqualToString:GIC_GUEST_USER]) {
        self.attentionTextView.text =  [NSString stringWithFormat: NSLocalizedString(@"Password recovery instructions text", @"Description text view when you whant to recover password"), login.user];
        self.recoveryTextField.text = login.user;
    }
    else{
        self.attentionTextView.text =  [NSString stringWithFormat: NSLocalizedString(@"Password recovery instructions text in guest mode", @"Description text view when you whant to recover password")];
        self.sendButton.enabled = NO;
        self.sendButton.alpha = 0.5;
    }
    
    [self.sendButton setTitle:NSLocalizedString(@"Send button", @"") forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Close button", @"") forState:UIControlStateNormal];
    
    self.attentionTextView.textColor = ME_UI_LIGHT_GREY_FONT_COLOR;
    
}

- (UIView*) containerView{
    return self.recoveryContainerView;
}

#pragma mark - TextField delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.text.length>0) {
        [self send:self];
    }
    else {
        [textField becomeFirstResponder];
    }
    
    return NO;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *_text=[NSMutableString stringWithString:textField.text];
    
    [_text replaceCharactersInRange:range withString:string];
    
    if (_text.length==0) {
        self.sendButton.alpha = 0.5;
        self.sendButton.enabled = NO;
    }
    else{
        self.sendButton.alpha = 1.;
        self.sendButton.enabled = YES;
    }
    
    return YES;
}

#pragma mark - Passport delegate

- (void) passport:(MEPassportAPI *)passport onComplete:(NSArray *)messages{
    
    [self hideActivity];

    MELoginConfirmViewController *confirmController = [self.storyboard
                                                       instantiateViewControllerWithIdentifier:ME_LOGIN_OCNFIRM_VC_ID];
    
    confirmController.descriptionText = NSLocalizedString(@"Recover confirmation text", @"Text describes how to recover password");
    confirmController.signUpViewController = self;
        
    [self presentViewController:confirmController animated:YES completion:nil];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [self cancel:alertView];
    }
    else{
        [self hideActivity];
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        [self showActivity];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.passport recoveryPassword:self.recoveryTextField.text];
        });
    }
}

#pragma mark - Action

- (IBAction)send:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self showActivity];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.passport recoveryPassword:self.recoveryTextField.text];
    });
}

- (IBAction)cancel:(id)sender {
    [super cancel:sender];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
