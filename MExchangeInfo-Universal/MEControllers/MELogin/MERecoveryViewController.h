//
//  MERecoveryViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEButtonLogin.h"
#import "MELoginCommonViewController.h"

@interface MERecoveryViewController : MELoginCommonViewController

@property (weak, nonatomic) IBOutlet UIView *recoveryContainerView;


@property (weak, nonatomic) IBOutlet UITextField *recoveryTextField;
@property (weak, nonatomic) IBOutlet UITextView *attentionTextView;

@property (weak, nonatomic) IBOutlet MEButtonLogin *sendButton;
@property (weak, nonatomic) IBOutlet MEButtonLogin *cancelButton;

- (IBAction)send:(id)sender;
- (IBAction)cancel:(id)sender;

@end
