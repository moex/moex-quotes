//
//  MEChangePasswordViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEChangePasswordViewController.h"
#import "MEPassportAPI.h"
#import "MEUIPreferences.h"
#import "MELoginConfirmViewController.h"
#import "MEDispatcher.h"

#define ME_LOGIN_CHANGING_OLD_PASSWORD_ISEMPTY NSLocalizedString(@"User current password is empty.", @"User name changing passowrd view has empty current passowrd")
#define ME_LOGIN_CHANGING_CONFIRAMTION_FAIL NSLocalizedString(@"Passwords don't match", @"Password and password confirmation do not match error text at changing password panel")


@interface MEChangePasswordViewController () <UITextFieldDelegate, UIAlertViewDelegate, MEPassportAPIDelegate>
@end

@implementation MEChangePasswordViewController
{
    NSString *lastFieldError;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.passwordOldTextField.placeholder = NSLocalizedString(@"Type old password", @"");
    self.passwordNewTextField.placeholder = NSLocalizedString(@"Type new password", @"");
    self.passwordConfirmTextField.placeholder = NSLocalizedString(@"Confirm new password", @"");
    
    self.passwordOldTextField.delegate = self;
    self.passwordNewTextField.delegate = self;
    self.passwordConfirmTextField.delegate = self;
    
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel button", @"") forState:UIControlStateNormal];
    [self.changeButton setTitle:NSLocalizedString(@"Change button", @"") forState:UIControlStateNormal];
    
    self.passwordConfirmBackLight.alpha = 0.0;
    self.passwordNewBackLight.alpha = 0.0;
    self.passwordNewBackLight.alpha = 0.0;
    
    [self disable_change];
    
	// Do any additional setup after loading the view.
}


- (UIView*) containerView{
    return self.changeContainerView;
}

- (void) disable_change{
    self.changeButton.alpha = 0.5;
    self.changeButton.enabled = NO;
    
}

- (void) enable_change{
    self.changeButton.alpha = 1.0;
    self.changeButton.enabled = YES;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [self cancel:self];
    }
    else{
        [self hideActivity];
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        [self showActivity];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.passport changePassword:self.passwordOldTextField.text withNewPassword:self.passwordNewTextField.text];
        });
    }
}

- (BOOL) checkForm{
    
    
    lastFieldError = nil;
    
    if (self.passwordOldTextField.text.length==0) {
        lastFieldError = ME_LOGIN_CHANGING_OLD_PASSWORD_ISEMPTY;
        return NO;
    }
    
    if (self.passwordNewTextField.text.length<ME_PASSPORT_PASSWORDLENGTH_REQUIRED) {
        lastFieldError = [NSString stringWithFormat:ME_LOGIN_REGISTRATION_PASSWORD_LIMIT, ME_PASSPORT_PASSWORDLENGTH_REQUIRED];
        return NO;
    }
    else if (![self.passwordNewTextField.text isEqualToString:self.passwordConfirmTextField.text]) {
        lastFieldError = ME_LOGIN_CHANGING_CONFIRAMTION_FAIL;
        return NO;
    }
    
    return YES;
}


#pragma mark - TextField delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        
        if ([self checkForm]) {
            [self enable_change];
        }
        else
            [self disable_change];
        
        if (lastFieldError) {
            UIAlertView *alert = [self getAlertView];
            alert.message = lastFieldError;
            alert.delegate = self;
            [alert show];
        }
        else{
            [self change:self];
        }
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - Passport delgate

- (void) passport:(MEPassportAPI *)passport onComplete:(NSArray *)messages{
    
    [self hideActivity];

    [super passport:passport onComplete:messages];
    
    MELoginConfirmViewController *confirmController = [self.storyboard
                                                       instantiateViewControllerWithIdentifier:ME_LOGIN_OCNFIRM_VC_ID];
    
    confirmController.descriptionText = NSLocalizedString(@"Change confirmation text", @"Text describes how to recover password");
    confirmController.signUpViewController = self;
    
    [[MEDispatcher sharedInstance] disconnect];
    
    [self presentViewController:confirmController animated:YES completion:nil];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *_text=[NSMutableString stringWithString:textField.text];
    
    [_text replaceCharactersInRange:range withString:string];
    
    lastFieldError = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:ME_UI_HIDESHOW_DURATION animations:^{
            if (textField == self.passwordOldTextField) {
                if (_text.length==0){
                    self.passwordOldBackLight.alpha = 1.0;
                }
                else
                    self.passwordOldBackLight.alpha = .0;
            }
            BOOL is_matched = NO;
            
            if (textField == self.passwordNewTextField)
                is_matched = [self.passwordConfirmTextField.text isEqualToString:_text];
            else
                is_matched = [self.passwordNewTextField.text isEqualToString:_text];
            
            if (!is_matched) {
                self.passwordNewBackLight.alpha = 1.0;
                self.passwordConfirmBackLight.alpha = 1.0;
            }
            else{
                self.passwordNewBackLight.alpha = .0;
                self.passwordConfirmBackLight.alpha = .0;
            }
            
            if (self.passwordConfirmBackLight.alpha+self.passwordNewBackLight.alpha+self.passwordOldBackLight.alpha<=0 )
                [self enable_change];
            else
                [self disable_change];
        }];
    });    
    return YES;
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)change:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self showActivity];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.passport changePassword:self.passwordOldTextField.text withNewPassword:self.passwordNewTextField.text];
    });
}

- (IBAction)cancel:(id)sender {
    [super cancel:sender];
}
@end
