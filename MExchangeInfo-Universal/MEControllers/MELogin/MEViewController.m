//
//  MEViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 25/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEViewController.h"
#import "MEUIPreferences.h"

@interface MEViewController ()
@end

@implementation MEViewController


- (UIActivityIndicatorView*) connectingActivity{
    if (_connectingActivity == nil) {
        _connectingActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _connectingActivity.autoresizesSubviews = ~UIViewAutoresizingNone;
        [self.view addSubview:_connectingActivity];
    }
    
    [_connectingActivity bringSubviewToFront:_connectingActivity.superview];
    _connectingActivity.frame = self.view.bounds;
    _connectingActivity.center = self.containerView.center;
    
    return _connectingActivity;
}


- (void) showActivity{
    [self.connectingActivity startAnimating];
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        //self.containerView.alpha = 0.8;
    }];
}

- (void) hideActivity{
    [self.connectingActivity stopAnimating];
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        //self.containerView.alpha = 1.0;
    }];
}


- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    _connectingActivity.center = self.containerView.center;
}


@end
