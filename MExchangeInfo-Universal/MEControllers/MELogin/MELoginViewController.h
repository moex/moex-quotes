//
//  MELoginViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/28/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEViewController.h"

@interface MELoginViewController : MEViewController
@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@property (weak, nonatomic) IBOutlet UIWebView *registerInfoTextView;

- (IBAction)signin:(id)sender;

@end
