//
//  MEChangePasswordViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/29/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEButtonLogin.h"
#import "MELoginCommonViewController.h"

@interface MEChangePasswordViewController : MELoginCommonViewController

@property (weak, nonatomic) IBOutlet UIView *changeContainerView;
@property (weak, nonatomic) IBOutlet UITextField *passwordOldTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmTextField;

@property (weak, nonatomic) IBOutlet UIImageView *passwordOldBackLight;
@property (weak, nonatomic) IBOutlet UIImageView *passwordNewBackLight;
@property (weak, nonatomic) IBOutlet UIImageView *passwordConfirmBackLight;

@property (weak, nonatomic) IBOutlet MEButtonLogin *changeButton;
@property (weak, nonatomic) IBOutlet MEButtonLogin *cancelButton;

- (IBAction)change:(id)sender;
- (IBAction)cancel:(id)sender;

@end
