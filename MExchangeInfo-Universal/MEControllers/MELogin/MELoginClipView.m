//
//  MELoginClipView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/28/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELoginClipView.h"

@implementation MELoginClipView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    self.layer.cornerRadius = 6.0;
    self.layer.masksToBounds = YES;   
}

@end
