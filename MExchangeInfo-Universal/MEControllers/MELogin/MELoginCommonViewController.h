//
//  MELoginCommonViewController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 03.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEPassportAPI.h"
#import "MEViewController.h"

@interface MELoginCommonViewController : MEViewController <MEPassportAPIDelegate>

@property (strong, nonatomic, readonly) MEPassportAPI *passport;

- (UIAlertView*) getAlertView;
- (NSString*) errorsToString:(NSArray*) errors;
- (IBAction) cancel:(id) sender;
@end
