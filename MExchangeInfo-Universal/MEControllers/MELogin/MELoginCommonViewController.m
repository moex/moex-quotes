//
//  MELoginCommonViewController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 03.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELoginCommonViewController.h"

@interface MELoginCommonViewController ()

@end

@implementation MELoginCommonViewController
{
    UIAlertView *alert;
}

#pragma mark - Passport delegate

- (void) passport:(MEPassportAPI *)passport onComplete:(NSArray *)messages{
}

- (void) passport:(MEPassportAPI *)passport onErrors:(NSArray *)errors{
    alert = [self getAlertView];
    alert.message = [self errorsToString:errors];
    [alert show];
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _passport = [[MEPassportAPI alloc] init];
    _passport.delegate = self;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIAlertView*) getAlertView{
    
    if (alert) {
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }
    
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", @"")
                                       message:NSLocalizedString(@"Registration could not complete because of an server occured a problem...", @"")
                                      delegate:self
                             cancelButtonTitle:NSLocalizedString(@"Close button", @"")
                             otherButtonTitles:NSLocalizedString(@"Try again", @""),
                             //otherButtonTitles:nil,
             nil];
    
    alert.cancelButtonIndex = 0;
    
    return alert;
}

- (NSString*) errorsToString:(NSArray *)errors{
    NSMutableString *messages = [[NSMutableString alloc] init];
    [messages appendFormat:@"\n"];
    
    for (NSError *error in errors) {
        if (error.localizedFailureReason) {
            [messages appendFormat:@"%@: ", error.localizedFailureReason];
        }
        [messages appendFormat:@"%@\n", error.localizedDescription];
    }
    return messages;
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
