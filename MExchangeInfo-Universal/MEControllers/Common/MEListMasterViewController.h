//
//  MEListMasterViewController.h
//  MExchange-MosQuotes
//
//  Created by denn on 12.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESplitViewController.h"
#import "METableView.h"

@interface MEListMasterViewController : MESplitViewController
@property (readonly,nonatomic) METableView *tableView;
@end
