//
//  MEDismissNavigaionController.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 17/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEDismissNavigaionController.h"
#import "MEConfig.h"
#import "MEUIPreferences.h"

@interface MEDismissNavigaionController () 
@property (nonatomic,strong) UIView *shieldView;
@end

@implementation MEDismissNavigaionController

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.clipsToBounds = NO;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOpacity = 0.5;
    self.view.layer.shadowRadius = 5.0;
    
    id<UIApplicationDelegate> app = [[UIApplication sharedApplication] delegate];
    UIView *superview = app.window;
    
    self.shieldView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.shieldView.backgroundColor = ME_UI_DIMMER_COLOR;
    [superview addSubview:self.shieldView];
    self.shieldView.alpha=0.0;            
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    [self.shieldView addGestureRecognizer:tapGesture];    
}

- (void) tapHandler:(UITapGestureRecognizer*)gesture{
    [self dismiss];    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self.childViewControllers firstObject]==self.topViewController) {        
        self.topViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                                                   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                   target:self
                                                                   action:@selector(dismiss)];    
    }
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //
    // if you want to back to root view controller 
    //
    //[self popToRootViewControllerAnimated:YES];
}

- (void) resetToRootViewController{    
    if ([self.childViewControllers firstObject]!=self.topViewController) {
       [self popToRootViewControllerAnimated:YES]; 
    }    
}

- (void) dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    if (!self.isPresenting) {        
        self.view.frame       = (CGRect){(CGPoint){SCREEN_WIDTH-320,0},(CGSize){320,SCREEN_HEIGHT}};;  
        self.shieldView.frame = (CGRect){(CGPoint){0,0},(CGSize){SCREEN_WIDTH-320,SCREEN_HEIGHT}};
    }
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
    self.view.layer.shadowPath = shadowPath.CGPath;
}

@end
