//
//  METPresentAnimationController.h
//  MExchange-MosQuotes
//
//  Created by denn on 17.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//


@import UIKit;


@interface METPresentAnimationController : NSObject <UIViewControllerAnimatedTransitioning>
@end
