//
//  MEListMasterViewController.m
//  MExchange-MosQuotes
//
//  Created by denn on 12.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEListMasterViewController.h"

@interface MEListMasterViewController ()
@property (strong, nonatomic) METableView *tableView;
@end

@implementation MEListMasterViewController

- (METableView*) tableView{
    // Table View setup
    if (!_tableView) {
        _tableView = [[METableView alloc] initWithFrame:self.view.bounds];        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.scrollsToTop = YES;
    }    
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.masterView = self.tableView;
    // Do any additional setup after loading the view.
}

@end
