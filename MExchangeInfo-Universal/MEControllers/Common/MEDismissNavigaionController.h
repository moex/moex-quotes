//
//  MEDismissNavigaionController.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 17/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEDismissNavigaionController : UINavigationController <UIGestureRecognizerDelegate>
@property (nonatomic,assign) BOOL isPresenting;
@property (nonatomic,readonly)  UIView *shieldView;
@property (nonatomic,weak)  UIViewController *fromViewController;
- (void) dismiss;
- (void) resetToRootViewController;
@end
