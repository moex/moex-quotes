//
//  METDismissAnimationController.m
//  MExchange-MosQuotes
//
//  Created by denn on 17.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//


#import "METDismissAnimationController.h"
#import "MEUIPreferences.h"
#import "MEDismissNavigaionController.h"

@implementation METDismissAnimationController

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return ME_UI_ANIMATION_DURATION/2.0;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    MEDismissNavigaionController *fromViewController = (MEDismissNavigaionController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    fromViewController.isPresenting = YES;
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    UIView *containerView = [transitionContext containerView];
    CGRect frame = containerView.bounds;

    [UIView animateWithDuration:duration
                     animations:^{
                         fromViewController.shieldView.alpha = 0.0;
                         fromViewController.view.frame = (CGRect){{containerView.bounds.size.width,0},frame.size};
                         fromViewController.shieldView.frame=containerView.bounds;
                     }
                     completion:^(BOOL finished) {
                         [fromViewController.view removeFromSuperview];
                         [transitionContext completeTransition:YES];
                         fromViewController.isPresenting = NO;
                     }];
}

@end
