//
//  MESplitViewController.h
//  MExchange-MosQuotes
//
//  Created by denn on 12.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MESplitViewController : UIViewController
@property (strong,nonatomic) UIView *masterView;
@property (strong,nonatomic) UIViewController *detailController;
@end
