//
//  MESplitViewController.m
//  MExchange-MosQuotes
//
//  Created by denn on 12.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESplitViewController.h"
#import "MEConfig.h"

@interface MESplitViewController () <UIGestureRecognizerDelegate>
@property (strong,nonatomic) UIView *masterViewContainer;
@property (strong,nonatomic) UINavigationController *contentController;
@end

@implementation MESplitViewController

- (CGRect) masterViewFrame{
        
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return (CGRect){
            (CGPoint){0,0},
            (CGSize) {SCREEN_WIDTH,SCREEN_HEIGHT}
        };
    }
    
    CGFloat offsetY = 0;
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        return (CGRect){
            (CGPoint){0,offsetY},
            (CGSize) {SCREEN_WIDTH,SCREEN_HEIGHT/3.0f}
        };
    }
    else{
        CGFloat w = SCREEN_WIDTH/3.; w=w<320.0?320.0:w;
        CGFloat width = (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?w:SCREEN_WIDTH;
        
        CGRect  navBounds = self.navigationController.navigationBar.bounds;        
        CGFloat heightTop = STATUSBAR_HEIGHT + navBounds.size.height;

        return (CGRect){
            (CGPoint){0,offsetY},
            (CGSize) {width,SCREEN_HEIGHT - offsetY-heightTop}
        };
    }
}

- (CGRect) contentViewFrame{
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return (CGRect){
            (CGPoint){0,0},
            (CGSize) {SCREEN_WIDTH,SCREEN_HEIGHT}
        };
    }
    
    CGFloat offsetY = 0.0;
    CGRect  navBounds = self.navigationController.navigationBar.bounds;
    
    CGRect conttainerFrame = [self masterViewFrame];
    CGFloat heightTop = STATUSBAR_HEIGHT + navBounds.size.height;
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        return (CGRect){
            (CGPoint){offsetY,conttainerFrame.origin.y+conttainerFrame.size.height},
            (CGSize) {SCREEN_WIDTH,SCREEN_HEIGHT-(conttainerFrame.origin.y+conttainerFrame.size.height+heightTop)+1}
        };
    }
    else{
        CGFloat width  = SCREEN_WIDTH-conttainerFrame.size.width;
        
        return (CGRect){
            (CGPoint){conttainerFrame.size.width, offsetY},
            (CGSize) {width+1,SCREEN_HEIGHT - offsetY - heightTop}
        };
    }
}

- (void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        self.masterViewContainer.frame = [self masterViewFrame];
        self.contentController.view.frame = [self contentViewFrame];
        
        CALayer *layer = self.contentController.view.layer;
        
        layer.shadowPath = [UIBezierPath bezierPathWithRect:self.contentController.view.bounds].CGPath;
        layer.shadowColor = [UIColor blackColor].CGColor;
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            layer.shadowOffset = CGSizeMake(-2.0f, 0.0f);
        }
        else{
            layer.shadowOffset = CGSizeMake(0.0f, -2.0f);
        }
        layer.shadowOpacity = 0.1f;
        layer.shadowRadius = 4.0f;
        layer.rasterizationScale = YES;
        self.contentController.view.clipsToBounds = NO;
    }
}

- (void)loadView
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    self.masterViewContainer = [[UIView alloc] initWithFrame:[self masterViewFrame]];
    self.masterViewContainer.autoresizingMask = ~UIViewAutoresizingNone;
    self.masterViewContainer.backgroundColor = [UIColor grayColor];
    
    [view addSubview:self.masterViewContainer];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        //
        // Content for right side of the pad version
        //
        self.contentController = [[UINavigationController alloc] initWithNavigationBarClass:[UINavigationBar class] toolbarClass:[UIToolbar class]];
        [self.contentController.view setFrame: [self contentViewFrame] ];
        self.contentController.view.autoresizingMask = ~UIViewAutoresizingNone;
        self.contentController.navigationBar.translucent = NO;
        self.contentController.navigationBarHidden = YES;
        
        [view addSubview:self.contentController.view];        
        [self addChildViewController:self.contentController];
    }
    
    self.view = view;
}

- (void) setDetailController:(UIViewController *)detailController{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.contentController setViewControllers:@[detailController]];
    }
    else
        _detailController = detailController;
}

- (void) setMasterView:(UIView *)masterView{
    // Table View setup
    if (_masterView) {
        [_masterView removeFromSuperview];
    }
    _masterView = masterView;   
    _masterView.frame = self.masterViewContainer.bounds;
    _masterView.autoresizingMask = ~UIViewAutoresizingNone;    
    if (self.contentController) {
        [self.masterViewContainer insertSubview:_masterView aboveSubview:self.contentController.view];        
    }
    else
        [self.masterViewContainer addSubview:_masterView];        
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    self.edgesForExtendedLayout=UIRectEdgeNone;    
    self.automaticallyAdjustsScrollViewInsets=NO;            
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}


- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}

@end
