//
//  METPresentAnimationController.m
//  MExchange-MosQuotes
//
//  Created by denn on 17.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METPresentAnimationController.h"
#import "MEConfig.h"
#import "MEUIPreferences.h"
#import "MEDismissNavigaionController.h"

//
// ios7 workarround 
//
@interface UIView (Extras)
- (CGRect)orientationCorrectedRect:(CGRect)rect ;
- (CGRect)orientationCorrectedRectInvert:(CGRect)rect ;
@end

@implementation UIView (Extras)

- (CGRect)orientationCorrectedRect:(CGRect)rect {
    CGAffineTransform ct = self.transform;
    
    if (!CGAffineTransformIsIdentity(ct)) {
        CGRect superFrame = self.superview.frame;
        CGPoint transOrigin = rect.origin;
        transOrigin = CGPointApplyAffineTransform(transOrigin, ct);
        
        rect.origin = CGPointZero;
        rect = CGRectApplyAffineTransform(rect, ct);
        
        if (rect.origin.x < 0.0) {
            transOrigin.x = superFrame.size.width + rect.origin.x + transOrigin.x;
        }
        if (rect.origin.y < 0.0) {
            transOrigin.y = superFrame.size.height + rect.origin.y + transOrigin.y;
        }
        rect.origin = transOrigin;
    }
    
    return rect;
}

- (CGRect)orientationCorrectedRectInvert:(CGRect)rect {
    CGAffineTransform ct = self.transform;
    
    if (!CGAffineTransformIsIdentity(ct)) {
        ct = CGAffineTransformInvert(ct);
        
        CGRect superFrame = self.superview.frame;
        superFrame = CGRectApplyAffineTransform(superFrame, ct);
        CGPoint transOrigin = rect.origin;
        transOrigin = CGPointApplyAffineTransform(transOrigin, ct);
        
        rect.origin = CGPointZero;
        rect = CGRectApplyAffineTransform(rect, ct);
        
        if (rect.origin.x < 0.0) {
            transOrigin.x = superFrame.size.width + rect.origin.x + transOrigin.x;
        }
        if (rect.origin.y < 0.0) {
            transOrigin.y = superFrame.size.height + rect.origin.y + transOrigin.y;
        }
        
        rect.origin = transOrigin;
    }
    
    return rect;
}
@end

@interface METPresentAnimationController() <UIGestureRecognizerDelegate>
@property (nonatomic,weak) UIViewController *fromViewController;
@property (nonatomic,strong) UITapGestureRecognizer *tapShieldGesture;
@end

@implementation METPresentAnimationController

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return ME_UI_ANIMATION_DURATION/2.0f;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    MEDismissNavigaionController *toViewController = (MEDismissNavigaionController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    toViewController.isPresenting = YES;

    UIView *containerView = [transitionContext containerView];

    CGRect containerBounds = containerView.bounds;
    CGRect endFrame = containerBounds;

    endFrame = UIEdgeInsetsInsetRect(endFrame, UIEdgeInsetsMake(0, endFrame.size.width-320, 0, 0));
    CGRect startFrame = (CGRect){{containerView.bounds.size.width,0},endFrame.size};
    
    toViewController.view.frame = startFrame;
    
    toViewController.view.alpha = 1.0;
    toViewController.fromViewController = self.fromViewController;
    [containerView addSubview:toViewController.view];
    
    toViewController.shieldView.alpha =0.0;
    toViewController.shieldView.frame=containerBounds;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         toViewController.shieldView.alpha =1.0;
                         toViewController.shieldView.frame = (CGRect){(CGPoint){0,0},
                             (CGSize){containerBounds.size.width-endFrame.size.width,containerBounds.size.height}};
                         toViewController.view.frame = endFrame;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                         toViewController.isPresenting = NO;
                     }
     ];
}

@end
