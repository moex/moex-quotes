//
//  METickerCardsDetailController.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/13/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerCardDetailController.h"
#import "METickerCardViewCell.h"
#import "NSDate+MEComparing.h"
#import "MEUIPreferences.h"
#import "METickerCardsViewController.h"
#import "MEDismissNavigaionController.h"

#import <MEinfoCXConnector/GIStompBody.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import <Crashlytics/Crashlytics.h>

static NSString *cellIdentifier = ME_TICKERCARD_ITEM_CELL;

@interface METickerCardDetailController () <METickerCardsItemDelegate>
@end

@implementation METickerCardDetailController

- (void) setTickerCards:(METickerCardsModel *)tickerCards{
    _tickerCards = tickerCards;
    _tickerCards.itemsDelegate = self;
}

- (void) backButtonHandler:(UIButton*)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerClass:[METickerCardViewCell class] forCellReuseIdentifier:cellIdentifier];
    self.tableView.allowsSelection = YES;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
    if (self.rootController.tickerLast!=nil && self.rootController.tickerLast!=self.rootController.ticker) {
        if (self.navigationController && [self.navigationController respondsToSelector:@selector(resetToRootViewController)]) {
            [self.navigationController performSelector:@selector(resetToRootViewController)];
        }        
    }    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [Answers logContentViewWithName:@"Ticker Card Detail View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{
                                      @"Ticker Card  Detail: card": [NSString stringWithFormat:@"%@:%@", self.currentCard.name,  self.currentCard.caption],
                                      }];
}


#pragma mark - Table view data source

- (void) didTickerCardItemUpdate:(MECard *)tickerCard{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.tickerCards itemsForCard:self.currentCard].count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    METickerCardViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    MECardItem *item = [self.tickerCards cardItemForCard:self.currentCard withIndex:indexPath.row]; 
        
    cell.caption = item.hint;
    cell.userInteractionEnabled = NO;
        
    if ([[item.name lowercaseString] isEqualToString:@"url"]) 
        cell.urls = item.value;    
    else
        cell.value = item.value;    
    
    cell.info = item.valueInfo;
        
    if (indexPath.row<[self.tickerCards itemsForCard:self.currentCard].count-1) 
        cell.devider.alpha = 1.0;    
    else
        cell.devider.alpha = 0.0;
        
    if (indexPath.row%2) 
        cell.contentView.backgroundColor = ME_TABLE_EVEN_COLOR;    
    else
        cell.contentView.backgroundColor = [UIColor clearColor];

    return cell;
}

-(BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx = @"(http://|https://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_]){2,6}))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    METickerCardViewCell *cell = (METickerCardViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    NSString *url = cell.firstUrl;
        
    if ( url ) {        
        if (![url hasPrefix:@"http"]) {
            url = [NSString stringWithFormat:@"http://%@", url];
        }        
        NSURL *_lounch_url = [NSURL URLWithString:url];        
        [[UIApplication sharedApplication] openURL:_lounch_url];   
    }
}

@end
