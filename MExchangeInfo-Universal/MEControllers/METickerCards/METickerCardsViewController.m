//
//  METickerCardsViewController.m
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 15/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerCardsViewController.h"
#import "MEUIPreferences.h"
#import "MEWatchListModel.h"
#import "METickerCardDetailController.h"
#import "MEMenuViewCell.h"
#import "MEDismissNavigaionController.h"


#import <Crashlytics/Crashlytics.h>

@interface METickerCardsViewController () <METickerCardsModelDelegate>
@property (nonatomic, strong) UIBarButtonItem *activityIndicator;
@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, strong) METicker *tickerLast;
@property (nonatomic, readonly) METicker *currentTicker;
@property (nonatomic, readonly) MEWatchListModel *watchList;
@property (nonatomic, readonly) METickerCardDetailController *detailViewController;
@property (nonatomic, strong) UIImage *sectionHeaderBgImage;
@end

@implementation METickerCardsViewController
{
    UIView *overView;
    NSIndexPath *lastSelectedIndexPath;
    UILabel *tickerInfoBoardLabel;
    
    NSInteger cardSectionCount;
    NSMutableArray<NSNumber *> *rowsForSection;

}

@synthesize watchList=_watchList, tickerCards=_tickerCards, detailViewController=_detailViewController;

- (MEWatchListModel*) watchList{
    if (!_watchList) {
        _watchList = [MEWatchListModel sharedInstance];
    }
    return _watchList;
}

- (METicker*) ticker{
    return self.watchList.activeTicker;
}

- (METickerCardDetailController*) detailViewController{
    if (!_detailViewController) {
        _detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tickerCardDetailTID"];
        _detailViewController.rootController = self;
    }
    return _detailViewController;
}

- (void) update{    
    if (self.currentTicker!=self.ticker || self.tickerCards.sections==0){
        _currentTicker=self.ticker;
        _tickerCards = [[METickerCardsModel alloc] initWithTicker:self.ticker];
        _tickerCards.delegate = self;
        [self.tickerCards update];
    }
}


- (UIAlertView*) alertView{
    if (!_alertView) {
        _alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Server error", nil)
                                                message:nil
                                               delegate:nil
                                      cancelButtonTitle:NSLocalizedString(@"Close button", nil)
                                      otherButtonTitles:nil];
    }
    
    return _alertView;
}

- (void) addActivityIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView startAnimating];
        self.navigationItem.rightBarButtonItem = self.activityIndicator;
        
        if (!self->overView) {
            self->overView = [[UIView alloc] initWithFrame:self.view.bounds];
            self->overView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
            self->overView.backgroundColor = [UIColor blackColor];
            [self.view addSubview:self->overView];
        }
        self->overView.alpha = 0.3;
        self.view.userInteractionEnabled = NO;
    });
}

// stop the animation of activityIndicator and hide it
- (void)hideActivityIndicator {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // we are done processing earthquakes, stop our activity indicator
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)self.activityIndicator.customView;
        [indicatorView stopAnimating];
        self.navigationItem.rightBarButtonItem = nil;
        
        self.view.userInteractionEnabled = YES;
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            self->overView.alpha = 0.0;
        }];
    });
}

- (UIBarButtonItem*) activityIndicator{
    if (!_activityIndicator) {
        UIActivityIndicatorView *activityIndicator =
        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activityIndicator = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    }
    
    return _activityIndicator;
}


- (void) updateTitle{
    self.navigationItem.title = self.ticker.caption;
    //self.navigationController.navigationBar.topItem.title = self.ticker.caption;
    tickerInfoBoardLabel.text = self.ticker.board.caption;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateTitle];
    [self update];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [Answers logContentViewWithName:@"Ticker Cards View"
                        contentType:nil
                          contentId:nil
                   customAttributes:@{
                                      @"Ticker Cards: ticker": self.ticker.ID,
                                      }];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.tickerLast = self.ticker;
}

- (void) didTickerCardError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.alertView.message = error.localizedDescription;
        [self.alertView show];
        [self hideActivityIndicator];
        [self reloadData];
    });
}


- (void) reloadData{
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{

        self->cardSectionCount = self.tickerCards.sections;
        self->rowsForSection = [NSMutableArray new];
        
        for (int i=0; i<self->cardSectionCount; i++) {
            NSInteger s = [self.tickerCards rowsForSection:i];
            NSNumber* sn = [NSNumber numberWithInteger:s];
            [self->rowsForSection addObject:sn];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateTitle];
            [self.tableView reloadData];
        });

    });
}

- (void) willTickerCardUpdate:(METickerCardsModel *)tickerCardsModel{
    [self addActivityIndicator];
}

- (void) didTickerCardUpdate:(METickerCardsModel *)tickerCardsModel{
    
    _tickerCards = tickerCardsModel;
    
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self hideActivityIndicator];
        [self reloadData];
    });
}


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UIImage *sectionHeaderBgImage = [UIImage imageNamed:@"grey_panel.png"];

        self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, sectionHeaderBgImage.size.height)];
    UIView *boardView = self.tableView.tableHeaderView;
    boardView.backgroundColor = [UIColor colorWithPatternImage:ME_UI_BACKGROUND_IMAGE];
    
    tickerInfoBoardLabel = [[UILabel alloc] initWithFrame:boardView.bounds];
    tickerInfoBoardLabel.font = [UIFont fontWithName:ME_UI_FONT size:16];
    tickerInfoBoardLabel.textAlignment = NSTextAlignmentCenter;
    tickerInfoBoardLabel.textColor = ME_UI_LIGHT_GREY_FONT_COLOR;
    tickerInfoBoardLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [boardView addSubview:tickerInfoBoardLabel];
    
    UIImage *image = [UIImage imageNamed:@"top_panel.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    [self.tableView registerClass:[MEMenuViewCell class] forCellReuseIdentifier:CellIdentifierMenu];
    [self.tableView registerClass:[MEMenuLoginCell class] forCellReuseIdentifier:CellIdentifierLogin];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:ME_UI_BACKGROUND_IMAGE];
    
    self.tableView.dataSource= self;
    self.tableView.delegate  = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage*)    sectionHeaderBgImage{
    if (!_sectionHeaderBgImage) {
        _sectionHeaderBgImage = [UIImage imageNamed:@"grey_panel.png"];
    }
    
    return _sectionHeaderBgImage;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self->cardSectionCount; 
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self->rowsForSection count]<=section) return 0;
    return  self->rowsForSection[section].integerValue;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MEBaseViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifierMenu forIndexPath:indexPath];
    MECard *card = [self.tickerCards cardForIndexPath:indexPath];

    cell.textLabel.text = card.caption;
    
    if (indexPath.row < [self.tickerCards rowsForSection:indexPath.section]-1)
        cell.devider.alpha = 1.0;
    else
        cell.devider.alpha = 0.0;

    return cell;
}

-(UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    METickerCardSection *_section = [self.tickerCards sectionForSection:section];
    
    if (!_section || _section.caption.length<=0) {
        return nil;
    }
    
    UIImageView *view   = [[UIImageView alloc] initWithImage:self.sectionHeaderBgImage];
    
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = ME_UI_MENU_FONT_COLOR;
    label.font = [UIFont fontWithName:ME_UI_FONT size:14.];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = _section.caption;
    
    [view addSubview:label];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:0 views:@{@"label": label}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[label]-2-|" options:0 metrics:0 views:@{@"label": label}]];
    
    return view;
}


#pragma mark - Table delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MECard *card = [self.tickerCards cardForIndexPath:indexPath];
    self.detailViewController.tickerCards = self.tickerCards;
    self.detailViewController.currentCard = card;
    self.detailViewController.navigationItem.title = card.caption;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

@end
