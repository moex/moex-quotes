//
//  METickerCardsTableView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/17/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerCardsTableView.h"
#import "MEUIPreferences.h"

@implementation METickerCardsTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
