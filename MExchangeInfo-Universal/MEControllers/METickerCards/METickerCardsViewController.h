//
//  METickerCardsViewController.h
//  MExchange-MosQuotes
//
//  Created by denis svinarchuk on 15/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METicker.h"
#import "METickerCardsModel.h"
@interface METickerCardsViewController : UITableViewController
@property (nonatomic, readonly) METickerCardsModel *tickerCards;
@property (nonatomic, readonly) METicker *ticker;
@property (nonatomic, readonly) METicker *tickerLast;
- (void) update;
@end
