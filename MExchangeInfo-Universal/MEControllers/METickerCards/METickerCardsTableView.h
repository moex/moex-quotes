//
//  METickerCardsTableView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/17/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METableView.h"

@interface METickerCardsTableView : METableView

@end
