//
//  METickerCardsDetailController.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/13/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "METickerCardsModel.h"

@class METickerCardsViewController;

@interface METickerCardDetailController : UITableViewController
@property (weak, nonatomic)   MECard *currentCard;
@property (strong, nonatomic) METickerCardsModel *tickerCards;
@property (weak, nonatomic)   METickerCardsViewController *rootController;
@end
