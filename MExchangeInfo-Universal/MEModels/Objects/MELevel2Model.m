//
//  MELevel2Model.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 20/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MELevel2Model.h"
#import "MEDispatcher.h"
#import <MEinfoCXConnector/GIQueue.h>

@interface NSNumber(number)
- (NSNumber*) number;
@end

@implementation NSNumber(number)

- (NSNumber*) number{ return self;}

@end

@interface __GIStompOrderbook : GIStompOrderbook
@property (nonatomic,strong) NSString *tickerID;
@end

@implementation __GIStompOrderbook
@synthesize ticker=_ticker;
+ (id) select:(NSString *)ticker{
    __GIStompOrderbook *book = [super select:ticker];
    book.tickerID = ticker;
    return book;
}

@end

//@interface __MELevel2Book : MELevel2Book
//- (id) initWithTickerID:(NSString*)tickerID;
//@property (nonatomic,readonly) NSString *tickerID;
//@end
//
static MELevel2Model *__shared_instance = nil;

@interface MELevel2Model() <GIStompDispatcherProtocol>
@property (nonatomic,readonly) MEDispatcher *dispatcher;
@property (atomic,readonly) MELevel2Book *activeLevel2Book;
@property (atomic,readonly) NSMutableDictionary *books;
@property (atomic,readonly) NSMutableDictionary *errors;
@property (atomic,assign) BOOL isLockedToUpdate;
@end

@implementation MELevel2Model

@synthesize activeLevel2Book=_activeLevel2Book, books=_books, errors=_errors, ticker=_ticker;

#pragma mark - DataSource

- (MELevel2Book*) level2Book{
    return self.activeLevel2Book;
}

- (NSUInteger) sections{
    NSInteger sections = 0;
    if (self.activeLevel2Book) {
        if (self.activeLevel2Book.bidRows.count>0) {
            sections++;
        }
        if (self.activeLevel2Book.offerRows.count>0) {
            sections++;
        }
    }
    return sections;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{
    if (!self.activeLevel2Book)
        return 0;
    if (section==1)
        return self.activeLevel2Book.offerRows.count;
    return self.activeLevel2Book.bidRows.count;
}

- (NSUInteger) allRows{
    return self.activeLevel2Book.bidRows.count+self.activeLevel2Book.offerRows.count;
}

- (MELevel2*) level2ForIndexPath:(NSIndexPath *)indexPath{
    @synchronized(self){
        NSArray  *rows;
        
        if (indexPath.section==0) {
            rows = self.activeLevel2Book.bidRows;
        }
        else if (indexPath.section==1){
            rows = self.activeLevel2Book.offerRows;
        }
        
        if (indexPath.row>=rows.count) {
            NSLog(@"Error: level2ForIndexPath bounds: [0...%lu] for path%@", (unsigned long)(rows.count-1), indexPath);
            return nil;
        }
        return [rows objectAtIndex:indexPath.row];
    }
}

- (MELevel2Type) level2TypeForSection:(NSUInteger)section{
    if (section==0) {
        return MELEVEL2_BID;
    }
    return MELEVEL2_OFFER;
}

- (void) beginTransaction{
    
    @synchronized(self){
        if (self.isLockedToUpdate) {
            return;
        }
        
        self.isLockedToUpdate = YES;
        
        //
        // set active last in queue
        //
        NSMutableArray *book = [self.books objectForKey:self.ticker.ID];
        if (book && book.count>0) {
            _activeLevel2Book = [book lastObject];
        }
        else if (book==nil){
            _activeLevel2Book = nil;
        }
    }
}

- (void) commitTransaction{
    
    @synchronized(self){        
        NSMutableArray *book = [self.books objectForKey:self.ticker.ID];
        //
        // flush all used objects 
        //
        if (book) {
            if (book.count>1) {
                [book removeObjectsInRange:NSMakeRange(0, book.count-1)];                
            }
        }        
        self.isLockedToUpdate = NO;
    }
}

#pragma mark - Model

- (MEDispatcher*) dispatcher{
    return [MEDispatcher sharedInstance];
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MELevel2Model alloc] init];
    });
    return __shared_instance;
}

- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        [GIStompOrderbook setDepth:ME_LEVEL2_ROWSLIMIT];
    }
    
    return self;
}

- (void) setLastError:(NSString *)lastError{
    if (self.ticker == nil) {
        return;
    }
    [self.errors setObject:lastError forKey:self.ticker.ID];
}

- (NSString*) lastError{
    if (self.ticker == nil) {
        return nil;
    }
    return [self.errors objectForKey:self.ticker.ID];
}

- (NSMutableDictionary*) errors{
    if (_errors) {
        return _errors;
    }
    
    _errors = [[NSMutableDictionary alloc] init];
    
    return _errors;
}

- (NSMutableDictionary*) books{
    if (_books) {
        return _books;
    }
    
    _books = [[NSMutableDictionary alloc] init];
    
    return _books;
}

- (MELevel2Book*) activeLevel2Book{
    
    if (self.ticker==nil || self.ticker.ID==nil) {
        return nil;
    }
    
    return _activeLevel2Book;
}

- (void) setTicker:(METicker *)ticker{
    
    if (_ticker!=nil && _ticker==ticker) {
        return;
    }
    
    if ([self.ticker.ID isEqualToString:ticker.ID]) {
        return;
    }
    
    [self unsubscribe];
    
    _ticker=ticker;
}

- (METicker*) ticker{
    return _ticker;
}

- (void) subscribe{
    if (!self.ticker.orderbookSubscription) {
        self.ticker.orderbookSubscription = [__GIStompOrderbook select:self.ticker.ID];
        self.ticker.orderbookSubscription.delegate = self;
    }
    
    [self.dispatcher registerMessage:self.ticker.orderbookSubscription];
}

- (void) unsubscribe{
    [self.dispatcher unregisterMessage:self.ticker.orderbookSubscription];
    if (self.books) {
        NSMutableArray *book = [self.books objectForKey:self.ticker.ID];
        if (book) {
            [self.books removeObjectForKey:self.ticker.ID];
        }
    }
}

- (void) updateStompLevel2:(__GIStompOrderbook*)level2{
    
    MELevel2Book *activeLevel2Book = [[MELevel2Book alloc] init];//[self.books objectForKey:level2.tickerID];
    
    activeLevel2Book.bidAskField = level2.structure.stream.fields[@"BUYSELL"];
    
    if (!activeLevel2Book.structure)
        activeLevel2Book.structure = level2.structure;
    
    
    GICommonStompFrame *c = level2.lastResponse;
    GIStompValue *max =  [c.body maxValueForColumn:@"QUANTITY"];
    
    @try {
        for (NSInteger i =0; i<c.body.countOfRows; i++) {
            MELevel2 *row = [[MELevel2 alloc] init];
            
            row.state = [c.body objectForIndex:i andKey:@"BUYSELL"];
            
            BOOL isBid = [row.state isEqualToString:@"B"];
            
            row.price = [c.body valueForIndex:i andKey:@"PRICE"];
            row.qty = [c.body valueForIndex:i andKey:@"QUANTITY"];
            
            NSNumber *q = [row.qty number];
            float maxF = [max number].floatValue;
            
            if (maxF>0.0) {
                //row.ratio = ABS(log2f(q.floatValue*1.1)/log2f(maxF*1.1));
                row.ratio = ABS(q.floatValue/maxF);
            }
            else
                row.ratio = 0.0;
            
            if (isBid) {
                row.type = MELEVEL2_BID;
                [activeLevel2Book addBid:row];
            }
            else{
                row.type = MELEVEL2_OFFER;
                [activeLevel2Book addOffer:row];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        [self commitTransaction];
        return;
    }    
    
    if (activeLevel2Book) {
        @synchronized(self){            
            NSMutableArray *objects = [self.books objectForKey:level2.tickerID];            
            if (!objects) {
                objects = [[NSMutableArray alloc] init];
            }
            else if (![self.ticker.ID isEqualToString:level2.tickerID]){
                [objects removeAllObjects];
            }
            [objects addObject:activeLevel2Book];
            [self.books setObject:objects forKey:level2.tickerID];           
        }
        
        if (self.delegate && [self.ticker.ID isEqualToString:level2.tickerID]) {
            _activeLevel2Book = activeLevel2Book;
            [self.delegate didLevel2ModelUpdate:self];
        }
    }    
}

- (void) didSubscriptionRestore:(id)frame{
    __GIStompOrderbook *level2=frame;
    if (self.delegate && [self.ticker.ID isEqualToString:level2.tickerID]) {
        [self.delegate didLevel2ModelUpdate:self];
    }
}

- (void) didFrameReceive:(id)frame{
    [self updateStompLevel2:frame];
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didLevel2Model:fault:)]) {
        //[self.delegate didLevel2Model:self fault:error];
    }
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didLevel2Model:fault:)]) {
        NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                              code: GI_STOMP_LOGIN_FAILED
                                          userInfo: @{
                                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"This ticker has not level2", @"Stomp error received"),
                                                      NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"The ticker %@ has not level2.", @"Stomp error access description."), self.ticker.hint]
                                                      }];
        [self.delegate didLevel2Model:self fault:nerror];
    }
}

- (void) didAccessDeny:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didLevel2Model:fault:)]) {
        NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                              code: GI_STOMP_LOGIN_FAILED
                                          userInfo: @{
                                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Level2 access denied for the ticker", @"Stomp error received"),
                                                      NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"Level2 access denied for the ticker %@", @"Stomp error access description."), self.ticker.hint]
                                                      }];
        
        [self.delegate didLevel2Model:self fault:nerror];
    }
}

@end
