//
//  MENewsModel.h
//  MExchange
//
//  Created by denn on 7/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import "MEDispatcher.h"
#import "MENewsSection.h"
#import "MENewsItem.h"
#import "MENewsIncrementals.h"

#define ME_NEWS_LIMIT 1000

@class MENewsItem, MENewsModel;

@protocol MENewsDelegate <NSObject>
@optional
- (void) didNewsError: (NSError*)error;
- (void) didNewsRestore: (MENewsModel*) list; // from core data
- (void) willNewsUpdate: (MENewsModel*) list;
- (void) didNewsUpdate: (MENewsModel*) list;
@end

@protocol MENewsItemDelegate <NSObject>
@optional
- (void) didNewsItemError: (NSError*)error;
- (void) didNewsItemUpdate: (MENewsItem*) news;
@end

@interface MENewsModel : NSObject

+ (id) sharedInstance;
@property (nonatomic,strong) id<MENewsDelegate> delegate;
@property (nonatomic,strong) id<MENewsItemDelegate> itemDelegate;

@property (nonatomic,readonly) NSArray *items;

- (void) subscribe;
- (void) unsubscribe;

- (void) requestContentByNewsId:(NSNumber*)newsId;

- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (MENewsSection*) sectionForSection:(NSUInteger)section;
- (MENewsItem*) newsForIndexPath:(NSIndexPath*)indexPath;
- (NSIndexPath *)indexPathForNews:(MENewsItem*)news;
- (void) saveContext;
- (void) truncate;
@end


