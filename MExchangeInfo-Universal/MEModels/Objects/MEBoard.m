//
//  MEBoard.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEBoard.h"
#import "MEReferences.h"

@implementation MEBoard

- (NSString*)description{
    return [NSString stringWithFormat:@"%@: isPimary=%i  %@ %@ engine=%@ market=%@", _ID, _isPrimary, _name, _caption, _engineID,_marketID];
}

@end

@implementation MEBoard (Referenced)

- (MEEngine*) engine{
    return [[[MEReferences sharedInstance] engines] objectForKey:_engineID];
}

//- (MEMarket*) market{
//    return nil;
//}

@end