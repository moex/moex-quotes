//
//  MEWatchListModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEWatchListModel.h"
#import "MEEnginesStomp.h"
#import "MEMarketsStomp.h"
#import "MEBoardsStomp.h"
#import "MEReferences.h"
#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import <MEinfoCXConnector/GIUtils.h>
#import "MEReferencesContext.h"
#import "MESettings.h"
#import "NSDictionary+Json.h"

#define __TICKERS_ID_LIST__CONTEXT__ @"__TICKERS_ID_LIST__CONTEXT__"

@interface MEWatchListModel () <MEReferencesProtocol>
@property(nonatomic,readonly)  GIOrderedDictionary *tickers;
@property (nonatomic,strong)   MEReferences *references;
@property (nonatomic,strong)   MEReferencesContext *context;
@end

static MEWatchListModel *__shared_instance = nil;

@implementation MEWatchListModel
{
    MEDispatcher *dispatcher;
    GIOrderedDictionary *tickerIndexPaths;
    NSUInteger lastIndexSize;
    NSDictionary *tradingStatus2Level2;
    NSDictionary *tradingStatus2SessionState;
}

@synthesize references = _references, tickers=_tickers;

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEWatchListModel alloc] init];
    });
    
    return __shared_instance;
}

- (GIOrderedDictionary*) tickers{
    if (!_tickers) {
        _tickers = [GIOrderedDictionary new];
    }
    return _tickers;
}

- (MEReferences*) references{
    if (_references) {
        return _references;
    }
    
    _references = [MEReferences sharedInstance];
    _references.delegate = self;
    
    [_references update];
    
    return _references;
}


- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        
        dispatcher = [MEDispatcher sharedInstance];
        
        tickerIndexPaths = [[GIOrderedDictionary alloc] init];
        
        tradingStatus2Level2 = @{
                                 @"N": @NO,
                                 @"O": @YES,
                                 @"C": @NO,
                                 @"F": @YES,
                                 @"B": @NO,
                                 @"T": @YES,
                                 @"L": @YES,
                                 @"D": @YES,
                                 @"I": @NO,
                                 @"E": @NO
                                 };
        
        tradingStatus2SessionState = @{
                                       @"N": @NO,
                                       @"O": @YES,
                                       @"C": @NO,
                                       @"F": @YES,
                                       @"B": @YES,
                                       @"T": @YES,
                                       @"L": @YES,
                                       @"D": @YES,
                                       @"I": @YES,
                                       @"E": @YES
                                       };
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveContextEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginError:) name:ME_LOGIN_ERROR_NOTIFICATION object:nil];
    }
    
    return self;
}

#pragma Backup values

- (void) loginError:(NSNotification*)event{
    [_references restoreFromOldVersion:event.object];
}

- (void) saveContextEnterBackground:(NSNotification*)event{
    [self saveContext];
}

#pragma Access
- (NSUInteger) sections{
    return self.tickers.count==0?0:1;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{
    return self.tickers.count;
}

- (METicker*) tickerForIndexPath:(NSIndexPath*)indexPath{
    @try {
        return [self.tickers objectAtIndex:indexPath.row];
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}

- (NSIndexPath*) tickerIndexPath:(NSString*)tickerString{
    for (int r=0; r<self.tickers.count; r++) {
        METicker *rticker = [self.tickers objectAtIndex:r];
        if ([tickerString isEqualToString:rticker.ID]) {
            return [NSIndexPath indexPathForRow:r inSection:0];
        }
    }
    return nil;
}

- (NSIndexPath*) indexPathForTicker:(METicker*)ticker{
    return tickerIndexPaths[ticker.ID];
}


#pragma Change

- (void) justAddTicker:(METicker*)ticker{
    
    lastIndexSize = 0;
    
    [self.tickers setObject:ticker forKey:ticker.ID];
    
    [tickerIndexPaths setObject:[self tickerIndexPath:ticker.ID] forKey:ticker.ID];
    [self subscribeTicker:ticker];
    
    if (!ticker.hint) {
        [self requestTickerInfo:ticker.ID];
    }
}

- (void) addTicker:(METicker*)ticker{
    @synchronized(self){                
        [self justAddTicker:ticker];
        [self saveContext];
    }
}

- (void) requestTickerInfo:(NSString*)tickerString{
    GIStompSearchTickers *_ticker = [GIStompSearchTickers select:tickerString];
    [dispatcher registerRequest:_ticker];
    _ticker.delegate = self;
}

- (void) subscribeTicker: (METicker*)ticker{
    @synchronized(self){
        if (!ticker.securitySubscription) {
            ticker.securitySubscription = [GIStompSecurity select:ticker.ID];
        }
        ticker.securitySubscription.delegate = self;
        [dispatcher registerMessage:ticker.securitySubscription];
    }
}

- (void) unsubscribeTicker: (METicker*)ticker{
    @synchronized(self){
        if (ticker.securitySubscription) {
            [dispatcher unregisterMessage:ticker.securitySubscription];
        }
    }
}

- (void) rebuildTickersIndex{
    @synchronized(self){
        [tickerIndexPaths removeAllObjects];
        lastIndexSize = 0;
        for (int r=0; r<self.tickers.count; r++) {
            METicker *rticker = [self.tickers objectAtIndex:r];
            [tickerIndexPaths setObject:[NSIndexPath indexPathForRow:r inSection:0] forKey:rticker.ID];
        }
    }
}

- (BOOL) removeTicker:(METicker*)ticker{
    
    if (!ticker) {
        return NO;
    }
    
    if (ticker.securitySubscription) {
        [dispatcher unregisterMessage:ticker.securitySubscription immediately:YES];
    }
    
    @synchronized(self){
        
        lastIndexSize = 0;
        
        GIOrderedDictionary *tickers = self.tickers;
        
        METicker *found_ticker = [tickers objectForKey:ticker.ID];
        if (!found_ticker) {
            return NO;
        }
        
        [tickers removeObjectForKey:ticker.ID];
        
        [self rebuildTickersIndex];
        
        [self saveContext];
        
        return YES;
    }
}

- (BOOL) swapTicker:(METicker*)tickerFirst withTicker:(METicker*)tickerSecond{
    
    @synchronized(self){
        
        lastIndexSize = 0;
        
        NSIndexPath *fromIndexPath = [tickerIndexPaths objectForKey:tickerFirst.ID];
        NSIndexPath *toIndexPath = [tickerIndexPaths objectForKey:tickerSecond.ID];
        
        if (!fromIndexPath || !toIndexPath)
            return NO;
        
        if ([fromIndexPath isEqual:toIndexPath])
            return NO;
        
        [self.tickers exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
        
        [self rebuildTickersIndex];
        
        [self saveContext];
        
        return YES;
    }
}

- (BOOL) moveTicker:(METicker*)tickerFirst withTicker:(METicker*)tickerSecond{
    @synchronized(self){
        
        lastIndexSize = 0;
        
        NSIndexPath *fromIndexPath = [tickerIndexPaths objectForKey:tickerFirst.ID];
        NSIndexPath *toIndexPath = [tickerIndexPaths objectForKey:tickerSecond.ID];
        
        if (!fromIndexPath || !toIndexPath)
            return NO;
        
        if ([fromIndexPath isEqual:toIndexPath])
            return NO;
        
        METicker *savedFirstTicker = [self.tickers objectAtIndex:fromIndexPath.row];
        [self.tickers removeObjectForKey:savedFirstTicker.ID];
        [self.tickers insertObject:savedFirstTicker forKey:savedFirstTicker.ID atIndex:toIndexPath.row];
        
        [self rebuildTickersIndex];
        
        [self saveContext];
        
        return YES;
    }
}

- (void) preset{
    GIStompSearchTickers *_preset = [GIStompSearchTickers selectPreset:@"default"];
    [dispatcher registerRequest:_preset];
    _preset.delegate = self;
}

- (void) subscribe{
    if ([self sections]==0) {
        [self references];
    }
    else{
        [self refreshListSubscription];
    }
}

- (void) unsubscribe{
    for (int r=0; r<self.tickers.count; r++)
        [self unsubscribeTicker:[self.tickers objectAtIndex:r]];
}


- (NSDictionary*) excludePropertyNames{
    return @{@"delegate": @NO};
}

- (void) saveContext{
    @synchronized(self){
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
            NSError *error;
            [self.context saveObject:self withContextName:__TICKERS_ID_LIST__CONTEXT__ error:&error];
            if (error) {
                NSLog(@"MEWatchListModel: %@,  %s:%i", error, __FILE__, __LINE__);
            }
        });
    }
}

- (NSDictionary*) _exlude_columns{
    return @{@"CAPTION": @YES, @"TICKER": @YES, @"LAST": @YES, @"CHANGE":@YES, @"BIDDEPTHT":@YES, @"OFFERDEPTHT":@YES, @"VALTODAY": @YES};
}


- (void) updateStompSecurity:(GIStompSecurity*)security{
    
    //NSLog(@" ticker = %@", security);
    
    BOOL doReloadAll = NO;
    
    if (!doReloadAll && lastIndexSize!=tickerIndexPaths.count)
        doReloadAll = YES;
    
    NSMutableArray *indexPaths = nil;
    NSMutableDictionary *attributes = nil;
    
    int i=0;
    for (GICommonStompFrame *c = [security.responseQueue pop]; c ; c=[security.responseQueue pop], i++) {
        GIStompBody *body = c.body;
        
        if (body.properties.type == GISTOMP_SNAPSHOT) {
            doReloadAll = YES;
        }
        
        if (!indexPaths)
            indexPaths = [NSMutableArray arrayWithCapacity:body.countOfRows];
        
        if (!attributes)
            attributes = [NSMutableDictionary dictionaryWithCapacity:body.countOfRows];
        
        NSString *tickerString =[body stringForIndex:0 andKey:@"TICKER"];
        
        NSIndexPath *indexPath =  [tickerIndexPaths objectForKey:tickerString];
        
        if (!indexPath)
            continue;
        
        METicker *ticker = [self tickerForIndexPath:indexPath];
        
        if (!ticker)
            continue;
        
        ticker.hasAccess = YES;
        
        if (body.properties.type == GISTOMP_SNAPSHOT){
            if (body.properties.comment != nil) {
                if ([body.properties.comment isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *comment = (NSDictionary *)body.properties.comment;                                        
                    NSDictionary *desctiption = [comment valueForKey:@"isdelayed"];
                    if (desctiption!=nil){
                        ticker.isDelayed = YES;
                        ticker.delayDescription = desctiption[GIC_current_lang()];
                    }
                }
            }
            else {
                ticker.isDelayed = NO;
                ticker.delayDescription = nil;
            }
        }

        GIStompValue *v=[body valueForIndex:0 andKey:@"LAST"];
        
        if (!IsEmpty(v)){
            ticker.prevPrice = ticker.lastPrice;
            ticker.lastPrice = v ;
            [attributes setValue:v forKey:@"LAST"];
        }
        
        v = [body valueForIndex:0 andKey:@"CHANGE"];
        if (!IsEmpty(v)){
            [attributes setValue:v forKey:@"CHANGE"];
            ticker.change = v;
        }
        
        v = [body valueForIndex:0 andKey:@"BID"];
        if (!IsEmpty(v)){
            ticker.prevBid = [ticker.bid copy];
            ticker.bid = v;
            [attributes setValue:v forKey:@"BID"];
        }
        
        v = [body valueForIndex:0 andKey:@"OFFER"];
        if (!IsEmpty(v)){
            ticker.prevOffer = [ticker.offer copy];
            ticker.offer = v;
            [attributes setValue:v forKey:@"OFFER"];
        }
        
        v = [body valueForIndex:0 andKey:@"BIDDEPTHT"];
        if (!IsEmpty(v)){
            ticker.bidDepth = v;
            [attributes setValue:v forKey:@"BIDDEPTHT"];
        }
        
        v = [body valueForIndex:0 andKey:@"OFFERDEPTHT"];
        if (!IsEmpty(v)){
            ticker.offerDepth = v;
            [attributes setValue:v forKey:@"OFFERDEPTHT"];
        }
        
        
        ticker.time = c.body.properties.timestamp;
        
        v=[body valueForIndex:0 andKey:@"VALTODAY"];
        
        if (!IsEmpty(v)) {
            ticker.valtoday = v;
            [attributes setValue:v forKey:@"VALTODAY"];
        }
        
        for (NSString *_columnName in c.body.columns) {
            
            if ([[self _exlude_columns] objectForKey:_columnName])
                continue;
            
            GIStompValue *_value = [c.body valueForIndex:0 andKey:_columnName];
            
            if (!IsEmpty(_value)){
                MEValue *_columnValue = [ticker.rows valueForKey:_columnName];
                
                if (![_value isEqual:_columnValue.value]) {
                    [attributes setValue:@YES forKey:_columnName];
                }
                
                if (!_columnValue) {
                    _columnValue = [[MEValue alloc] initFromValue:_value andField: security.structure.stream.fields[_columnName]];
                    if (!_columnValue || !_columnValue.field.caption) continue;
                    if (!_columnValue.field.isHidden && !_columnValue.field.isSystem) {
                        [ticker.rows setObject:_columnValue forKey:_columnName];
                    }
                }
                else{
                    _columnValue.value = _value;
                }
            }
            if ([_columnName isEqualToString:@"TRADINGSTATUS"]) {
                if (IsEmpty(_value))
                    ticker.tradingStatusBoolean = ticker.hasLevel2 = NO;
                else{
                    ticker.tradingStatusBoolean = [[tradingStatus2SessionState valueForKey:[_value string]] boolValue];
                    ticker.hasLevel2 = [[tradingStatus2Level2 valueForKey:[_value string]] boolValue];
                }
                [attributes setValue:@YES forKey:_columnName];
            }
        }
        
        [indexPaths addObject:indexPath];
    }
    
    if (self.delegate) {
        if (doReloadAll)
            [self.delegate didWatchListUpdate:self];
        else
            [self.delegate didWatchListUpdate:self atIndexPaths:[self filterRepeat:indexPaths] withChangedAttributes:attributes];
    }
    
    lastIndexSize = tickerIndexPaths.count;
}

- (void) updateStompSearchedTickers:(GIStompMessage*)frame{
    GIStompMessage *m = frame;
    
    for (int i=0; i<m.lastResponse.body.countOfRows; i++) {
        
        NSDictionary *row = [m.lastResponse.body rowByIndex:i];
        NSString *tickerString = [row valueForKey:@"ticker"];
        NSIndexPath *indexPath = tickerIndexPaths[tickerString];
        METicker *ticker;
        
        if (indexPath)
            ticker = [self tickerForIndexPath:indexPath];
        else
            ticker = [METicker tickerFromString:[row valueForKey:@"ticker"]];
        
        if ([[row valueForKey:@"primary_boardid"] isEqualToString:ticker.board.name])
            ticker.board.isPrimary = YES;
        else
            ticker.board.isPrimary = NO;
        
        ticker.caption = [row valueForKey:@"name"];
        ticker.hint = [row valueForKey:@"shortname"];
        ticker.isin = [row valueForKey:@"isin"];
        ticker.lastError = nil;
        
        MEIssuer *issuer = [[MEIssuer alloc] init];
        
        issuer.ID = [m.lastResponse.body numberForIndex:i andKey:@"emitent_id"];
        issuer.name = ticker.name;
        issuer.caption = [row valueForKey:@"emitent_title"];
        
        ticker.issuer = issuer;
        
        if (!indexPath)
            [self justAddTicker:ticker];
    }
    
    [self saveContext];
    
    if (self.delegate) {
        [self.delegate performSelector:@selector(didWatchListUpdate:) withObject:self];
    }
}

- (void) didSubscriptionRestore:(id)frame{
    if (self.delegate) {
        [self.delegate didWatchListUpdate:self];
    }
}

- (void) didFrameReceive:(id)frame{
   // NSLog(@"----\n%@\n----\n", frame);
    @try {
        if ([frame isKindOfClass:[GIStompSearchTickers class]]) {
            [self updateStompSearchedTickers:frame];
        }
        else if ([frame isKindOfClass:[GIStompSecurity class]]){
            [self updateStompSecurity:(GIStompSecurity*)frame];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

-(NSArray *)filterRepeat:(NSMutableArray*)inarray
{
    NSMutableArray * resultArray =[NSMutableArray array];
    [inarray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![resultArray containsObject: obj]) {
            [resultArray addObject: obj];
        }
    }];
    return resultArray;
}

- (void) refreshListSubscription{
    for (int s=0; s<[self sections]; s++) {
        //MEMarketSection *section = [self.markets objectAtIndex:s];
        //[section subscribe];
        for (int r=0; r<[self rowsForSection:s]; r++) {
            METicker *ticker = [self tickerForIndexPath:[NSIndexPath indexPathForRow:r inSection:s]];
            [self justAddTicker:ticker];
        }
    }
}

- (void) willReferenceUpdate:(MEReferences *)reference{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willWatchListUpdate:)]) {
        [self.delegate willWatchListUpdate:self];
    }
}

- (void) willFrameSend:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willWatchListUpdate:)]) {
        [self.delegate willWatchListUpdate:self];
    }
}

- (MEReferencesContext*) context{
    if (!_context) {
        _context= [MEReferencesContext sharedInstance];
        NSTimeInterval interval;
        [_context restoreObject:self withContextName:__TICKERS_ID_LIST__CONTEXT__ withLastTimeSave:&interval];
    }
    return _context;
}

- (void) didReferenceUpdate:(MEReferences *)reference{
    [self context];
    if ([self sections]==0) {
        [self preset];
    }
    else{
        [self refreshListSubscription];
    }
}

- (BOOL) fixErrorSubscriptionForTicker:(id)frame{
    GIStompSecurity *secutrity = frame;
    for (int s=0; s<[self sections]; s++) {
        for (int r=0; r<[self rowsForSection:s]; r++) {
            METicker *ticker = [self tickerForIndexPath:[NSIndexPath indexPathForRow:r inSection:s]];
            GIStompSecurity *tickerFrame = ticker.securitySubscription;
            if ([secutrity.request.headers[@"id"] isEqual:tickerFrame.request.headers[@"id"]]){
                ticker.tradingStatusBoolean = NO;
                NSError *error = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                     code: GI_STOMP_LOGIN_FAILED
                                                 userInfo: @{
                                                             NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Ticker cannot been subscribed", @"Stomp error received"),
                                                             NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"%@ cannot not be subscribed. The ticker could change listing.", @"Stomp error received description"), ticker.hint]
                                                             }];
                ticker.lastError = error;
                if (self.delegate && [self.delegate respondsToSelector:@selector(didWatchList:ticker:fault:)]) {
                    //[self.delegate performSelector:@selector(didWatchList:fault:) withObject:self withObject:error];
                    [self.delegate didWatchList:self ticker:ticker fault:error];
                }
                return YES;
            }
        }
    }
    return NO;
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    if ([self fixErrorSubscriptionForTicker:frame]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didWatchList:ticker:fault:)]) {
        [self.delegate didWatchList:self ticker:nil fault:error];
    }
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    GIStompSecurity *subs = frame;
    METicker *t = [self tickerForIndexPath:[self tickerIndexPath:subs.ticker]];
    t.securitySubscription = nil;
}

- (NSString*) stompToTickerStirng:(GIStompMessage*)m{
    NSString *_parameter_name = @"TICKER";
    @try {
        NSString *_selector = m.request.headers[@"selector"];
        NSArray  *_ks = [_selector componentsSeparatedByString:@"="];
        _parameter_name = _ks[0];
        
        NSMutableString *_tickerString = [NSMutableString stringWithString: _ks[1]];
        [_tickerString replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, _tickerString.length)];
        
        return _tickerString;
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }
    
}

- (void) didAccessDeny:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didWatchList:ticker:fault:)]) {
        METicker *ticker = [self tickerForIndexPath: [self tickerIndexPath:[self stompToTickerStirng:frame]]];
        ticker.hasAccess = NO;
        
        NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                              code: GI_STOMP_LOGIN_FAILED
                                          userInfo: @{
                                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Access denied", @"Stomp error received"),
                                                      NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"Access denied for the ticker %@", @"Stomp error access description."), ticker.hint]
                                                      }];
        
        ticker.lastError = nerror;
    }
}

- (void) didReference:(MEReferences *)reference fault:(NSError *)error withLastUpdateTime:(NSDate *)upDateTime{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didWatchList:ticker:fault:)]) {
        //[self.delegate performSelector:@selector(didWatchList:fault:) withObject:self withObject:error];
        [self.delegate didWatchList:self ticker:nil fault:error];
    }
}

@end
