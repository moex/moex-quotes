//
//  METurnoversModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 25/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoversModel.h"


static METurnoversModel *__shared_instance = nil;

@interface METurnoversModel()
@property (atomic,readonly) NSMutableDictionary *sectors;
@end

@implementation METurnoversModel


+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[METurnoversModel alloc] init];
    });
    
    return __shared_instance;
}

@synthesize sectors=_sectors;

- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    return self;
}


- (NSMutableDictionary*) sectors{
    if (!_sectors) {
        _sectors = [[NSMutableDictionary alloc] initWithCapacity:5];
    }
    
    return _sectors;
}

- (METurnoversSector*) sectorWithDelegate:(id<METurnoversModelProtocol>)delegate withEngineName:(NSString *)engineName withMarketName:(NSString *)marketName{
    
    NSString *key = [NSString stringWithFormat:@"%@-%@", engineName==nil?@"-":engineName, marketName==nil?@"-":marketName];
    METurnoversSector *sector = [self.sectors objectForKey:key];
    
    if (!sector) {
        sector = [[METurnoversSector alloc] initWithEngine:engineName withMarket:marketName];
        [self.sectors setObject:sector forKey:key];
    }
    sector.delegate = delegate;
    return sector;
}

@end
