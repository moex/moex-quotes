//
//  METicker.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEinfoCXConnector/GIStomp.h>
#import "MEBoard.h"
#import "MEIssuer.h"
#import <MEContextArchiver/MEObject.h>


/**
 *  This is additional valu which ticker can have.
 */
@interface MEValue : NSObject
/**
 *  Filed.
 */
@property(readonly) GIField *field;

/**
 *  Field value.
 */
@property id       value;

/**
 *  Create ticker field.
 *
 *  @param value value.
 *  @param field field
 *
 *  @return MEValue instance.
 */
- (id) initFromValue:(id)value andField:(GIField*)field;
@end



/**
 *  Ticker desctiption.
 */
@interface METicker : MEObject

/**
 *  Initiate ticker from composed ticker string.
 *
 *  @return METicker instance.
 */
+ (id) tickerFromString:(NSString*)tickerString;
+ (MEBoard*) boardFromTickerString:(NSString*)tickerString;
+ (MEMarket*) marketFromTickerString:(NSString*)tickerString;

/**
 *  Full ticker name such as <MARKETPLACE>.<BOARDID>.<SECID>
 */
@property(nonatomic,strong, readonly) NSString *ID;

/**
 *  Localized ticker caption.
 */
@property(nonatomic,strong) NSString *caption;

/**
 *  Localized ticker hint.
 */
@property(nonatomic,strong) NSString *hint;

/**
 *  System security ID.
 */
@property(nonatomic,strong, readonly) NSString *name;

/**
 *  Internetinal security identify number.
 */
@property(nonatomic,strong) NSString *isin;

/**
 *  Market place id.
 */
@property(nonatomic,strong, readonly) NSString *marketPlaceID;


/**
 *  Trading status mnemonic.
 */
@property(nonatomic,strong, readonly) NSString *tradingStatus;

/**
 *  Trades YES/NO.
 */
@property(nonatomic) BOOL tradingStatusBoolean;

/**
 *  Trading status desctiprion.
 */
@property(nonatomic,strong, readonly) NSString *tradingStatusDescription;

/**
 *  The ticker has level2 feature (orders book).
 */
@property(nonatomic) BOOL hasLevel2;

/**
 *  Has permitions to subscribe the ticker?
 */
@property(nonatomic) BOOL hasAccess;

@property(nonatomic) BOOL isDelayed;

@property(nonatomic) NSString *delayDescription;

/**
 *  Trade board.
 */
@property(nonatomic, readonly) MEBoard *board;

/**
 *  Trade market.
 */
@property(nonatomic, readonly) MEMarket *market;

/**
 *  Ticker issuer (emitter).
 */
@property(nonatomic, strong) MEIssuer *issuer;

/**
 *  Previous ticker price.
 */
@property(nonatomic, strong) GIStompValue *prevPrice;

/**
 *  Last price.
 */
@property(nonatomic, strong) GIStompValue *lastPrice;

/**
 *  Change last price from last trade date price.
 */
@property(nonatomic, strong) GIStompValue *change;

/**
 *  Depth of bids.
 */
@property(nonatomic, strong) GIStompValue *bid;
@property(nonatomic, strong) GIStompValue *prevBid;
@property(nonatomic, strong) GIStompValue *bidDepth;

/**
 *  Depth of asks.
 */
@property(nonatomic, strong) GIStompValue *offer;
@property(nonatomic, strong) GIStompValue *prevOffer;
@property(nonatomic, strong) GIStompValue *offerDepth;

/**
 *  Total intraday trades volume for the ticker.
 */
@property(nonatomic, strong) GIStompValue *valtoday;

/**
 *  Last time data update.
 */
@property(nonatomic, strong) NSDate *time;

/**
 *  Rest of ticker values which extends standard prices and market metrics. 
 *  @see MEValue.
 */
@property(nonatomic,strong) GIOrderedDictionary *rows;

@property(nonatomic,strong) GIStompSecurity *securitySubscription;
@property(nonatomic,strong) GIStompOrderbook *orderbookSubscription;

@property(nonatomic,strong) NSError *lastError;

@end
