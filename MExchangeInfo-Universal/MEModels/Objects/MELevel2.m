//
//  MELevel2.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 31.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MELevel2.h"

@interface MELevel2()
@end

@implementation MELevel2

- (NSString*) description{
    if (self.type==MELEVEL2_BID)
        return [NSString stringWithFormat:@"B %f %8@ %@", self.ratio, [self.qty string], [self.price string]];
    else
        return [NSString stringWithFormat:@"     %@ %8@ %f A", [self.price string], [self.qty string], self.ratio];
}

@end

@interface MELevel2Book()

- (void) addBid:(MELevel2*) bid;
- (void) addOffer:(MELevel2*) offer;

- (void) reset;

@end

@implementation MELevel2Book
{
    NSMutableArray *bidRows_, *offerRows_;
}

- (id) copyWithZone:(NSZone *)zone{
    MELevel2Book *_new_obj = [[[self class] allocWithZone:zone] init];

    if (_new_obj) {
        _new_obj->_bidAskField = self->_bidAskField;
        _new_obj->bidRows_ = [self->bidRows_ copyWithZone:zone];
        _new_obj->offerRows_ = [self->offerRows_ copyWithZone:zone];
        _new_obj->_structure = self->_structure;
    }
    
    return _new_obj;
}

- (id) init{
    self = [super init];
    
    if (self) {
        bidRows_ = [[NSMutableArray alloc] initWithCapacity:0];
        offerRows_ = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    return self;
}

- (NSArray*) bidRows{
        return bidRows_;
}

- (NSArray*) offerRows{
        return offerRows_;
}

- (void) addBid:(MELevel2*) bid{
        [bidRows_ addObject:bid];
}

- (void) addOffer:(MELevel2*) offer{
        [offerRows_ addObject:offer];
}

- (void) reset{
    @synchronized (self){
        [offerRows_ removeAllObjects];
        [bidRows_ removeAllObjects];
    }
}


- (NSString*) description{
    @synchronized (self){
        NSMutableString *_ret = [[NSMutableString alloc] init];
        for (MELevel2 *_row in self.bidRows) {
            [_ret appendFormat:@"%@\n",_row];
        }
        for (MELevel2 *_row in self.offerRows) {
            [_ret appendFormat:@"\t\t\t%@\n",_row];
        }
        return _ret;
    }
}

@end
