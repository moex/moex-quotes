//
//  MECandlesModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 09.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "METicker.h"
#import <MEinfoCXConnector/GIStompSubscriptions.h>
#import <MESChartView/MESChartData.h>
#import <MESChartView/MESChartSeries.h>

@class MECandlesModel;

@protocol MECandlesModelProtocol <NSObject> 
- (void) didCandlesModel:(MECandlesModel*)candles accessDeny:(NSError*)error;
- (void) didCandlesModel:(MECandlesModel*)candles fault:(NSError*)error;
- (void) willCandlesModelUpdateData:(MECandlesModel*)candles;
- (void) didCandlesModelUpdateData:(MECandlesModel*)candles;
- (void) didCandlesModelUpdateIntervals:(MECandlesModel*)candles;
@end

@interface MECandlesModel : NSObject

+ (NSDate*) dateFromString:(NSString*)dateString;

+ (id) sharedInstance;

@property (nonatomic,assign) NSInteger size;
@property (nonatomic,readonly) NSArray *intervals;
@property (nonatomic,strong) NSString *currentInterval;
@property (nonatomic,strong) id<MECandlesModelProtocol> delegate;
@property (atomic,weak) METicker *ticker;
@property (nonatomic,assign) MESChartSeriesType seriesType;


- (NSUInteger) series;
- (NSUInteger) dataCountAtSeries:(NSUInteger)seriesIndex;
- (MESChartDataRange*) dataRangeAtSeriesIndex:(NSInteger)seriesIndex;
- (MESChartData*)  dataAtIndex:(NSInteger)index forSeriesIndex:(NSInteger)seriesIndex;

- (void) subscribeIntervals;
- (void) subscribe;
- (void) unsubscribe;
@end
