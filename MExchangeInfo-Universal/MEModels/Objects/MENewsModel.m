//
//  MENewsModel.m
//  MExchange
//
//  Created by denn on 7/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//
// VERY HELPFULL: https://www.cocoanetics.com/2012/07/multi-context-coredata/
// TODO: try to migrate to MagicalRecord: https://github.com/magicalpanda/MagicalRecord/tree/release/3.0
//
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import <ObjectiveSugar/ObjectiveSugar.h>
#import "MENewsModel.h"
#import "NSDate+MEComparing.h"
#import "MENewsListStomp.h"
#import "MENewsContentStomp.h"
#import "MENewsSection.h"
#import "MENewsItem.h"
#import "NSDate+MEComparing.h"
#import "MESettings.h"

static MENewsModel *__shared_news_model = nil;

@interface MENewsModel() <GIStompDispatcherProtocol>
@property (nonatomic, strong) MEDispatcher *dispatcher;
@property (nonatomic, weak)   MESettings   *settings;
@property (nonatomic, strong) dispatch_queue_t newsBackgroundQueue;
@property (nonatomic, strong) dispatch_queue_t newsMainQueue;
@property (nonatomic, strong) NSNumber *curentNewsID;
@property (nonatomic, readonly) NSManagedObjectContext *backgroundContext;
@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, readonly) CoreDataManager *coreDataManager;
@property (nonatomic, readonly) NSDate *oldestDate;
@end

@implementation MENewsModel
{
    NSMutableDictionary *newsIndexPaths;
    BOOL                isSubscribed;
    MENewsListStomp     *newsList;
    NSInteger           sectionCount;
}

@synthesize coreDataManager=_coreDataManager, mainContext=_mainContext, backgroundContext=_backgroundContext;

- (NSDate*) oldestDate{
    return  [NSDate dateWithTimeIntervalSinceNow:-(3600.*24.*[self.settings.newsKeepDays floatValue])];
}

- (CoreDataManager*) coreDataManager{
    if (!_coreDataManager) {
        _coreDataManager = [[CoreDataManager alloc] init];
        _coreDataManager.modelName = @"MENewsDB";
        _coreDataManager.databaseName = [NSString stringWithFormat:@"%@/MosQuotes3-%@.sqlite",[[NSLocale preferredLanguages] objectAtIndex:0], _coreDataManager.modelName];
    }
    return _coreDataManager;
}

- (NSManagedObjectContext *) backgroundContext{
    if (!_backgroundContext) {
        _backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _backgroundContext.persistentStoreCoordinator = [self.coreDataManager persistentStoreCoordinator];  
        [_backgroundContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }
    return _backgroundContext;
}

- (NSManagedObjectContext*) mainContext{
    if (!_mainContext) {
        _mainContext=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainContext.parentContext = self.backgroundContext;
    }
    return _mainContext;
}

+ (id) sharedInstance{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_news_model = [[MENewsModel alloc] init];
    });
    
    return __shared_news_model;
}

- (id) init{
    
    if (__shared_news_model) {
        self = __shared_news_model;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        
        self.settings = [MESettings sharedInstance];
        
        newsIndexPaths = [NSMutableDictionary dictionaryWithCapacity:0];
        _dispatcher = [MEDispatcher sharedInstance];
        
        sectionCount = -1;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveContextEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
    }
    
    return self;
}


- (dispatch_queue_t) newsBackgroundQueue{
    if (_newsBackgroundQueue) {
        return _newsBackgroundQueue;
    }
    
    _newsBackgroundQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    dispatch_queue_set_specific(_newsBackgroundQueue, (__bridge void *)self, (__bridge void *)(_newsBackgroundQueue), NULL);
    
    return _newsBackgroundQueue;
}

- (dispatch_queue_t) newsMainQueue{
    if (_newsMainQueue) {
        return _newsMainQueue;
    }
    
    _newsMainQueue = dispatch_get_main_queue(); //dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    dispatch_queue_set_specific(_newsMainQueue, (__bridge void *)self, (__bridge void *)(_newsMainQueue), NULL);
    
    return _newsMainQueue;
}

- (void) saveContextEnterBackground:(NSNotification*)event{
    [self saveContext];
}

- (void) saveContext{
    [self.coreDataManager saveContext];
}

- (NSDate*) publicTime: (id) input{
    static NSDateFormatter *dateFormatter = nil; if(!dateFormatter) dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [dateFormatter dateFromString: input];
}


- (void) downloadContent:(NSNumber *)newsId delegateUpdates:(BOOL)delegateUpdates{
    //
    // to keep server overload
    //
        
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        MENewsContentStomp *news_content = [MENewsContentStomp selectByID:newsId];
        news_content.delegate = self;
        news_content.userData = [NSNumber numberWithBool:delegateUpdates];
        
        [self.dispatcher registerRequest:news_content];            
    });
}

- (void) requestContentByNewsId:(NSNumber*)newsId{
    @try {        
        if (newsId == nil) 
            return;        
        
        self.curentNewsID = newsId;            
        dispatch_async(self.newsMainQueue, ^{
            MENewsItem *itemEntity =  [MENewsItem where:[NSPredicate predicateWithFormat:@"newsId = %@", newsId] inContext:self.mainContext].first;                        
            if (itemEntity && itemEntity.isContentDownloaded.boolValue && self.itemDelegate && [self.itemDelegate respondsToSelector:@selector(didNewsItemUpdate:)]) {
                [self.itemDelegate didNewsItemUpdate:itemEntity];
                return ;
            }
            
            [self downloadContent:newsId delegateUpdates:YES];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__,__LINE__);
    }
}

- (void) refreshNewsList:(GIStompMessage *)m{
    
    @try {        
        NSDate *older_date = self.oldestDate;
        
        dispatch_async(self.newsBackgroundQueue, ^{
            
            [self.backgroundContext performBlockAndWait:^{
                                
                MENewsIncrementals *inc = [MENewsIncrementals findOrCreate:@{@"tableId":@"MENewsItem"} inContext:self.backgroundContext];
                                                    
                for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
                    
                    //
                    // Ordering as it received
                    //
                    int64_t order = inc.orderLast.longLongValue+(int64_t)1+(int64_t)c.body.countOfRows;
                    inc.orderLast = [NSNumber numberWithLongLong:(order)];
                    [inc save];

                    for (int i=0; i<c.body.countOfRows; i++) {
                        
                        
                        NSNumber *newsid = [c.body numberForIndex:i andKey:@"ID"];
                        NSString *public_at = [c.body stringForIndex:i andKey:@"PUBLIC_TIME"];
                        NSDate   *public_at_date = [self publicTime:public_at];
                        
                        if (([[public_at_date dateOnly] compare:[older_date dateOnly]] == NSOrderedAscending)) {
                            continue;
                        }
                        
                        MENewsSection *sectionEntity = [[MENewsSection where:@{@"date": [[self publicTime: public_at] dateOnly]} inContext:self.backgroundContext] firstObject]; 
                        
                        if (!sectionEntity){
                            sectionEntity = [MENewsSection createInContext:self.backgroundContext];
                            sectionEntity.date = [public_at_date dateOnly];                    
                        }
                        
                        MENewsItem *itemEntity =  [[MENewsItem where: @{@"newsId": newsid} inContext:self.backgroundContext] firstObject];
                        
                        if (!itemEntity) {
                            itemEntity = [MENewsItem createInContext:self.backgroundContext];
                            itemEntity.isStared = @NO;
                            itemEntity.isRead = @NO;
                            itemEntity.isContentDownloaded = @NO;
                            itemEntity.order = [NSNumber numberWithLongLong:(order)];
                        }
                        
                        itemEntity.newsId = newsid;
                        itemEntity.caption = [c.body stringForIndex:i andKey:@"CAPTION"];
                        itemEntity.shortcut = [c.body stringForIndex:i andKey:@"SHORTCUT"];
                        itemEntity.publicTime = public_at_date;
                        itemEntity.lastUpdateTime = [NSDate date];
                        itemEntity.newsSection = sectionEntity;
                                                
                        [itemEntity save];                                         
                        [sectionEntity save];                                            
                                                                        
                        order--;
                    }
                }                
                                
                [[MENewsItem where:[NSPredicate predicateWithFormat:@"publicTime < %@", [older_date dateOnly]] 
                         inContext:self.backgroundContext] 
                 each:^(MENewsItem *item) {
                     [item delete];
                 }];

                [[MENewsSection where:[NSPredicate predicateWithFormat:@"date < %@", [older_date dateOnly]] 
                         inContext:self.backgroundContext] 
                 each:^(MENewsSection *item) {
                     [item delete];
                 }];

            }];
            
            //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), self.newsMainQueue, ^{
                [self.delegate didNewsUpdate: self];                
            //});
        });
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) responseNewsContent:(MENewsContentStomp*)m{
    
    @try {
        NSNumber *newsid = [m.lastResponse.body numberForIndex:0 andKey:@"ID"];
        NSString *content = [m.lastResponse.body stringForIndex:0 andKey:@"CONTENT"];

        if (content==nil) {
            if (self.itemDelegate && [self.itemDelegate respondsToSelector:@selector(didNewsItemError:)]) {
                NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                      code: GI_STOMP_ERROR
                                                  userInfo: @{
                                                              NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"News content did not received", @"Stomp error received"),
                                                              NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"News content did not received from server. Try to load later.", @"Stomp error access description.")]
                                                              }];

                [self.itemDelegate didNewsItemError:nerror];
            }
            return;
        }
                
        dispatch_async(self.newsMainQueue, ^{            
            MENewsItem *itemEntity = [MENewsItem where:@{@"newsId": newsid} inContext:self.mainContext].first;

            [self.mainContext performBlockAndWait:^{                                
                itemEntity.content = content;
                itemEntity.isContentDownloaded = [NSNumber numberWithBool:YES];                
                [itemEntity save];
                [self.coreDataManager saveContext];
            }];
        
            if (self.itemDelegate && [self.itemDelegate respondsToSelector:@selector(didNewsItemUpdate:)]) {
                if (self.curentNewsID && [self.curentNewsID isEqualToNumber:itemEntity.newsId]) 
                    [self.itemDelegate didNewsItemUpdate:itemEntity];                                
            }
        });

    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

- (NSUInteger) sections{
    if (sectionCount<=0) {    
        sectionCount = [self newsSections].count;
    }
    if (sectionCount<0) {
        return 0;
    }
    return sectionCount;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{
    MENewsSection *s = [[self newsSections] objectAtIndex:section];
    return s.newsItems.count;
}

- (NSArray*) newsItemsWithDate:(NSDate*)date{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"publicTime>=%@ and publicTime<=%@", date, [date dateByAddingTimeInterval:3600*24]];
    return [MENewsItem where:predicate 
                             inContext:self.mainContext order:@{@"order": @"DESC"}];
}

- (MENewsItem*) newsForIndexPath:(NSIndexPath *)indexPath{    
    MENewsSection *s = [[self newsSections] objectAtIndex:indexPath.section];
    return [[self newsItemsWithDate:s.date] objectAtIndex:indexPath.row];
    
}

- (MENewsSection*) sectionForSection:(NSUInteger)section{
    return [[self newsSections] objectAtIndex:section];
}

- (NSIndexPath *)indexPathForNews:(MENewsItem*)news{
    @try {
        MENewsSection *newsSection = [MENewsSection where:[NSPredicate predicateWithFormat:@"date = %@", [news.publicTime dateOnly]] 
                                                inContext:self.mainContext
                                                    order:@{@"date": @"DESC"}
                                      ].firstObject;

        int section=0;
        NSDate *date;
        for (MENewsSection *item in [self newsSections]) {
            if ([item.date isEqual:newsSection.date]){ 
                date=item.date;
                break;
            }
            section++;
        }


        int row=0;
        for (MENewsItem *item in [self newsItemsWithDate:date]) {
            if ([item.newsId isEqual:news.newsId]) break;                            
            row++;
        }
        
        return [NSIndexPath indexPathForRow:row inSection:section];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }
    return nil;
};

- (NSArray*) newsSections{
    return [MENewsSection where:[NSPredicate predicateWithFormat:@"date >= %@", [self.oldestDate dateOnly]] inContext:self.mainContext order:@{@"date": @"DESC"}];
    //return [MENewsSection allInContext:self.mainContext order:@{@"date": @"DESC"}];
}

- (NSArray*) items{
    return [MENewsItem where:[NSPredicate predicateWithFormat:@"publicTime >= %@", [self.oldestDate dateOnly]] inContext:self.mainContext order:@{@"order": @"DESC"}];
}

- (void) didAccessDeny:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didNewsError:)]) {
        NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                              code: GI_STOMP_LOGIN_FAILED
                                          userInfo: @{
                                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Access denied", @"Stomp error received"),
                                                      NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"Access denied to browse news", @"Stomp error access description.")]
                                                      }];
        
        dispatch_async(self.newsMainQueue, ^{
            [self.delegate didNewsError:nerror];
        });
    }
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    if (self.dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"%@ %s:%i", frame, __FILE__, __LINE__);
    }
    
    [self.dispatcher unregisterMessage:newsList immediately:YES];
    
    isSubscribed = NO;
    newsList = nil;
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    if (self.dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"%@ %s:%i", frame, __FILE__, __LINE__);
    }
    
    [self.dispatcher unregisterMessage:newsList immediately:YES];
    isSubscribed = NO;
    newsList = nil;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didNewsError:)]) {
        dispatch_async(self.newsMainQueue, ^{
            [self.delegate didNewsError:error];
        });
    }
}

- (void) didSubscriptionRestore:(id)frame{    
    if ([frame isKindOfClass:[MENewsListStomp class]]) {
                
        if (self.delegate && [self.delegate respondsToSelector:@selector(didNewsUpdate:)]) {
            dispatch_async(self.newsMainQueue, ^{
                [self.delegate didNewsUpdate: self];
            });
        }
    }
    else if ([frame isKindOfClass:[MENewsContentStomp class]]){
        [self responseNewsContent:frame];
    }
}

- (void) didFrameReceive:(id)frame{
    if ([frame isKindOfClass:[MENewsListStomp class]]) {
        [self refreshNewsList:frame];
    }
    else if ([frame isKindOfClass:[MENewsContentStomp class]]){
        [self responseNewsContent:frame];
    }
}

- (void) willFrameSend:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willNewsUpdate:)] && frame == newsList) {
        dispatch_async(self.newsMainQueue, ^{
            [self.delegate willNewsUpdate:self];
        });
    }
}

- (void) subscribe{
    NSInteger sections = [self sections];
    
    if (sections >0  && self.delegate && [self.delegate respondsToSelector:@selector(didNewsRestore:)]) {
        dispatch_async(self.newsMainQueue, ^{                
            [self.delegate didNewsRestore:self];
        });
    }
        
    if (self.dispatcher && !isSubscribed) {
        newsList = [MENewsListStomp select];
        newsList.delegate = self;
        [self.dispatcher registerMessage:newsList];
        isSubscribed = YES;            
    } 
}

- (void) unsubscribe{
    if (self.dispatcher && newsList && isSubscribed) {
        [self.dispatcher unregisterMessage:newsList];
        isSubscribed = NO;
    }
    [self saveContextEnterBackground:nil];
}

- (void) truncate{            
    if (self.dispatcher) {
        [self.dispatcher unregisterMessage:newsList immediately:YES];
    }
    
    sectionCount = -1;
    isSubscribed = NO;

    dispatch_async(self.newsBackgroundQueue, ^{
        [self.backgroundContext performBlockAndWait:^{        
            [MENewsItem deleteAllInContext:self.backgroundContext];            
            [MENewsSection deleteAllInContext:self.backgroundContext];
            [self.backgroundContext save:nil];                
        }];
        
        [self.coreDataManager saveContext];
    });            
}

@end
