//
//  METurnoversModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 25/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "METurnoversSector.h"


@interface METurnoversModel : NSObject

+ (id) sharedInstance;

- (METurnoversSector*) sectorWithDelegate:(id<METurnoversModelProtocol>)delegate withEngineName:(NSString*)engineName withMarketName:(NSString*)marketName;

@end
