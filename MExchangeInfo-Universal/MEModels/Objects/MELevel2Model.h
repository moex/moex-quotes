//
//  MELevel2Model.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 20/03/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEObject.h"
#import "MELevel2.h"
#import "METicker.h"

#define ME_LEVEL2_ROWSLIMIT 10

@class MELevel2Model;

@protocol MELevel2ModelProtocol <NSObject>
- (void) didLevel2ModelUpdate:(MELevel2Model *)level2;
@optional
- (void) didLevel2Model:(MELevel2Model*)level2 fault:(NSError*)error;
@end


@interface MELevel2Model : MEObject
/**
 *  Shared model.
 *
 *  @return MEWatchListModel.
 */
+ (id) sharedInstance;

@property (atomic,weak) METicker *ticker;
@property (nonatomic,strong) id<MELevel2ModelProtocol> delegate;
@property (nonatomic,strong) NSString *lastError;

/**
 *  Sections
 *
 *  @return number of sections.
 */
- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (NSUInteger) allRows;
- (MELevel2*) level2ForIndexPath:(NSIndexPath*)indexPath;
- (MELevel2Type) level2TypeForSection:(NSUInteger)section;
- (MELevel2Book*) level2Book;
- (void) subscribe;
- (void) unsubscribe;

- (void) beginTransaction;
- (void) commitTransaction;

@end
