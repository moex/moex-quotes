//
//  METurnoversModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEObject.h"
#import "METurnoverItem.h"
#import "METurnoversStomp.h"
#import <MEinfoCXConnector/GIStompDispatcher.h>

@class METurnoversSector;
@class METurnoversInfo;

@protocol METurnoversModelProtocol <NSObject>
- (void) willTurnoversUpdate:(METurnoversInfo *)turnoversInfo;
- (void) didTurnoversUpdate:(METurnoversInfo *)turnoversInfo;
- (void) didTurnoversRestore:(METurnoversInfo *)turnoversInfo;
@optional
- (void) didTurnovers:(METurnoversInfo*)turnoversInfo fault:(NSError*)error;
@end

@interface METurnoversInfo : MEObject
@property(nonatomic,strong) METurnoverItem      *total;
@property(nonatomic,readonly) METurnoversSector  *turnovers;
@end

@interface METurnoversSector : MEObject<GIStompDispatcherProtocol>

@property(nonatomic,readonly) NSString *engineName;
@property(nonatomic,readonly) NSString *marketName;
@property(nonatomic,strong) id<METurnoversModelProtocol> delegate;

- (id) initWithEngine:(NSString*)engine withMarket:(NSString*)market;

- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (METurnoverItem*) turnoverForIndexPath:(NSIndexPath*)indexPath;


- (void) subscribe;
- (void) unsubscribe;

@end