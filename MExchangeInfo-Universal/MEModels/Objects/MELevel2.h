//
//  MELevel2.h
//  MExchange
//
//  Created by denn on 5/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIStompBody.h"

typedef enum{
    MELEVEL2_BID,
    MELEVEL2_OFFER
} MELevel2Type;


@interface MELevel2 : NSObject
@property(nonatomic) MELevel2Type type;
@property(nonatomic) NSString *state;
@property(nonatomic) GIStompValue *qty, *price;
@property(nonatomic) float ratio;
@end


@interface MELevel2Book : NSObject <NSCopying>
@property(nonatomic, readonly) NSArray *bidRows, *offerRows;
@property(nonatomic, strong) GIField *bidAskField;
@property(nonatomic, strong) GIStompStructure *structure;

- (void) addBid:(MELevel2*) bid;
- (void) addOffer:(MELevel2*) offer;

- (void) reset;

@end