//
//  METicker.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METicker.h"
#import "MEDispatcher.h"
#import "MEReferences.h"

@interface METicker () <NSCopying>
@property(nonatomic,strong) NSString *ID;
@property(nonatomic,strong) NSString *marketPlaceID;
@property(nonatomic,strong) NSString *name;
@property(strong,strong) NSString *tradingStatus;
@property(nonatomic,strong) NSString *tradingStatusDescription;
@property(nonatomic,strong) MEBoard *board;
@property(nonatomic,strong) MEMarket *market;
@end

@implementation MEValue

- (id) initFromValue:(id)value andField:(GIField*)field{
    self = [super init];
    
    if (self) {
        _field = field;
        _value = value;
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@", _field, _value];
}

@end


@implementation METicker

- (id) init{
    self = [super init];
        
    if (self) {
        _hasAccess = YES;
        _rows = [[GIOrderedDictionary alloc] init];
        _securitySubscription = nil;
        _orderbookSubscription = nil;
    }
    
    return self;
}

- (id) copyWithZone:(NSZone *)zone{
    METicker *_ret = [[METicker allocWithZone:zone] init];
    for (NSString *propName in [super propertyList]) {
        [_ret setValue:[self valueForKey:propName] forKey:propName];
    }
    _ret->_hasAccess = self->_hasAccess;
    _ret->_hasLevel2 = self->_hasLevel2;
    _ret->_tradingStatusBoolean = self->_tradingStatusBoolean;
    return _ret;
}

+ (id) tickerFromString:(NSString*)tickerString{
    /* 
     * Full ticker name such as <MARKETPLACE>.<BOARDID>.<SECID>
     */
    METicker *ticker = [[METicker alloc] init];

    NSArray  *_m      = [tickerString componentsSeparatedByString:@"."];
    
    ticker.ID = tickerString;
    ticker.name =  _m[2];
    ticker.marketPlaceID = _m[0];
    
    MEReferences *enginesReference = [MEReferences sharedInstance];
    
    ticker.board = [enginesReference.boards findByName:_m[1]];
    ticker.market = [enginesReference.markets objectForKey:ticker.board.marketID];
    
    return ticker;
}

+ (MEBoard*) boardFromTickerString:(NSString*)tickerString{
    NSArray  *_m      = [tickerString componentsSeparatedByString:@"."];
    MEReferences *enginesReference = [MEReferences sharedInstance];
    return [enginesReference.boards findByName:_m[1]];
}

+ (MEMarket*) marketFromTickerString:(NSString*)tickerString{
    MEReferences *enginesReference = [MEReferences sharedInstance];
    MEBoard      *board = [METicker boardFromTickerString:tickerString];
    return [enginesReference.markets objectForKey:board.marketID];   
}

- (MEBoard*) board{
    if (_board==nil) {
        _board = [METicker boardFromTickerString:self.ID];
    }
    return _board;
}

- (MEMarket*) market{
    if (_market==nil) {
        _market = [METicker marketFromTickerString:self.ID];
    }
    return _market;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@ %@ %@ [issuer: %@]", _ID, _name, _caption, _board, _issuer];
}

- (NSDictionary*) excludePropertyNames{
    return @{@"securitySubscription": @NO, @"board": @NO, @"market": @NO, @"orderbookSubscription":@NO};
}

- (void) __setDict:(NSMutableDictionary*)dictionary value:(id)value forKey:(NSString*)key{
    if (value){
        if ([value isKindOfClass:[GIStompValue class]])
            [dictionary setValue:[value number] forKey:key];
        else
            [dictionary setValue:value forKey:key];
    }
}

- (NSArray*) __propertiesToSave{
    return @[@"ID",@"caption",@"name",@"hint",@"marketPlaceID",@"lastPrice",@"prevPrice",@"change",@"bid",@"offer",@"prevBid",@"prevOffer",@"offerDepth",@"bidDepth",@"valtoday"];
};

- (NSDictionary*) toDictionary{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [self __setDict:dict value:[NSNumber numberWithBool:_hasAccess] forKey:@"hasAccess"];
    [self __setDict:dict value:_board.ID forKey:@"boardID"];
    [self __setDict:dict value:_market.ID forKey:@"marketID"];
    [self __setDict:dict value:[NSNumber numberWithFloat: [_time timeIntervalSinceReferenceDate]] forKey:@"time"];
    if (_issuer) {
        [dict setValue:[_issuer toDictionary] forKey:@"issuer"];
    }

    for (NSString *property_name in [self __propertiesToSave]) {
        [self __setDict:dict value:[self valueForKey:property_name] forKey:property_name];
    }
    
    return dict;
}


- (void) fromDictionary:(NSDictionary *)dictionary{
    
    MEReferences *enginesReference = [MEReferences sharedInstance];
    
    _hasAccess = [[dictionary valueForKey:@"hasAccess"] boolValue];
    NSDictionary *issuerDict = [dictionary valueForKey:@"issuer"];
    if (issuerDict) {
        _issuer = [[MEIssuer alloc] init];
        [_issuer fromDictionary:issuerDict];
    }
    
    self.board = [enginesReference.boards objectForKey:[dictionary valueForKey:@"boardID"]];
    self.market = [enginesReference.markets objectForKey:[dictionary valueForKey:@"marketID"]];
    _time = [NSDate dateWithTimeIntervalSinceReferenceDate:[[[GIStompValue alloc] initFromId:[dictionary valueForKey:@"time"]] floatValue]];
    
    for (NSString *property_name in [self __propertiesToSave]) {
        id v = [dictionary valueForKey:property_name];
        
        if (!v)
            continue;
                    
        if ([v isKindOfClass:[NSNumber class]])
            [self setValue: [[GIStompValue alloc] initFromId:v] forKey:property_name];
        else
            [self setValue: v forKey:property_name];
    }    
}

@end
