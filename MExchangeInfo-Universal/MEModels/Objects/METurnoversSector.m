//
//  METurnoversModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoversSector.h"
#import "MEDispatcher.h"
#import "METurnoversStomp.h"
#import "METurnoverItem.h"
#import "MEReferencesContext.h"


#define __TURNOVERS_ID__CONTEXT__ @"__TURNOVERS_ID__CONTEXT__"

@interface METurnoversInfo()
@property(nonatomic,strong) GIOrderedDictionary *rows;
@property(nonatomic,strong) METurnoversSector  *turnovers;
@end

@implementation METurnoversInfo

- (id) init{
    self = [super init];
    
    if (self) {
        _total = nil;
        _rows = [[GIOrderedDictionary alloc] init];
    }
    
    return self;
}

- (NSDictionary*) excludePropertyNames{
    return @{@"turnovers": @NO};
}

- (NSString*) description{
    NSMutableString *desc = [[NSMutableString alloc] init];
    
    if (_total) {
        [desc appendString:@"\n"];
        [desc appendString:[_total description]];
    }
    
    [desc appendString:@"\n"];
    
    for (id i in _rows) {
        [desc appendString: [NSString stringWithFormat:@"\t%@", [[_rows objectForKey:i] description]]];
        [desc appendString:@"\n"];
    }
    return desc;
}

@end

@interface METurnoversSector()
@property(nonatomic,strong) METurnoversInfo *turnoversInfo;
@end

@implementation METurnoversSector
{
    MEDispatcher *dispatcher;
    BOOL  isSubscibed;
    METurnoversStomp *turnoversStomp;
    NSDateFormatter  *dateFormatter;
}

@synthesize engineName=_engineName, marketName=_marketName;

- (NSDictionary*) excludePropertyNames{
    return @{@"delegate": @NO};
}

- (id) _init_{
    
    self = [super init];
    
    if (self) {
        dispatcher = [MEDispatcher sharedInstance];
        dateFormatter = [[NSDateFormatter alloc] init] ;
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    
    return self;
}


- (METurnoversInfo*) turnoversInfo{
    if (!_turnoversInfo) {
        _turnoversInfo = [[METurnoversInfo alloc] init];
    }
    
    _turnoversInfo.turnovers = self;
    return _turnoversInfo;
}

- (id) init{
    return [self initWithEngine:nil];
}

- (id) initWithEngine:(NSString *)engine{
    return [self initWithEngine:engine withMarket:nil];
}

- (NSString*) contextName{
    if (self.engineName && self.marketName) {
        return [NSString stringWithFormat:@"%@-%@-%@",__TURNOVERS_ID__CONTEXT__, self.engineName, self.marketName];
    }
    else if (self.engineName) {
        return [NSString stringWithFormat:@"%@-%@",__TURNOVERS_ID__CONTEXT__, self.engineName];
    }
    return __TURNOVERS_ID__CONTEXT__;
}

- (id) initWithEngine:(NSString *)engine withMarket:(NSString *)market{
    self = [self _init_];
    
    if (self) {
        _engineName = engine;
        _marketName = market;
        NSTimeInterval lastTime;
        MEReferencesContext *context = [MEReferencesContext sharedInstance];
        [context restoreObject:self withContextName:[self contextName] withLastTimeSave:&lastTime];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveContextEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    return self;
}

- (void) saveContextEnterBackground:(NSNotification*)event{
    [self saveContext];
}


- (void) saveContext{
    MEReferencesContext *context = [MEReferencesContext sharedInstance];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSError *error;
        [context saveObject:self withContextName:[self contextName] error:&error];
        if (error) {
            NSLog(@"METurnoversModel: %@,  %s:%i", error, __FILE__, __LINE__);
        }
    });
}

- (void) errorEvent:(id)frame error:(GIError*)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTurnovers:fault:)]) {
        [self.delegate didTurnovers:self.turnoversInfo fault:error];
    }
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    [self errorEvent:frame error:error];
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    //[self errorEvent:frame error:error];
}

- (void) computeRates:( METurnoversInfo*)info{
    if (info==nil) {
        return;
    }
    CGFloat max = 0;
    for (int i=0; i<info.rows.count; i++) {
        METurnoverItem *item = [info.rows objectAtIndex:i];
        if (item && item.valtoday && max<item.valtoday.value.floatValue) {
            max=item.valtoday.value.floatValue;
        }
    }
    
    for (int i=0; i<info.rows.count; i++) {
        METurnoverItem *item = [info.rows objectAtIndex:i];
        CGFloat v = (item && item.valtoday)?item.valtoday.value.floatValue:0;
        if (max>0.0)
            item.valueRatio = [NSNumber numberWithFloat:v/max];
        else
            item.valueRatio = [NSNumber numberWithFloat:0.0];
    }
}

- (void) didSubscriptionRestore:(id)frame{
    if (self.turnoversInfo.rows.count>0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTurnoversRestore:)]) {
            [self.delegate didTurnoversRestore:self.turnoversInfo];
        }
    }
}

- (void) willFrameSend:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willTurnoversUpdate:)]) {
        [self.delegate willTurnoversUpdate:self.turnoversInfo];
    }
}

- (void) didAccessDeny:(id)frame withError:(GIError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTurnovers:fault:)]) {
        
        NSError *nerror = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                              code: GI_STOMP_LOGIN_FAILED
                                          userInfo: @{
                                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Access denied", @"Stomp error received"),
                                                      NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"Access denied for the turnovers", @"Stomp error access description.")]
                                                      }];
        
        [self.delegate didTurnovers:self.turnoversInfo fault:nerror];
    }
}


- (void) didFrameReceive:(id)frame{
    
    METurnoversInfo *info = self.turnoversInfo;
    
    GIStompMessage *m = frame;
    
    BOOL do_compute_totals = YES;
    
    for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
        GIStompBody *body = c.body;
        
        if (c.body.properties.type == GISTOMP_SNAPSHOT){
            [info.rows removeAllObjects];
            info.total = nil;
        }
        
        for (NSInteger i =0; i<c.body.countOfRows; i++) {
            METurnoverItem *item;
            
            NSNumber *ID = [body numberForIndex:i andKey:@"ID"];
            NSString *name = [body stringForIndex:i andKey:@"NAME"];
            
            if (c.body.properties.type == GISTOMP_SNAPSHOT) {
                item = [[METurnoverItem alloc] init];
                item.ID = ID;
                
                if ([name isEqualToString:@"TOTALS"] || [name isEqualToString:@"TOTAL"]){
                    info.total = item;
                    do_compute_totals = NO;
                }
                else{
                    [info.rows setObject:item forKey:item.ID];
                }
            }
            else{
                if ([name isEqualToString:@"TOTALS"] || [name isEqualToString:@"TOTAL"]){
                    item = info.total;
                    do_compute_totals = NO;
                }
                else
                    item = [info.rows objectForKey:ID];
            }
            
            item.name = name;
            item.caption = [body stringForIndex:i andKey:@"CAPTION"];
            item.valtoday = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"VALTODAY"] andValue: [body valueForIndex:i andKey:@"VALTODAY"]];
            item.valtodayUsd = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"VALTODAY_USD"] andValue: [body valueForIndex:i andKey:@"VALTODAY_USD"]];
            item.numtrades = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"NUMTRADES"] andValue: [body valueForIndex:i andKey:@"NUMTRADES"]];
            
            NSString *tm = [[body valueForIndex:i andKey:@"UPDATETIME"] string];
            item.updatetime = [dateFormatter dateFromString:tm];
        }
    }
    
    if (do_compute_totals) {
        info.total = [[METurnoverItem alloc] init];
        info.total.name = @"TOTALS";
        info.total.caption =  NSLocalizedString(@"Total market value, today", @"");
        info.total.valtoday = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"VALTODAY"] andValue:nil];
        info.total.valtodayUsd = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"VALTODAY_USD"] andValue:nil];
        info.total.numtrades = [[METurnoverField alloc] initWithCaption:m.structure.stream.fields[@"NUMTRADES"] andValue:nil];
        CGFloat v=0.0, vu=0.0, nt=0.0;
        for (int i=0; i<info.rows.count; i++) {
            METurnoverItem *item = [info.rows objectAtIndex:i];
            v += item.valtoday.value.floatValue;
            vu+= item.valtodayUsd.value.floatValue;
            nt+= item.numtrades.value.floatValue;
        }
        
        info.total.valtoday.value = [[GIStompValue alloc] initFromId:[NSNumber numberWithFloat:v]];
        info.total.valtodayUsd.value = [[GIStompValue alloc] initFromId:[NSNumber numberWithFloat:vu]];
        info.total.numtrades.value = [[GIStompValue alloc] initFromId:[NSNumber numberWithInteger:nt]];
    }
    
    [self computeRates:info];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTurnoversUpdate:)]) {
        [self.delegate didTurnoversUpdate:info];
    }
}

- (NSUInteger) sections{
    if (self.turnoversInfo && self.turnoversInfo.rows) {
        return self.turnoversInfo.rows.count>0?1:0;
    }
    return 0;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{
    if (self.turnoversInfo && self.turnoversInfo.rows) {
        return self.turnoversInfo.rows.count;
    }
    
    return 0;
}

- (METurnoverItem*) turnoverForIndexPath:(NSIndexPath*)indexPath{
    @try {
        return  [self.turnoversInfo.rows objectAtIndex:indexPath.row];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        return nil;
    }
}

- (void) subscribe{
    
    if (self.turnoversInfo.rows.count>0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTurnoversUpdate:)]) {
            [self.delegate didTurnoversUpdate:self.turnoversInfo];
        }
    }
    
    if (!turnoversStomp){
        
        if (self.marketName!=nil && self.engineName!=nil)
            turnoversStomp = [METurnoversStomp selectForEngine:self.engineName andMarket:self.marketName];
        
        else if (self.engineName!=nil)
            turnoversStomp = [METurnoversStomp selectForEngine:self.engineName];
        
        else
            turnoversStomp = [METurnoversStomp select];
        
    }
    turnoversStomp.delegate = self;
    [dispatcher registerMessage:turnoversStomp];
}


- (void) unsubscribe{    
    if (turnoversStomp) {
        [dispatcher unregisterMessage:turnoversStomp];
    }
    isSubscibed=NO;
    [self saveContext];
}


@end
