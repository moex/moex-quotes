//
//  MESearchListModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 13.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "METicker.h"
#import "MEEngine.h"
#import "MEDispatcher.h"


@class MESearchListModel;

@protocol MESearchListModelProtocol <NSObject>

- (void) didSearchList:(MESearchListModel*)watchlist fault:(NSError*)error;
- (void) didSearchListReceived:(MESearchListModel *)tickers;

@end

@interface MESearchListModel : NSObject<GIStompDispatcherProtocol>

@property(nonatomic) BOOL enableOnlyPrimary;

@property(nonatomic,strong) id<MESearchListModelProtocol> delegate;

- (void) searchTickers:(NSString*)part;
- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (METicker*) tickerForIndexPath:(NSIndexPath*)indexPath;

- (void) removeAllTickers;

@end
