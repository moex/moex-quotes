//
//  MEWatchListModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "METicker.h"
#import "MEMarketSection.h"
#import "MEEngine.h"
#import "MEDispatcher.h"
#import "MELevel2.h"
#import <MEContextArchiver/MEContextArchiver.h>


@class MEWatchListModel;

@protocol MEWatchListModelProtocol <NSObject>

- (void) willWatchListUpdate:(MEWatchListModel *)watchlist;
- (void) didWatchListUpdate:(MEWatchListModel *)watchlist;
- (void) didWatchListUpdate:(MEWatchListModel *)watchlist atIndexPaths:(NSArray*)indexPaths withChangedAttributes: (NSDictionary*)attributes;

@optional
- (void) didWatchList:(MEWatchListModel*)watchlist ticker:(METicker*)ticker fault:(NSError*)error;

@end

/**
 *  Watch list model.
 */
@interface MEWatchListModel : MEObject <GIStompDispatcherProtocol>

/**
 *  Markets watched.
 */
//@property(nonatomic,strong) GIOrderedDictionary *markets;

/**
 *  Delegate model events.
 */
@property(nonatomic,strong) id<MEWatchListModelProtocol> delegate;

/**
 *  Shared model.
 *
 *  @return MEWatchListModel.
 */
+ (id) sharedInstance;

/**
 *  Save the object context.
 */
- (void) saveContext;


/**
 *  Sections
 *
 *  @return number of sections.
 */
- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (METicker*) tickerForIndexPath:(NSIndexPath*)indexPath;
- (NSIndexPath*) indexPathForTicker:(METicker*)ticker;

/**
 *  Manage
 *
 *  @param indexPath active index path wich has slected
 */
@property (nonatomic,strong) METicker *activeTicker;

- (void) addTicker:(METicker*)ticker;
- (BOOL) removeTicker:(METicker*)ticker;

/**
 *  Exchange two tickers position.
 *  
 *  @param tickerFirst  first ticker.
 *  @param tickerSecond second ticker.
 *  
 *  @return YES in case when tickers swaped.
 */
- (BOOL) swapTicker:(METicker*)tickerFirst withTicker:(METicker*)tickerSecond;

/**
 *  Move one ticker to position of the second. Second moves to position after first.
 *  
 *  @param tickerFirst  firts.
 *  @param tickerSecond second.
 *  
 *  @return YES if moving available.
 */
- (BOOL) moveTicker:(METicker*)tickerFirst withTicker:(METicker*)tickerSecond;

- (void) subscribe;
- (void) unsubscribe;

@end
