//
//  MENewsItemBase.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 31.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsItemBase.h"
#import "MENewsSectionBase.h"

@interface MENewsItemBase()
@property (nonatomic,strong) NSString *cachedShortcut;
@end

@implementation MENewsItemBase

@dynamic caption;
@dynamic content;
@dynamic isContentDownloaded;
@dynamic isRead;
@dynamic isStared;
@dynamic lastUpdateTime;
@dynamic newsId;
@dynamic publicTime;
@dynamic shortcut;
@dynamic newsSection;
@dynamic order;

@synthesize cachedShortcut=_cachedShortcut;

- (NSString*) cachedShortcut{
    if (!_cachedShortcut) {
        _cachedShortcut = self.shortcut;
    }
    return _cachedShortcut;
}

@end
