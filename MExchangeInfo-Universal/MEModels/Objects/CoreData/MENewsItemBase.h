//
//  MENewsItemBase.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 31.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MENewsSectionBase;

@interface MENewsItemBase : NSManagedObject

@property (retain) NSString * caption;
@property (retain) NSString * content;
@property (retain) NSNumber * isContentDownloaded;
@property (retain) NSNumber * isRead;
@property (retain) NSNumber * isStared;
@property (retain) NSDate * lastUpdateTime;
@property (retain) NSNumber * newsId;
@property (retain) NSNumber * order;
@property (retain) NSDate * publicTime;
@property (retain) NSString * shortcut;
@property (retain) MENewsSectionBase *newsSection;

@property (nonatomic,readonly) NSString *cachedShortcut;

@end
