//
//  MENewsSectionBase.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 31.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MENewsItemBase;

@interface MENewsSectionBase : NSManagedObject

@property (retain) NSDate * date;
@property (retain) NSArray *newsItems;
@end

@interface MENewsSectionBase (CoreDataGeneratedAccessors)

- (void)insertObject:(MENewsItemBase *)value inNewsItemsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromNewsItemsAtIndex:(NSUInteger)idx;
- (void)insertNewsItems:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeNewsItemsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInNewsItemsAtIndex:(NSUInteger)idx withObject:(MENewsItemBase *)value;
- (void)replaceNewsItemsAtIndexes:(NSIndexSet *)indexes withNewsItems:(NSArray *)values;
- (void)addNewsItemsObject:(MENewsItemBase *)value;
- (void)removeNewsItemsObject:(MENewsItemBase *)value;
- (void)addNewsItems:(NSArray *)values;
- (void)removeNewsItems:(NSArray *)values;
@end
