//
//  MENewsItem.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 24.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MENewsItemBase.h"

@interface MENewsItem : MENewsItemBase
@end

@interface MENewsItemBase (MENewsItem)
@property(strong, readonly) MENewsItem *next,*previous;
@end