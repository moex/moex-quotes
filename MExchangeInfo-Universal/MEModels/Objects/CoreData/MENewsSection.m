//
//  MENewsSectionEntity.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 23.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsSection.h"
#import "MENewsItem.h"
#import "NSDate+MEComparing.h"


@implementation MENewsSectionBase (MENewsSection)

- (NSString*) caption{
    if ([self.date isToday]) {
        return  NSLocalizedString(@"Today", @"News list section header with name Today date");
    }
    else if ([self.date isYesterday]){
        return NSLocalizedString(@"Yesterday", @"News list section header with name Today date");
    }
    else{
        static NSDateFormatter * date_format = nil; if (!date_format) date_format = [[NSDateFormatter alloc] init];
        [date_format setDateFormat: @"EEEE, d MMMM YYYY"]; // ICU
        return [date_format stringFromDate: self.date];
    }
}

@end

@implementation MENewsSection
@end
