//
//  MENewsSectionBase.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 31.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsSectionBase.h"
#import "MENewsItemBase.h"


@implementation MENewsSectionBase

@dynamic date;
@dynamic newsItems;

@end
