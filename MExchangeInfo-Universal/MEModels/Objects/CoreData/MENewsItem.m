//
//  MENewsItem.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 24.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MENewsItem.h"
#import "MENewsItem.h"
#import "MENewsSectionBase.h"
#import "MENewsSection.h"
#import "NSDate+MEComparing.h"

#import <ObjectiveRecord/ObjectiveRecord.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

@implementation MENewsItem
@end

@implementation MENewsItemBase (MENewsItem)

- (MENewsItem*) next{
    NSArray *items=[MENewsItem where:[NSPredicate predicateWithFormat:@"order<%@", self.order]
                           inContext:self.managedObjectContext order:@{@"order": @"DESC"} limit:@1];
        
    return items.lastObject;

}

- (MENewsItem*) previous{
    NSArray *items=[MENewsItem where:[NSPredicate predicateWithFormat:@"order>%@", self.order] 
                   inContext:self.managedObjectContext order:@{@"order": @"ASC"} limit:@1];
    return items.lastObject;
}

@end
