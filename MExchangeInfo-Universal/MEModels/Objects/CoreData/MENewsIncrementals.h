//
//  MENewsIncrementals.h
//  MExchange-MosQuotes
//
//  Created by denn on 19.12.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MENewsIncrementals : NSManagedObject

@property (nonatomic, retain) NSNumber * orderLast;
@property (nonatomic, retain) NSString * tableId;

@end
