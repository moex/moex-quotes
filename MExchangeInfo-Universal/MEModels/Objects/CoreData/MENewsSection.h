//
//  MENewsSectionEntity.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 23.01.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MENewsSectionBase.h"

@class MENewsItem;

@interface MENewsSection : MENewsSectionBase
@end

@interface MENewsSectionBase (MENewsSection)
@property (readonly,strong) NSString *caption;
@end
