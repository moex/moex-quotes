//
//  MEMarket.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEContextArchiver/MEObject.h>
#import "MEEngine.h"
#import <MEinfoCXConnector/GIStompBody.h>
#import "METurnoversStomp.h"

/**
 *  Market description.
 */
@interface MEMarket : MEObject
/**
 *  The market system ID.
 */
@property(nonatomic,strong) NSNumber *ID;

/**
 *  Market ID.
 */
@property(nonatomic,strong) NSString *name;

/**
 *  The market localized title.
 */
@property(nonatomic,strong) NSString *caption;

/**
 *  Engine ID.
 */
@property(nonatomic,strong) NSNumber *engineID;

///**
// *  Last valtoday for the market totaly
// */
//@property(nonatomic,strong) GIStompValue *valtoday;
//
//@property(nonatomic,strong) METurnoversStomp *turnoversSubscription;
//
@end


@interface MEMarket (Referenced)
- (MEEngine*) engine;
@end

