//
//  MEBoard.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEEngine.h"
#import "MEMarket.h"
#import <MEContextArchiver/MEObject.h>

@interface MEBoard : MEObject
/**
 *  System trade board.
 */
@property(nonatomic,strong) NSNumber *ID;

/**
 *  System board name.
 */
@property(nonatomic,strong) NSString *name;
/**
 * Localized board name.
 */
@property(nonatomic,strong) NSString *caption;

/**
 * Is primary board ?
 */
@property(nonatomic) BOOL isPrimary;

/**
 *  Market ID.
 */
@property(nonatomic,strong) NSNumber *marketID;

/**
 *  Engine ID.
 */
@property(nonatomic,strong) NSNumber *engineID;

@end


@interface MEBoard (Referenced)
- (MEEngine *) engine;
@end