//
//  MEEngine.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEEngine.h"

@implementation MEEngine

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@/%@",_ID,_name,_caption /*,_valtoday*/];
}

- (NSDictionary*) excludePropertyNames{
    return @{@"turnoversSubscription": @NO, @"valtoday": @NO};
}

@end
