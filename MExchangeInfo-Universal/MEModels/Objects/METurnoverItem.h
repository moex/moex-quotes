//
//  METurnover.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEObject.h"
#import <MEinfoCXConnector/GIStompBody.h>

@interface METurnoversFieldItem : MEObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *caption;
@property(nonatomic,strong) NSString *hint;
@end

@interface METurnoverField : MEObject
@property(nonatomic,strong) METurnoversFieldItem   *field;
@property(nonatomic,strong) GIStompValue           *value;

- (id) initWithCaption:(GIField*)aField andValue:(GIStompValue*)value;

@end

@interface METurnoverItem : MEObject
@property(nonatomic,strong) NSNumber *ID;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *caption;
@property(nonatomic,strong) METurnoverField *valtoday;
@property(nonatomic,strong) METurnoverField *valtodayUsd;
@property(nonatomic,strong) METurnoverField *numtrades;
@property(nonatomic,strong) NSDate       *updatetime;
@property(nonatomic,strong) NSNumber *valueRatio;
@end
