//
//  MEMarket.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMarketSection.h"
#import "MEReferences.h"
#import "METurnoversModel.h"

@interface MEMarketSection() <METurnoversModelProtocol>
@property (nonatomic,strong) METurnoversSector *engineSector;
@property (nonatomic,strong) METurnoversSector *marketSector;
@property(nonatomic,strong) MEMarket* market;
@property(nonatomic,strong) GIOrderedDictionary *tickers;
@end

@implementation MEMarketSection

@synthesize tickers=_tickers, market=_market, engineValtoday=_engineValtoday, marketValtoday=_marketValtoday, marketSector=_marketSector, engineSector=_engineSector;

- (NSDictionary*) excludePropertyNames{
    return @{@"engineValtoday": @NO, @"marketValtoday": @NO, @"engineSector": @NO, @"marketSector": @NO};
}

- (GIOrderedDictionary*) tickers{
    if (_tickers) {
        return _tickers;
    }
    
    _tickers = [[GIOrderedDictionary alloc] init];
    
    return _tickers;
}

- (void) setTickers:(GIOrderedDictionary *)tickers{
    _tickers = tickers;
}


- (void) setMarket:(MEMarket *)market{
    _market = market;
}

- (id) initWithMarket:(MEMarket*)market{
    self = [super init];
    if (self) {
        _market = market;
        _marketSector = nil;
        _engineSector = nil;
    }
    return self;
}

- (MEEngine*) engine{
    return [[[MEReferences sharedInstance] engines] objectForKey:self.market.engineID];
}

- (void) setEngineSector:(METurnoversSector *)engineSector{
    _engineSector=engineSector;
}

- (void) setMarketSector:(METurnoversSector *)marketSector{
    _marketSector = marketSector;
}

- (METurnoversSector*) engineSector{
    if (_engineSector) {
        _engineSector.delegate=self;
        return _engineSector;
    }
    _engineSector =  [[METurnoversModel sharedInstance] sectorWithDelegate:self withEngineName:[self engine].name withMarketName:nil];
    return _engineSector;
}

- (METurnoversSector*) marketSector{
    if (_marketSector) {
        _marketSector.delegate = self;
        return _marketSector;
    }
    _marketSector =  [[METurnoversModel sharedInstance] sectorWithDelegate:self withEngineName:[self engine].name withMarketName:self.market.name];
    return _marketSector;
}

- (void) subscribe{
    [self.marketSector subscribe];
    [self.engineSector subscribe];
}

- (void) unsubscribe{
    [self.engineSector unsubscribe];
    [self.marketSector unsubscribe];
}


- (void) willTurnoversUpdate:(METurnoversInfo *)turnoversInfo{
}

- (void) didTurnoversRestore:(METurnoversInfo *)turnoversInfo{
    if (turnoversInfo.turnovers.marketName==nil) {
        self.engineValtoday = turnoversInfo.total.valtoday.value;
    }
    else {
        self.marketValtoday = turnoversInfo.total.valtoday.value;
    }
}

- (void) didTurnoversUpdate:(METurnoversInfo *)turnoversInfo{
    if (turnoversInfo.turnovers.marketName==nil) {
        self.engineValtoday = turnoversInfo.total.valtoday.value;
    }
    else {
        self.marketValtoday = turnoversInfo.total.valtoday.value;
    }
}

- (void) setEngineValtoday:(GIStompValue *)engineValtoday{
    [self willChangeValueForKey:@"engineValtoday"];
    _engineValtoday = engineValtoday;
    [self didChangeValueForKey:@"engineValtoday"];
}

- (void) setMarketValtoday:(GIStompValue *)marketValtoday{
    [self willChangeValueForKey:@"marketValtoday"];
    _marketValtoday = marketValtoday;
    [self didChangeValueForKey:@"marketValtoday"];
}

@end
