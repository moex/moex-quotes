//
//  MECandlesModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 09.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MECandlesModel.h"
#import "MEDispatcher.h"
#import <MEinfoCXConnector/GIOrderedDictionary.h>


@interface __GIStompCandles: GIStompCandles
@property (nonatomic,strong) NSString* interval;
@property (nonatomic,strong) NSString* tickerID;
@property (nonatomic,readonly) NSString* selectorKey;
@end

@implementation __GIStompCandles
@synthesize interval=_interval, selectorKey=_selectorKey;

+ (id) select:(NSString *)ticker andInterval:(NSString *)interval{
    __GIStompCandles *c=[super select:ticker andInterval:interval];
    c.interval = interval;
    c.tickerID = ticker;
    return c;
}

- (NSString*) selectorKey{
    if (_selectorKey) {
        return _selectorKey;
    }
    
    _selectorKey = [NSString stringWithFormat:@"%@:%@", self.tickerID, self.interval];
    
    return _selectorKey;
}

@end

@interface __MESChartArray : NSObject
@property (nonatomic,strong) GIOrderedDictionary *array;
@property (nonatomic,strong) NSNumber *min;
@property (nonatomic,strong) NSNumber *max;
@property (nonatomic,strong) NSNumber *lineMin;
@property (nonatomic,strong) NSNumber *lineMax;

- (id) objectAtIndex:(NSUInteger)index;
- (void) addObject:(id) anObject;
- (void) removeObjectsInRange:(NSRange)range;
@end

@implementation __MESChartArray
@synthesize max = _max, min = _min;

- (id) objectAtIndex:(NSUInteger)index{
    if (index>=[self.array count]) {
        NSLog(@"Error bounds for candles model: index=%lu for bound[0...%lu]",(unsigned long)index, (unsigned long)self.array.count);
        return Nil;
    }
    return [self.array objectAtIndex:index];
}

-(void) removeObjectsInRange:(NSRange)range{
    [self.array removeObjectsInRange:range];
}

- (void) addObject:(id) anObject{
    MESChartData *data = anObject;
    return [self.array setObject:data forKey:data.xValue];
}

- (GIOrderedDictionary*) array{
    if (_array) {
        return _array;
    }
    
    _array = [[GIOrderedDictionary alloc] initWithCapacity:1];
    
    return _array;
}

- (NSUInteger) count{
    return [self.array count];
}

- (NSNumber*) min{
    if (_min) {
        return _min;
    }
    
    _min = [NSNumber numberWithFloat:MAXFLOAT];
    
    return _min;
}

- (NSNumber*) max{
    if (_max) {
        return _max;
    }
    
    _max = [NSNumber numberWithInt:0];
    
    return _max;
}

- (void) setMax:(NSNumber *)max{
    if (!_max) {
        _max = self.max;
    }
    _max = max;
}

- (void) setMin:(NSNumber *)min {
    if (!_min) {
        _min = self.min;
    }
    _min = min;
}

@end

static MECandlesModel *__shared_instance = nil;

@interface MECandlesModel() <GIStompDispatcherProtocol>
@property (nonatomic,strong) NSMutableArray *intervals;
@property (atomic,readonly) __MESChartArray *candles, *volumes;
@property (nonatomic,strong) MEDispatcher *dispatcher;
@property (nonatomic,strong) __GIStompCandles *candlesSubscription;
@property (nonatomic,strong) NSMutableDictionary *subscriptions;
@property (nonatomic,strong) NSMutableDictionary *candlesStore;
@property (nonatomic,strong) NSMutableDictionary *volumesStore;
@property (nonatomic,readonly) NSString* selectorKey;
@end

@implementation MECandlesModel
{
    NSInteger currentIndex;
    BOOL hasSnapshot;
}

@synthesize candles=_candles, volumes=_volumes, currentInterval=_currentInterval, selectorKey=_selectorKey, ticker=_ticker;

+ (NSDate*) dateFromString:(NSString*)date {
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [dateFormatter dateFromString:date];
}

- (void) setTicker:(METicker *)ticker{
    _ticker = ticker;
    hasSnapshot = NO;
}

- (METicker*) ticker{
    return _ticker;
}

- (NSString*) selectorKey{
    if (!self.ticker) {
        return nil;
    }
    _selectorKey = [NSString stringWithFormat:@"%@:%@", self.ticker.ID, self.currentInterval];
    return _selectorKey;
}

- (NSMutableDictionary*) subscriptions{
    if (_subscriptions) {
        return _subscriptions;
    }
    _subscriptions = [[NSMutableDictionary alloc] initWithCapacity:5];
    return _subscriptions;
}

- (NSMutableDictionary*) volumesStore{
    if (_volumesStore) {
        return _volumesStore;
    }
    _volumesStore = [[NSMutableDictionary alloc] initWithCapacity:5];
    return _volumesStore;
}

- (NSMutableDictionary*) candlesStore{
    if (_candlesStore) {
        return _candlesStore;
    }
    
    _candlesStore = [[NSMutableDictionary alloc] initWithCapacity:5];
    
    return _candlesStore;
}

- (__GIStompCandles*) candlesSubscription{
    if (self.selectorKey == nil) {
        return nil;
    }
    _candlesSubscription = [self.subscriptions objectForKey:self.selectorKey];
    if (!_candlesSubscription) {
        _candlesSubscription = [__GIStompCandles select:self.ticker.ID andInterval:self.currentInterval];
        _candlesSubscription.delegate = self;
        [self.subscriptions setObject:_candlesSubscription forKey:self.selectorKey];
    }
    
    return _candlesSubscription;
}

- (MEDispatcher*) dispatcher{
    if (_dispatcher) {
        return _dispatcher;
    }
    _dispatcher = [MEDispatcher sharedInstance];
    return _dispatcher;
}


+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MECandlesModel alloc] init];
    });
    
    return __shared_instance;
}

- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        _size = 72;
    }
    
    return self;
}

- (__MESChartArray*) candles{
    
    if (self.selectorKey==nil) {
        return nil;
    }
    
    _candles = [self.candlesStore objectForKey:self.selectorKey];
    
    if (!_candles) {
        _candles = [[__MESChartArray alloc] init];
        [self.candlesStore setObject:_candles forKey:self.selectorKey];
    }
    
    return _candles;
}

- (__MESChartArray*) volumes{
    
    _volumes = [self.volumesStore objectForKey:self.selectorKey];
    
    if (!_volumes) {
        _volumes = [[__MESChartArray alloc] init];
        [self.volumesStore setObject:_volumes forKey:self.selectorKey];
    }
    
    return _volumes;
}

- (NSArray*) intervals{
    return @[@"M1", @"M10", @"H1", @"D1", @"m1"];
}

- (NSString*) currentInterval{
    if (_currentInterval) {
        return _currentInterval;
    }
    _currentInterval = [self.intervals objectAtIndex:0];
    return _currentInterval;
}

- (void) setCurrentInterval:(NSString *)currentInterval{
    currentIndex = 0;
    _currentInterval = currentInterval;
    hasSnapshot = NO;
}

- (NSUInteger) series{
    return  self.candles.count>0?self.volumes.count>0?2:1:0;
}

- (NSUInteger) dataCountAtSeries:(NSUInteger)seriesIndex{
    NSUInteger count = 0;
    switch (0) {
        case 0:
            count = self.candles.count;
            break;
        case 1:
            count = self.volumes.count;
            break;
        default:
            break;
    }
    return count;
}

- (MESChartData*) dataAtIndex:(NSInteger)index forSeriesIndex:(NSInteger)seriesIndex{
    
    MESChartData *data=nil;
    
    switch (seriesIndex) {
        case 0:
            data = [self.candles objectAtIndex:index];
            break;
        case 1:
            data = [self.volumes objectAtIndex:index];
            break;
        default:
            break;
    }
    
    return data;
}

- (MESChartDataRange*) dataRangeAtSeriesIndex:(NSInteger)seriesIndex{
    
    MESChartDataRange *range = nil;
    
    NSNumber *max = self.candles.max;
    NSNumber *min = self.candles.min;
    
    if (self.seriesType==MESCHART_LINE) {
        max = self.candles.lineMax;
        min = self.candles.lineMin;
    }
    
    switch (seriesIndex) {
        case 0:
            range = [[MESChartDataRange alloc] initWithMinimum:min withMaximum:max];
            break;
        case 1:
            range = [[MESChartDataRange alloc] initWithMinimum:self.volumes.min withMaximum:self.volumes.max];
            break;
        default:
            break;
    }
    
    return range;
}

- (void) subscribeIntervals{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModelUpdateIntervals:)]) {
        [self.delegate didCandlesModelUpdateIntervals:self];
    }
}

- (void) subscribe{
    if (self.candlesSubscription) {
        [self.dispatcher registerMessage:self.candlesSubscription];
    }
}

- (void) unsubscribe{
    if (self.candlesSubscription) {
        [self.dispatcher unregisterMessage:self.candlesSubscription];        
    }
}

- (void) dealloc{
    [self unsubscribe];
}

#pragma mark - Stomp delegate

- (void) didAccessDeny:(id)frame withError:(GIError *)error{
    if (self.dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"Candles Access: %@: %@",error, frame);
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModel:accessDeny:)]) {
        [self.delegate didCandlesModel:self accessDeny:error];
    }
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    if (self.dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"Candles Error: %@: %@",error, frame);
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModel:fault:)]) {
        [self.delegate didCandlesModel:self fault:error];
    }
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    if (self.dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"Candles interupt: %@",error);
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModel:fault:)]) {
        [self.delegate didCandlesModel:self fault:error];
    }
}

- (void) willFrameSend:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willCandlesModelUpdateData:)]) {
        [self.delegate willCandlesModelUpdateData:self];
    }
}

- (void) didSubscriptionRestore:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModelUpdateData:)]) {
        [self.delegate didCandlesModelUpdateData:self];
    }
}

- (void) didFrameReceive:(id)frame{
    __GIStompCandles *m = frame;
    
    BOOL setFirstRange = NO;
    if (currentIndex==0) {
        setFirstRange = YES;
    }
        
    __MESChartArray *candles = [self.candlesStore objectForKey:m.selectorKey];
    
    if (!candles) {
        candles = [[__MESChartArray alloc] init]; [self.candlesStore setObject:candles forKey:m.selectorKey];
    }
    
    __MESChartArray *volumes = [self.volumesStore objectForKey:m.selectorKey];
    if (!volumes) {
        volumes = [[__MESChartArray alloc] init]; [self.volumesStore setObject:volumes forKey:m.selectorKey];
    }
    
    for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
        
        BOOL isSnap = NO;
        
        if (c.body.properties.type == GISTOMP_SNAPSHOT) {
            [candles.array removeAllObjects];
            [volumes.array removeAllObjects];
            currentIndex = 0;
            isSnap = YES;
            hasSnapshot = YES;
        }
        
        for (int i=0; i<c.body.countOfRows; i++) {
            
            MESChartData *dataPoint = [[MESChartData alloc] init];
            
            NSString *from = [NSString stringWithFormat:@"%@",[c.body stringForIndex:i andKey:@"FROM"]];
            
            if (candles.array.count>0 && !isSnap) {
                MESChartData *data = [candles.array lastObject];
                NSDate *lastDateTime = data.xValue;//[MECandlesModel dateFromString:data.xValue];
                NSDate *fromTime = [MECandlesModel dateFromString:from];
                
                if ([lastDateTime timeIntervalSince1970]>[fromTime timeIntervalSince1970]) {
                    // Skip existed candles
                    continue;
                }
            }
            
            dataPoint.index = currentIndex;
            dataPoint.xValue = [MECandlesModel dateFromString:[NSString stringWithFormat:@"%@", from]];
            MESChartOHLC *ohlc = [[MESChartOHLC alloc]
                                  initWithOpen:[c.body numberForIndex:i andKey:@"OPEN"]
                                  withHigh:[c.body numberForIndex:i andKey:@"HIGH"]
                                  withLow:[c.body numberForIndex:i andKey:@"LOW"]
                                  withClose:[c.body numberForIndex:i andKey:@"CLOSE"]];
            
            dataPoint.yValue =ohlc;
            
            [candles addObject:dataPoint];
            
            MESChartData *volumePoint = [[MESChartData alloc] init];
            volumePoint.index = currentIndex;
            volumePoint.xValue =  dataPoint.xValue;
            volumePoint.yValue = [c.body numberForIndex:i andKey:@"VOLUME"];
            
            [volumes addObject:volumePoint];
            
            currentIndex ++;
        }
    }
    
    if (candles.count>self.size) {
        [candles removeObjectsInRange:NSMakeRange(0, candles.count-self.size)];
        [volumes removeObjectsInRange:NSMakeRange(0, volumes.count-self.size)];
    }
    
    candles.min = [NSNumber numberWithFloat:MAXFLOAT];
    candles.lineMin = [NSNumber numberWithFloat:MAXFLOAT];

    candles.max = [NSNumber numberWithFloat:0];
    candles.lineMax = [NSNumber numberWithFloat:0];
    
    volumes.min = [NSNumber numberWithFloat:0];
    volumes.max = [NSNumber numberWithFloat:0];
    
    for (int i=0; i<candles.count; i++) {
        
        MESChartData *dataPoint = [candles objectAtIndex:i];
        MESChartData *volumePoint = [volumes objectAtIndex:i];
        
        volumePoint.index = dataPoint.index = i;
        
        MESChartOHLC *ohlc = dataPoint.yValue;
        
        
        if (candles.min.floatValue>ohlc.low.floatValue)
            candles.min = ohlc.low;

        if (candles.lineMin.floatValue>ohlc.close.floatValue)
            candles.lineMin = ohlc.close;

        if (candles.max.floatValue<ohlc.high.floatValue)
            candles.max = ohlc.high;

        if (candles.lineMax.floatValue<ohlc.close.floatValue)
            candles.lineMax = ohlc.close;
        
        NSNumber *v=volumePoint.yValue;
        
        if (volumes.max.floatValue<v.floatValue)
            volumes.max = v;
        
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCandlesModelUpdateData:)] && [m.selectorKey isEqualToString:self.selectorKey]) {
        [self.delegate didCandlesModelUpdateData:self];
    }
}


@end
