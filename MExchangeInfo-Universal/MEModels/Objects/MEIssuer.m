//
//  MEIssuer.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEIssuer.h"

@implementation MEIssuer

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@ %@", _ID, _name, _caption];
}

@end
