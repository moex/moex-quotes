//
//  MEMarket.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMarket.h"
#import "MEEngine.h"
#import "MEReferences.h"

@implementation MEMarket
- (NSString*)description{
    return [NSString stringWithFormat:@"%@: %@ %@ [%@]", _ID,_name, _caption, _engineID /*, _valtoday*/];
}
@end

@implementation MEMarket (Referenced)

- (MEEngine*) engine{
    return [[[MEReferences sharedInstance] engines] objectForKey:_engineID];
}

- (NSDictionary*) excludePropertyNames{
    return @{@"turnoversSubscription": @NO, @"valtoday": @NO};
}

@end