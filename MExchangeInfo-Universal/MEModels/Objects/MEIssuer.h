//
//  MEIssuer.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEContextArchiver/MEObject.h>

/**
 *  Common issuer info
 */
@interface MEIssuer : MEObject

/**
 *  Issuer system ID.
 */
@property(nonatomic,strong) NSNumber *ID;

/**
 *  Issuer name.
 */
@property(nonatomic,strong) NSString *name;

/**
 *  Issuer localized name.
 */
@property(nonatomic,strong) NSString *caption;

@end
