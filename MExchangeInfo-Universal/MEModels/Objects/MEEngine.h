//
//  MEEngine.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import <MEinfoCXConnector/GIStompBody.h>
#import <MEContextArchiver/MEObject.h>
#import "METurnoversStomp.h"

@interface MEEngine : MEObject
@property(nonatomic,strong) NSNumber *ID;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *caption;
/**
 *  Last valtoday for the engine totaly
 */
//@property(nonatomic,strong) GIStompValue *valtoday;
//
//@property(nonatomic,strong) METurnoversStomp *turnoversSubscription;

@end
