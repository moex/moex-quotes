//
//  MESearchListModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 13.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MESearchListModel.h"
#import "MEReferences.h"
#import "MESettings.h"
#import <Crashlytics/Crashlytics.h>

@implementation MESearchListModel
{
    GIOrderedDictionary *tickers;
    MEDispatcher *dispatcher;
    MEReferences *enginesReference;
    
    NSCache *cache;
}

- (id) init{
    self = [super init];
    
    dispatcher = [MEDispatcher sharedInstance];
    tickers = [[GIOrderedDictionary alloc] init];
    cache = [[NSCache alloc] init];
    
    return self;
}

- (NSString*) cacheKey:(NSString*)part{
    return [NSString stringWithFormat:@"%li-%@",(long)[[NSUserDefaults standardUserDefaults] integerForKey:ME_SEARCH_ONLY_PRIMARY_BOARD], part];
};

- (void) searchTickers:(NSString*)part{
    
    NSString *key = [self cacheKey:part];
    
    GIOrderedDictionary *result = [cache objectForKey:key];
    
    if (result) {
        tickers = result;
        if (self.delegate) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate performSelector:@selector(didSearchListReceived:) withObject:self];
            });
        }
    }
    else{
        [self removeAllTickers];
        GIStompSearchTickers *_tickers = [GIStompSearchTickers selectByMask:part];
                
        [dispatcher registerRequest:_tickers];
        _tickers.delegate = self;
    }
    
    NSString *primary = _enableOnlyPrimary? @"primary":@"all";
    
    [Answers logSearchWithQuery:@"Search Ticker"
               customAttributes:@{
                                  @"Ticker Part" : part,
                                  @"Mode" : primary
                                  }];
    
}

- (METicker*) tickerForIndexPath:(NSIndexPath *)indexPath{
    return [tickers objectAtIndex:indexPath.row];
}

- (NSUInteger) sections{
    return tickers.count>0?1:0;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{
    return tickers.count;
}

- (void) removeAllTickers{
    [tickers removeAllObjects];
    [cache removeAllObjects];
}

- (NSString*) requestKey:(GIStompMessage*)m{
    NSString *_parameter_name = @"pattern";
    @try {
        NSString *_selector = m.request.headers[@"selector"];
        NSArray  *_ks = [_selector componentsSeparatedByString:@"="];
        _parameter_name = _ks[0];
        NSMutableString *_part = [NSMutableString stringWithString: _ks[1]];
        [_part replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, _part.length)];
        
        return [self cacheKey:_part];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }

}

- (void) didFrameReceive:(id)frame{
    
    if ([frame isKindOfClass:[GIStompSearchTickers class]]) {
        GIStompMessage *m = frame;
        
        
        GIOrderedDictionary *new_tickers = [[GIOrderedDictionary alloc] init];
        
        for (int i=0; i<m.lastResponse.body.countOfRows; i++) {
            
            NSDictionary *row = [m.lastResponse.body rowByIndex:i];
            
            METicker *ticker = [METicker tickerFromString:[row valueForKey:@"ticker"]];
            
            if ([[row valueForKey:@"primary_boardid"] isEqualToString:ticker.board.name])
                ticker.board.isPrimary = YES;
            else
                ticker.board.isPrimary = NO;
            
            ticker.caption = [row valueForKey:@"name"];
            ticker.hint = [row valueForKey:@"shortname"];
            ticker.isin = [row valueForKey:@"isin"];
                        
            MEIssuer *issuer = [[MEIssuer alloc] init];
            
            issuer.ID = [m.lastResponse.body numberForIndex:i andKey:@"emitent_id"];
            issuer.name = ticker.name;
            issuer.caption = [row valueForKey:@"emitent_title"];
            
            ticker.issuer = issuer;
            
            if (![[NSUserDefaults standardUserDefaults] integerForKey:ME_SEARCH_ONLY_PRIMARY_BOARD] && !ticker.board.isPrimary)
                continue;
                        
            [new_tickers setObject:ticker forKey:ticker.ID];
        }
        
        [cache setObject:new_tickers forKey:[self requestKey:m]];
        
        if (self.delegate) {
            tickers=new_tickers;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate performSelector:@selector(didSearchListReceived:) withObject:self];
            });
        }
    }
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    if (self.delegate) {
        [self.delegate performSelector:@selector(didSearchList:fault:) withObject:error];
    }
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    if (self.delegate) {
        [self.delegate performSelector:@selector(didSearchList:fault:) withObject:error];
    }
}
@end
