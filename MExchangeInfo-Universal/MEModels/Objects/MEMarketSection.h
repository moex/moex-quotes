//
//  MEMarket.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import <MEContextArchiver/MEObject.h>
#import "MEMarket.h"
#import "MEEngine.h"

/**
 *  Market section in watched list.
 */
@interface MEMarketSection : MEObject

/**
 *  Market.
 */
@property(nonatomic,readonly) MEMarket* market;

/**
 *  Tickers watched at the market.
 */
@property(nonatomic,readonly) GIOrderedDictionary *tickers;
@property(nonatomic,strong) GIStompValue *engineValtoday;
@property(nonatomic,strong) GIStompValue *marketValtoday;


/**
 *  This market id belongs the engine.
 */
- (MEEngine*) engine;

/**
 *  Create Market section for watchlist model.
 *  
 *  @param market market.
 *  
 *  @return instance.
 */
- (id) initWithMarket:(MEMarket*)market;

- (void) subscribe;
- (void) unsubscribe;

@end
