//
//  METurnover.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoverItem.h"
#import <MEinfoCXConnector/GIUtils.h>

@implementation METurnoversFieldItem

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@/%@", _name, _caption, _hint];
}

@end

@implementation METurnoverField

- (id) init{
    self = [super init];
    return self;
}

- (id) initWithCaption:(GIField*)aField andValue:(GIStompValue*)value{
    self = [super init];
    if (self) {
        _field = [[METurnoversFieldItem alloc] init];
        _field.name = aField.name;
        _field.caption = aField.caption;
        _field.hint = aField.hint;
        _value = value;
    }
    return self;
}

- (NSDictionary*) toDictionary{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:2];
    @try {        
        if (_field) {
            [dict setObject:@{@"name": _field.name, @"caption": _field.caption, @"hint": _field.hint} forKey:@"field"];
        }
        if (!IsEmpty(_value) && !IsEmpty([_value number])) {
            [dict setObject:[_value number] forKey:@"value"];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception,  __FILE__, __LINE__);
    }
    return dict;
}

- (void) fromDictionary:(NSDictionary *)dictionary{
    _field = [[METurnoversFieldItem alloc] init];
    NSDictionary *fd = [dictionary objectForKey:@"field"];
    _field.name = [fd objectForKey:@"name"];
    _field.caption = [fd objectForKey:@"caption"];
    _field.hint = [fd objectForKey:@"hint"];
    _value = [[GIStompValue alloc] initFromId:[dictionary objectForKey:@"value"]];
}

@end

@implementation METurnoverItem

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@:%@  %@/%@/%@ %@ ratio=%@", self.ID, self.name, self.caption, [self.valtoday.value string], [self.valtodayUsd.value string], [self.numtrades.value string], _updatetime, _valueRatio];
}


@end
