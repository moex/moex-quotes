//
//  MEStompTurnovers.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METurnoversStomp.h"

@implementation METurnoversStomp
+ (NSString*) destination{
    return @"turnovers";
}


+ (id)select{
    METurnoversStomp *s=[[METurnoversStomp alloc] init];
    [s selectForMarketPlace:@"COMMON"];
    return s;
}

+ (id)selectForEngine:(NSString*)engine{
    METurnoversStomp *s=[[METurnoversStomp alloc] init];
    [s selectForMarketPlace:@"COMMON"];
    [s.request setHeader:@"selector" withValue:[NSString stringWithFormat:@"engine='%@'", engine]];
    return s;
}

+ (id)selectForEngine:(NSString*)engine andMarket:(NSString*)market{
    METurnoversStomp *s=[[METurnoversStomp alloc] init];
    [s selectForMarketPlace:@"COMMON"];
    [s.request setHeader:@"selector" withValue:[NSString stringWithFormat:@"engine='%@' and market='%@'", engine, market]];
    return s;
}

@end

