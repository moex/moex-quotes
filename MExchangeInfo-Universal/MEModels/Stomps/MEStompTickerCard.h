//
//  MEStompTickerCard.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "GIStompRequests.h"

@interface MEStompTickerCard : GIRequestStompFrame
+ (id)selectCardList;
+ (id)selectCard:(NSString*)cardName forTicker:(NSString*)ticker;
@end
