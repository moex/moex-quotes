//
//  MEStompTickerCard.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/20/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEStompTickerCard.h"

@implementation MEStompTickerCard

+ (NSString*) destination{
    return @"COMMON.ticker.card";
}

+ (id)selectCardList{
    NSString *selector = [NSString stringWithFormat:@"list=True"];
    MEStompTickerCard *news = [[MEStompTickerCard alloc] init];
    [news  selectFor:selector];
    return news;
}

+ (id)selectCard:(NSString*)cardName forTicker:(NSString*)ticker{
    NSString *selector = [NSString stringWithFormat:@"ticker=%@ and cardname=%@", ticker, cardName];
    MEStompTickerCard *card = [[MEStompTickerCard alloc] init];
    [card  selectFor:selector];
    return card;
}
@end
