//
//  MEStompTurnovers.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 1/10/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "GIStompSubscriptions.h"

@interface METurnoversStomp : GIStompSelector
+ (id)select;
+ (id)selectForEngine:(NSString*)engine;
+ (id)selectForEngine:(NSString*)engine andMarket:(NSString*)market;
@end
