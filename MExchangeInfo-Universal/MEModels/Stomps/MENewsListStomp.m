//
//  MELastNewsStomp.m
//  MExchange
//
//  Created by denn on 10/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MENewsListStomp.h"

@implementation MENewsListStomp
+ (NSString*) destination{
    return @"news";
}

+ (id)select{
    MENewsListStomp *s=[[MENewsListStomp alloc] init];
    [s selectForMarketPlace:@"COMMON"];
    [s.request setHeader:@"selector" withValue:[NSString stringWithFormat:@"language=%@", GIC_current_lang()]];
    return s;
}

@end
