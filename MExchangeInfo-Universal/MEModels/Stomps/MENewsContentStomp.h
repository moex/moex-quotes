//
//  MENewsContentStomp.h
//  MExchange
//
//  Created by denn on 10/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIStompRequests.h"

@interface MENewsContentStomp : GIRequestStompFrame
+ (id) selectByID:(NSNumber *)newsid;
@end
