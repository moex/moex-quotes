//
//  MELastNewsStomp.h
//  MExchange
//
//  Created by denn on 10/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIStompSubscriptions.h"

@interface MENewsListStomp : GIStompSelector
+ (id)select;
@end
