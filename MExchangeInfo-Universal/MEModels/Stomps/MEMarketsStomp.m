//
//  MEMarketsStomp.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMarketsStomp.h"

@implementation MEMarketsStomp

+ (NSString*) destination{
    return @"COMMON.markets";
}

+ (id) select{
    MEMarketsStomp *markets = [[MEMarketsStomp alloc] init];
    [markets  selectFor:nil];
    return markets;
}


@end
