//
//  MEMarketsStomp.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIStompRequests.h"

@interface MEMarketsStomp : GIRequestStompFrame
+ (id)select;

@end
