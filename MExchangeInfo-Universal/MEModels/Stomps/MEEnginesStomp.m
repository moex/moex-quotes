//
//  MEEnginesStomp.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEEnginesStomp.h"

@implementation MEEnginesStomp


+ (NSString*) destination{
    return @"COMMON.engines";
}

+ (id) select{
    MEEnginesStomp *o = [[MEEnginesStomp alloc] init];
    [o  selectFor:nil];
    return o;
}

@end
