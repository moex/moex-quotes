//
//  MENewsContentStomp.m
//  MExchange
//
//  Created by denn on 10/13/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MENewsContentStomp.h"

@implementation MENewsContentStomp

+ (NSString*) destination{
    return @"COMMON.news.content";
}

+ (id) selectByID:(NSNumber *)newsid{
    NSString *selector = [NSString stringWithFormat:@"ID=%@", newsid];
    MENewsContentStomp *news = [[MENewsContentStomp alloc] init];
    [news  selectFor:selector];
    return news;
}

@end

