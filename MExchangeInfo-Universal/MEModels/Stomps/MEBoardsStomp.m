//
//  MEBoardsStomp.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEBoardsStomp.h"

@implementation MEBoardsStomp

+ (NSString*) destination{
    return @"COMMON.boards";
}

+ (id) select{
    MEBoardsStomp *o = [[MEBoardsStomp alloc] init];
    [o  selectFor:nil];
    return o;
}
@end
