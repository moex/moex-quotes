//
//  MEEnginesReference.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/16/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEEnginesReference.h"

@implementation MEEnginesReference

- (void) createFromStomp:(GICommonStompFrame *)frame{
    
    for (int i=0; i<frame.body.countOfRows; i++) {
        MEEngine *engine = [[MEEngine alloc] init];
        engine.ID = [frame.body numberForIndex:i andKey:@"ID"];
        engine.name = [frame.body stringForIndex:i andKey:@"NAME"];
        engine.caption = [frame.body stringForIndex:i andKey:@"CAPTION"];
        
        [self insertObject:engine forKey:engine.ID atIndex:i];
    }
}

- (MEEngine*) findByName:(NSString *)name{
    for (id key in self) {
        MEEngine *engine = [self objectForKey:key];
        if ([engine.name isEqualToString:name]) {
            return engine;
        }
    }
    return nil;
}

@end
