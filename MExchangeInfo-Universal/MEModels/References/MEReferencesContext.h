//
//  MEReferencesContext.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/10/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEContextArchiver/MEContextArchiver.h>

#define ME_REFERENCE_PLIST_NAME @"MEReferences.plist"

@interface MEReferencesContext : MEContextArchiver

+ (id) sharedInstance;

@end
