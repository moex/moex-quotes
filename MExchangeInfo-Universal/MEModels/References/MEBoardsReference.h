//
//  MEBoardsReference.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import "MEDispatcher.h"
#import "MEBoard.h"

@interface MEBoardsReference : GIOrderedDictionary
- (void) createFromStomp:(GICommonStompFrame *)frame;
- (MEBoard*) findByName:(NSString*)name;
@end
