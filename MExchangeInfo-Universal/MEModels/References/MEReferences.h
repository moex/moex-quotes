//
//  MEEnginesReference.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import <MEContextArchiver/MEObject.h>

#import "MEMarketsReference.h"
#import "MEBoardsReference.h"
#import "MEEnginesReference.h"
#import "MEReferencesProtocol.h"
#import "MEEngine.h"

/**
 *  This is the root collection of trading engines wich remote service connected.
 */
@interface MEReferences : MEObject

/**
 *  Markets collection associated with an engine.
 */
@property(nonatomic,strong, readonly) MEEnginesReference* engines;

/**
 *  Markets collection associated with an engine.
 */
@property(nonatomic,strong, readonly) MEMarketsReference* markets;

/**
 *  Boards associated with an engine and some market.
 */
@property(nonatomic,strong, readonly) MEBoardsReference* boards;

/**
 *  Delegate events.
 */
@property(nonatomic,strong) id<MEReferencesProtocol> delegate;

/**
 *  Create singletone instance.
 *
 *  @return Reference.
 */
+ (id) sharedInstance;

/**
 *  Update reference.
 */
- (void) update;

- (void) restoreFromOldVersion:(NSError*)error;

@end
