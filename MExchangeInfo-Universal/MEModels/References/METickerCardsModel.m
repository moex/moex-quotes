//
//  METickerCardsModel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

//#import "MEMagicalRecord.h"
#import <ObjectiveRecord/ObjectiveRecord.h>
#import <ObjectiveSugar/ObjectiveSugar.h>
#import "METickerCardsModel.h"
#import "MEDispatcher.h"
#import "MEStompTickerCard.h"
#import "NSDate+MEComparing.h"
#import "GIUtils.h"
#import "GIStompBody.h"

#define DEBUG_CARDS 0

@implementation METickerCardSection
@end

@interface METickerCardsModel() <GIStompDispatcherProtocol>
@property (nonatomic, strong) MEDispatcher *dispatcher;
@property (nonatomic, retain) NSFetchedResultsController *cardsController;
@property (atomic, assign) NSInteger refreshExecuteCounter;
@property (nonatomic, readonly) NSManagedObjectContext *backgroundContext;
@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, readonly) CoreDataManager *coreDataManager;
@property (nonatomic, strong) dispatch_queue_t backgroundQueue;
@property (nonatomic, strong) dispatch_queue_t mainQueue;
@property (nonatomic, strong) NSMutableDictionary *cache;
@end

@implementation METickerCardsModel
{
    METickerCards *currentCards;
    MEStompTickerCard *tickerCardList;    
    NSError    *lastError;
}

@synthesize coreDataManager=_coreDataManager, mainContext=_mainContext, backgroundContext=_backgroundContext;

- (id)initWithTicker:(METicker *)ticker{
    self = [super init];
    
    if (self) {
        _ticker = ticker;
        _dispatcher = [MEDispatcher sharedInstance];
        _refreshExecuteCounter = 0;
    }
    
    return self;
}


- (NSMutableDictionary*) cache{
    if (!_cache) {
        _cache = [[NSMutableDictionary alloc] initWithCapacity:30];
    }
    return _cache;
}

- (dispatch_queue_t) backgroundQueue{
    if (_backgroundQueue) {
        return _backgroundQueue;
    }
    
    _backgroundQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    dispatch_queue_set_specific(_backgroundQueue, (__bridge void *)self, (__bridge void *)(_backgroundQueue), NULL);
    
    return _backgroundQueue;
}

- (dispatch_queue_t) mainQueue{
    if (_mainQueue) {
        return _mainQueue;
    }
    
    _mainQueue = dispatch_get_main_queue(); //dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    dispatch_queue_set_specific(_mainQueue, (__bridge void *)self, (__bridge void *)(_mainContext), NULL);
    
    return _mainQueue;
}

- (CoreDataManager*) coreDataManager{
    if (!_coreDataManager) {
        _coreDataManager = [[CoreDataManager alloc] init];
        _coreDataManager.modelName = @"METickerCard";
        _coreDataManager.databaseName = [NSString stringWithFormat:@"%@/MosQuotes3-%@.sqlite",[[NSLocale preferredLanguages] objectAtIndex:0], _coreDataManager.modelName];
    }
    return _coreDataManager;
}

- (NSManagedObjectContext *) backgroundContext{
    if (!_backgroundContext) {
        _backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _backgroundContext.persistentStoreCoordinator = [self.coreDataManager persistentStoreCoordinator];  
        [_backgroundContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }
    return _backgroundContext;
}

- (NSManagedObjectContext*) mainContext{
    if (!_mainContext) {
        _mainContext=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainContext.parentContext = self.backgroundContext;
    }
    return _mainContext;
}

- (NSFetchedResultsController *)cardsController {
    
    
    if (_cardsController != nil) {
    
        //dispatch_async(_backgroundQueue, ^{
            
            NSError *error;

            if (![self->_cardsController performFetch:&error]) {
                NSLog(@"Error: %@, %s:%i", error, __FILE__, __LINE__);
            }
       // });
      
        return _cardsController;
    }
    
    NSError *error;

    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MECard class])];

    request.predicate = [NSPredicate predicateWithFormat:@"ticker.tickerID = %@ AND is_hidden = 0", self.ticker.ID];
    request.sortDescriptors=@[[NSSortDescriptor sortDescriptorWithKey:@"tag_order" ascending:YES]];    
    
    _cardsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request 
                                                           managedObjectContext:self.mainContext 
                                                             sectionNameKeyPath:@"tag" 
                                                                      cacheName:self.ticker.ID];
    
    if (![_cardsController performFetch:&error]) {
        NSLog(@"Error: %@, %s:%i", error, __FILE__, __LINE__);
    }
    
    return _cardsController;
}


- (NSUInteger) sections{    
    if (lastError) return 0;
    return self.cardsController.sections.count;
}

- (NSUInteger) rowsForSection:(NSUInteger)section{    
    if (lastError) return 0;    
    return [[self.cardsController.sections objectAtIndex:section] numberOfObjects];
}

- (METickerCardSection*) sectionForSection:(NSUInteger)section{
    
    if (lastError) return 0;

    id <NSFetchedResultsSectionInfo> sectionInfo = [self.cardsController.sections objectAtIndex:section];
    METickerCardSection *cardsection = [[METickerCardSection alloc] init];
    MECard *card = [[sectionInfo objects] objectAtIndex:0];
    cardsection.name = card.tag;
    cardsection.caption = card.tag_caption;
    return cardsection;
}

- (MECard*) cardForIndexPath:(NSIndexPath*)indexPath{    
    if (lastError) return nil;
    return [self.cardsController objectAtIndexPath:indexPath];
}

- (NSArray*) itemsForCard:(MECard*)card{
    
    NSString *key = [NSString stringWithFormat:@"%@-%@", self.ticker.ID, card.name];
    
    NSArray  *items = [self.cache objectForKey:key];
    
    if (items==nil) {
        items = [[MECardItem where:[NSPredicate predicateWithFormat:@"(card.ticker.tickerID=%@) AND (card.name=%@) AND (is_hidden=0) AND (value!=%@)", self.ticker.ID, card.name, [NSNull null]] inContext:self.mainContext order:@{@"sort_order": @"ASC"}] copy];
        [self.cache setObject:items forKey:key];
    }
    
#if DEBUG_CARDS
    [items each:^(MECardItem *item) {
        NSLog(@" --- CARD ITEM[%@]: %@[%@]=%@ is hidden=%@", item.card.name, item.caption, item.type, item.value, item.is_hidden);
    }];
#endif
    
    return items;    
};

- (MECardItem *) cardItemForCard:(MECard*)card withIndex:(NSInteger)index{
    return [[self itemsForCard:card] objectAtIndex:index];
}

- (void) setupRoot{
    if (!currentCards) {
        [self.backgroundContext performBlockAndWait:^{
            currentCards = [METickerCards find:@{@"tickerID": self.ticker.ID} inContext:self.backgroundContext];
            if (!currentCards) {
                currentCards = [METickerCards createInContext:self.backgroundContext];
                currentCards.tickerID = self.ticker.ID;
                [currentCards save];
            }            
        }];
    }
}

- (void) update{

    lastError = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(willTickerCardUpdate:)]) {
        [self.delegate willTickerCardUpdate:self];
    }
    
    if ([self sections]>0) {
        NSInteger cardsToUpdated=[MECard countWhere:[NSPredicate
                                                     predicateWithFormat:@"ticker.tickerID = %@ AND updated_at<=%@", self.ticker.ID, [NSDate dateWithTimeIntervalSinceNow:-ME_TICKER_CARD_EXPIRE_TIME]] inContext:self.backgroundContext];
        
        if (cardsToUpdated==0) {
            [self.delegate didTickerCardUpdate:self];
            return;
        }        
    }
    
    dispatch_async(self.backgroundQueue, ^{
        [self setupRoot];
        
        if (!self->tickerCardList) {
            self->tickerCardList= [MEStompTickerCard selectCardList];
            self->tickerCardList.delegate = self;
            [self requestStomp:self->tickerCardList];
        }        
    });
}


- (void) requestStomp:(GIRequestStompFrame*)request{
    [self.dispatcher registerRequest:request];
}

#pragma mark ME Dispatcher handle

- (void) didFrameError:(id)frame withError:(GIError *)error{
    lastError = error;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTickerCardError:)]) {
        [self.delegate didTickerCardError:error];
    }
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    
    lastError = error;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTickerCardError:)]) {
        [self.delegate didTickerCardError:error];
    }
}

- (void) didFrameReceive:(id)frame{
    GIStompMessage *m = frame;
    
    if (frame == tickerCardList) {
        [self initializeSchema:m];
    }
    else {
        [self updateCards:m];
    }
}

#pragma mark Core Data


- (void) initializeSchema:(GIStompMessage*)m{
    
    dispatch_async(self.backgroundQueue, ^{
        
#if DEBUG_CARDS
        NSLog(@" *** initializeSchema: %@", m);
#endif
        [self.backgroundContext performBlockAndWait:^{
           
            METickerCards *cards = [METickerCards find:@{@"tickerID": self.ticker.ID} inContext:self.backgroundContext];
            
            NSLog(@" *** METickerCards: %@", cards);

            self.refreshExecuteCounter = 0;            
            for (GICommonStompFrame *c = [m.responseQueue pop]; c != nil ; c = [m.responseQueue pop]) {
                
                NSLog(@" *** METickerCards: %@", c.name);
                NSLog(@" *** METickerCards: %u", c.body.properties.type);

                if (c.body == nil ) continue;
                
                for (int i=0; i<c.body.countOfRows; i++) {
                    
                    NSString *name = [c.body stringForIndex:i andKey:@"CARDID"];
                    
                    MECard *card = [self MR_cardWithName:name inContext:self.backgroundContext withCards:cards];
                    
                    card.caption = [c.body stringForIndex:i andKey:@"CAPTION"];
                    card.tag = [c.body stringForIndex:i andKey:@"TAG_ORDER"];
                    card.tag_caption = [c.body stringForIndex:i andKey:@"TAG_CAPTION"];
                    card.tag_order = [c.body numberForIndex:i andKey:@"TAG_ORDER"];
                    
                    card.card_order = [NSNumber numberWithInt:i];
                    card.is_hidden = @YES;
                    
                    [card save];
                }
            }                                
        }];
        
        METicker *t=self.ticker;                        
        [[MECard where:[NSPredicate predicateWithFormat:@"ticker.tickerID = %@", t.ID] inContext:self.backgroundContext] each:^(MECard *card) {
#if DEBUG_CARDS
            NSLog(@" *** ### Request: %@ for %@", card.name, t.ID);
#endif
//            MEStompTickerCard *tickerSession = [MEStompTickerCard selectCard:card.name forTicker:t.ID withEngine:t.market.engine.name andMarket:t.market.name];
            MEStompTickerCard *tickerSession = [MEStompTickerCard selectCard:card.name forTicker:t.ID];
            tickerSession.delegate = self;
            [self requestStomp:tickerSession];
        }];
    });
}

- (void) updateCards:(GIStompMessage*)m{
    
    __block NSString *cardname;
    
    [[m.request.headers[@"selector"] componentsSeparatedByString:@" "] each:^(NSString *str) {
        if ([str containsString:@"cardname"]) {
            cardname = [str componentsSeparatedByString:@"="][1];
        }
    }];
    
    if (cardname==nil) {
        NSLog(@" *** Error: could not update NULL cardname...");
        return;
    }    
        
    dispatch_async(self.backgroundQueue, ^{
        __block NSString *cardID;
                
        [self.backgroundContext performBlockAndWait:^{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ticker.tickerID = %@ AND name = %@", self.ticker.ID, cardname];
            MECard *card = nil;            
            card = [MECard find:predicate inContext:self.backgroundContext];                
            NSInteger hiddens=0;

            int rows = 0;
            for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
                
                if ( IsEmpty(c.body) ) continue;
                
                for (int i=0; i<c.body.countOfRows; i++) {
                    
                    cardID=[[c.body stringForIndex:i andKey:@"CARDID"] copy];                                        
                    
                    NSString *name = [c.body stringForIndex:i andKey:@"NAME"];
                    MECardItem *itemEntity = [self MR_itemCardWithName:name inContext:self.backgroundContext withCard:card];
                    
                    itemEntity.caption = [c.body stringForIndex:i andKey:@"CAPTION"];
                    itemEntity.hint = [c.body stringForIndex:i andKey:@"HINT"];
                    itemEntity.is_hidden = [c.body numberForIndex:i andKey:@"IS_HIDDEN"];
                    itemEntity.sort_order = [c.body numberForIndex:i andKey:@"SORT_ORDER"];
                    itemEntity.precision = [c.body numberForIndex:i andKey:@"PRECISION"];
                    
                    NSString *type = [c.body stringForIndex:i andKey:@"TYPE"];
                    
                    itemEntity.type = type;
                                        
                    if ([itemEntity.type isEqualToString:@"date"]) 
                        itemEntity.value = [NSDate dateFromString:[c.body stringForIndex:i andKey:@"VALUE"]];                    
                    else if ([type isEqualToString:@"number"]){
                        itemEntity.value = [c.body numberForIndex:i andKey:@"VALUE"];
                    }
                    else
                        itemEntity.value = [[c.body valueForIndex:i andKey:@"VALUE"] valueOfObject];
                    
                    if ([itemEntity.value isEqual:[NSNull null]] || itemEntity.value==nil) 
                        itemEntity.is_hidden=@1;
                    
                    itemEntity.valueInfo = [c.body stringForIndex:i andKey:@"VALUE_INFO"];
                    itemEntity.updated_at = [NSDate date];
                    
                    [itemEntity save];
                    
                    if (itemEntity.is_hidden==nil) {
                        hiddens++;
                    }
                    else if (itemEntity.is_hidden.boolValue==YES)                                                                     
                        hiddens++;
                    
                    rows++;
                }
            }
            
            card.updated_at = [NSDate date];
            
            if (rows>0){
                if (rows==hiddens) 
                    card.is_hidden=@YES;
                else
                    card.is_hidden = @NO;
            }
            else 
                card.is_hidden = @YES;            
            
            [card save];            
        }];
        
        self.refreshExecuteCounter++;
        
        //
        // all cards can be responsd
        //
        NSInteger allCards=[MECard countWhere:[NSPredicate predicateWithFormat:@"ticker.tickerID = %@", self.ticker.ID] inContext:self.backgroundContext];
        
        //
        // cards has been updated at the moment
        //
        NSInteger cardsUpdated=[MECard countWhere:[NSPredicate 
                                                   predicateWithFormat:@"ticker.tickerID = %@ AND updated_at>=%@", self.ticker.ID, [[NSDate date] dateOnly]] inContext:self.backgroundContext];
                
        if (cardsUpdated>=allCards) {
#if DEBUG_CARDS
            [self.backgroundContext performBlockAndWait:^{
                [[MECard where:[NSPredicate predicateWithFormat:@"ticker.tickerID = %@", self.ticker.ID]  inContext:self.backgroundContext] each:^(MECard *item) {
                    if (item.items.count==0 || item.is_hidden.boolValue==YES) {
                        NSLog(@" --- CARD[%@]: count ->%lu is hidden=%@", item.name, (unsigned long)item.items.count, item.is_hidden);            
                        //[item delete];
                    }
                    else {
                        NSLog(@" +++ CARD[%@]: count ->%lu is hidden=%@", item.name, (unsigned long)item.items.count, item.is_hidden);            
                        [[MECardItem where:[NSPredicate predicateWithFormat:@"card.ticker.tickerID = %@ AND card.name = %@", self.ticker.ID, item.name] inContext:self.backgroundContext] each:^(MECardItem *object) {
                                NSLog(@"    ^--- CITEM[%@]: count ->%@/%@ is hidden=%@ updated at=%@", object.name, object.caption, object.value, object.is_hidden, object.updated_at);
                        }];
                    }
                }];    
                [self.backgroundContext save:nil];
            }];
#endif
            [self.delegate didTickerCardUpdate:self];
        }        
    });
}

- (MECard*) MR_cardWithName:(NSString*)cardName inContext:(NSManagedObjectContext*)context withCards:(METickerCards*)cards{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ticker.tickerID = %@ AND name = %@", self.ticker.ID, cardName];
    __block MECard *card = [MECard find:predicate inContext:context];    
    [context performBlockAndWait:^{
        if (!card) {
            card = [MECard createInContext:context];
            card.ticker = cards;
            card.name = cardName;
        }                
    }];
    return card;
}

- (MECardItem*) MR_itemCardWithName:(NSString*)name inContext:(NSManagedObjectContext*)context withCard:(MECard*) card{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"card.ticker.tickerID = %@ AND card.name = %@ AND name = %@", _ticker.ID, card.name, name];    
    __block MECardItem *itemEntity = [MECardItem find:predicate inContext:context];    
    [context performBlockAndWait:^{
        if (!itemEntity) {
            itemEntity = [MECardItem createInContext:context];
            itemEntity.name = name;
            itemEntity.card = card;
        }        
    }];
    
    return itemEntity;
}

- (void) truncate{    
    dispatch_async(self.backgroundQueue, ^{
        [self.backgroundContext performBlockAndWait:^{        
            [MECardItem deleteAllInContext:self.backgroundContext];            
            [MECard deleteAllInContext:self.backgroundContext];
            [METickerCards deleteAllInContext:self.backgroundContext];
            [self.backgroundContext save:nil];                
        }];
        
        [self.coreDataManager saveContext];
    });            
}

@end
