//
//  METickerCardsModel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//
// TODO: try to migrate to MagicalRecord: https://github.com/magicalpanda/MagicalRecord/tree/release/3.0
//

#import <Foundation/Foundation.h>
#import "METickerCards.h"
#import "MECard.h"
#import "MECardItem.h"
#import "METicker.h"

#define ME_TICKER_CARD_EXPIRE_TIME 3600

@class METickerCardsModel;

@interface METickerCardSection : NSObject
@property(nonatomic,strong) NSString *caption, *name;
@end

@protocol METickerCardsModelDelegate <NSObject>

- (void) didTickerCardUpdate: (METickerCardsModel*)tickerCardsModel;

@optional
- (void) didTickerCardError: (NSError*)error;
- (void) willTickerCardUpdate: (METickerCardsModel*)tickerCardsModel;
- (void) didTickerCardItemUpdate: (MECard*)tickerCard;
@end

@protocol METickerCardsItemDelegate <NSObject>
@optional
- (void) didTickerCardItemUpdate: (MECard*)tickerCard;
@end


@interface METickerCardsModel : NSObject

- (id) initWithTicker:(METicker*) ticker;

@property (atomic,readonly) METicker *ticker;
@property id<METickerCardsModelDelegate> delegate;
@property id<METickerCardsItemDelegate> itemsDelegate;

- (void) update;
- (void) truncate;

- (NSUInteger) sections;
- (NSUInteger) rowsForSection:(NSUInteger)section;
- (METickerCardSection*) sectionForSection:(NSUInteger)section;
- (MECard*) cardForIndexPath:(NSIndexPath*)indexPath;
- (MECardItem *) cardItemForCard:(MECard*)card withIndex:(NSInteger)index;
- (NSArray*) itemsForCard:(MECard*)card;

@end
