//
//  MEBoardsReference.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEBoardsReference.h"
#import "MEBoardsStomp.h"
#import "MEBoard.h"
#import "MEReferences.h"

@interface MEBoardsReference() //<GIStompDispatcherProtocol>
@end

@implementation MEBoardsReference

- (id) init{
    
        self = [super init];
        
        if (self) {
        }
        
        return self;
}

- (void) createFromStomp:(GICommonStompFrame *)frame{
    
    for (int i=0; i<frame.body.countOfRows; i++) {
        
        MEBoard *board = [[MEBoard alloc] init];
        board.ID = [frame.body numberForIndex:i andKey:@"ID"];
        board.name = [frame.body stringForIndex:i andKey:@"NAME"];
        board.caption = [frame.body stringForIndex:i andKey:@"CAPTION"];
        
        board.isPrimary = [[frame.body numberForIndex:i andKey:@"IS_PRIMARY"] boolValue];
        
        board.marketID = [frame.body numberForIndex:i andKey:@"MARKETID"];
        board.engineID = [frame.body numberForIndex:i andKey:@"ENGINEID"];
        
        [self insertObject:board forKey:board.ID atIndex:i];
    }
}

- (MEBoard*) findByName:(NSString *)name{
    for (id key in self) {
        MEBoard *board = [self objectForKey:key];
        if ([board.name isEqualToString:name]) {
            return board;
        }
    }
    return nil;
}

@end
