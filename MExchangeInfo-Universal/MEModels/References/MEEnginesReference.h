//
//  MEEnginesReference.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/16/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import "MEDispatcher.h"
#import "MEObject.h"
#import "MEEngine.h"

@interface MEEnginesReference : GIOrderedDictionary
- (void) createFromStomp:(GICommonStompFrame *)frame;
- (MEEngine*) findByName:(NSString*)name;
@end
