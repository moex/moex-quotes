//
//  MECard.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MECardItem, METickerCards;

@interface MECard : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * tag;
@property (nonatomic, retain) NSString * tag_caption;
@property (nonatomic, retain) NSNumber * tag_order;
@property (nonatomic, retain) NSNumber * card_order;
@property (nonatomic, retain) NSNumber * is_hidden;
@property (nonatomic, retain) NSSet *items;
@property (nonatomic, retain) METickerCards *ticker;
@property (nonatomic, retain) NSDate *updated_at;
@end

@interface MECard (CoreDataGeneratedAccessors)

- (void)addItemsObject:(MECardItem *)value;
- (void)removeItemsObject:(MECardItem *)value;
- (void)addItems:(NSSet *)values;
- (void)removeItems:(NSSet *)values;

@end
