//
//  MECardItem.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/13/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MECardItem.h"
#import "MECard.h"


@implementation MECardItem

@dynamic caption;
@dynamic hint;
@dynamic is_hidden;
@dynamic name;
@dynamic precision;
@dynamic sort_order;
@dynamic type;
@dynamic updated_at;
@dynamic value;
@dynamic valueInfo;
@dynamic card;


@end
