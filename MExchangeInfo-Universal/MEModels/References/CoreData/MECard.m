//
//  MECard.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/12/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MECard.h"
#import "MECardItem.h"
#import "METickerCards.h"


@implementation MECard

@dynamic caption;
@dynamic name;
@dynamic tag;
@dynamic tag_order;
@dynamic tag_caption;
@dynamic card_order;
@dynamic is_hidden;
@dynamic items;
@dynamic ticker;
@dynamic updated_at;

@end
