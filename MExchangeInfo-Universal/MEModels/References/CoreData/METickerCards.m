//
//  METickerCards.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METickerCards.h"
#import "MECard.h"


@implementation METickerCards

@dynamic tickerID;
@dynamic cards;

@end
