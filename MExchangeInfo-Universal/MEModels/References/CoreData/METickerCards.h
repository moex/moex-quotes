//
//  METickerCards.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/11/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MECard;

@interface METickerCards : NSManagedObject

@property (nonatomic, retain) NSString * tickerID;
@property (nonatomic, retain) NSSet *cards;
@end

@interface METickerCards (CoreDataGeneratedAccessors)

- (void)addCardsObject:(MECard *)value;
- (void)removeCardsObject:(MECard *)value;
- (void)addCards:(NSSet *)values;
- (void)removeCards:(NSSet *)values;

@end
