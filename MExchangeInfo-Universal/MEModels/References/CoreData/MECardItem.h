//
//  MECardItem.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 2/13/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MECard;

@interface MECardItem : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSString * hint;
@property (nonatomic, retain) NSNumber * is_hidden;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * precision;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSObject* value;
@property (nonatomic, retain) NSString* valueInfo;
@property (nonatomic, retain) MECard *card;

@end
