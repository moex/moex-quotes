//
//  MEEnginesReference.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 07.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEReferences.h"
#import "MEEngine.h"
#import "MEDispatcher.h"
#import "MEEnginesStomp.h"
#import "MEReferencesContext.h"
#import "MEBoardsReference.h"
#import "MEBoardsStomp.h"
#import "MEReferencesContext.h"
#import "MEMarket.h"
#import "MEMarketsStomp.h"
#import "NSDate+MEComparing.h"
#import "MEErrors.h"

static MEReferences* __shared_instance = nil;

@interface MEReferences() <GIStompDispatcherProtocol>
@property(nonatomic,strong) MEEnginesReference* engines;
@property(nonatomic,strong) MEMarketsReference* markets;
@property(nonatomic,strong) MEBoardsReference* boards;
@end

@implementation MEReferences
{
    MEDispatcher *dispatcher;
}

#pragma MEContextArcgiver protocol
- (NSDictionary*) excludePropertyNames{
    return @{@"delegate": @NO};
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEReferences alloc] init];
    });
    return __shared_instance;
}


- (void) update{
    
     if (self.delegate){
         [self.delegate performSelector:@selector(willReferenceUpdate:) withObject:self];
     }

    self.engines = [[MEEnginesReference alloc] init];

    [[MEReferencesContext sharedInstance] restoreObject:self withContextName:NSStringFromClass([MEReferences class]) ifLastSaveIsUptoday:1];
        
    if (self.engines.count==0) {
        //
        // Request engines
        //

        MEEnginesStomp *_enginesStomp = [MEEnginesStomp select];
        [dispatcher registerRequest:_enginesStomp];
        _enginesStomp.delegate = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreFromOldVersionOnTimeOut) object:nil];
            [self performSelector:@selector(restoreFromOldVersionOnTimeOut) withObject:nil afterDelay:self->dispatcher.config.connectionTimeout];
        });
    }
    else if (self.delegate){
        dispatch_async(dispatch_get_main_queue(), ^{
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreFromOldVersionOnTimeOut) object:nil];
        });
        [self.delegate performSelector:@selector(didReferenceUpdate:) withObject:self];
    }
}

- (void) restoreFromOldVersionOnTimeOut{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreFromOldVersionOnTimeOut) object:nil];
    });

    NSError *nerror = [NSError
                       errorWithDomain:GI_ERROR_DOMAIN_PREFIX
                       code:GI_CONNECTION_TIMEOUT
                       userInfo:@{
                                  NSLocalizedDescriptionKey: [NSString stringWithFormat: NSLocalizedString(@"Application could not update references because of a problem with the network. Last update was %i days ago.", @""), 1],
                                  NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Application could not update references.", @"")
                                  }
                       ];
    
    [self restoreFromOldVersion:nerror];
}

- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        dispatcher = [MEDispatcher sharedInstance];
    }
    
    return self;
}

- (void) restoreFromOldVersion:(NSError*)error{
    NSTimeInterval interval = 0.0;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreFromOldVersionOnTimeOut) object:nil];
    });

    [[MEReferencesContext sharedInstance] restoreObject:self withContextName:NSStringFromClass([MEReferences class]) withLastTimeSave:&interval];
    NSInteger uptoday = [NSDate howManyDaysDifferenceBetween:[NSDate date] and:[NSDate dateWithTimeIntervalSince1970:interval]];

    if (dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"MEReferences Error: %@ uptoday=%li/%f prevdate=%@ today=%@", error.localizedDescription, (long)uptoday, interval, [NSDate dateWithTimeIntervalSince1970:interval], [NSDate date]);
    }        
    
    if (_engines.count==0) {
        //
        // Request engines
        //
        if (self.delegate && [self.delegate respondsToSelector:@selector(didReference:fault:withLastUpdateTime:)]) {
            [self.delegate didReference:self fault:error withLastUpdateTime:[NSDate dateWithTimeIntervalSince1970:interval]];
        }
    }
    else if (self.delegate){
        
        [self.delegate performSelector:@selector(didReferenceUpdate:) withObject:self];
        NSError *nerror = [NSError
                           errorWithDomain:error.domain
                           code:error.code
                           userInfo:@{
                                      NSLocalizedDescriptionKey: [NSString stringWithFormat: NSLocalizedString(@"Application could not update references because of a problem with the network. Last update was %i days ago.", @""), uptoday],
                                      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Application could not update references.", @"")
                                      }
                           ];
        
        if ([self.delegate respondsToSelector:@selector(didReference:fault:withLastUpdateTime:)]) {
            [self.delegate didReference:self fault:nerror withLastUpdateTime:[NSDate dateWithTimeIntervalSince1970:interval]];
        }
    }
}

- (void) requestMarketsReference{
    self.markets = [[MEMarketsReference alloc] init];
    
    MEMarketsStomp *_marketStomp = [MEMarketsStomp select];
    [dispatcher registerRequest:_marketStomp];
    _marketStomp.delegate = self;
}

- (void) requestBoardsReference{
    self.boards = [[MEBoardsReference alloc] init];
    
    MEBoardsStomp *_boardstopm = [MEBoardsStomp select];
    [dispatcher registerRequest:_boardstopm];
    _boardstopm.delegate = self;
}

- (void) didFrameError:(id)frame withError:(GIError *)error{
    [self restoreFromOldVersion:error];
}

- (void) didFrameInterrupt:(id)frame withError:(GIError *)error{
    [self restoreFromOldVersion:error];
}

- (void) willFrameSend:(id)frame{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willReferenceUpdate:)]) {
        [self.delegate willReferenceUpdate:self];
    }
}

- (void) didFrameReceive:(id)frame{
    
    GIStompMessage *m = frame;
    
    if ([frame isKindOfClass:[MEEnginesStomp class]]) {
        [self.engines createFromStomp:m.lastResponse];
        
        //
        // Request markets reference when engines has been received.
        //
        
        [self requestMarketsReference];        
    }
    else if ([frame isKindOfClass:[MEMarketsStomp class]]){
        [self.markets createFromStomp:m.lastResponse];
        
        //
        // Then boards
        //
        [self requestBoardsReference];
        
    }
    else if ([frame isKindOfClass:[MEBoardsStomp class]]){
        
        [self.boards createFromStomp:m.lastResponse];
        
        
        @synchronized(self){
            //
            // After all save context.
            //
            
            NSError *error;
            [[MEReferencesContext sharedInstance] saveObject:self withContextName:NSStringFromClass([MEReferences class]) error:&error];
            
            if (dispatcher.loglevel>=GI_STOMP_LOG_DEBUG) {
                NSLog(@"MEReferenses Saved: %@", self);
            }

            if (error) {
                NSLog(@"MEReferencesContext error: %@, %s:%i", error, __FILE__, __LINE__);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreFromOldVersionOnTimeOut) object:nil];
            });
            
            
            if (self.delegate) {
                [self.delegate performSelector:@selector(didReferenceUpdate:) withObject:self];
            }
        }
    }
}

- (NSString*) description{
    return [NSString stringWithFormat:@"Engines: %@\nMarkets: %@\nBoards: %@", self.engines, self.markets, self.boards];
}

@end
