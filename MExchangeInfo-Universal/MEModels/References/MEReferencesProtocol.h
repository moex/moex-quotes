//
//  MEReferencesProtocol.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/11/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MEReferences;

@protocol MEReferencesProtocol <NSObject>

- (void) willReferenceUpdate:(MEReferences*)reference;
- (void) didReferenceUpdate:(MEReferences*)reference;

@optional
- (void) didReference:(MEReferences*)reference fault:(NSError*)error withLastUpdateTime:(NSDate*)upDateTime;

@end
