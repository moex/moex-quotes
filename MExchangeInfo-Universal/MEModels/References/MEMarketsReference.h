//
//  MEMarketsReference.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <MEinfoCXConnector/GIOrderedDictionary.h>
#import "MEDispatcher.h"
#import "MEMarket.h"

@interface MEMarketsReference : GIOrderedDictionary
- (void) createFromStomp:(GICommonStompFrame *)frame;
- (MEMarket*) findByName:(NSString*)name andEingineID:(NSNumber*)engineID;
@end
