//
//  MEReferencesContext.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/10/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEReferencesContext.h"
#import "NSDate+MEComparing.h"

@implementation MEReferencesContext

+ (id) sharedInstance{
    return [MEContextArchiver sharedInstanceWithFile:ME_REFERENCE_PLIST_NAME];
}

@end
