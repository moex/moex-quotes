//
//  MEMarketsReference.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/9/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMarketsReference.h"
#import "MEMarketsStomp.h"
#import "MEReferences.h"
#import "MEMarket.h"

@implementation MEMarketsReference


- (id) init{
    
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) createFromStomp:(GICommonStompFrame *)frame{
    
    for (int i=0; i<frame.body.countOfRows; i++) {
        MEMarket *market = [[MEMarket alloc] init];
        market.ID = [frame.body numberForIndex:i andKey:@"ID"];
        market.name = [frame.body stringForIndex:i andKey:@"NAME"];
        market.caption = [frame.body stringForIndex:i andKey:@"CAPTION"];
        market.engineID = [frame.body numberForIndex:i andKey:@"ENGINEID"];
        [self insertObject:market forKey:market.ID atIndex:i];
    }
}

- (MEMarket*) findByName:(NSString *)name andEingineID:(NSNumber *)engineID{
    for (id key in self) {
        MEMarket *market = [self objectForKey:key];
        if ([market.name isEqualToString:name] && [market.engineID isEqualToNumber:engineID]) {
            return market;
        }
    }
    return nil;
}

@end
