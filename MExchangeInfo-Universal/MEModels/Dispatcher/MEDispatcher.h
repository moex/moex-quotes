//
//  MEDispatcher.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <MEinfoCXConnector/GIStompDispatcher.h>
#import "MEConfig.h"

#define ME_DISPATCHER_WILL_CONNECT @"ME_DISPATCHER_WILL_CONNECT"
#define ME_DISPATCHER_STOPPED_EVENT @"ME_DISPATCHER_STOPPED_EVENT"
#define ME_DISPATCHER_ERROR_EVENT @"ME_DISPATCHER_ERROR_EVENT"

@interface MEDispatcher : GIStompDispatcher
@property(nonatomic,strong,readonly) MEConfig *config;
+ (id) sharedInstance;
- (void) reconnect;
@end
