//
//  MEDispatcher.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEDispatcher.h"
#import "MEConfig.h"
#import "MESettings.h"
#import "MEAlertsView.h"
#import "MEUIPreferences.h"
#import <Crashlytics/Crashlytics.h>

#define ME_ALLERT_CLOSE NSLocalizedString(@"Close button", @"Allert close button text")
#define ME_ALLERT_RECONNECT NSLocalizedString(@"Reconnect button", @"Allert reconnect button text")

static MEDispatcher *__shared_instance = nil;

@interface MEDispatcher() <GIStompEventDispatchHandler,GIStompLoginDispatchHandler,GIStompErrorDispatchHandler>
@property (atomic,assign) BOOL isReconnecting;
@property (atomic,assign) NSUInteger reconnectingCounter;
@end

@implementation MEDispatcher
{
    UIAlertView *alert;
    NSTimer *connectionWatchDogTimer;
    NSTimer *pongWatchDogTimer;
    BOOL    isModeSwitching;
    MESettings *settings;
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEDispatcher alloc] initWithConfig:[MEConfig sharedConfig]];
    });
    return __shared_instance;
}

- (void) activateNetworkIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
}

- (void) deActivateNetworkIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
}

- (id) initWithConfig:(GIConfig *)aConfig{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super initWithConfig:aConfig];
    
    if (self) {
    
        settings = [MESettings sharedInstance];

#ifdef DEBUG
        self.loglevel = GI_STOMP_LOG_DEBUG;
#else
        self.loglevel = GI_STOMP_LOG_INFO;
#endif
        
        _config = (MEConfig*)aConfig;

        _config.connectionTimeout = 60;
        _config.reconnectCount = 100;
        _config.readTimeout = 180;
        _config.reconnectTimeout = 3;

        self.reconnectingCounter = 0;
        self.eventHandler = self;
        self.errorHandler = self;
        self.loginHandler = self;
        self.hasPermanentConnection = YES;
        self.deferredTime = 30.0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backgroundHanlder:) name:UIApplicationDidEnterBackgroundNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(foregroundHanlder:) name:UIApplicationWillEnterForegroundNotification object:nil];
            
        });
    }
    
    return self;
}

- (void) backgroundHanlder:(NSNotification*)event{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0)), dispatch_get_main_queue(), ^{
        
        self->isModeSwitching = YES;
        
        if ([self->settings.closeInActiveConnection boolValue]) {
            if (self->connectionWatchDogTimer){
                [self->connectionWatchDogTimer invalidate];
                self->connectionWatchDogTimer = nil;
            }
        }
    });
}

- (void) foregroundHanlder:(NSNotification*)event{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.config.reconnectTimeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->isModeSwitching = NO;
    });
}

- (void) checkConnection:(NSTimer*)timer{
    
    if (!self.isRunning) {
        

        if (isModeSwitching == NO){
            [[MEAlertsView sharedInstance] pushMessageText: NSLocalizedString(@"Dispatcher connection closed", @"Connection closed") withStatus:ME_ALERT_FATAL];
        }
        [connectionWatchDogTimer invalidate];
        connectionWatchDogTimer = nil;
        [self reconnect];
    }
}

- (void) pongConnection:(NSTimer*)timer{
    [pongWatchDogTimer invalidate];
    [self reconnect];
}


- (void) reconnect{
    [self activateNetworkIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self disconnect];
        sleep(1);
        [self connect];
        self.isReconnecting = NO;
    });
}

- (void) alertMessage:(NSString*)text withTitle:(NSString*)title{
    
    if (self.isReconnecting) {
        return;
    }
    
    self.isReconnecting = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self->alert) {
            self->alert.hidden = YES;
        }
        
        self->alert = [[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:ME_ALLERT_CLOSE otherButtonTitles:ME_ALLERT_RECONNECT, nil];
        self->alert.cancelButtonIndex = 0;
        [self->alert show];
    });
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ME_DISPATCHER_ERROR_EVENT object:nil];
        return;
    }
    [self reconnect];
}

- (void) channelReconnecting{
    [self activateNetworkIndicator];
    self.isReconnecting = YES;
    self.reconnectingCounter++;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self->isModeSwitching == NO){
            if (self.reconnectingCounter>=10){
                [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Connection problem", @"") withStatus:ME_ALERT_FATAL];
                self.reconnectingCounter = 0;
            }
            else {
                [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Channel reconnecting event", @"") withStatus:ME_ALERT_WARN];
            }
        }
    });
}

- (void) channelConnectionRestored{
    [self deActivateNetworkIndicator];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->isModeSwitching == NO){
            [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Connection restored", @"") withStatus:ME_ALERT_NETWORK_OK];
        }
    });
    if (connectionWatchDogTimer) {
        [connectionWatchDogTimer invalidate];
        connectionWatchDogTimer = nil;
    }
}

- (void) channelClosed{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Connection transport closed", @"") withStatus:ME_ALERT_NETWORK_LOST];
    });
}

- (void) dispatcherWillStop{
    [self activateNetworkIndicator];
    [connectionWatchDogTimer invalidate];
    connectionWatchDogTimer = nil;
}

- (void) dispatcherDidStop{
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_DISPATCHER_STOPPED_EVENT object:nil];


    [[MEAlertsView sharedInstance] pushMessageText: NSLocalizedString(@"Dispatcher connection closed", @"Connection closed") withStatus:ME_ALERT_WARN];
    
    [self deActivateNetworkIndicator];
}

- (void) channelWillActive{
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_DISPATCHER_WILL_CONNECT object:nil];
    [self activateNetworkIndicator];
}

- (void) channelOpened{    
    [self activateNetworkIndicator];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Connection opened", @"Connection opened") withStatus:ME_ALERT_NETWORK_OK];
    });
    
    if (connectionWatchDogTimer){
        [connectionWatchDogTimer invalidate];
        connectionWatchDogTimer = nil;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.config.reconnectTimeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->connectionWatchDogTimer = [NSTimer scheduledTimerWithTimeInterval:self.config.reconnectTimeout target:self selector:@selector(checkConnection:) userInfo:nil repeats:YES];
    });
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    
    if (pongWatchDogTimer) {
        [pongWatchDogTimer invalidate];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.config.reconnectTimeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->pongWatchDogTimer = [NSTimer scheduledTimerWithTimeInterval:self.config.connectionTimeout target:self selector:@selector(checkConnection:) userInfo:nil repeats:YES];
    });

    if (roundTrip>=2){
        [self activateNetworkIndicator];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Too slow connection", @"Too slow connection") withStatus:ME_ALERT_WARN_NEED_SHOW];
        });
    }
    else if (roundTrip<0){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[MEAlertsView sharedInstance] pushMessageText:NSLocalizedString(@"Connection heartbeat problem", @"Ping connection interupted") withStatus:ME_ALERT_WARN];
        });
    }
    else{
        [self deActivateNetworkIndicator];
    }
}

- (void) didStompError:(GIError *)error{
    [self activateNetworkIndicator];
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_DISPATCHER_ERROR_EVENT object:error];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_FATAL];
    });
}

- (void) didSystemError:(GIError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{

        if (self->isModeSwitching == NO) {
            [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_FATAL];
        }
    });
    
    if (error.code == GICSP_ERROR_RETRIES_LIMIT || error.code == GICSP_ERROR_CONNECTION_FAIL || error.code == GICSP_ERROR_CERT_INVALID){

        [self deActivateNetworkIndicator];
        [[NSNotificationCenter defaultCenter] postNotificationName:ME_DISPATCHER_STOPPED_EVENT object:error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertMessage:error.localizedDescription withTitle:error.localizedFailureReason];
        });
    }        
}

- (BOOL) validateEmail: (NSString*)email{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:email] == YES)
        return YES;
    return NO;
}


- (void) didStompLogin:(NSDictionary *)connection{
    
    self.config.isAuthorized = YES;
    self.isReconnecting = NO;
    
    [self deActivateNetworkIndicator];
    
    GIAuth *auth = [connection valueForKey:@"login"];
    
    NSMutableString *login_mess = [NSMutableString stringWithFormat: NSLocalizedString(@"You connected as: $user$ to domain: $domain$", @"Login connection info")];
    [login_mess replaceOccurrencesOfString:@"$user$" withString:auth.user options:NSCaseInsensitiveSearch range:NSMakeRange(0, login_mess.length)];
    [login_mess replaceOccurrencesOfString:@"$domain$" withString:auth.domain options:NSCaseInsensitiveSearch range:NSMakeRange(0, login_mess.length)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEAlertsView sharedInstance] pushMessageText:login_mess withStatus:ME_ALERT_WARN];
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_LOGIN_SUCCESS_NOTIFICATION object:nil];
    
    NSString *loginMethod = [self validateEmail:auth.user] ? @"Email":@"Nickname";
    
    [Answers logLoginWithMethod:loginMethod
                        success:@YES
               customAttributes:@{
                                  @"Auth Domain" : auth.domain
                                  }];
}

- (void) didStompLoginError:(GIError *)error withConnectionResponse:(GICommonStompFrame *)response{    
    self.config.isAuthorized = NO;
    
    [connectionWatchDogTimer invalidate];
    connectionWatchDogTimer = nil;
    
    [pongWatchDogTimer invalidate];
    
    [self disconnect];
    
    [self deActivateNetworkIndicator];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[MEAlertsView sharedInstance] pushMessageText:error.localizedDescription withStatus:ME_ALERT_FATAL];
    });
    
    if (error.code == GICSP_ERROR_RETRIES_LIMIT) {
        self.isReconnecting = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertMessage:error.localizedDescription withTitle:error.localizedFailureReason];
        });
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ME_LOGIN_ERROR_NOTIFICATION object:error];
    
    GIAuth *auth = self.config.login;

    NSString *loginMethod = [self validateEmail:auth.user] ? @"Email":@"Nickname";
    
    [Answers logLoginWithMethod:loginMethod
                        success:@NO
               customAttributes:@{
                                  @"Auth Domain" : auth.domain,
                                  @"Error": error.localizedDescription
                                  }];
}

@end
