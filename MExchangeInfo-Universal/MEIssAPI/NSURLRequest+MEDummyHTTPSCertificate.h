//
//  NSURLRequest+MEDummyHTTPSCertificate.h
//  MExchange
//
//  Created by denn on 6/3/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

//#if DEBUG

@interface NSURLRequest (MEDummyHTTPSCertificate)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

//#endif
