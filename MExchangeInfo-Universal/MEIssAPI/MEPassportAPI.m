//
//  MEPassportAPI.m
//  MExchange
//
//  Created by denn on 6/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEPassportAPI.h"
#import "MEConfig.h"
//#import "NSURLRequest+MEDummyHTTPSCertificate.h"

#if DEBUG || ADHOC

@interface NSURLRequest (DummyInterface)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

@implementation NSURLRequest (DummyInterface)
+ (BOOL) allowsAnyHTTPSCertificateForHost:(NSString *)host{
    NSLog(@"DEBUG! Allow any cert for host: %@", host);
    return YES;
}
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host{}
@end

#endif


static NSString * AFBase64EncodedStringFromString(NSString *string) {
    NSData *data = [NSData dataWithBytes:[string UTF8String] length:[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}


@interface MEPassportAPI()

@property (nonatomic) GIConfig *config;

@end

@implementation MEPassportAPI

- (id) init{
    
    self = [super init];
    
    if (self) {
        _config = [MEConfig sharedConfig];
    }
    
    return self;
}



- (BOOL) makeRequestWithJson: (NSString*) jsonString resourceURL: (NSURL*) url withMethod: (NSString*)method{
    
    
    //
    // just convert to UTF8 data stream will insert to HTTP-POST-BODY
    //
    NSData *myRequestData = [ NSData dataWithBytes: [ jsonString UTF8String ] length: [ jsonString length ] ];
    
    //
    // Prepare a request object to optioanal registration URL, i.e.: https://passport.mciex.ru/<lang>/registration
    // lang may be taken from the follow list: ru,en
    //
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:_config.connectionTimeout];
    
    //
    // Set basic authentication
    //
    
    NSString *basicAuthCredentials = [NSString stringWithFormat:@"%@:%@", _config.login.user,_config.login.password];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", AFBase64EncodedStringFromString(basicAuthCredentials)];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    //
    // configure request to send the data apply POST method
    //
    [request setHTTPMethod: method];
    
    //
    // choose application/json content-type option
    //
    [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    
    //
    // append json-body
    //
    [request setHTTPBody: myRequestData];
    
    
    NSHTTPURLResponse *response;
    NSError *err;
    //
    // send request
    //
    
    NSData *returnData;
    
    returnData = [ NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    
    NSLog(@" *** MEPassport[%li]: %@: %@", (long)response.statusCode, [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding], err);
    
    //
    // errors processing
    // error returns as a json array
    // any row contains two columns: [<form-field-key>, utf8-string-with error description]
    //
    if (err || response.statusCode>302) {
        
        if (err) {
            NSMutableArray *errors = [[NSMutableArray alloc] init];
            [errors addObject:err];
            [self.delegate performSelector:@selector(passport:onErrors:) withObject:self withObject:errors];
        }
        else{
            NSMutableArray *err_errors = [[NSMutableArray alloc] init];
            if (response.statusCode==403) {
                NSError *error = [NSError
                                        errorWithDomain:@"http.passport"
                                        code:response.statusCode
                                        userInfo: @{ NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Passport connection error", @"Passport connection error")],
                                                     NSLocalizedDescriptionKey: NSLocalizedString(@"Passport connection forbidden", @""),
                                                     }
                                        ];
                [err_errors addObject:error];
            }
            else if (response.statusCode>=500){
                NSError *error = [NSError
                                  errorWithDomain:@"http.passport"
                                  code:response.statusCode
                                  userInfo: @{ NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Passport connection error", @"Passport connection error")],
                                               NSLocalizedDescriptionKey: NSLocalizedString(@"Passport connection error. Server has returned wrong code", @""),
                                               }
                                  ];
                [err_errors addObject:error];
            }
            else{
                NSError *errjson;
                NSArray *errors = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingAllowFragments error:&errjson];
                
                
                for (NSArray *error in errors) {
                    NSString *errfield = error[0];
                    NSString *errmess  = error[1];
                    
                    NSError *field_error = [NSError
                                            errorWithDomain:@"http.passport"
                                            code:response.statusCode
                                            userInfo: @{ NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Filed %@", @"Passport reason filed is wrong"), errfield],
                                                         NSLocalizedDescriptionKey: errmess,
                                                         }
                                            ];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(passport:onInputName:withError:)]) {
                        [self.delegate passport:self onInputName:errfield withError:field_error];
                    }
                    [err_errors addObject:field_error];
                }
            }
            [self.delegate performSelector:@selector(passport:onErrors:) withObject:self withObject:err_errors];
        }
        
        return NO;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(passport:onComplete:)]) {
        [self.delegate performSelector:@selector(passport:onComplete:) withObject:self withObject:nil];
    }
    
    return YES;
}

- (BOOL) registryUser: (NSString*)username withEmail:(NSString*)email andPassword:(NSString*)password{
    
    //
    // Prepare json to request add new user
    //
    NSString *jsonString = [NSString stringWithFormat:@" { \"user\": { \"nickname\": \"%@\", \"email\": \"%@\", \"password\": \"%@\", \"password_confirmation\": \"%@\"} }", username, email, password,password];
    
    return [self makeRequestWithJson:jsonString resourceURL:[[MEConfig sharedConfig]registrationUrl] withMethod:@"POST"];
}

- (BOOL) recoveryPassword: (NSString*) credentials{
    NSString *jsonString = [NSString stringWithFormat:@" { \"user\": { \"credentials\": \"%@\"} }", credentials];
    return [self makeRequestWithJson:jsonString resourceURL:[[MEConfig sharedConfig]passwordRecoveryUrl] withMethod:@"POST"];
}

- (BOOL) changePassword: (NSString*)old withNewPassword:(NSString*)newPassword{
    NSString *jsonString = [NSString stringWithFormat:@" { \"user\": { \"current_password\": \"%@\", \"password\": \"%@\", \"password_confirmation\": \"%@\" } }", old, newPassword, newPassword];
    return [self makeRequestWithJson:jsonString resourceURL:[[MEConfig sharedConfig]passwordChangeUrl] withMethod: @"PUT"];
}

@end
