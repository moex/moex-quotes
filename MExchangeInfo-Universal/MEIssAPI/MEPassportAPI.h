//
//  MEPassportAPI.h
//  MExchange
//
//  Created by denn on 6/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MEinfoCXConnector/GIConfig.h>

#define ME_PASSPORT_TEXTLENGTH_REQUIRED 6
#define ME_PASSPORT_PASSWORDLENGTH_REQUIRED 6
#define ME_LOGIN_REGISTRATION_PASSWORD_LIMIT NSLocalizedString(@"Password should have %i minimum symbols", @"User password registration error message text")
#define ME_LOGIN_REGISTRATION_CONFIRAMTION_FAIL NSLocalizedString(@"Passwords don't match", @"Password and password confirmation do not match error text")

@class MEPassportAPI;

/**
 *  Passport API delegate.
 */
@protocol MEPassportAPIDelegate <NSObject>

/**
 *  Handle when passport.moex.com return back errors.
 *  
 *  @param errors NSErrors array.
 */
- (void) passport:(MEPassportAPI*)passport onErrors: (NSArray*) errors;

/**
 *  Handle when passport return ok and messages.
 *
 *  @param messages NSString array.
 */
- (void) passport:(MEPassportAPI *)passport onComplete: (NSArray*) messages;

@optional
/**
 *  This delegate will hanlde in case field can't be matched exactly.
 *  
 *  @param name input field name or id
 *  @param error NSError
 */
- (void) passport:(MEPassportAPI *)passport onInputName:(NSString*)name withError: (NSError*) error;

@end

/**
 *  Passport.moex.com REST API
 */
@interface MEPassportAPI : NSObject

@property id <MEPassportAPIDelegate> delegate;

- (BOOL) registryUser: (NSString*)username withEmail:(NSString*)email andPassword:(NSString*)password;
- (BOOL) recoveryPassword: (NSString*) credentials;

- (BOOL) changePassword: (NSString*)old withNewPassword:(NSString*)newPassword;

@end
