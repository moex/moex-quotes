//
//  NSURLRequest+MEDummyHTTPSCertificate.m
//  MExchange
//
//  Created by denn on 6/3/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "NSURLRequest+MEDummyHTTPSCertificate.h"
#import "MEConfig.h"

#if DEBUG

@implementation NSMutableURLRequest (MEDummyHTTPSCertificate)
+ (BOOL) allowsAnyHTTPSCertificateForHost:(NSString *)host{
    if (![[MEConfig sharedConfig] validatesSecureCertificate]) {
        NSLog(@"DEBUG! Allow any cert for host: %@", host);
        return YES;
    }
    return NO;
}
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host{}
@end

#endif