//
//  MEIssQuery.m
//  MExchange
//
//  Created by denn on 4/25/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEIssQuery.h"
#import "NSURL+PathParameters.h"
#import "GIUtils.h"

#define ME_ISS_QUERY_TO_MANY_SEARCHES NSLocalizedString(@"Too many tickers found, change searching string to reduce result", @"Too many tickers found")
#define ME_ISS_QUERY_TO_MANY_SEARCHES_REASON NSLocalizedString(@"Too many tickers found", @"Too many tickers found reason for aler view")

@implementation MEIssQuery
{
    NSURL    *issBaseUrl;
    NSString *lang;
    
    NSCache  *queryCache;
}

- (id) init{
    self = [super init];
    
    if (self) {
        issBaseUrl = [NSURL URLWithString:ME_ISS_URL];
        lang = @"en";        
        NSString *languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([languageCode hasPrefix:@"ru"])
            lang = @"ru";
        
        queryCache = [[NSCache alloc] init];        
        [queryCache setTotalCostLimit:ME_ISS_MAXIMUM_CACHE_COST];
    }
    
    return self;
}


- (id) _getJSONUrl:(NSString*)command parameters: (NSDictionary*) parameters error:(NSError**)error{
        
    NSDictionary *_options = @{
                              @"lang": lang,
                              @"iss.json": @"extended",
                              @"iss.meta": @"off"
                              };

    NSURL *_url = [[[issBaseUrl URLByAppendingPathComponent:command] URLByAppendingParameters:parameters] URLByAppendingParameters:_options];
    
    NSString *_cacheKey = [[_url standardizedURL] description];
    
    id ret = [queryCache objectForKey:_cacheKey];
    
    if (!IsEmpty(ret)) {
        return ret;
    }
    
    NSMutableURLRequest *_req=[NSMutableURLRequest requestWithURL:_url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:ME_ISS_CONNECTION_TIMEOUT];
    
    NSURLResponse *_response;
    
    NSData *_resultSetData = [NSURLConnection sendSynchronousRequest:_req returningResponse:&_response error:error];

    if (*error) {
        NSLog(@"ISS Query error: %@",*error);
        return nil;
    }

    ret = [NSJSONSerialization JSONObjectWithData:_resultSetData options:NSJSONReadingAllowFragments|NSJSONWritingPrettyPrinted error:error];
    
    if (!IsEmpty(ret)) {
        [queryCache setObject:ret forKey:_cacheKey];
    }
    
    return ret;
}

- (NSArray*) siteNews:(NSError**)error{
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/sitenews.json"];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1][@"sitenews"];
    }
    
    return nil;
}

- (NSArray*) siteNewsItem:(NSNumber*)newsID error:(NSError**)error{
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/sitenews/%@.json", newsID];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1][@"content"];
    }    
    return nil;
}

-(NSDictionary*) tickerSessionSpecs:(NSString*)engine andMarket:(NSString*)market withTickerString:(NSString*)tickerString error:(NSError**) error{
    NSArray *_comp= [tickerString componentsSeparatedByString:@"."];
    NSString *_tickerId = _comp[_comp.count-1];
    NSString *_boardId = _comp[_comp.count-2];
    
    NSDictionary *_params = @{
                              @"ss.only": @"securities"
                              };
    NSString *_command = [NSString stringWithFormat:@"/engines/%@/markets/%@/boards/%@/securities/%@.json", engine, market, _boardId, _tickerId];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    

    if (!_res || [_res[1][@"securities"] count]==0) {
        return nil;
    }
    return _res[1][@"securities"][0];        
}

- (NSArray*) tickerDescription:(NSString *)tickerString error:(NSError *__autoreleasing *)error{
    NSArray *_comp= [tickerString componentsSeparatedByString:@"."];
    NSString *_tickerId = _comp[_comp.count-1];
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/securities/%@.json", _tickerId];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];

    if (_res) {
        return _res[1][@"description"];
    }
    
    return nil;
}

- (NSArray*) tickerBoards:(NSString*)tickerString error:(NSError**) error{
    NSArray *_comp= [tickerString componentsSeparatedByString:@"."];
    NSString *_tickerId = _comp[_comp.count-1];
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/securities/%@.json", _tickerId];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1][@"boards"];
    }
    
    return nil;
}

- (NSArray*) tickerIndices:(NSString*)tickerString error:(NSError**) error{
    NSArray *_comp= [tickerString componentsSeparatedByString:@"."];
    NSString *_tickerId = _comp[_comp.count-1];
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/securities/%@/indices.json", _tickerId];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1][@"indices"];
    }
    
    return nil;
}

- (NSDictionary*) tickerEmitter:(NSString*)emitterID error:(NSError**) error{
    
    if (IsEmpty(emitterID)) {
        return nil;
    }
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/emitters/%@.json", emitterID];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (!_res || [_res[1][@"emitter"] count]==0) {
        return nil;
    }
    return _res[1][@"emitter"][0];
}

- (NSDictionary*) tickerEmitterColumns:(NSString*)emitterID error:(NSError**) error{
    if (IsEmpty(emitterID)) {
        return nil;
    }
    
    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/emitters/columns.json"];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (!_res) {
        return nil;
    }
    return _res[1][@"emitter"];
}

- (NSArray*) tickerEmitterSecurities:(NSString*)emitterID error:(NSError**) error{
    if (IsEmpty(emitterID)) {
        return nil;
    }

    NSDictionary *_params = @{};
    NSString *_command = [NSString stringWithFormat:@"/emitters/%@/securities.json", emitterID];
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1][@"securities"];
    }
    
    return nil;
}

- (NSDictionary*) index:(NSError *__autoreleasing *)error{    
    NSDictionary *_params = @{
                              @"ss.only": @"engines,markets,boards"
                              };
    NSString *_command = @"/index.json";
    
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1];
    }
    
    return nil;    
} 


- (NSDictionary*) columnsForEngine:(NSString*)engine andMarket:(NSString*)market error:(NSError **)error{
    NSDictionary *_params = @{
                              @"ss.only": @"securities,marketdata"
                              };    
    NSString *_command = [NSString stringWithFormat:@"/engines/%@/markets/%@/securities/columns.json", engine, market];
    NSArray *_res = [self _getJSONUrl:_command parameters:_params error:error];
    
    if (_res) {
        return _res[1];
    }
    
    return nil;
}

- (NSMutableArray*) searchTicker:(NSString *)part error:(NSError **)error{
            
    NSMutableArray *_result_set = [NSMutableArray arrayWithCapacity:1];
    NSArray *_res=nil;
    NSInteger start=0;
    
    NSInteger count=0;
    do {    
        NSDictionary *_params = @{
                                  @"q": [NSString stringWithFormat:@"%@",part],
                                  @"start": [NSString stringWithFormat:@"%li",(long)start],
                                  @"is_trading": @"1"
                                };
        NSString *_command = @"/securities.json";
        
        _res = [self _getJSONUrl:_command parameters:_params error:error];
        
        if (*error) 
            return _result_set;        
        
        if (_res) {
            _res = _res[1][@"securities"];
            [_result_set addObjectsFromArray:_res];
            start+=_res.count;
        }
        
        
        if (count>=ME_ISS_SEARCH_MAXIMUM_TICKERS_LIMIT && _res.count>0) {
            count+=_res.count;
            //
            // limit
            //
            NSDictionary *userInfo = [
                                      NSDictionary
                                      dictionaryWithObjects:[NSArray arrayWithObjects: ME_ISS_QUERY_TO_MANY_SEARCHES , ME_ISS_QUERY_TO_MANY_SEARCHES_REASON, nil]
                                      forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                                      ];
            
            NSError *_error = [[NSError alloc ] initWithDomain:@"iss.securties.search"
                                       code: 10
                                   userInfo: userInfo];
            
            *error = [_error copy];
            
            break;
        }
        count+=_res.count;
        
    } while (_res && _res.count>0);
    
    return _result_set;
}

@end
