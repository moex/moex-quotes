//
//  MEIssQuery.h
//  MExchange
//
//  Created by denn on 4/25/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEConfig.h"

#define ME_ISS_URL  @"http://moex.com/iss"
#define ME_ISS_CONNECTION_TIMEOUT 30
#define ME_ISS_SEARCH_MAXIMUM_TICKERS_LIMIT   256
#define ME_ISS_MAXIMUM_CACHE_COST  1024*1024 // bytes

@interface MEIssQuery : NSObject

- (NSDictionary*)   index: (NSError**)error;
- (NSMutableArray*) searchTicker:(NSString*) part error:(NSError**) error;
- (NSDictionary*)   columnsForEngine:(NSString*)engine andMarket:(NSString*)market error:(NSError **)error;

- (NSArray*) tickerDescription:(NSString*)tickerString error:(NSError**) error;
- (NSArray*) tickerBoards:(NSString*)tickerString error:(NSError**) error;
- (NSArray*) tickerIndices:(NSString*)tickerString error:(NSError**) error;
- (NSDictionary*) tickerEmitter:(NSString*)emitterID error:(NSError**) error;
- (NSDictionary*) tickerEmitterColumns:(NSString*)emitterID error:(NSError**) error;
- (NSArray*) tickerEmitterSecurities:(NSString*)emitterID error:(NSError**) error;

-(NSDictionary*) tickerSessionSpecs:(NSString*)engine andMarket:(NSString*)market withTickerString:(NSString*)tickerString error:(NSError**) error;

- (NSArray*) siteNews:(NSError**)error;
- (NSArray*) siteNewsItem:(NSNumber*)newsID error:(NSError**)error;

@end
