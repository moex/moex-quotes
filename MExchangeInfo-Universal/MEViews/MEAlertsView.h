//
//  MEAlertLoggerView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 19.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MEAlertLogger.h>

/**
 *  Alert status 
 */

typedef enum {
    ME_ALERT_NETWORK_OK=1,
    ME_ALERT_NETWORK_LOST=2,
    ME_ALERT_WARN=3,
    ME_ALERT_WARN_NEED_SHOW=4,
    ME_ALERT_FATAL=5
} MEAlertType;

/**
 *  This is view logs messages as a stacked journal or log.
 */
@interface MEAlertsView : NSObject //MEAlertLogger <MEAlertLoggerDelegate>
+ (id) sharedInstance;
- (void) pushMessageText:(NSString *)text withStatus:(MEAlertType)status;
@end
