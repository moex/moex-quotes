//
//  MEActionSheet.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 08/04/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MEActionSheet;

@protocol MEActionSheetDelegate <NSObject>
@optional
- (void) actionSheet:(MEActionSheet *)acthionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex;
- (void) actionSheet:(MEActionSheet *)acthionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;
- (void) actionSheet:(MEActionSheet *)acthionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void) actionSheetCancel:(MEActionSheet *)acthionSheet;
@end

@interface MEActionSheet : UIView
- (id) initWithTitle:(NSString*)title delegate:(id<MEActionSheetDelegate>)delegate cancelButtonTitle:(NSString*)cancelTitle buttonTitles:(NSArray*)titles;
- (void) show;
@property (nonatomic,strong) id<MEActionSheetDelegate> delegate;
@end
