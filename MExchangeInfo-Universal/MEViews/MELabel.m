//
//  MELabel.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 17.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MELabel.h"

@implementation MELabel
{
    NSMutableParagraphStyle *paragraphStyle;
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self options];
    return self;
}

- (void) options{
    self.maxFontSize = 32;
    self.minFontSize = 5;
    self.numberOfLines = 3;
    self.lineBreakMode = NSLineBreakByWordWrapping;
    self.adjustsFontSizeToFitWidth = NO;
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    [self options];
    self.maxFontSize = self.font.pointSize;
    return self;
}

- (void) setText:(NSString *)text{
    UIFont *font = [UIFont fontWithName:self.font.fontName size:self.maxFontSize];
    
    for(int i = font.pointSize; i > self.minFontSize; i=i-1)
    {
        font = [font fontWithSize:i];
        
        // This step is important: We make a constraint box
        // using only the fixed WIDTH of the UILabel. The height will
        // be checked later.
        CGSize constraintSize = CGSizeMake(self.frame.size.width, MAXFLOAT);
        
        // This step checks how tall the label would be with the desired font.
        
        _computedBounds = [
                           [[NSMutableAttributedString alloc]
                            initWithString: text
                            attributes :@{
                                          NSFontAttributeName: font,
                                          NSParagraphStyleAttributeName:paragraphStyle
                                          }]
                           boundingRectWithSize:constraintSize
                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                           context:nil
                           ];
        
        if(_computedBounds.size.height <= self.frame.size.height)
            break;
    }
    
    self.font = font;
    [super setText:text];
}

- (void) setOriginalText: (NSString*) text{
    [super setText:text];
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
