//
//  MEActionSheet.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 08/04/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEActionSheet.h"
#import "MEUIPreferences.h"

@interface MEActionSheet()
@property (nonatomic,strong) UIView *containerView;
@property (nonatomic,strong) UIButton *cancelButton;
@property (nonatomic,strong) NSMutableArray *buttons;
@end

@implementation MEActionSheet
{
    UITapGestureRecognizer *tapGest;
}

- (NSMutableArray*) buttons{
    if (_buttons) {
        return _buttons;
    }
    
    _buttons = [[NSMutableArray alloc] init];
    
    return _buttons;
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withRadius:(CGSize)radius
{
    CAShapeLayer *shape = [CAShapeLayer layer]; shape.bounds = view.bounds;
    shape.path=[UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radius].CGPath;
    view.layer.mask = shape;
    view.layer.masksToBounds = YES;
}

- (id) initWithTitle:(NSString*)title delegate:(id<MEActionSheetDelegate>)delegate cancelButtonTitle:(NSString*)cancelTitle buttonTitles:(NSArray*)titles{
    
    id<UIApplicationDelegate> app = [[UIApplication sharedApplication] delegate];
    UIView *superview = app.window;
    self = [super initWithFrame:superview.bounds];
    self.backgroundColor = ME_UI_DIMMER_COLOR;
    self.alpha = 0.0;
    self.autoresizingMask = ~UIViewAutoresizingNone;
    [superview addSubview:self];
    
    
    CGRect rect = self.bounds; rect.origin.y = rect.size.height;
    _containerView = [[UIView alloc] initWithFrame:CGRectInset(rect, 10, 10)];
    _containerView.autoresizingMask = ~UIViewAutoresizingNone;
    [self addSubview:_containerView];
    
    _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    
    _cancelButton.showsTouchWhenHighlighted = YES;
    _cancelButton.titleLabel.font = [UIFont fontWithName:ME_UI_FONT size:ME_UI_BUTTON_FONT_SIZE];
    [_cancelButton setTitleColor:ME_UI_DARK_GREY_FONT_COLOR forState:UIControlStateNormal];
    [_cancelButton setTitle:cancelTitle forState:UIControlStateNormal];

    _cancelButton.backgroundColor  = [UIColor whiteColor];
    _cancelButton.tag = 0;
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_cancelButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_containerView addSubview:_cancelButton];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[button]-0-|" options:0 metrics:nil views:@{@"button":_cancelButton}]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(44)]-0-|" options:0 metrics:nil views:@{@"button":_cancelButton}]];
    
    //[self setMaskTo:_cancelButton byRoundingCorners:UIRectCornerBottomRight|UIRectCornerBottomLeft withRadius:CGSizeMake(44, 44)];
    _cancelButton.layer.cornerRadius = ME_MESSAGE_VIEW_CONER_RADIUS;
    _cancelButton.layer.masksToBounds = YES;

    for (int i=0; i<[titles count]; i++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];

        [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:ME_UI_DARK_GREY_FONT_COLOR forState:UIControlStateNormal];

        button.showsTouchWhenHighlighted = YES;
        button.layer.cornerRadius = ME_MESSAGE_VIEW_CONER_RADIUS;
        button.layer.masksToBounds = YES;

        button.titleLabel.font = [UIFont fontWithName:ME_UI_BOLD_FONT size:ME_UI_BUTTON_FONT_SIZE];
        button.titleLabel.adjustsFontSizeToFitWidth=YES;
        button.titleLabel.minimumScaleFactor=0.6;
        button.backgroundColor  = [UIColor whiteColor];

        button.tag = i;
        button.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_containerView addSubview:button];

        if (i==0) {
            [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(44)]-5-[cancel]" options:0 metrics:nil views:@{@"button":button, @"cancel": _cancelButton}]];
        }
        else{
            [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(44)]-1-[prevbutton]" options:0 metrics:nil views:@{@"button":button, @"prevbutton": [self.buttons lastObject]}]];
        }
        
        [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[button]-0-|" options:0 metrics:nil views:@{@"button":button}]];
        
        [self.buttons addObject:button];
    }
    
    tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    tapGest.numberOfTapsRequired = 1;
    tapGest.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGest];
    
    self.delegate = delegate;
    
    return self;
}

- (void) buttonAction:(UIButton*)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionSheet:willDismissWithButtonIndex:)]) {
        [self.delegate actionSheet:self willDismissWithButtonIndex:_cancelButton.tag];
    }
    if (sender == _cancelButton) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(actionSheetCancel:)]) {
            [self.delegate actionSheetCancel:self];
        }
    }
    else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(actionSheet:clickedButtonAtIndex:)]) {
            [self.delegate actionSheet:self clickedButtonAtIndex:sender.tag];
        }
    }
    
    [self dissmiss];
}

- (void) tapHandler:(UITapGestureRecognizer*) gesture{
    [self dissmiss];
}

- (void) dissmiss{
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION
                     animations:^{
                         CGRect rect = self.bounds; rect.origin.y = rect.size.height;
                         self.containerView.frame = CGRectInset(rect, 10, 10);
                         self.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         if (self.delegate && [self.delegate respondsToSelector:@selector(actionSheet:didDismissWithButtonIndex:)]) {
                             [self.delegate actionSheet:self didDismissWithButtonIndex:self->_cancelButton.tag];
                         }
                     }
     ];
}

- (void) show{
    [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
        self.alpha = 1.;
        CGRect rect = self.bounds; rect.origin.y = 0;
        self.containerView.frame = CGRectInset(rect, 10, 10);
    }];
}


@end
