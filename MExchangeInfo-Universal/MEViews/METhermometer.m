//
//  METhermometer.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METhermometer.h"
#import "MEUIPreferences.h"
#import "NSAttributedString+MEFormatedString.h"

@interface METhermometer()
@property(nonatomic,strong) UILabel *bidCaptionLabel;
@property(nonatomic,strong) UILabel *offerCaptionLabel;
@property(nonatomic,strong) UILabel *bidLabel;
@property(nonatomic,strong) UILabel *offerLabel;
@property(nonatomic,strong) UILabel *timeUpdateLabel;
@property(nonatomic,strong) UISlider *slider;
@end

@implementation METhermometer
{
    UIFont *timeUpdateFont;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.bidCaptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
        self.offerCaptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
        self.bidLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
        self.offerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 10)];
        self.timeUpdateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 10)];
        self.slider = [[UISlider alloc] init];
        
        self.slider.userInteractionEnabled = NO;
        
        [self addSubview:self.slider];
        [self addSubview:self.bidCaptionLabel];
        [self addSubview:self.offerCaptionLabel];
        [self addSubview:self.bidLabel];
        [self addSubview:self.offerLabel];
        [self addSubview:self.timeUpdateLabel];
        
        self.bidCaptionLabel.font = self.offerCaptionLabel.font = [UIFont fontWithName:ME_UI_FONT size:10];
        self.bidCaptionLabel.numberOfLines = self.offerCaptionLabel.numberOfLines = 2;
        self.bidCaptionLabel.minimumScaleFactor = self.offerCaptionLabel.minimumScaleFactor = 0.5;
        self.bidCaptionLabel.adjustsFontSizeToFitWidth = self.offerCaptionLabel.adjustsFontSizeToFitWidth = YES;
        
        self.bidCaptionLabel.textAlignment = NSTextAlignmentLeft;
        self.offerCaptionLabel.textAlignment = NSTextAlignmentRight;
        self.timeUpdateLabel.textAlignment = NSTextAlignmentCenter;
        
        self.timeUpdateLabel.font = timeUpdateFont = [UIFont fontWithName:ME_UI_FONT size:11];
        self.timeUpdateLabel.textColor = self.offerCaptionLabel.textColor = ME_UI_GREY_FONT_COLOR;
        
        self.bidLabel.font = self.offerLabel.font = [UIFont fontWithName:ME_UI_FONT size:12];
        self.bidCaptionLabel.textColor = self.offerCaptionLabel.textColor = ME_UI_GREY_FONT_COLOR;
        self.bidLabel.adjustsFontSizeToFitWidth = self.offerLabel.adjustsFontSizeToFitWidth = YES;
        self.bidLabel.minimumScaleFactor = self.offerLabel.minimumScaleFactor = 0.5;
        self.bidLabel.textAlignment = NSTextAlignmentLeft;
        self.offerLabel.textAlignment = NSTextAlignmentRight;

        self.slider.translatesAutoresizingMaskIntoConstraints=NO;
        self.bidCaptionLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.offerCaptionLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.bidLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.offerLabel.translatesAutoresizingMaskIntoConstraints=NO;
        self.timeUpdateLabel.translatesAutoresizingMaskIntoConstraints=NO;
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[slider(>=10)]-0-|" options:0 metrics:nil views:@{@"slider": self.slider}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[slider(>=10)]-20-|" options:0 metrics:nil views:@{@"slider": self.slider}]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.bidLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.offerLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.timeUpdateLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeCenterX multiplier:1 constant:-5]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[bidCaptionLabel(>=10)]-5-[timeUpdate(<=120)]-5-[offerCaptionLabel(>=10)]-5-|" options:0 metrics:nil views:@{@"bidCaptionLabel": self.bidCaptionLabel, @"timeUpdate": self.timeUpdateLabel, @"offerCaptionLabel": self.offerCaptionLabel}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[slider]-5-[bidCaptionLabel]-0-|" options:0 metrics:nil views:@{@"bidCaptionLabel": self.bidCaptionLabel, @"slider": self.slider}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[slider]-5-[timeUpdate]-0-|" options:0 metrics:nil views:@{@"timeUpdate": self.timeUpdateLabel, @"slider": self.slider}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[slider]-5-[offerCaptionLabel]-0-|" options:0 metrics:nil views:@{@"offerCaptionLabel": self.offerCaptionLabel, @"slider": self.slider}]];

        GIStompValue *v = [[GIStompValue alloc] initFromId:[NSNumber numberWithFloat:0]];
        [self setUpThermometer:v withOfferDepth:v];
    }
    return self;
}

- (void) setTimeUpdate:(NSDate*)date{
    self.timeUpdateLabel.attributedText = [NSAttributedString coloredDate:date withBaseFontSize:timeUpdateFont.pointSize];
}

- (void) setBidDepth:(GIStompValue*)bidDepth withOfferDepth:(GIStompValue*)askDepth{
    
    float  _fp_=0;
    
    self.bidLabel.text = [bidDepth string];
    self.offerLabel.text = [askDepth string];
    
    _fp_ =  bidDepth.floatValue/((bidDepth.floatValue+askDepth.floatValue)+0.001)+0.01 ;
    
    if (bidDepth.floatValue==0.0 && askDepth.floatValue==0.0) {
        _fp_ = 0.5;
    }
    else if (_fp_<0.2)
        _fp_ = 0.2;
    else if (_fp_>0.8)
        _fp_ = 0.8;
    
    [self setUpThermometer:bidDepth withOfferDepth:askDepth];
    [self.slider setValue: _fp_ animated:YES];
}

- (UIImage*) bidasckThermometerImage: (NSString*) name{
    return [[UIImage imageNamed: name] resizableImageWithCapInsets:UIEdgeInsetsMake(9, 9, 9, 9) resizingMode:UIImageResizingModeStretch];
}

- (void) setUpThermometer:(GIStompValue*)bidDepth withOfferDepth:(GIStompValue*)offerDepth{
    static UIImage *_offerProgressImgRed = nil;
    static UIImage *_bidProgressImgGreen = nil;
    static UIImage *_thumbProgressImage = nil;
    
    static UIImage *_offerProgressImgWhite = nil;
    static UIImage *_bidProgressImgWhite = nil;
    static UIImage *_thumbProgressImageWhite = nil;
    
    UIImage *offerProgressImg;
    UIImage *bidProgressImg;
    UIImage *thumbSliderImg;
    
    if (!_bidProgressImgGreen)
        _bidProgressImgGreen = [self bidasckThermometerImage:@"bidofferdepth_progress_green.png"];
    
    if (!_offerProgressImgRed)
        _offerProgressImgRed =  [self bidasckThermometerImage:@"bidofferdepth_progress_red.png"];
    
    if (!_thumbProgressImage)
        _thumbProgressImage = [UIImage imageNamed:@"bidofferdepth_progress_divider.png"] ;
    
    if (!_bidProgressImgWhite)
        _bidProgressImgWhite = [self bidasckThermometerImage:@"bidofferdepth_progress_white.png"];
    
    if (!_offerProgressImgWhite)
        _offerProgressImgWhite =  [self bidasckThermometerImage:@"bidofferdepth_progress_white.png"];
    
    if (!_thumbProgressImageWhite)
        _thumbProgressImageWhite = [UIImage imageNamed:@"bidofferdepth_progress_divider_white.png"] ;
    
    
    UIColor *_textColor;
    
    if ([bidDepth number].intValue == [offerDepth number].intValue && [bidDepth number].intValue ==0) {
        thumbSliderImg = _thumbProgressImageWhite;
        offerProgressImg = _offerProgressImgWhite;
        bidProgressImg = _bidProgressImgWhite;
        _textColor = [UIColor grayColor];
    }
    else{
        thumbSliderImg = _thumbProgressImage;
        offerProgressImg = _offerProgressImgRed;
        bidProgressImg = _bidProgressImgGreen;
        _textColor = [UIColor whiteColor];
    }
    
    [self.slider setMinimumTrackImage: bidProgressImg forState: UIControlStateNormal];
    [self.slider setMaximumTrackImage: offerProgressImg forState: UIControlStateNormal];
    [self.slider setThumbImage:thumbSliderImg forState:UIControlStateNormal];
    
    self.bidLabel.textColor = self.offerLabel.textColor = _textColor;
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
