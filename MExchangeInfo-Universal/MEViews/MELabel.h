//
//  MELabel.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 17.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MELabel : UILabel
@property(nonatomic,readonly) CGRect computedBounds;
@property(nonatomic) NSInteger maxFontSize, minFontSize;
- (void) setOriginalText: (NSString*) text;
@end
