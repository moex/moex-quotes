//
//  MEArrowedViewCell.m
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 09.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEArrowedViewCell.h"
#import "MEArrowView.h"
#import "MEUIPreferences.h"

//
//
// TODO: refact with MEBaseViewCell 
//
//

@implementation MEArrowedViewCell

{
    MEArrowView *shapedView;
    UIImageView *selectionBackgroundImageView;
}

- (void) addDevider{
    _devider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"devider.png"]];
    self.devider.frame = CGRectMake(0, 43, 0, self.devider.bounds.size.height);
    self.devider.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.contentView addSubview:self.devider];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView  attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView  attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.shouldIndentWhileEditing = YES;
        self.showsReorderControl = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;        
        selectionBackgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"panel_selected.png"]];
        selectionBackgroundImageView.backgroundColor=[UIColor clearColor];
        selectionBackgroundImageView.alpha = 0.0;
                
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            self.contentView.superview.clipsToBounds = NO;
            
            selectionBackgroundImageView.clipsToBounds = NO;
            selectionBackgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;        
            [self.contentView insertSubview:selectionBackgroundImageView atIndex:0]; 
            
            shapedView = [[MEArrowView alloc] initWithFrame:CGRectMake(self.bounds.size.width-1, 0, self.bounds.size.height, self.bounds.size.height)];  
            
            NSNumber *width = [NSNumber numberWithFloat:-shapedView.baseWidth+1];
            
            shapedView.backgroundColor = [UIColor clearColor];
            shapedView.image = selectionBackgroundImageView.image;
            shapedView.arrowColor = ME_UI_LIGHT_GREY_ARROW_COLOR;
            
            [self.contentView addSubview:shapedView];
            shapedView.translatesAutoresizingMaskIntoConstraints = NO;
            shapedView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bg]-0-[arrow(10)]-(width)-|" options:0 metrics:@{@"width": width} views:@{@"bg": selectionBackgroundImageView, @"arrow":shapedView}]];          
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[bg]-0-|" options:0 metrics:nil views:@{@"bg": selectionBackgroundImageView}]];          
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[arrow]-0-|" options:0 metrics:nil views:@{@"arrow": shapedView}]];          
            
            shapedView.alpha = 0.0;            
        }
        else{
            selectionBackgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
            [self addSubview:selectionBackgroundImageView]; [self sendSubviewToBack:selectionBackgroundImageView];
        }
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected || self.isHighlighted){
        selectionBackgroundImageView.alpha=1.0;
        shapedView.alpha = 1.0;
    }
    else{
        selectionBackgroundImageView.alpha=0.0;
        shapedView.alpha = 0.0;
    }
}


- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted || self.isSelected){
        selectionBackgroundImageView.alpha=1.0;
        shapedView.alpha = 1.0;
    }
    else{
        selectionBackgroundImageView.alpha=0.0;
        shapedView.alpha = 0.0;
    }
}



@end
