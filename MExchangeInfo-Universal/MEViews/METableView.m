//
//  METableView.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METableView.h"
#import "MEUIPreferences.h"

#define  __ME_TABLE_VIEW_DEFAULTS_REST_KEY  [NSString stringWithFormat:@"%@-%@", self.restorationIdentifier, @"lastSelectedIndexPath"]

@interface METableView()
@property(nonatomic,readonly) UIView *messageTextViewContainer;
@property(nonatomic,readonly) CGRect messageContainerFrame;
@property(nonatomic,readonly) CGRect messageTextFrame;
@end

@implementation METableView

@synthesize lastSelectedIndexPath = _lastSelectedIndexPath, messageTextView=_messageTextView, messageTextViewContainer=_messageTextViewContainer;

- (void) setMessageText:(NSString *)messageText{
    dispatch_async(    dispatch_get_main_queue(), ^{
        
        if (self->_messageText==nil && messageText==nil) {
            return;
        }
        if ([self->_messageText isEqualToString:messageText] && self.messageTextViewContainer.alpha >=0.5) {
            return;
        }
        
        self->_messageText = messageText;
        
        [UIView animateWithDuration:ME_UI_ANIMATION_DURATION animations:^{
            if (self->_messageText==nil) {
                self.messageTextViewContainer.alpha = 0.0;
                self.scrollEnabled = YES;
            }
            else{
                self.messageTextView.text = self->_messageText;
                self.messageTextViewContainer.alpha = 1.0;
                self.scrollEnabled = NO;
            }
        }];
    });
}

- (CGRect) messageContainerFrame{
    return CGRectMake(0, fabs(self.contentInset.top), self.bounds.size.width, fmaxf(self.bounds.size.height,self.contentSize.height)-fabs(self.contentInset.top));
}

- (CGRect) messageTextFrame{
    return CGRectMake(10, 10, self.messageContainerFrame.size.width-20, self.messageContainerFrame.size.height-20);
}

- (UIView*) messageTextViewContainer{
    if (!_messageTextViewContainer) {
        _messageTextViewContainer = [[UIView alloc] initWithFrame:self.messageContainerFrame];
        _messageTextViewContainer.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        _messageTextViewContainer.alpha=0.0;
        _messageTextViewContainer.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
        [self addSubview:_messageTextViewContainer];
        
    }
    return _messageTextViewContainer;
}



- (UITextView*) messageTextView {
    if (!_messageTextView) {
        
        _messageTextView = [[UITextView alloc] initWithFrame:self.messageTextFrame];
        
        _messageTextView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        
        _messageTextView.font = [UIFont fontWithName:ME_UI_FONT size:16];
        _messageTextView.editable = NO;
        _messageTextView.scrollEnabled = NO;
        _messageTextView.userInteractionEnabled = YES;
        _messageTextView.dataDetectorTypes = UIDataDetectorTypeLink|UIDataDetectorTypeAddress;
        _messageTextView.backgroundColor = [UIColor clearColor];
        
        
        _messageTextView.textColor = ME_UI_GREY_FONT_COLOR;
        _messageTextView.textAlignment = NSTextAlignmentNatural;
        
        [self.messageTextViewContainer addSubview:_messageTextView];
    }
    
    return _messageTextView;
}

-(void) setLastSelectedIndexPath:(NSIndexPath *)lastSelectedIndexPath{
    _lastSelectedIndexPath = lastSelectedIndexPath;
    
    if (self.restorationIdentifier) {
        
        [[NSUserDefaults standardUserDefaults]
         setObject:@[[NSNumber numberWithInteger:
                      lastSelectedIndexPath.row], 
                     [NSNumber numberWithInteger:
                      lastSelectedIndexPath.section]]
         forKey:__ME_TABLE_VIEW_DEFAULTS_REST_KEY];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSIndexPath*) lastSelectedIndexPath{
    
    if (_lastSelectedIndexPath) {
        return _lastSelectedIndexPath;
    }
    
    if (self.restorationIdentifier) {
        NSArray *_lastSelected = [[NSUserDefaults standardUserDefaults] objectForKey:__ME_TABLE_VIEW_DEFAULTS_REST_KEY];
        if (_lastSelected)
            _lastSelectedIndexPath = [NSIndexPath indexPathForRow:[_lastSelected[0] integerValue] inSection:[_lastSelected[1] integerValue]];
    }
    
    if (!_lastSelectedIndexPath)
        _lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
    return _lastSelectedIndexPath;
}

//- (unsigned long) getTag{
//    return (unsigned long)(__bridge void*)self;
//}
//
//- (UIImageView*) getShadow:(UIView*)view{
//    return (UIImageView*) [view viewWithTag:[self getTag]];
//}
//
//- (void) setHidden:(BOOL)hidden{
//    [super setHidden:hidden];
//
//    UIImageView *_shadow = [self getShadow:self.superview];
//    if (_shadow)
//        [_shadow setHidden:hidden];
//
//}
//
//- (void) setAlpha:(CGFloat)alpha{
//    [super setAlpha:alpha];
//
//    UIImageView *_shadow = [self getShadow:self.superview];
//    if (_shadow)
//        _shadow.alpha=self.hidden?0.0:alpha;
//
//}
//
//- (void) layoutSubviews{
//    [self checkVisible];
//    [super layoutSubviews];
//}

//- (void) checkVisible{
//    @try {
//        NSArray *visibleRows = [self indexPathsForVisibleRows];
//        NSIndexPath *_indexOfTop;
//        UIView *header = nil;
//        UIImageView *_shadow = [self getShadow:self.superview];
//        
//        //    if (_shadow && self.contentOffset.y==0) {
//        //        _shadow.alpha = 0.0;
//        //        return;
//        //    }
//        
//        if (visibleRows && visibleRows.count>0) {
//            _indexOfTop=[visibleRows objectAtIndex:0];
//            header = [self headerViewForSection:_indexOfTop.section];
//        }
//        
//        if (header && !self.enableTopShadowOnly) {
//            UIImageView *_header_shadow = [self getShadow:header];
//            
//            if (!_header_shadow) {
//                _header_shadow    = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_shadow.png"]];
//                _header_shadow.tag = [self getTag];
//                [header addSubview:_header_shadow];
//            }
//            
//            if (visibleRows && visibleRows.count>0) {
//                for (int i=0; i<[self numberOfSections]; i++) {
//                    UIView *lheader = [self headerViewForSection:i];
//                    if (lheader!=header) {
//                        UIImageView *_lshadow = (UIImageView*) [lheader viewWithTag:[self getTag]];
//                        _lshadow.alpha = 0.0;
//                    }
//                }
//            }
//            
//            _header_shadow.alpha = 0.0;
//            
//            CGPoint basePoint = [header convertPoint:[header bounds].origin toView:self.superview];
//            
//            UITableViewCell *cell = [[self visibleCells] objectAtIndex:0];
//            
//            CGPoint basePoint_cell = [cell convertPoint:[cell bounds].origin toView:self.superview];
//            
//            CGFloat delta = (basePoint.y+header.bounds.size.height-basePoint_cell.y);
//            
//            if (delta<=0.0){
//                // Clean up shadow under header
//                _header_shadow.alpha = 0.0;
//            }
//            else{
//                // Shadow down
//                _header_shadow.alpha = self.hidden?0.0:1.0;
//                _header_shadow.frame = CGRectMake(0, header.frame.size.height, header.frame.size.width, _header_shadow.frame.size.height);
//            }
//        }
//        else{
//            if (!_shadow) {
//                _shadow    = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_shadow.png"]];
//                _shadow.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, _shadow.bounds.size.height);
//                _shadow.tag = [self getTag];
//                [self.superview addSubview:_shadow];
//                _shadow.alpha = 0.0;
//            }
//            
//            _shadow.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, _shadow.bounds.size.height);
//            
//            if (self.contentOffset.y<=0.0){
//                // Clean up shadow under top view
//                _shadow.alpha = 0.0;
//            }
//            else{
//                _shadow.alpha = self.hidden?0.0:1.0;
//            }
//        }
//        
//    }
//    @catch (NSException *exception) {
//        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
//    }
//}

//- (void) setFrame:(CGRect)frame{
//    [super setFrame:frame];
//    [self checkVisible];
//}
//
//- (void) setCenter:(CGPoint)center{
//    [super setCenter:center];
//    [self checkVisible];
//}


@end
