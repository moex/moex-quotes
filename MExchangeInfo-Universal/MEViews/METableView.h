//
//  METableView.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface METableView : UITableView
@property(nonatomic,assign) BOOL enableTopShadowOnly;
@property(nonatomic,strong) NSIndexPath *lastSelectedIndexPath;
@property(nonatomic,strong) NSString *messageText;
@property(nonatomic,readonly) UITextView *messageTextView;
@end
