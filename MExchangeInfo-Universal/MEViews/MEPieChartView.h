//
//  PieChartView.h
//  PieChartViewDemo
//
//  Created by Strokin Alexey on 8/27/13.
//  Copyright (c) 2013 Strokin Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MEPieChartViewDelegate;
@protocol MEPieChartViewDataSource;

@interface MEPieChartView : UIView 

@property (nonatomic, assign) id <MEPieChartViewDataSource> datasource;
@property (nonatomic, assign) id <MEPieChartViewDelegate> delegate;

-(void)reloadData;

@end



@protocol MEPieChartViewDelegate <NSObject>

- (CGFloat)centerCircleRadius;

@end



@protocol MEPieChartViewDataSource <NSObject>

@required
- (NSUInteger)numberOfSlicesInPieChartView:(MEPieChartView *)pieChartView;
- (double)pieChartView:(MEPieChartView *)pieChartView valueForSliceAtIndex:(NSUInteger)index;
- (UIColor *)pieChartView:(MEPieChartView *)pieChartView colorForSliceAtIndex:(NSUInteger)index;

@optional
- (NSString*)pieChartView:(MEPieChartView *)pieChartView titleForSliceAtIndex:(NSUInteger)index;

@end
