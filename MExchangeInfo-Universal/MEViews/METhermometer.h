//
//  METhermometer.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MEinfoCXConnector/GIStompBody.h>

@interface METhermometer : UIView
@property(nonatomic,strong,readonly) UILabel *bidCaptionLabel;
@property(nonatomic,strong,readonly) UILabel *offerCaptionLabel;
@property(nonatomic,strong,readonly) UILabel *bidLabel;
@property(nonatomic,strong,readonly) UILabel *offerLabel;
- (void) setBidDepth:(GIStompValue*)bidDepth withOfferDepth:(GIStompValue*)askDepth;
- (void) setTimeUpdate:(NSDate*)date;
@end
