//
//  MEBasVieweCell.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEBaseViewCell : UITableViewCell
@property(nonatomic,strong) UIImageView *devider;
@property(nonatomic,strong) UIImageView *selectionBackgroundImageView;
- (void) addDevider;
@end
