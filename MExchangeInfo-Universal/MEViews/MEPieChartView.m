//
//  PieChartView.m
//  PieChartViewDemo
//
//  Created by Strokin Alexey on 8/27/13.
//  Copyright (c) 2013 Strokin Alexey. All rights reserved.
//

#import "MEPieChartView.h"
#import <QuartzCore/QuartzCore.h>


@implementation MEPieChartView

@synthesize delegate;
@synthesize datasource;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil)
    {
        //initialization
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

-(void)reloadData
{
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    
    //prepare
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat theHalf = rect.size.width/2;
    CGFloat lineWidth = theHalf;
    if ([self.delegate respondsToSelector:@selector(centerCircleRadius)])
    {
        lineWidth -= [self.delegate centerCircleRadius];
        NSAssert(lineWidth <= theHalf, @"wrong circle radius");
    }
    CGFloat radius = theHalf-lineWidth/2;
    
    CGFloat centerX = theHalf;
    CGFloat centerY = rect.size.height/2;
    
    //drawing
    
    double sum = 0.0f;
    NSUInteger slicesCount = [self.datasource numberOfSlicesInPieChartView:self];

    for (NSUInteger i = 0; i < slicesCount; i++)
    {
        sum += [self.datasource pieChartView:self valueForSliceAtIndex:i];
    }
    
    float startAngle = - M_PI_2;
    float endAngle = 0.0f;
    
    for (int i = 0; i < slicesCount; i++)
    {
        double value = [self.datasource pieChartView:self valueForSliceAtIndex:i];
        
        endAngle = startAngle + M_PI*2*value/sum;
        float startAngleEnd = startAngle+M_PI/360.*1.2;
        float endAngleEnd = endAngle-M_PI/360.*1.2;
        
        if (startAngleEnd>endAngleEnd)
            // too low
            continue;
        
        CGContextAddArc(context, centerX, centerY, radius, startAngleEnd, endAngleEnd, false);
        
        UIColor  *drawColor = [self.datasource pieChartView:self colorForSliceAtIndex:i];
        
        CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
        CGContextSetLineWidth(context, lineWidth);
        CGContextStrokePath(context);
        startAngle += M_PI*2*value/sum;
    }
}

@end
