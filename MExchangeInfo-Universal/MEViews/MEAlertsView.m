#import "MEAlertsView.h"
#import "MEUIPreferences.h"
#import "MESettings.h"
#import <TSMessage.h>

static MEAlertsView *__shared_instance = nil;

@implementation MEAlertsView
{
    MESettings *settings;
}

- (id) init{
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void) pushMessageText:(NSString *)text withStatus:(MEAlertType)status{
    
    if (status<ME_ALERT_WARN_NEED_SHOW) {
        if (![settings.showWarns boolValue]) {
            return; 
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        TSMessageNotificationType type = TSMessageNotificationTypeSuccess;
        UIImage *image = nil;
        
        switch (status) {
            case ME_ALERT_WARN:
                type = TSMessageNotificationTypeWarning;
                break;
            case ME_ALERT_FATAL:
                type = TSMessageNotificationTypeError;
                //image = [UIImage imageNamed:@"alert_fatal_bg"];
                image = [UIImage imageNamed:@"alert_warn_bg"];
                break;
            case ME_ALERT_WARN_NEED_SHOW:
                type = TSMessageNotificationTypeMessage;
                break;
            case ME_ALERT_NETWORK_LOST:
                type = TSMessageNotificationTypeError;
                //image = [UIImage imageNamed:@"alert_network_lost_bg"];
                image = [UIImage imageNamed:@"alert_warn_bg"];
                break;
            case ME_ALERT_NETWORK_OK:
                type = TSMessageNotificationTypeSuccess;
                //image = [UIImage imageNamed:@"alert_network_ok_bg"];
                image = [UIImage imageNamed:@"alert_warn_bg"];
                break;
            default:
                break;
        }
        
        [TSMessage showNotificationInViewController:[UIApplication sharedApplication].keyWindow.rootViewController
                                              title:nil
                                           subtitle:text
                                              image:image
                                               type:type
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:nil
                                     buttonCallback:nil
                                         atPosition:TSMessageNotificationPositionBottom
                               canBeDismissedByUser:YES];        
    });
}

- (void) setup{
    
    if (self) {
        settings = [MESettings sharedInstance];
    }
}

+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEAlertsView alloc] init];
    });
    return __shared_instance;
}
@end
