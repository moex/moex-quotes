//
//  MEBasVieweCell.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 12/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEBaseViewCell.h"

@implementation MEBaseViewCell

- (UIImageView*) devider{
    if (!_devider) {
        _devider=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"devider.png"]];
    }
    return _devider;
}

- (void) addDevider{
    self.devider.frame = CGRectMake(0, 43, 0, self.devider.bounds.size.height);
    self.devider.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.contentView addSubview:self.devider];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView  attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView  attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.devider attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    
    self.devider.layer.shouldRasterize=YES;
    self.devider.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (UIImageView*) selectionBackgroundImageView{
    if (!_selectionBackgroundImageView) {
        _selectionBackgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"panel_selected.png"]];
        _selectionBackgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _selectionBackgroundImageView.alpha = 0.0;
    }
    return _selectionBackgroundImageView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.shouldIndentWhileEditing = YES;
        self.showsReorderControl = YES;
        
        self.selectionBackgroundImageView.layer.shouldRasterize = YES;
        self.selectionBackgroundImageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        self.backgroundView.layer.shouldRasterize = YES;
        self.backgroundView.layer.rasterizationScale = [UIScreen mainScreen].scale;

        [self addSubview:self.selectionBackgroundImageView]; [self sendSubviewToBack:self.selectionBackgroundImageView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;        
                
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected || self.isHighlighted)
        self.selectionBackgroundImageView.alpha=1.0;
    else
        self.selectionBackgroundImageView.alpha=0.0;
}


- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted || self.isSelected)
        self.selectionBackgroundImageView.alpha=1.0;
    else
        self.selectionBackgroundImageView.alpha=0.0;
}

@end
