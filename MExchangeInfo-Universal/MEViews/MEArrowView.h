//
//  MEShapedView.h
//  METestShapePath
//
//  Created by denis svinarchuk on 09.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEArrowView : UIView
@property(nonatomic,assign) CGFloat baseWidth, baseHeight;
@property(nonatomic,strong) UIColor *arrowColor;
@property(nonatomic,strong) UIImage *image;

-(id) initWithImage:(UIImage*)image;
@end
