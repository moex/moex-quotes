//
//  NSDate+MEComparing.h
//  MExchange
//
//  Created by denn on 7/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//
//   partialy copped from: https://github.com/erica/NSDate-Extensions/blob/master/NSDate-Utilities.m
//

#import <Foundation/Foundation.h>

#define D_MINUTE	60
#define D_HOUR		3600
#define D_DAY		86400
#define D_WEEK		604800
#define D_YEAR		31556926

@interface NSDate (MEComparing)

+ (NSDate *) dateOnly:(NSDate*)datetime;
- (NSDate *) dateOnly;

+ (NSDate *) dateWithDaysFromNow: (NSInteger) days;
+ (NSDate *) dateTomorrow;
+ (NSDate *) dateYesterday;

- (BOOL) isSameDay: (NSDate *) aDate;
- (BOOL) isToday;
- (BOOL) isTomorrow;
- (BOOL) isYesterday;

+ (NSInteger) howManyDaysDifferenceBetween:(NSDate*)startDate and:(NSDate*)endDate;

+ (NSString*) dateStringToFormatter: (NSString*)dateString; // yyyy-mm-dd
+ (NSDate*) dateFromString: (NSString*)date;

@end
