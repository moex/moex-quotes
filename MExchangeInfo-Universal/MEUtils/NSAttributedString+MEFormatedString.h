//
//  NSAttributedString+MEColloredString.h
//  MExchange
//
//  Created by denn on 5/15/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIStomp.h"

#define ME_PRINT_EXEPTON(exception)  NSLog(@"%@ at %s:%i", exception, __FILE__, __LINE__)

@interface NSAttributedString (MEFormatedString)

+ (NSAttributedString *) compoundValToday:(NSNumber*) valtoday;
+ (NSAttributedString *) compoundValToday:(NSNumber*) valtoday withAbvColor:(UIColor*)color andFont:(UIFont*)font;
+ (NSAttributedString*) changePrice:(GIStompValue*) lastPrice andChange: (GIStompValue*) change;
+ (NSAttributedString*) coloredPrice:(GIStompValue*) lastPrice andPrevPrice:(GIStompValue*) prevPrice;
+ (NSAttributedString*) coloredPrice:(GIStompValue*)lastPrice andPrevPrice:(GIStompValue*)prevPrice comparedTo:(NSAttributedString*)comparedString;
+ (NSAttributedString*) coloredDate:(NSDate *)date withBaseFontSize:(CGFloat)fontSize;

@end
