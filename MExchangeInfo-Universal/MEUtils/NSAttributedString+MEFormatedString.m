//
//  NSAttributedString+MEColloredString.m
//  MExchange
//
//  Created by denn on 5/15/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "NSAttributedString+MEFormatedString.h"
#import "GIUtils.h"
#import "MEUIPreferences.h"

#define ME_DEPHIS [[NSAttributedString alloc] initWithString:@"-"]
#define ME_TICKER_INFO_COLOR [UIColor colorWithRed:60.0/255. green:60.0/255. blue:60.0/255.0 alpha:1.0]

@implementation NSAttributedString (MEFormatedString)

+ (NSAttributedString *) compoundValToday:(NSNumber*) valtoday withAbvColor:(UIColor*)color andFont:(UIFont *)font{
    
    if (IsEmpty(valtoday)) {
        return ME_DEPHIS;
    }
    
    float _f = valtoday.floatValue;
    
    if (_f==0.0)
        return ME_DEPHIS;
    
    static NSNumberFormatter *numberFormatter = nil;
    
    
    if (!numberFormatter) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        
        [numberFormatter setLocale:[NSLocale currentLocale]];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:1];
        [numberFormatter setMinimumFractionDigits:1];
    }
    
    NSString *_ff;
    NSMutableAttributedString *_abv = [NSMutableAttributedString alloc];
    
    if (_f/1000000000.0>100.0) {
        _f/=1000000000.0;
        _abv = [_abv initWithString:NSLocalizedString(@"billion", @"Text prints after compound big number: milliard")];
    }
    else if (_f/1000000.0>100.0){
        _f/=1000000.0;
        _abv = [_abv initWithString:NSLocalizedString(@"million", @"Text prints after compound big number: million")];
    }
    else if (_f/1000.0>100.0){
        _f/=1000.0;
        _abv = [_abv initWithString:NSLocalizedString(@"thousand", @"Text prints after compound big number: thousand")];
    }
    else return [[NSMutableAttributedString alloc] initWithString:[numberFormatter stringFromNumber:valtoday]];
    
    _ff = [NSString stringWithFormat:@"%@ ", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:_f]]];
    
    if (font != nil) [_abv addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _abv.length)];
    [_abv addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, _abv.length)];
    
    NSMutableAttributedString *_ret = [[NSMutableAttributedString alloc] initWithString:_ff];
    
    [_ret appendAttributedString:_abv];
    
    if (IsEmpty(_ret)) {
        return ME_DEPHIS;
    }
    
    return _ret;
}

+ (NSAttributedString *) compoundValToday:(NSNumber*) valtoday {
    return [self compoundValToday:valtoday withAbvColor:ME_TICKER_INFO_COLOR andFont:nil];
}


+ (NSAttributedString*) changePrice:(GIStompValue*) lastPrice andChange: (GIStompValue*) change{
    
    @try {
        NSNumber *_lastPrice = [lastPrice number];
        NSNumber *_change = [change number];
        
        UIColor *_trendColor = ME_TICKER_INFO_COLOR ;
        
        NSMutableAttributedString *_ch = [[NSMutableAttributedString alloc] initWithAttributedString:ME_DEPHIS];
        
        if (_lastPrice && _lastPrice.floatValue!=0) {
            
            float _changePrcnt = _change.floatValue/_lastPrice.floatValue*100.0;
            NSString *_sign = @"";
            if (_change.floatValue>0)
                _sign=@"+";
            
            NSArray *prcnt = @[[NSNumber numberWithFloat:_changePrcnt],@2];        
            NSString *lastChangeStr = [NSString stringWithFormat:@"%@%@ (%@%@%%)", _sign, [change string], _sign, [[GIValueFormatter sharedObject] string:prcnt]];
            
            if(_change.floatValue>0)
                _trendColor = ME_UI_TREND_POSITIVE_COLOR;
            else if (_change.floatValue<0)
                _trendColor = ME_UI_TREND_NEGATIVE_COLOR;
            
            _ch = [[NSMutableAttributedString alloc] initWithString: lastChangeStr];
        }
        
        [_ch addAttribute:NSForegroundColorAttributeName value: _trendColor range:NSMakeRange(0,[_ch length])];
        
        return _ch;
        
    }
    @catch (NSException *exception) {
        ME_PRINT_EXEPTON(exception);
    }
    return ME_DEPHIS;
}

+ (NSAttributedString*) coloredPrice:(GIStompValue*)lastPrice andPrevPrice:(GIStompValue*)prevPrice{
    return [self coloredPrice:lastPrice andPrevPrice:prevPrice comparedTo:nil];
}

+ (NSAttributedString*) coloredPrice:(GIStompValue*)lastPrice andPrevPrice:(GIStompValue*)prevPrice comparedTo:(NSAttributedString*)comparedString{
    
    @try {
        
        if (comparedString) {
            if (lastPrice.floatValue == prevPrice.floatValue)
                return comparedString;
        }
        
        NSNumber *_prev = [prevPrice number];
        NSNumber *_last = [lastPrice number];
        
        if (IsEmpty(_prev) || IsEmpty(_last)) {
            return [[NSAttributedString alloc] initWithString:[lastPrice string]];
        }
        
        if (_last.floatValue==_prev.floatValue) {
            return [[NSAttributedString alloc] initWithString:[lastPrice string]];
        }
        
        UIColor *_trendColor= _prev.floatValue<_last.floatValue?ME_UI_TREND_POSITIVE_COLOR:ME_UI_TREND_NEGATIVE_COLOR;
        
        NSString *_lastPriceString = [lastPrice string];
        NSString *_prevPriceString = [prevPrice string];
        NSUInteger from = 0;
        
        if (_prevPriceString.length==_lastPriceString.length) {
            for (; from<_lastPriceString.length && from<_prevPriceString.length; from++) {
                if ([_lastPriceString characterAtIndex:from]!=[_prevPriceString characterAtIndex:from]) {
                    break;
                }
            }
        }
        
        NSMutableAttributedString *_ret = [[NSMutableAttributedString alloc] initWithString: _lastPriceString];
        if (from>0)
            [_ret addAttribute:NSForegroundColorAttributeName value: _trendColor range:NSMakeRange(from,[_ret length]-from)];
        
        return _ret;
    }
    @catch (NSException *exception) {
        //ME_PRINT_EXEPTON(exception);
    }
    return ME_DEPHIS;
}


+ (NSAttributedString*) coloredDate:(NSDate *)date withBaseFontSize:(CGFloat)fontSize{
    
    static NSDateFormatter *dateFormatter = nil;
    
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    
    if (!date) {
        date = [NSDate dateWithTimeIntervalSince1970:time(0)];
    }
    
    NSMutableAttributedString *_retdate = [[NSMutableAttributedString alloc] initWithString:[NSDateFormatter localizedStringFromDate:date
                                                                                                                           dateStyle:NSDateFormatterShortStyle
                                                                                                                           timeStyle:NSDateFormatterNoStyle]];
    NSMutableAttributedString *_rettime = [[NSMutableAttributedString alloc] initWithString:[NSDateFormatter localizedStringFromDate:date
                                                                                                                           dateStyle:NSDateFormatterNoStyle
                                                                                                                           timeStyle:NSDateFormatterMediumStyle]];
    
    [_retdate appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
    [_retdate addAttribute:NSFontAttributeName value:[UIFont fontWithName:ME_UI_FONT size:fontSize-3] range:NSMakeRange(0, _retdate.length)];
    [_rettime addAttribute:NSFontAttributeName value:[UIFont fontWithName:ME_UI_BOLD_FONT size:fontSize] range:NSMakeRange(0, _rettime.length)];

    [_retdate appendAttributedString:_rettime];
    
    return _retdate;
}



@end
