//
//  UIApplication+MEOrientationSize.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/20/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "UIApplication+MEOrientation.h"

@implementation UIApplication (MEOrientation)

- (UIViewController*) applicationTopController {
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topViewController.presentedViewController) { topViewController = topViewController.presentedViewController; }
    return topViewController;
}


- (CGSize) currentSize
{
    return [self sizeInOrientation:[UIDevice currentDevice].orientation];
}

- (CGSize) sizeInOrientation:(UIDeviceOrientation)orientation
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        size = CGSizeMake(size.height, size.width);
    }
    return size;
}

@end
