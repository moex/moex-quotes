//
//  NSDictionary+Json.h
//  MExchange-MosQuotes
//
//  Created by Denis Svinarchuk on 03/08/16.
//  Copyright © 2016 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PathParameters)
- (NSString*) json;
@end
