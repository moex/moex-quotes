//
//  NSDate+MEComparing.m
//  MExchange
//
//  Created by denn on 7/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "NSDate+MEComparing.h"

#define DATE_COMPONENTS (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]


@implementation NSDate (MEComparing)


+ (NSDateFormatter*) dateFormatter{
    static NSDateFormatter *dateFormatter = nil;
    static NSString *dateFormat = @"yyyy-MM-dd";
    if (!dateFormatter) dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    
    return dateFormatter;
}

+ (NSString*) dateStringToFormatter: (NSString*) date{
    
    NSDate *_date = [[self dateFormatter] dateFromString: date];
    
    return [NSDateFormatter localizedStringFromDate:_date
                                          dateStyle:NSDateFormatterMediumStyle
                                          timeStyle:NSDateFormatterNoStyle];
}

+ (NSDate*) dateFromString: (NSString*)date{
    return [[self dateFormatter] dateFromString: date];
}

+ (NSDate *) dateOnly:(NSDate*)datetime{
    NSDateComponents *components = [[NSCalendar currentCalendar] 
                                    components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                    fromDate:datetime];
     return [[NSCalendar currentCalendar] 
                         dateFromComponents:components];
}

- (NSDate*) dateOnly{
    return [NSDate dateOnly:self];
}

+ (NSDate *) dateWithDaysFromNow: (NSInteger) days
{
    // Thanks, Jim Morrison
	return [[NSDate date] dateByAddingDays:days];
}

+ (NSDate *) dateWithDaysBeforeNow: (NSInteger) days
{
    // Thanks, Jim Morrison
	return [[NSDate date] dateBySubtractingDays:days];
}

+ (NSDate *) dateTomorrow
{
	return [NSDate dateWithDaysFromNow:1];
}

+ (NSDate *) dateYesterday
{
	return [NSDate dateWithDaysBeforeNow:1];
}

- (BOOL) isEqualToDateIgnoringTime: (NSDate *) aDate
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];
	return ((components1.year == components2.year) &&
			(components1.month == components2.month) &&
			(components1.day == components2.day));
}

- (BOOL) isSameDay: (NSDate *) aDate{
    return [self isEqualToDateIgnoringTime:aDate];
}

- (BOOL) isToday
{
	return [self isEqualToDateIgnoringTime:[NSDate date]];
}

- (BOOL) isTomorrow
{
	return [self isEqualToDateIgnoringTime:[NSDate dateTomorrow]];
}

- (BOOL) isYesterday
{
	return [self isEqualToDateIgnoringTime:[NSDate dateYesterday]];
}

- (NSDate *) dateByAddingDays: (NSInteger) dDays
{
	NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_DAY * dDays;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

- (NSDate *) dateBySubtractingDays: (NSInteger) dDays
{
	return [self dateByAddingDays: (dDays * -1)];
}

+ (NSInteger) howManyDaysDifferenceBetween:(NSDate*)startDate and:(NSDate*)endDate
{
    NSDate * firstMidnight = [self getMidnightDateFromDate: startDate];
    NSDate * secondMidnight = [self getMidnightDateFromDate: endDate];
    NSTimeInterval timeBetween = [firstMidnight timeIntervalSinceDate: secondMidnight];
        
    NSInteger numberOfDays = ((timeBetween+.5) / 86400.);

    return (NSInteger)ABS(numberOfDays);
}

+ (NSDate *) getMidnightDateFromDate:(NSDate *)originalDate
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:originalDate];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *midnight = [[NSCalendar currentCalendar] dateFromComponents:components];
    return midnight;
}

@end
