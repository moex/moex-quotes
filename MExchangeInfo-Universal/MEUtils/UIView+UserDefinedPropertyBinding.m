//
//  UIView+UserDefinedPropertyBinding.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/14/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "UIView+UserDefinedPropertyBinding.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation UIView (UserDefinedPropertyBinding)

static char const * const UndefinedObjectsDictKey = "MEUndefinedObjectsDict";

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    NSMutableDictionary *undefinedDict = nil;
    if ( objc_getAssociatedObject(self, UndefinedObjectsDictKey) ) {
        undefinedDict = objc_getAssociatedObject(self, UndefinedObjectsDictKey);
    }
    else {
        undefinedDict = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, UndefinedObjectsDictKey, undefinedDict, OBJC_ASSOCIATION_RETAIN);
    }
    [undefinedDict setValue:value forKey:key];
}

- (id) valueForUndefinedKey:(NSString *)key{
    NSMutableDictionary *undefinedDict = nil;
    if ( objc_getAssociatedObject(self, UndefinedObjectsDictKey) ) {
        undefinedDict = objc_getAssociatedObject(self, UndefinedObjectsDictKey);
        return [undefinedDict valueForKeyPath:key];
    }
    return nil;
}

@end
