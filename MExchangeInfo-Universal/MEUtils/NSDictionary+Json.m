//
//  NSDictionary+Json.m
//  MExchange-MosQuotes
//
//  Created by Denis Svinarchuk on 03/08/16.
//  Copyright © 2016 Moscow Exchange. All rights reserved.
//

#import "NSDictionary+Json.h"

@implementation NSDictionary (PathParameters)


- (NSString*) json {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end
