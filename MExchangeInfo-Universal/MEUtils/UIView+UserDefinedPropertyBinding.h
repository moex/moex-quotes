//
//  UIView+UserDefinedPropertyBinding.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/14/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UserDefinedPropertyBinding)

@end
