//
//  UIImage+MEBlurBoxImage.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/18/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//
//  Author: http://indieambitions.com/idevblogaday/perform-blur-vimage-accelerate-framework-tutorial/
//

#import <UIKit/UIKit.h>

@interface UIImage (MEBlurBoxImage)
-(UIImage *)boxbWithBlurRadius:(CGFloat) blur;
@end
