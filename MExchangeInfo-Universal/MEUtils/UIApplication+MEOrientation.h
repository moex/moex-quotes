//
//  UIApplication+MEOrientationSize.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/20/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (MEOrientation)
- (UIViewController*) applicationTopController;
- (CGSize) currentSize;
@end
